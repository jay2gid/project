//
//  MyCartViewController.m
//  ToolsOnWheel
//
//  Created by Shikhar Khanna on 11/01/18.
//  Copyright © 2018 Kapil Goyal. All rights reserved.
//

#import "MyCartViewController.h"
#import "CustomNavigation.h"
#import "MFSideMenu.h"
#import "CommonHeader.h"
#import "MyCartTVCell.h"
#import "CartEditView.h"
#import "AppDelegate.h"
#import "CartItemData.h"
#import "UIImageView+WebCache.h"
#import "Config.h"
#import "LoginVC.h"
#import "ASIFormDataRequest.h"
#import "Utils.h"
#import "JSON.h"
#import "MBProgressHUD.h"
#import "CheckoutViewController.h"

@interface MyCartViewController () {
    CartEditView *editView;
}

@end

@implementation MyCartViewController

- (void)viewDidLoad {
    [super viewDidLoad];

    [self performSelector:@selector(updateUI) withObject:nil afterDelay:0.01];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(updateCart:)name:@"UPDATE_CART_VALUE" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(updateTable) name:@"CHECK_CART_ITEM" object:nil];

    arrCartId = [[NSMutableArray alloc] init];
    arrCartItem = [[NSMutableArray alloc] init];
    
    btnHome.layer.shadowColor = [UIColor colorWithRed:0 green:0 blue:0 alpha:0.5].CGColor;
    btnHome.layer.shadowOffset = CGSizeMake(4, 4);
    btnHome.layer.shadowRadius = 5;
    btnHome.layer.shadowOpacity = 0.5;
    btnHome.layer.masksToBounds = NO;
    
//    isCalender = false;
//    isDropDown = false;
}

-(void)viewWillAppear:(BOOL)animated
{
    [self.menuContainerViewController setPanMode:MFSideMenuPanModeNone];
    cartTotal = 0;
    for (int i=0; i<[[AppDelegate sharedDelegate].arrCartItems count]; i++)
    {
        CartItemData *cart = [[AppDelegate sharedDelegate].arrCartItems objectAtIndex:i];
        cartTotal = cartTotal+[cart.itemPrice intValue];
    }
    [lblTotalAmount setAttributedText:[[AppDelegate sharedDelegate] getColoredString:[UIColor whiteColor] string:[NSString stringWithFormat:@"%d",cartTotal]]];
    [self updateMyUI ];
}
-(void)updateTable
{
    [tblCart reloadData];
    [Utils showAlertMessage:KMessageTitle Message:@"These items are not available on this time. please choose other time"];

}

-(void)updateUI
{
    [CustomNavigation addTarget:self backRequired:NO title:@"CART"];
    for (UIView *v1 in self.view.subviews)
    {
        if ([v1 isKindOfClass:[CommonHeader class]])
        {
            [v1 setBackgroundColor:[UIColor whiteColor]];
            for (UIView *viewTmp in v1.subviews)
            {
                if ([viewTmp isKindOfClass:[UIButton class]])
                {
                    if (viewTmp.tag==1)
                    {
                        viewTmp.hidden=YES;
                    }
                    else if(viewTmp.tag==106)
                    {
                        [(UIButton*)viewTmp addTarget:self action:@selector(btnBackClicked:)
                                     forControlEvents:UIControlEventTouchUpInside];
                    }
                    else if(viewTmp.tag==102)
                    {
                        [(UIButton*)viewTmp setHidden:YES];
                    }
                    
                }
                else if ([viewTmp isKindOfClass:[UIImageView class]])
                {
                    if (viewTmp.tag==101)
                    {
                        [(UIImageView*)viewTmp setImage:[UIImage imageNamed:@"backP.png"]];
                    }
                }
                
            }
        }
    }
}

-(NSString*)currentDate
{
    NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
    [dateFormat setDateFormat:@"yyyy-MM-dd hh:mma"];
    NSString *strCurrentDate = [dateFormat stringFromDate:[NSDate date]];
    NSLog(@"strCurrentDate=%@",strCurrentDate);
    return strCurrentDate;
}
-(void)updateCart:(NSNotification*)obj
{
    [self updateMyUI];
}

-(void)updateMyUI
{
    if ([[AppDelegate sharedDelegate].arrCartItems count] > 0)
    {
        checkoutView.hidden = false;
        tblCart.hidden = false;
        viewEmpty.hidden = true;
        btnHome.hidden = true;
        int total = 0;
        for (int i=0; i<[[AppDelegate sharedDelegate].arrCartItems count]; i++)
        {
            CartItemData *cart = [[AppDelegate sharedDelegate].arrCartItems objectAtIndex:i];
            total = total+[cart.itemPrice intValue];
        }
        [lblTotalAmount setAttributedText:[[AppDelegate sharedDelegate] getColoredString:[UIColor whiteColor] string:[NSString stringWithFormat:@"%d",total]]];

        [tblCart reloadData];
    }
    else
    {
        checkoutView.hidden = true;
        tblCart.hidden = true;
        viewEmpty.hidden = false;
        btnHome.hidden = false;
        btnHome.layer.cornerRadius = 10.0f;
    }}

-(IBAction)moveToCart:(id)sender
{
    MyCartViewController *vc = [[MyCartViewController alloc] initWithNibName:@"MyCartViewController" bundle:nil];
    [self.navigationController pushViewController:vc animated:YES];
}

#pragma mark - UITableViewDataSource
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [[AppDelegate sharedDelegate].arrCartItems count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    static NSString *CellIdentifier= @"MyCartTVCell";
    
    MyCartTVCell* cell = nil;
    cell = (MyCartTVCell*)[tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil)
    {
        NSString *nibNameOrNil= @"MyCartTVCell";
        
        NSArray *topLevelObjects = [[NSBundle mainBundle] loadNibNamed:nibNameOrNil owner:nil options:nil];
        
        for(id currentObject in topLevelObjects)
        {
            if([currentObject isKindOfClass:[MyCartTVCell class]])
            {
                cell = (MyCartTVCell*)currentObject;
                break;
            }
        }
    }
    cell.imageContainer.layer.cornerRadius = 65/2;
    cell.imageContainer.clipsToBounds = true;
    //cell.imageContainer.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"circle.png"]];
    
    cell.imgBackground.layer.cornerRadius = 65/2;
    cell.imgBackground.clipsToBounds = true;
    cell.imgBackground.image =[UIImage imageNamed:@"circle.png"];
    [cell.contentView setBackgroundColor:[UIColor clearColor]];
    [cell setBackgroundColor:[UIColor clearColor]];
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    
    
    cell.viewContainer.layer.cornerRadius = 5.0f;
    UIBezierPath *shadowPath = [UIBezierPath bezierPathWithRect:cell.viewContainer.bounds];
    cell.viewContainer.layer.masksToBounds = NO;
    cell.viewContainer.layer.shadowColor = [UIColor blackColor].CGColor;
    cell.viewContainer.layer.shadowOffset = CGSizeMake(0.0f, 5.0f);
    cell.viewContainer.layer.shadowOpacity = 0.5f;
    cell.viewContainer.layer.shadowPath = shadowPath.CGPath;
    
    
    [cell.btnEdit setTag:indexPath.row];
    [cell.btnEdit addTarget:self action:@selector(btnEditPressed:) forControlEvents:UIControlEventTouchUpInside];
    
    CartItemData *cart = [[AppDelegate sharedDelegate].arrCartItems objectAtIndex:indexPath.row];
    if ([cart.itemStatus isEqualToString:@"0"] || [cart.itemAvailable isEqualToString:@"0"])
    {
        [cell.viewSeprator setHidden:NO];
    }
    else
    {
        [cell.viewSeprator setHidden:YES];
    }
    cell.lblServiceDate.text = cart.itemDate;
    cell.lblTime.text = cart.itemTime;
    cell.lblTitle.text = [NSString stringWithFormat:@"%@: %@",cart.itemName,cart.itemSubName];

    cell.lblQty.text = cart.itemQty;
    
    [cell.lblPrice setAttributedText:[[AppDelegate sharedDelegate] getColoredString:[UIColor blackColor] string:cart.itemPrice]];

    cell.lblPrice.layer.cornerRadius = 10.0f;
    cell.lblPrice.layer.borderColor = [UIColor orangeColor].CGColor;
    cell.lblPrice.layer.borderWidth = 2.0f;
    [cell.imgViewSmall sd_setImageWithURL:[NSURL URLWithString:cart.itemImage] placeholderImage:[UIImage imageNamed:@"banner1.png"]];
    
    
    [cell.btnDelete setTag:indexPath.row];
    [cell.btnDelete addTarget:self action:@selector(btnCancelPressed:) forControlEvents:UIControlEventTouchUpInside];
    
    return cell;
}


-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    
}


-(CartEditView *)editView:(int)tag
{
    NSArray *arrNibs = [NSArray arrayWithArray:[[NSBundle mainBundle] loadNibNamed:NSStringFromClass([CartEditView class]) owner:self options:nil]];
    
    if (editView!=nil)
    {
        [editView removeFromSuperview];
        editView = nil;
    }
    
    editView = [arrNibs firstObject];
    editView.btnTag = tag;
    editView.controller = self;
    return editView;
}


-(void)btnEditPressed:(UIButton *)sender
{
    int tag = (int)sender.tag;
    editView = [self editView:tag];
    [self.view addSubview:editView];
    [editView updateView:tag];
    editView.frame = self.view.bounds;
}


-(void)btnCancelPressed:(UIButton *)sender
{
    btnCancel = sender;
    
    // [Utils showAlertMessage:KMessageTitle Message:@"Are you sure you want to delete servcie from cart?"];
    UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:KMessageTitle message:@"Are you sure you want to delete servcie from cart?" delegate:self cancelButtonTitle:@"No" otherButtonTitles:@"Yes", nil];
    [alertView show];
}

- (IBAction)btnHomePressed:(id)sender {
    
    NSArray *viewControllers = [[self navigationController] viewControllers];
    for( int i=0;i<[viewControllers count];i++){
        id obj=[viewControllers objectAtIndex:i];
        if([obj isKindOfClass:[HomeVC class]]){
            [[self navigationController] popToViewController:obj animated:YES];
        }
    }
}


-(void)btnBackClicked:(id)sender
{
    NSArray *viewControllers = [[self navigationController] viewControllers];
    for( int i=0;i<[viewControllers count];i++){
        id obj=[viewControllers objectAtIndex:i];
        if([obj isKindOfClass:[HomeVC class]]){
            [[self navigationController] popToViewController:obj animated:YES];
        }
    }
}


- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex NS_DEPRECATED_IOS(2_0, 9_0);
{
    if(buttonIndex==1)
    {
        [[AppDelegate sharedDelegate].arrCartItems removeObjectAtIndex:btnCancel.tag];
        [self updateMyUI];
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)btnCheckOut:(id)sender
{
    if (![[NSUserDefaults standardUserDefaults] objectForKey:@"userid"])
    {
        LoginVC *login = [[LoginVC alloc] initWithNibName:@"LoginVC" bundle:nil];
        [self.navigationController pushViewController:login animated:YES];
    }
    else
    {
        if ([AppDelegate checkNetwork])
        {
            strCheck = @"fineuser";
            [MBProgressHUD showHUDAddedTo:[AppDelegate sharedDelegate].window animated:YES];
            ASIFormDataRequest *request = [ASIFormDataRequest requestWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@%@",ServerAdd3,KBlackListUser]]];
            [request setDelegate:self];
            [request setPostValue:USER_ID forKey:@"userId"];
            [request startAsynchronous];
        }
    }
}


-(BOOL)checkTimeInterval:(NSTimeInterval)timeInterval
{
    NSInteger ti = timeInterval;
    //  int ms = ((interval % 1) * 1000)
    //  int seconds = ti % 60;
    int minutes = (ti / 60) % 60;
    int hours = (int)(ti / 3600);
    
    if ([self currentHour] > 16  && [self currentHour] > 18) {
        
        if (hours>=1 || minutes >=59) {
            return TRUE;
        }
        else
            return false;
    }else{
        if ((hours<=0) || ((hours==1) && (minutes<30)) )
        {
            return FALSE;
        }
        else
            return TRUE;
    }
}

- (NSInteger)currentHour
{
//    In practice, these calls can be combined
//    NSDate *now = [NSDate date];
//    NSCalendar *calendar = [NSCalendar currentCalendar];
//    NSDateComponents *components = [calendar components:NSCalendarUnitHour fromDate:now];
//    return [components hour];
    
    NSDateFormatter* dateFormatter = [[NSDateFormatter alloc] init];
    dateFormatter.dateFormat = @"HH";
    return [[dateFormatter stringFromDate:[NSDate date]] integerValue];
    
}

- (NSInteger)currentMin
{
    NSDateFormatter* dateFormatter = [[NSDateFormatter alloc] init];
    dateFormatter.dateFormat = @"mm";
    return [[dateFormatter stringFromDate:[NSDate date]] integerValue];
}

#pragma mark Request Finished
- (void)requestFinished:(ASIHTTPRequest *)request
{
    [MBProgressHUD hideAllHUDsForView:[AppDelegate sharedDelegate].window animated:YES];
    NSString *receivedString = [request responseString];
    NSLog(@"str=%@",receivedString);
    NSDictionary *dictMain=[receivedString JSONValue];
    
    if ([strCheck isEqualToString:@"fineuser"]
        )
    {
        if([[dictMain objectForKey:@"status"] boolValue] && [[dictMain objectForKey:@"result"] isEqualToString:@"fine user"])
        {
            int count=0;
            [arrCartId removeAllObjects];
            for (int i=0; i<[[AppDelegate sharedDelegate].arrCartItems count]; i++)
            {
                CartItemData *cart = [[AppDelegate sharedDelegate].arrCartItems objectAtIndex:i];
                NSLog(@"cart.itemDateTime=%@",cart.itemDateTime);
                NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
                [dateFormat setDateFormat:@"yyyy-MM-dd hh:mma"];
                //NSDate *date = [dateFormat dateFromString:@"2018-01-20 09:44PM"];
                NSDate *date = [dateFormat dateFromString:cart.itemDateTime];
                
                NSLog(@"date=%@",date);
                NSTimeInterval timeInterval = [date timeIntervalSinceDate:[NSDate date]];
                BOOL isFlag = [self checkTimeInterval:timeInterval];
                if (!isFlag)
                {
                    count++;
                    cart.itemStatus = @"0";
                    [arrCartId addObject:cart.itemId];
                }
                NSLog(@"timeinterval=%f",timeInterval);
            }
            
            if (count>0)
            {
                [tblCart reloadData];
                [Utils showAlertMessage:KMessageTitle Message:@"Please choose correct date and time"];
            }
            else
            {
                [arrCartItem removeAllObjects];
                for (int i=0; i<[[AppDelegate sharedDelegate].arrCartItems count]; i++)
                {
                    CartItemData *cart = [[AppDelegate sharedDelegate].arrCartItems objectAtIndex:i];
                    NSMutableDictionary *dict = [[NSMutableDictionary alloc] init];
                    [dict setObject:cart.itemId forKey:@"cateId"];
                    [dict setObject:cart.itemDate forKey:@"orderServiceDate"];
                    [dict setObject:cart.itemTime forKey:@"orderServiceTime"];
                    [dict setObject:[[NSUserDefaults standardUserDefaults] objectForKey:@"userid"] forKey:@"userId"];
                    [dict setObject:[NSString stringWithFormat:@"%d",i+1] forKey:@"cart_id"];
                    [arrCartItem addObject:dict];
                }

                if ([AppDelegate checkNetwork])
                {
                    strCheck = @"cartdata";
                    [MBProgressHUD showHUDAddedTo:[AppDelegate sharedDelegate].window animated:YES];
                    ASIFormDataRequest *request = [ASIFormDataRequest requestWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@%@",ServerAdd3,kCartCheckForAvailablity]]];
                    [request setDelegate:self];
                    NSString *jsonString = [arrCartItem JSONRepresentation];
                    [request setPostValue:jsonString forKey:@"cartData"];
                    [request startAsynchronous];
                }
                
            }
        }
        else if(![[dictMain objectForKey:@"status"] boolValue])
        {
            [Utils showAlertMessage:KMessageTitle Message:[dictMain objectForKey:@"result"]];
        }
    }
    else if ([strCheck isEqualToString:@"cartdata"])
    {
        int count=0;
        NSMutableArray *arrResult = [dictMain objectForKey:@"resultendArray"];
        for (int i=0; i<[arrResult count]; i++)
        {
            for (int j=0; j<[[AppDelegate sharedDelegate].arrCartItems count]; j++)
            {
                BOOL isFlag = [[[arrResult objectAtIndex:i] objectForKey:@"status"] boolValue];
                if (!isFlag)
                {
                    count++;
                    NSString *strCartId = [[arrResult objectAtIndex:i] objectForKey:@"cart_id"];
                    CartItemData *cart = [[AppDelegate sharedDelegate].arrCartItems objectAtIndex:j];
                    if ([cart.cartId isEqualToString:strCartId])
                    {
                        cart.itemAvailable = @"0";
                    }
                }
            }
            
        }
        if (count>0)
        {
            [tblCart reloadData];
            [Utils showAlertMessage:KMessageTitle Message:@"These items are not available on this time. please choose other time"];
        }
        else
        {
            CheckoutViewController *objCheckOut = [[CheckoutViewController alloc] initWithNibName:@"CheckoutViewController" bundle:nil];
            NSString *str = [lblTotalAmount.text substringWithRange:NSMakeRange(1, [lblTotalAmount.text length]-1)];
            objCheckOut.strCartAmount =  str;
            objCheckOut.arrCartItem = arrCartItem;
            [self.navigationController pushViewController:objCheckOut animated:YES];

        }
//        if(![[dictMain objectForKey:@"status"] boolValue])
//        {
//            [Utils showAlertMessage:KMessageTitle Message:[dictMain objectForKey:@"result"]];
//        }
    }
}

- (void)requestFailed:(ASIHTTPRequest *)request
{
    NSLog(@"Fail");
    [MBProgressHUD hideAllHUDsForView:[AppDelegate sharedDelegate].window animated:YES];
    // [Utils showAlertMessage:KMessageTitle Message:@"Network error"];
}

@end
