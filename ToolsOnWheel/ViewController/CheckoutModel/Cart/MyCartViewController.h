//
//  MyCartViewController.h
//  ToolsOnWheel
//
//  Created by Shikhar Khanna on 11/01/18.
//  Copyright © 2018 Kapil Goyal. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MyCartViewController : UIViewController<UIAlertViewDelegate>
{
    
    __weak IBOutlet UIView *viewEmpty;
    IBOutlet UIView *checkoutView;
    IBOutlet UILabel *lblTotalAmount;
    IBOutlet UITableView *tblCart;
    UIButton *btnCancel;
    NSMutableArray *arrCartId;
    NSMutableArray *arrCartItem;
    int cartTotal;
    NSString *strCheck;
    IBOutlet UIButton *btnHome;

}
-(IBAction)btnCheckOut:(id)sender;
@end
