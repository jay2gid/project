//
//  CartEditView.m
//  ToolsOnWheel
//
//  Created by Shikhar Khanna on 10/01/18.
//  Copyright © 2018 Kapil Goyal. All rights reserved.
//

#import "CartEditView.h"
#import "CartItemData.h"
#import "AppDelegate.h"

#import "ASIFormDataRequest.h"
#import "MBProgressHUD.h"
#import "Config.h"
#import "JSON.h"
#import "Utils.h"

@implementation CartEditView{
    UILabel *lbl;

}
@synthesize btnTag;
-(void)updateView:(int)tag
{
    btnTag = tag;
    //[[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(getChagneDate:) name:kDateChange object:nil];

    btnCancel.layer.cornerRadius = 20.0f;
    btnUpdate.layer.cornerRadius = 20.0f;
    viewContainer.layer.cornerRadius = 10.0f;
    
    viewQty.layer.borderWidth = 1.0f;
    viewQty.layer.borderColor = [UIColor colorWithRed:235/255.0 green:235/255.0 blue:235/255.0 alpha:1.0f].CGColor;
    viewQty.layer.cornerRadius = 5.0f;
    
    viewComment.layer.borderWidth = 1.0f;
    viewComment.layer.borderColor = [UIColor colorWithRed:235/255.0 green:235/255.0 blue:235/255.0 alpha:1.0f].CGColor;
    viewComment.layer.cornerRadius = 5.0f;
    
    txtViewComment.text = @"Additional Comment....";
    txtViewComment.delegate = self;
    
    CartItemData *cart = [[AppDelegate sharedDelegate].arrCartItems objectAtIndex:btnTag];
    txtFDate.text = cart.itemDate;

    txtFTime.text = cart.itemTime;
    lblQty.text = cart.itemQty;
    txtViewComment.text = cart.itemComment;
    singlePrice= [cart.itemPrice intValue]/[cart.itemQty intValue];
    count = [cart.itemQty intValue];
    // lblAmount.text = [NSString stringWithFormat:@"Rs.%@",cart.itemPrice];
    
    [lblAmount setAttributedText:[[AppDelegate sharedDelegate] getColoredString:[UIColor orangeColor] string:cart.itemPrice]];

    
    if ([lblQty.text intValue]==10){
        [btnPlus setUserInteractionEnabled:FALSE];
    }
    if ([lblQty.text intValue]==1){
        [btnMinus setUserInteractionEnabled:FALSE];
    }
    arrDate = [[NSMutableArray alloc] init];
    arrTimeSlot = [[NSMutableArray alloc] init];
    
    
    lbl = [[UILabel alloc] initWithFrame:CGRectMake(6.0, 0.0,150, 28.0)];
    
    
    [lbl setText:@"Additional comment...."];
    [lbl setBackgroundColor:[UIColor clearColor]];
    [txtViewComment addSubview:lbl];
    lbl.textColor = txtViewComment.textColor;
    lbl.alpha = 0.7;
    lbl.font = txtViewComment.font;
    
    isCalender = false;
    isDropDown = false;
}

- (BOOL) textViewShouldBeginEditing:(UITextView *)textView {
    if (isCalender || isDropDown) {
        return false;
    }
    return YES;
}

-(void) textViewDidChange:(UITextView *)textView{
    if(txtViewComment.text.length == 0){
        lbl.hidden = false;
    }else{
        lbl.hidden = true;
    }
}

#pragma mark Date Selection

-(void)onDateSelected:(NSString *)date {

    isCalender = false;
    [[NSUserDefaults standardUserDefaults] removeObjectForKey:@"datepopup"];
    [[NSUserDefaults standardUserDefaults] synchronize];
    strDate = date;
    
    txtFDate.text = strDate;
    
    txtFDate.text = [NSString stringWithFormat:@"%@/%@/%@",[strDate substringWithRange:NSMakeRange(8, 2)],[strDate substringWithRange:NSMakeRange(5, 2)],[strDate substringWithRange:NSMakeRange(0, 4)]];
    
    if ([AppDelegate checkNetwork]) {
        strCheck = @"timeslot";
        [MBProgressHUD showHUDAddedTo:[AppDelegate sharedDelegate].window animated:YES];
        ASIFormDataRequest *request = [ASIFormDataRequest requestWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@%@",ServerAdd3,kTimeSlotDate]]];
        [request setDelegate:self];
        [request setPostValue:[[NSUserDefaults standardUserDefaults] objectForKey:@"maincatid"] forKey:@"cateId"];
        [request setPostValue:strDate forKey:@"selectDate"];
        [request startAsynchronous];
    }
}

#pragma mark - UIDatePicker Methods
-(void)updateDateTextField:(UIDatePicker *)picker
{
    NSDateFormatter *df = [NSDateFormatter new];
    [df setDateFormat:@"dd-MM-yyyy"];
    txtFDate.text = [NSString stringWithFormat:@"%@",[df stringFromDate:picker.date]];
}

-(void)updateTimeTextField:(UIDatePicker *)picker {
    
    NSDateFormatter *df = [NSDateFormatter new];
    [df setDateFormat:@"hh:mm a"];
    txtFTime.text = [NSString stringWithFormat:@"%@",[df stringFromDate:picker.date]];
}

//-(NSDate *)getDateAfterInterval:(NSInteger)interVal
//{
//    NSDateComponents *components = [[NSDateComponents alloc] init];
//    [components setDay:interVal];
//    NSDate *finalDate = [[NSCalendar currentCalendar] dateByAddingComponents:components toDate:[NSDate date] options:0];
//    return finalDate;
//}

#pragma mark - Touches

//-(void)touchesBegan:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event
//{
//    [self endEditing:true];
//}


#pragma mark - UIButton Actions
-(IBAction)btnDateClicked:(id)sender
{
    END_KEY_VIEW
    if (!isDropDown) {
        
        isCalender = true;
        [[NSUserDefaults standardUserDefaults] setObject:@"cart" forKey:@"datepopup"];
        [[NSUserDefaults standardUserDefaults] synchronize];
        if ([AppDelegate checkNetwork])
        {
            strCheck = @"disabledate";
            [MBProgressHUD showHUDAddedTo:[AppDelegate sharedDelegate].window animated:YES];
            ASIFormDataRequest *request = [ASIFormDataRequest requestWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@%@",ServerAdd3,kDisableDate]]];
            [request setDelegate:self];
            [request startAsynchronous];
            
            CalPopUp *viewPopUp  = [[[NSBundle mainBundle] loadNibNamed:@"CalPopUp" owner:self options:nil] objectAtIndex:0];
            viewPopUp.caldelegate = self;
            [self addSubview:viewPopUp];
            viewPopUp.frame = self.bounds;
            [viewPopUp UpdateView];
            
            
            
            [WebServiceCalls GET:kDisableDate parameter:nil completionBlock:^(id JSON, WebServiceResult result)
             {
                 @try{
                     NSLog(@"%@", JSON);
                    

                 }
                 @catch (NSException *exception) { }
                 @finally { }
             }];
            
            
            
            
            
        }
    }
    
   
}


#pragma mark - DropDown delegate
- (IBAction)generateDropDown:(UIButton*)sender
{
    END_KEY_VIEW
    if (!isCalender){
    
    btnSender = sender;
    
    if ([strCheck isEqualToString:@"timeslot"])
    {
//        if (self.dropDownView)
//        {
//            [_dropDownView removeFromSuperview];
//            _dropDownView = nil;
//        }
//
//        CGRect rect = [sender convertRect:sender.bounds toView:viewContainer];
//        self.dropDownView = [[DropDownView alloc] initWithFrame:CGRectMake(rect.origin.x, rect.origin.y-37, rect.size.width+8, rect.size.height) target:self];
//        [self.dropDownView setBackgroundColor:[UIColor clearColor]];
//        [self.dropDownView setTag:sender.tag];
//
//        [self downloadDropDownData:sender];
//
//        if ([self.dropDownView.myDataarray count]){
//            isDropDown = true;
//            [viewContainer addSubview:self.dropDownView];
//        }
//
//        [self.dropDownView performSelector:@selector(openDropDown) withObject:nil afterDelay:0.1];
        
        [self openTimePicker];
        
        
    }
    else
    {
        txtFTime.text = @"Select Time";
        strCheck = @"onlytime";
        NSString *strDateSelected = [NSString stringWithFormat:@"%@-%@-%@",[txtFDate.text substringWithRange:NSMakeRange(6, 4)],[txtFDate.text substringWithRange:NSMakeRange(3, 2)],[txtFDate.text substringWithRange:NSMakeRange(0, 2)]];
        
        if ([AppDelegate checkNetwork])
        {
            [MBProgressHUD showHUDAddedTo:[AppDelegate sharedDelegate].window animated:YES];
            ASIFormDataRequest *request = [ASIFormDataRequest requestWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@%@",ServerAdd3,kTimeSlotDate]]];
            [request setDelegate:self];
            [request setPostValue:[[NSUserDefaults standardUserDefaults] objectForKey:@"maincatid"] forKey:@"cateId"];
            [request setPostValue:strDateSelected forKey:@"selectDate"];
            [request startAsynchronous];
        }
        }
    
    }
}


-(void)openTimePicker{
    
    [ActionSheetStringPicker showPickerWithTitle:@"Select time slot"
                                            rows:arrTimeSlot
                                initialSelection:0
                                       doneBlock:^(ActionSheetStringPicker *picker, NSInteger selectedIndex, id selectedValue) {
                                           NSLog(@"Picker: %@, Index: %ld, value: %@",
                                                 picker, (long)selectedIndex, selectedValue);
                                           
                                           // NSLog(@"%ld",(long)self->btnSender.tag);
                                           self->txtFTime.text = self->arrTimeSlot[selectedIndex];
                                           
                                       }
                                     cancelBlock:^(ActionSheetStringPicker *picker) {
                                         NSLog(@"Block Picker Canceled");
                                     }
                                          origin:btnSender];
    
}

//
//-(void)didSelectIndex:(int)index ForDropDown:(DropDownView*)dropdown
//{
//    isDropDown = false;
//
//    if (dropdown.strDropDownValue)
//    {
//        NSArray *list = [NSArray arrayWithArray:arrTimeSlot];
//        NSLog(@"selected data :%@",[list objectAtIndex:index]);
//        txtFTime.text = dropdown.strDropDownValue;
//    }
//    [dropdown.myLabel setText:@""];
//    [self.dropDownView removeFromSuperview];
//}

- (void)downloadDropDownData:(UIButton*)sender
{
    NSArray *list = [NSArray arrayWithArray:arrTimeSlot];
    [self.dropDownView setDataArray:list];
    
}


-(IBAction)btnPlusPressed:(id)sender
{
    count= [[lblQty text] intValue];
    if (count<10)
    {
        [lblQty setText:[NSString stringWithFormat:@"%i",++count]];
        
        [lblAmount setAttributedText:[[AppDelegate sharedDelegate] getColoredString:[UIColor orangeColor] string:[NSString stringWithFormat:@"%i",count*singlePrice]]];

        
        if (count==10)
        {
            [btnPlus setUserInteractionEnabled:FALSE];
            [btnMinus setUserInteractionEnabled:TRUE];

        }
        else
        {
            [btnPlus setUserInteractionEnabled:TRUE];
            [btnMinus setUserInteractionEnabled:TRUE];

        }
    }
}

-(IBAction)btnMinusPressed:(id)sender
{
    count= [[lblQty text] intValue];
    if(count>1)
    {
        [lblQty setText:[NSString stringWithFormat:@"%i",--count]];
        [lblAmount setAttributedText:[[AppDelegate sharedDelegate] getColoredString:[UIColor orangeColor] string:[NSString stringWithFormat:@"%i",count*singlePrice]]];

        if (count==1)
        {
            [btnMinus setUserInteractionEnabled:FALSE];
            [btnPlus setUserInteractionEnabled:TRUE];
        }
        else
        {
            [btnMinus setUserInteractionEnabled:TRUE];
            [btnPlus setUserInteractionEnabled:TRUE];
        }
    }
}


- (void)requestFinished:(ASIHTTPRequest *)request
{
    [MBProgressHUD hideAllHUDsForView:[AppDelegate sharedDelegate].window animated:YES];
   
    NSString *receivedString = [request responseString];
    NSLog(@"str=%@",receivedString);
    NSDictionary *dictMain=[receivedString JSONValue];
    
    if ([strCheck isEqualToString:@"disabledate"])
    {
        if([[dictMain objectForKey:@"status"] boolValue])
        {
            NSMutableArray *arrDisableDate = [dictMain objectForKey:@"result"];
            [arrDate removeAllObjects];
            for (int i=0; i<[arrDisableDate count]; i++)
            {
                [arrDate addObject:[[arrDisableDate objectAtIndex:i] objectForKey:@"date"]];
            }
        }
        else if(![[dictMain objectForKey:@"status"] boolValue])
        {
            [Utils showAlertMessage:KMessageTitle Message:[dictMain objectForKey:@"result"]];
        }
    }
    else if ([strCheck isEqualToString:@"timeslot"])
    {
        if([[dictMain objectForKey:@"status"] boolValue])
        {
            [arrTimeSlot removeAllObjects];
            //[arrTimeSlot addObject:@"Select Time"];
            [viewTime setUserInteractionEnabled:YES];
            txtFTime.text = @"Select Time";
            NSMutableArray *arrTime = [dictMain objectForKey:@"timeSlot"];
            for (int i=0; i<[arrTime count]; i++)
            {
                [arrTimeSlot addObject:[arrTime objectAtIndex:i] ];
            }
        }
        else if(![[dictMain objectForKey:@"status"] boolValue])
        {
            [viewTime setUserInteractionEnabled:FALSE];
            txtFTime.text = @"Select Time";
            [Utils showAlertMessage:KMessageTitle Message:@"Sorry! Booking are closed for today."];
        }
    }
    else if ([strCheck isEqualToString:@"onlytime"])
    {
        if([[dictMain objectForKey:@"status"] boolValue])
        {
            [arrTimeSlot removeAllObjects];
            // [arrTimeSlot addObject:@"Select Time"];
            NSMutableArray *arrTime = [dictMain objectForKey:@"timeSlot"];
            for (int i=0; i<[arrTime count]; i++)
            {
                [arrTimeSlot addObject:[arrTime objectAtIndex:i] ];
            }
            
            [self openTimePicker];

        }
        else if(![[dictMain objectForKey:@"status"] boolValue])
        {
            [Utils showAlertMessage:KMessageTitle Message:@"Sorry! Booking are closed for today."];
        }
    }
}


- (void)requestFailed:(ASIHTTPRequest *) request
{
    //NSLog(@"Fail");
    [MBProgressHUD hideAllHUDsForView:[AppDelegate sharedDelegate].window animated:YES];
}



#pragma mark - UIButton Actions
- (IBAction)btnUpdatePressed:(id)sender
{
    //update your values here
    if ([txtFTime.text isEqualToString:@"Select Time"])
    {
        [Utils showAlertMessage:KMessageTitle Message:@"please select time"];
    }
    else
    {
        CartItemData *cart = [[AppDelegate sharedDelegate].arrCartItems objectAtIndex:btnTag];
        cart.itemComment = txtViewComment.text;
        cart.itemDate = txtFDate.text;
        cart.itemTime = txtFTime.text;
        cart.itemQty = lblQty.text;
        cart.itemStatus = @"1";
        cart.itemAvailable = @"1";
        if ([txtViewComment.text isEqualToString:@"Additional Comment...."])
        {
            cart.itemComment = @"";
        }
        else
        {
            cart.itemComment = txtViewComment.text;
        }
        NSString *strDate1 = [NSString stringWithFormat:@"%@-%@-%@",[txtFDate.text substringWithRange:NSMakeRange(6, 4)],[txtFDate.text substringWithRange:NSMakeRange(3, 2)],[txtFDate.text substringWithRange:NSMakeRange(0, 2)]];
        
        NSString *strDateTime = [NSString stringWithFormat:@"%@ %@",strDate1,[txtFTime.text substringWithRange:NSMakeRange(0, 7)]];
        cart.itemDateTime = strDateTime;
        NSLog(@"cart.itemDateTime=%@",cart.itemDateTime);

        cart.itemPrice = [NSString stringWithFormat:@"%d",count*singlePrice];
        [[NSNotificationCenter defaultCenter] postNotificationName:@"UPDATE_CART_VALUE" object:nil];
        [self removeFromSuperview];
    }
}

- (IBAction)btnCancelPressed:(id)sender
{
    [self removeFromSuperview];
}
- (BOOL)textView:(UITextView *)textView shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text {
    
    if([text isEqualToString:@"\n"]) {
        [textView resignFirstResponder];
        return NO;
    }
    
    return YES;
}
@end
