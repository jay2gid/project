//
//  CartEditView.h
//  ToolsOnWheel
//
//  Created by Shikhar Khanna on 10/01/18.
//  Copyright © 2018 Kapil Goyal. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "DropDownView.h"

@interface CartEditView : UIView<UITextViewDelegate,calenderDelegate>
{
    IBOutlet UIView *viewCartEdit;
    IBOutlet UIView *view;
    IBOutlet UIView *viewComment;
    IBOutlet UIView *viewQty;
    IBOutlet UIView *viewContainer;
    IBOutlet UITextField *txtFDate;
    IBOutlet UITextField *txtFTime;
    IBOutlet UILabel *lblAmount;

    IBOutlet UITextView *txtViewComment;

    IBOutlet UIButton *btnUpdate;
    IBOutlet UIButton *btnCancel;
    
    IBOutlet UIButton *btnPlus;
    IBOutlet UIButton *btnMinus;
    
    IBOutlet UILabel *lblQty;
    int singlePrice;
    int count;
    NSMutableArray *arrTimeSlot;
    NSString *strCheck;
    NSMutableArray *arrDate;
    
    UIButton *btnSender;
    NSString *strDate;
    IBOutlet UIView *viewTime;
}
-(void)updateView:(int)tag;

@property(retain, nonatomic)UIViewController *controller;
@property(assign, nonatomic)int btnTag;
@property (retain,nonatomic)DropDownView *dropDownView;

-(IBAction)btnDateClicked:(id)sender;
@end
