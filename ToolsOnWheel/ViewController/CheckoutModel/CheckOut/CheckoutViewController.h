//
//  CheckoutViewController.h
//  ToolsOnWheel
//
//  Created by Shikhar Khanna on 12/01/18.
//  Copyright © 2018 Kapil Goyal. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "DropDownView.h"
#import "BSKeyboardControls.h"

@interface CheckoutViewController : UIViewController<UITextFieldDelegate,BSKeyboardControlsDelegate,UIScrollViewDelegate,transictionDelegate>
{
    IBOutlet UILabel *lblFinalAmount;
    IBOutlet UIView *viewInnerContainer;
    IBOutlet UILabel *lblTotalAmount;
    IBOutlet NSLayoutConstraint *rewardTop;
    
    IBOutlet NSLayoutConstraint *rewardHeight;
    IBOutlet UITextField *txtCode;
    IBOutlet UITextField *txtCodeRupees;
    IBOutlet UITextField *txtFieldAddress;
    IBOutlet UITextField *txtFieldLandmark;
    IBOutlet UITextField *txtFieldEmail;

    IBOutlet UIButton *btnOfferCode;
    
    
    NSString *strCheckout;
    NSMutableArray *arrCodes;
    NSMutableArray *arrPrice;
    NSMutableArray *arrDate;
    int codeIndex;
    IBOutlet UIButton *btnWallet;
    IBOutlet UILabel *lblWallettTxt;
    BOOL isWallet;
    int walletAmount;
    
    int offerAmount;
    BOOL  isDefaultAddress;

    BSKeyboardControls *keyboardControls;
    UIScrollView *scrlView;
    IBOutlet UIButton *btnSaveAddress;
    IBOutlet UIButton *btnPayNow;
    
    NSString *strAmount;
    NSString *strAddress;
    NSMutableArray *arrCartCheckout;
    IBOutlet UIImageView *imgCheck;
    BOOL boolPayment;
    
    IBOutlet UIButton *btnPayOnline;
    IBOutlet UIButton *btnCashDelivery;
    BOOL boolRadio;
}
@property (retain,nonatomic)DropDownView *dropDownView;
@property (retain,nonatomic)NSString *strCartAmount;
@property (retain,nonatomic)NSMutableArray  *arrCartItem;

-(IBAction)btnOfferCodeClicked:(id)sender;
-(IBAction)btnRewardPointClicked:(id)sender;
-(IBAction)btnPayNowClicked:(id)sender;
-(IBAction)btnSaveAddressClicked:(id)sender;
-(IBAction)btnPaymentMethodClicked:(id)sender;
@end
