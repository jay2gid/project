//
//  CheckoutViewController.m
//  ToolsOnWheel
//
//  Created by Shikhar Khanna on 12/01/18.
//  Copyright © 2018 Kapil Goyal. All rights reserved.
//

#import "CheckoutViewController.h"
#import "CustomNavigation.h"
#import "MFSideMenu.h"
#import "CommonHeader.h"
#import "ASIFormDataRequest.h"
#import "Utils.h"
#import "JSON.h"
#import "MBProgressHUD.h"
#import "AppDelegate.h"
#import "Config.h"
#import "DHValidation.h"
#import "CartItemData.h"
#import "ThankYouVC.h"




@interface CheckoutViewController ()<mainDelegate>

@end

@implementation CheckoutViewController
{
    BOOL isCCAvenue;
    NSString *orderID;
    BOOL onThisScreen;
}
@synthesize strCartAmount;
- (void)viewDidLoad
{
    [super viewDidLoad];
    strAmount = strCartAmount;
    [btnWallet setHidden:YES];
    [lblWallettTxt setHidden: YES];
    rewardHeight.constant = 0;
    
    viewInnerContainer.layer.cornerRadius = 10.0f;
    [self performSelector:@selector(updateUI) withObject:nil afterDelay:0.01];
    arrDate = [[NSMutableArray alloc] init];
    arrCodes = [[NSMutableArray alloc] init];
    arrPrice = [[NSMutableArray alloc] init];
    strCheckout = @"offer";
//    [lblWallettTxt setHidden: YES];
    txtFieldEmail.text = [[NSUserDefaults standardUserDefaults] objectForKey:@"email"];
    txtFieldAddress.text = [[NSUserDefaults standardUserDefaults] objectForKey:@"address"];
    txtFieldLandmark.text = [[NSUserDefaults standardUserDefaults] objectForKey:@"areaName"];
    
   // lblFinalAmount.text = [NSString stringWithFormat:@"Rs.%d",[strCartAmount intValue]];
    //lblTotalAmount.text = [NSString stringWithFormat:@"Rs.%d",[strCartAmount intValue]];
    
    lblFinalAmount.attributedText = [[AppDelegate sharedDelegate] getColoredString:[UIColor colorWithRed:130/255.0 green:167/255.0 blue:195/255.0 alpha:1.0] string:[NSString stringWithFormat:@"%d",[strCartAmount intValue]]];
    
    lblTotalAmount.attributedText = [[AppDelegate sharedDelegate] getColoredString:[UIColor colorWithRed:130/255.0 green:167/255.0 blue:195/255.0 alpha:1.0] string:[NSString stringWithFormat:@"%d",[strCartAmount intValue]]];
    
   
    
    // Do any additional setup after loading the view from its nib.
    
    [self checkOfferApi];
    
    [AppDelegate sharedDelegate].mainDelegate = self;
    onThisScreen = true;
}

-(void)checkOfferApi{
    
    
//    if ([AppDelegate checkNetwork])
//    {
//        [MBProgressHUD showHUDAddedTo:[AppDelegate sharedDelegate].window animated:YES];
//        ASIFormDataRequest *request = [ASIFormDataRequest requestWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@%@",ServerAdd3,kListOfOffer]]];
//        [request setPostValue:[[NSUserDefaults standardUserDefaults] objectForKey:@"secureSignature"] forKey:@"secureSignature"];
//        [request setPostValue:[[NSUserDefaults standardUserDefaults] objectForKey:@"userid"] forKey:@"userId"];
//        [request setDelegate:self];
//        [request startAsynchronous];
//    }
//
    
    
    [Helper showLoading];
    NSDictionary *param =@{@"userId":USER_ID,
                           @"secureSignature": SECURE_SIGNATURE_API};
    
    [WebServiceCalls POST:kListOfOffer parameter:param completionBlock:^(id JSON, WebServiceResult result)
     {
         [Helper stopLoading];
         @try
         {
             NSDictionary *dictMain = JSON;
            
             if([[dictMain objectForKey:@"status"] boolValue] )
             {
                 NSArray *arrCompTmp = [[[[dictMain objectForKey:@"result"] objectForKey:@"offerData"] objectForKey:@"codeNames"] componentsSeparatedByString:@";"];
                 NSArray *arrPriceTmp = [[[[dictMain objectForKey:@"result"] objectForKey:@"offerData"] objectForKey:@"price"] componentsSeparatedByString:@";"];
                 NSArray *arrEndDateTmp = [[[[dictMain objectForKey:@"result"] objectForKey:@"offerData"] objectForKey:@"endDate"] componentsSeparatedByString:@";"];
                 [self->arrCodes removeAllObjects];
                 [self->arrPrice removeAllObjects];
                 [self->arrDate removeAllObjects];
                 
                 [self->arrCodes addObject:@"Select Offer"];
                 for (int i=0; i<[arrCompTmp count]; i++)
                 {
                     [self->arrCodes addObject:[arrCompTmp objectAtIndex:i]];
                 }
                 for (int i=0; i<[arrPriceTmp count]; i++)
                 {
                     [self->arrPrice addObject:[arrPriceTmp objectAtIndex:i]];
                 }
                 for (int i=0; i<[arrEndDateTmp count]; i++)
                 {
                     [self->arrDate addObject:[arrEndDateTmp objectAtIndex:i]];
                 }
                 
                 if ([self->arrCodes count]==0){
                     [self->btnWallet setUserInteractionEnabled:NO];
                     [self->txtCode setText:@"No offers Today"];
                     self->txtCodeRupees.attributedText=[[AppDelegate sharedDelegate] getColoredString:[UIColor colorWithRed:130/255.0 green:167/255.0 blue:195/255.0 alpha:1.0] string:@"0"];
                 }else{
                     [self->btnWallet setUserInteractionEnabled:YES];
                 }
                 
                 [self checkWallet];
             }
             else if(![[dictMain objectForKey:@"status"] boolValue])
             {
                 if (![[dictMain objectForKey:@"result"] isEqualToString:@"Currently You have no Offers"])
                 {
                     [Utils showAlertMessage:KMessageTitle Message:[dictMain objectForKey:@"result"]];
                 }
                 [self->txtCode setText:@"No offers Today"];
                 //   [txtCodeRupees setText:@"0"];
                 
                 self->txtCodeRupees.attributedText=[[AppDelegate sharedDelegate] getColoredString:[UIColor colorWithRed:130/255.0 green:167/255.0 blue:195/255.0 alpha:1.0] string:@"0"];
                 [self checkWallet];
             }
         }
         @catch (NSException *exception) { }
         @finally {
             
         }
     }];
}

-(void)checkWallet{
    
//    self->strCheckout = @"wallet";
//    [MBProgressHUD showHUDAddedTo:[AppDelegate sharedDelegate].window animated:YES];
//    ASIFormDataRequest *request = [ASIFormDataRequest requestWithURL:[NSURL URLWithString:[NSString  stringWithFormat:@"%@%@",ServerAdd3,kWalletAmount]]];
//    [request setDelegate:self];
//    [request setPostValue:[[NSUserDefaults standardUserDefaults] objectForKey:@"userid"] forKey:@"userId"];
//    [request startAsynchronous];
    
    
    
    [Helper showLoading];
    NSDictionary *param =@{@"userId":USER_ID};
    
    [WebServiceCalls POST:kWalletAmount parameter:param completionBlock:^(id JSON, WebServiceResult result)
     {
         [Helper stopLoading];
         @try
         {

             NSDictionary *dictMain = JSON;
             if([[dictMain objectForKey:@"status"] boolValue] )
             {
                 if ([[dictMain objectForKey:@"amount"] intValue]<50)
                 {
                     //                isWallet = FALSE;
                     [self->btnWallet setHidden:YES];
                     [self->lblWallettTxt setHidden: YES];
                     self->rewardHeight.constant = 0;
                 }
                 else
                 {
                     //                isWallet = TRUE;
                     self->rewardHeight.constant = 22;
                     
                     [self->btnWallet setHidden:NO];
                     [self->lblWallettTxt setHidden: NO];
                     self->walletAmount = [[dictMain objectForKey:@"amount"] intValue];
                    
                     //        if (walletAmount>=50)
                     //           {
                     //                    lblFinalAmount.text = [NSString stringWithFormat:@"Rs.%d",[strCartAmount intValue]-50];
                     //                    lblTotalAmount.text = [NSString stringWithFormat:@"Rs.%d",[strCartAmount intValue]-50];
                     //                }
                 }
             }
         }
         @catch (NSException *exception) { }
         @finally { }
     }];
}


-(void)didBecomeActive{
    if (onThisScreen) {
        [self.navigationController popViewControllerAnimated:true];
    }
}


-(void)viewDidDisappear:(BOOL)animated{
    onThisScreen = false;
}


-(void)updateUI
{
    [CustomNavigation addTarget:self backRequired:NO title:@"CHECKOUT"];
    for (UIView *v1 in self.view.subviews)
    {
        if ([v1 isKindOfClass:[CommonHeader class]])
        {
            v1.backgroundColor = [UIColor whiteColor];
            for (UIView *viewTmp in v1.subviews)
            {
                if ([viewTmp isKindOfClass:[UIButton class]])
                {
                    if (viewTmp.tag==1)
                    {
                        viewTmp.hidden=YES;
                    }
                    else if(viewTmp.tag==106)
                    {
                        [(UIButton*)viewTmp addTarget:self action:@selector(btnBackClicked:) forControlEvents:UIControlEventTouchUpInside];
                    }
                    else if(viewTmp.tag==102)
                    {
                        UIButton *btn = (UIButton *)viewTmp;
                        btn.hidden = true;
                    }
                    
                }
                else if ([viewTmp isKindOfClass:[UIImageView class]])
                {
                    if (viewTmp.tag==101)
                    {
                        [(UIImageView*)viewTmp setImage:[UIImage imageNamed:@"backP.png"]];
                    }
                }
            }
        }
    }
}

-(IBAction)btnOfferCodeClicked:(id)sender
{
    
}

#pragma mark - DropDown List methods
- (IBAction)generateDropDown:(UIButton*)sender
{
    //arrGender = [NSMutableArray arrayWithObjects:@"Male",@"Female",nil];
    //    arrPrefix = [NSMutableArray arrayWithObjects:@"Mr",@"Mrs",@"Miss",nil];
    
    if (self.dropDownView)
    {
        [_dropDownView removeFromSuperview];
        _dropDownView = nil;
    }
    
    CGRect rect = [sender convertRect:sender.bounds toView:self.view];
    self.dropDownView = [[DropDownView alloc] initWithFrame:CGRectMake(rect.origin.x, rect.origin.y-37, rect.size.width+8, rect.size.height) target:self];
    [self.dropDownView setBackgroundColor:[UIColor clearColor]];
    [self.dropDownView setTag:sender.tag];
    
    [self downloadDropDownData:sender];
    
    if ([self.dropDownView.myDataarray count])
    {
        [self.view addSubview:self.dropDownView];
    }
    
    [self.dropDownView performSelector:@selector(openDropDown) withObject:nil afterDelay:0.1];
}



#pragma mark - DropDown delegate
-(void)didSelectIndex:(int)index ForDropDown:(DropDownView*)dropdown
{
    if (dropdown.strDropDownValue)
    {
        NSArray *list = [NSArray arrayWithArray:arrCodes];
        NSLog(@"selected data :%@",[list objectAtIndex:index]);
        codeIndex = index;
        txtCode.text = dropdown.strDropDownValue;
        if ([txtCode.text isEqualToString:@"Select Offer"])
        {
            //txtCodeRupees.text = @"Rs.0";
            
            txtCodeRupees.attributedText = [[AppDelegate sharedDelegate] getColoredString:[UIColor colorWithRed:130/255.0 green:167/255.0 blue:195/255.0 alpha:1.0] string:@"0"];

            offerAmount = 0;
        }
        else
        {
//            txtCodeRupees.text = [NSString stringWithFormat:@"Rs.%@",[arrPrice objectAtIndex:codeIndex-1]];
            txtCodeRupees.attributedText = [[AppDelegate sharedDelegate] getColoredString:[UIColor colorWithRed:130/255.0 green:167/255.0 blue:195/255.0 alpha:1.0] string:[NSString stringWithFormat:@"%@",[arrPrice objectAtIndex:codeIndex-1]]];

            
            offerAmount = [[arrPrice objectAtIndex:codeIndex-1] intValue];
            
        }
        
        if (walletAmount>=50 && isWallet)
        {
            //lblFinalAmount.text = [NSString stringWithFormat:@"Rs.%d",[strCartAmount intValue]-50 - offerAmount];
            
            lblFinalAmount.attributedText = [[AppDelegate sharedDelegate] getColoredString:[UIColor colorWithRed:130/255.0 green:167/255.0 blue:195/255.0 alpha:1.0] string:[NSString stringWithFormat:@"%d",[strCartAmount intValue]-50 - offerAmount]];

            //lblTotalAmount.text = [NSString stringWithFormat:@"Rs.%d",[strCartAmount intValue]-50 - offerAmount];
        }
        else
        {
            //lblFinalAmount.text = [NSString stringWithFormat:@"Rs.%d",[strCartAmount intValue]-offerAmount];
            lblFinalAmount.attributedText = [[AppDelegate sharedDelegate] getColoredString:[UIColor colorWithRed:130/255.0 green:167/255.0 blue:195/255.0 alpha:1.0] string:[NSString stringWithFormat:@"%d",[strCartAmount intValue]-offerAmount]];

            //lblTotalAmount.text = [NSString stringWithFormat:@"Rs.%d",[strCartAmount intValue]-offerAmount];
        }
    }
    
    [dropdown.myLabel setText:@""];
    [self.dropDownView removeFromSuperview];
}


- (void)downloadDropDownData:(UIButton*)sender
{
        NSArray *list = [NSArray arrayWithArray:arrCodes];
        [self.dropDownView setDataArray:list];
}

- (void)requestFinished:(ASIHTTPRequest *)request
{
    [MBProgressHUD hideAllHUDsForView:[AppDelegate sharedDelegate].window animated:YES];
    NSString *receivedString = [request responseString];
    NSLog(@"str=%@",receivedString);
    NSDictionary *dictMain=[receivedString JSONValue];
    
    if ([strCheckout isEqualToString:@"offer"])
    {
        if([[dictMain objectForKey:@"status"] boolValue] )
        {
            NSArray *arrCompTmp = [[[[dictMain objectForKey:@"result"] objectForKey:@"offerData"] objectForKey:@"codeNames"] componentsSeparatedByString:@";"];
            NSArray *arrPriceTmp = [[[[dictMain objectForKey:@"result"] objectForKey:@"offerData"] objectForKey:@"price"] componentsSeparatedByString:@";"];
            NSArray *arrEndDateTmp = [[[[dictMain objectForKey:@"result"] objectForKey:@"offerData"] objectForKey:@"endDate"] componentsSeparatedByString:@";"];
            [arrCodes removeAllObjects];
            [arrPrice removeAllObjects];
            [arrDate removeAllObjects];

            [arrCodes addObject:@"Select Offer"];
            for (int i=0; i<[arrCompTmp count]; i++)
            {
                [arrCodes addObject:[arrCompTmp objectAtIndex:i]];
            }
            for (int i=0; i<[arrPriceTmp count]; i++)
            {
                [arrPrice addObject:[arrPriceTmp objectAtIndex:i]];
            }
            for (int i=0; i<[arrEndDateTmp count]; i++)
            {
                [arrDate addObject:[arrEndDateTmp objectAtIndex:i]];
            }
            
            if ([arrCodes count]==0)
            {
                [btnWallet setUserInteractionEnabled:NO];
                [txtCode setText:@"No offers Today"];
                txtCodeRupees.attributedText=[[AppDelegate sharedDelegate] getColoredString:[UIColor colorWithRed:130/255.0 green:167/255.0 blue:195/255.0 alpha:1.0] string:@"0"];
            }
            else
            {
                [btnWallet setUserInteractionEnabled:YES];
            }
            if ([AppDelegate checkNetwork])
            {
                strCheckout = @"wallet";
                [MBProgressHUD showHUDAddedTo:[AppDelegate sharedDelegate].window animated:YES];
                ASIFormDataRequest *request = [ASIFormDataRequest requestWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@%@",ServerAdd3,kWalletAmount]]];
                [request setDelegate:self];
                [request setPostValue:[[NSUserDefaults standardUserDefaults] objectForKey:@"userid"] forKey:@"userId"];
                [request startAsynchronous];
            }
            
        }
        else if(![[dictMain objectForKey:@"status"] boolValue])
        {
            if (![[dictMain objectForKey:@"result"] isEqualToString:@"Currently You have no Offers"])
            {
                [Utils showAlertMessage:KMessageTitle Message:[dictMain objectForKey:@"result"]];
            }
            [txtCode setText:@"No offers Today"];
            //   [txtCodeRupees setText:@"0"];
            
            txtCodeRupees.attributedText=[[AppDelegate sharedDelegate] getColoredString:[UIColor colorWithRed:130/255.0 green:167/255.0 blue:195/255.0 alpha:1.0] string:@"0"];

            if ([AppDelegate checkNetwork])
            {
                strCheckout = @"wallet";
                [MBProgressHUD showHUDAddedTo:[AppDelegate sharedDelegate].window animated:YES];
                ASIFormDataRequest *request = [ASIFormDataRequest requestWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@%@",ServerAdd3,kWalletAmount]]];
                [request setDelegate:self];
                [request setPostValue:[[NSUserDefaults standardUserDefaults] objectForKey:@"userid"] forKey:@"userId"];
                [request startAsynchronous];
            }
            
        }
    }
    else if ([strCheckout isEqualToString:@"wallet"])
    {
        if([[dictMain objectForKey:@"status"] boolValue] )
        {
            if ([[dictMain objectForKey:@"amount"] intValue]<50)
            {
//                isWallet = FALSE;
                [btnWallet setHidden:YES];
                [lblWallettTxt setHidden: YES];
                rewardHeight.constant = 0;
            }
            else
            {
//                isWallet = TRUE;
                rewardHeight.constant = 22;

                [btnWallet setHidden:NO];
                [lblWallettTxt setHidden: NO];
                walletAmount = [[dictMain objectForKey:@"amount"] intValue];
//                if (walletAmount>=50)
//                {
//                    lblFinalAmount.text = [NSString stringWithFormat:@"Rs.%d",[strCartAmount intValue]-50];
//                    lblTotalAmount.text = [NSString stringWithFormat:@"Rs.%d",[strCartAmount intValue]-50];
//                }
            }
        }
    }
    else if ([strCheckout isEqualToString:@"address"])
    {
        if([[dictMain objectForKey:@"status"] boolValue])
        {
            [[NSUserDefaults standardUserDefaults] setObject:txtFieldAddress.text forKey:@"address"];
            [[NSUserDefaults standardUserDefaults] setObject:txtFieldLandmark.text forKey:@"areaName"];
            [[NSUserDefaults standardUserDefaults] setObject:txtFieldEmail.text forKey:@"email"];
            [[NSUserDefaults standardUserDefaults] synchronize];
            
            if ([AppDelegate checkNetwork])
            {
                strCheckout = @"cartdata";
                [MBProgressHUD showHUDAddedTo:[AppDelegate sharedDelegate].window animated:YES];
                ASIFormDataRequest *request = [ASIFormDataRequest requestWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@%@",ServerAdd3,kCartCheckForAvailablity]]];
                [request setDelegate:self];
                [request setPostValue:[self.arrCartItem JSONRepresentation] forKey:@"cartData"];
                [request startAsynchronous];
            }
        }else {
            [Utils showAlertMessage:KMessageTitle Message:[dictMain objectForKey:@"result"]];
        }
    }
    else if ([strCheckout isEqualToString:@"cartdata"])
    {
        int count=0;
        NSMutableArray *arrResult = [dictMain objectForKey:@"resultendArray"];
        for (int i=0; i<[arrResult count]; i++)
        {
            for (int j=0; j<[[AppDelegate sharedDelegate].arrCartItems count]; j++)
            {
                BOOL isFlag = [[[arrResult objectAtIndex:i] objectForKey:@"status"] boolValue];
                if (!isFlag)
                {
                    count++;
                    NSString *strCartId = [[arrResult objectAtIndex:i] objectForKey:@"cart_id"];
                    CartItemData *cart = [[AppDelegate sharedDelegate].arrCartItems objectAtIndex:j];
                    if ([cart.cartId isEqualToString:strCartId])
                    {
                        cart.itemAvailable = @"0";
                    }
                }
            }
        }
        if (count>0){
            [[NSNotificationCenter defaultCenter] postNotificationName:@"CHECK_CART_ITEM" object:nil];
            [self.navigationController popViewControllerAnimated:YES];
        }
        else
        {
            [self arrayForOrder];
            if ([AppDelegate checkNetwork])
            {
                strCheckout = @"order";
                [MBProgressHUD showHUDAddedTo:[AppDelegate sharedDelegate].window animated:YES];
                ASIFormDataRequest *request = [ASIFormDataRequest requestWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@%@",ServerAdd,kOrder]]];
                [request setDelegate:self];

                [request setPostValue:strAmount forKey:@"totalAmount"];
                [request setPostValue:[[NSUserDefaults standardUserDefaults] objectForKey:@"userid"] forKey:@"userId"];
                [request setPostValue:@"ios" forKey:@"orderFrom"];

                if (isCCAvenue) {
                    
                    [request setPostValue:@"4" forKey:@"orderStatus"];
                    [request setPostValue:@"ONLINE" forKey:@"paymentMethod"];
                  
                    if (ORDER_ID_OLD) {
                        [request setPostValue:ORDER_ID_OLD forKey:@"orderId"];
                    }
                }
                
                else {
                    
                    [request setPostValue:@"COD" forKey:@"paymentMethod"];
                    [request setPostValue:@"1" forKey:@"orderStatus"];
                }
                
                [request setPostValue:[NSString stringWithFormat:@"%d",offerAmount] forKey:@"offerAmount"];
              
                if (walletAmount >=50 && isWallet){
                    [request setPostValue:[NSString stringWithFormat:@"%d",50] forKey:@"walletAmount"];
                }else{
                    [request setPostValue:@"0" forKey:@"walletAmount"];
                }
                
                [request setPostValue:[[NSUserDefaults standardUserDefaults] objectForKey:@"refcode"] forKey:@"refcode"];
                [request setPostValue:strAddress forKey:@"orderAddress"];
                [request setPostValue:[[NSUserDefaults standardUserDefaults] objectForKey:@"secureSignature"] forKey:@"secureSignature"];
                
                
                
                [request setPostValue:[arrCartCheckout JSONRepresentation] forKey:@"data"];
                [request startAsynchronous];
            }
        }
    }
    else if ([strCheckout isEqualToString:@"order"])
    {
        if([[dictMain objectForKey:@"status"] boolValue])
        {
            orderID = [Helper getString:dictMain[@"orderId"]];
            if (isCCAvenue) {
                [self openCCAvenue];
                
            }else{
                // [Utils showAlertMessage:KMessageTitle Message:[dictMain objectForKey:@"result"]];
                [[AppDelegate sharedDelegate].arrCartItems removeAllObjects];
                ThankYouVC *thank = [[ThankYouVC alloc] initWithNibName:@"ThankYouVC" bundle:nil];
                thank.strOrderId = [dictMain objectForKey:@"orderId"];
                [self.navigationController pushViewController:thank animated:YES];
            }
        }
        else
        {
            [Utils showAlertMessage:KMessageTitle Message:[dictMain objectForKey:@"result"]];
        }
    }
}

- (void)requestFailed:(ASIHTTPRequest *)request
{
    NSLog(@"Fail");
    [MBProgressHUD hideAllHUDsForView:[AppDelegate sharedDelegate].window animated:YES];
    // [Utils showAlertMessage:KMessageTitle Message:@"Network error"];
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)btnBackClicked:(id)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}

- (IBAction)btnRewardPointClicked:(id)sender
{
    if(isWallet)
    {
        isWallet = FALSE;
//        [lblWallettTxt setHidden:YES];
        [btnWallet setImage:[UIImage imageNamed:@"check.png"] forState:UIControlStateNormal];
        //lblFinalAmount.text = [NSString stringWithFormat:@"Rs.%d",[strCartAmount intValue]-offerAmount];
        lblFinalAmount.attributedText = [[AppDelegate sharedDelegate] getColoredString:[UIColor colorWithRed:130/255.0 green:167/255.0 blue:195/255.0 alpha:1.0] string:[NSString stringWithFormat:@"%d",[strCartAmount intValue]-offerAmount]];
        
        //lblTotalAmount.text = [NSString stringWithFormat:@"Rs.%d",[strCartAmount intValue]-offerAmount];
    }
    else
    {
        //  [lblWallettTxt setHidden:NO];
        isWallet = TRUE;
        [btnWallet setImage:[UIImage imageNamed:@"checkfill.png"] forState:UIControlStateNormal];
        //  lblFinalAmount.text = [NSString stringWithFormat:@"Rs.%d",[strCartAmount intValue]-50-offerAmount];
        lblFinalAmount.attributedText = [[AppDelegate sharedDelegate] getColoredString:[UIColor colorWithRed:130/255.0 green:167/255.0 blue:195/255.0 alpha:1.0] string:[NSString stringWithFormat:@"%d",[strCartAmount intValue]-50-offerAmount]];
        //  lblTotalAmount.text = [NSString stringWithFormat:@"Rs.%d",[strCartAmount intValue]-50-offerAmount];
    }
}

-(void)addKeyboardControls
{
    // Initialize the keyboard controls
    keyboardControls = [[BSKeyboardControls alloc] init];
    
    // Set the delegate of the keyboard controls
    keyboardControls.delegate = self;
    
    // Add all text fields you want to be able to skip between to the keyboard controls
    // The order of thise text fields are important. The order is used when pressing "Previous" or "Next"
    
    keyboardControls.textFields = [NSArray arrayWithObjects:txtFieldEmail,txtFieldAddress,txtFieldLandmark,nil];
    
    
    // Set the style of the bar. Default is UIBarStyleBlackTranslucent.
    // keyboardControls.barStyle = UIBarStyleBlackTranslucent;
    
    // Set the tint color of the "Previous" and "Next" button. Default is black.
    keyboardControls.previousNextTintColor = [UIColor blackColor];
    
    // Set the tint color of the done button. Default is a color which looks a lot like the original blue color for a "Done" butotn
    keyboardControls.doneTintColor = [UIColor colorWithRed:34.0/255.0 green:164.0/255.0 blue:255.0/255.0 alpha:1.0];
    
    // Set title for the "Previous" button. Default is "Previous".
    keyboardControls.previousTitle = @"Previous";
    
    // Set title for the "Next button". Default is "Next".
    keyboardControls.nextTitle = @"Next";
    
    // Add the keyboard control as accessory view for all of the text fields
    // Also set the delegate of all the text fields to self
    for (id textField in keyboardControls.textFields)
    {
        if ([textField isKindOfClass:[UITextField class]])
        {
            ((UITextField *) textField).inputAccessoryView = keyboardControls;
            ((UITextField *) textField).delegate = self;
        }
        
    }
}

-(void)scrollViewToCenterOfScreen:(UIView *)theView
{
    CGFloat viewCenterY = theView.center.y;
    CGRect applicationFrame = [[UIScreen mainScreen] bounds];
    CGFloat availableHeight = applicationFrame.size.height - 280; // Remove area covered by keyboard
    
    CGFloat y = viewCenterY - availableHeight / 2.0;
    if (y < 0) {
        y = 0;
    }
    [scrlView setContentOffset:CGPointMake(0, y) animated:YES];
    
}

- (void)keyboardControlsDonePressed:(BSKeyboardControls *)controls
{
    [controls.activeTextField resignFirstResponder];
    [scrlView setContentOffset:CGPointMake(0, 0) animated:YES];
}

- (void)keyboardControlsPreviousNextPressed:(BSKeyboardControls *)controls withDirection:(KeyboardControlsDirection)direction andActiveTextField:(id)textField
{
    [textField becomeFirstResponder];
    [self scrollViewToCenterOfScreen:textField];
}

#pragma mark UITextFieldDelegate
- (BOOL)textFieldShouldBeginEditing:(UITextField *)textField
{
    if ([keyboardControls.textFields containsObject:textField])
        keyboardControls.activeTextField = textField;
    
    [self scrollViewToCenterOfScreen:textField];
    return YES;
    
}

-(BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [textField resignFirstResponder];
    // [controls.activeTextField resignFirstResponder];
    [scrlView setContentOffset:CGPointMake(0, 0) animated:YES];
    return YES;
}

-(BOOL)valiateCheckout
{
    txtFieldEmail.text =[txtFieldEmail.text  stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
    txtFieldAddress.text=[txtFieldAddress.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
    txtFieldLandmark.text=[txtFieldLandmark.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
    
    
    DHValidation *aValidation = [[DHValidation alloc]init];
    
    
    BOOL flag = [aValidation validateEmail:txtFieldEmail.text];
    
    
    
    if ([txtFieldEmail.text length]>0  && !flag)
    {
        [Utils showAlertMessage:KMessageTitle Message:@"Please enter valid email address"];
        [txtFieldEmail becomeFirstResponder];
        return false;
    }
    else if(txtFieldAddress.text == NULL || [txtFieldAddress.text length] == 0)
    {
        [Utils showAlertMessage:KMessageTitle Message:@"Please enter address"];
        [txtFieldAddress becomeFirstResponder];
        return false;
    }
    else if ([txtFieldAddress.text length]>0)
    {
        if ([txtFieldAddress.text length]<20)
        {
            [Utils showAlertMessage:KMessageTitle Message:@"Address should be of atleast 20 characters."];
            [txtFieldAddress becomeFirstResponder];
            return false;
        }
        else
        {
            return true;
        }
    }
    else
    {
        return true;
    }
}

-(IBAction)btnPayNowClicked:(id)sender
{
    if (!boolRadio){
        [Utils showAlertMessage:KMessageTitle Message:@"Please select payment method"];
    } else {
        
        if (isDefaultAddress)
        {
            if ([self valiateCheckout])
            {
                if ([AppDelegate checkNetwork])
                {
                    strCheckout = @"address";
                    [MBProgressHUD showHUDAddedTo:[AppDelegate sharedDelegate].window animated:YES];
                    ASIFormDataRequest *request = [ASIFormDataRequest requestWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@%@",ServerAdd3,kUpdateUserInfo]]];
                    [request setDelegate:self];
                    [request setPostValue:[[NSUserDefaults standardUserDefaults] objectForKey:@"userid"] forKey:@"userId"];
                    [request setPostValue:[[NSUserDefaults standardUserDefaults] objectForKey:@"name"] forKey:@"name"];
                    [request setPostValue:[[NSUserDefaults standardUserDefaults] objectForKey:@"phone"] forKey:@"mob"];
                    [request setPostValue:txtFieldEmail.text forKey:@"email"];
                    
                    //        NSData *imageData1=UIImageJPEGRepresentation(imgViewProfile.image, 1.0);
                    //
                    //        if(imageData1 != nil)
                    //            [request setFile:imageData1 withFileName:[NSString stringWithFormat:@"%ld",(long)[NSDate timeIntervalSinceReferenceDate]] andContentType:@"image/png" forKey:@"image"];
                    
                    txtFieldLandmark.text=[txtFieldLandmark.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
                    if (txtFieldLandmark.text.length>0)
                    {
                        strAddress = [NSString stringWithFormat:@"%@/n/nLandmark:%@",txtFieldAddress.text,txtFieldLandmark.text];
                        [request setPostValue:strAddress forKey:@"userAddress"];
                    }
                    else if (txtFieldAddress.text.length==0)
                    {
                        strAddress = [NSString stringWithFormat:@"/n/nLandmark:%@",txtFieldLandmark.text];
                        [request setPostValue:strAddress forKey:@"userAddress"];
                    }
                    else
                    {
                        strAddress = txtFieldAddress.text;
                        [request setPostValue:strAddress forKey:@"userAddress"];
                    }
                    [request startAsynchronous];
                }
            }
        }
        else {
            
            if ([self valiateCheckout])
            {
                txtFieldLandmark.text=[txtFieldLandmark.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
                if (txtFieldLandmark.text.length>0)
                {
                    strAddress = [NSString stringWithFormat:@"%@/n/nLandmark:%@",txtFieldAddress.text,txtFieldLandmark.text];
                }
                else if (txtFieldAddress.text.length==0)
                {
                    strAddress = [NSString stringWithFormat:@"/n/nLandmark:%@",txtFieldLandmark.text];
                }
                else
                {
                    strAddress = txtFieldAddress.text;
                }
                if ([AppDelegate checkNetwork])
                {
                    strCheckout = @"cartdata";
                    [MBProgressHUD showHUDAddedTo:[AppDelegate sharedDelegate].window animated:YES];
                    ASIFormDataRequest *request = [ASIFormDataRequest requestWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@%@",ServerAdd3,kCartCheckForAvailablity]]];
                    [request setDelegate:self];
                    [request setPostValue:[self.arrCartItem JSONRepresentation] forKey:@"cartData"];
                    [request startAsynchronous];
                }
                
            }
        }
    }
    
}

-(IBAction)btnPaymentMethodClicked:(id)sender
{
    UIButton *btn = sender;
    if (btn.tag==100)
    {
        isCCAvenue = true;
        boolRadio = TRUE;
        [btnPayNow setTitle:@"Pay now" forState:UIControlStateNormal];
        [btnPayOnline setImage:[UIImage imageNamed:@"radio.png"] forState:UIControlStateNormal];
        [btnCashDelivery setImage:[UIImage imageNamed:@"radioG.png"] forState:UIControlStateNormal];

    }
    else
    {
        isCCAvenue = false;
        boolRadio = TRUE;
        [btnPayNow setTitle:@"Place order" forState:UIControlStateNormal];
        [btnPayOnline setImage:[UIImage imageNamed:@"radioG.png"] forState:UIControlStateNormal];
        [btnCashDelivery setImage:[UIImage imageNamed:@"radio.png"] forState:UIControlStateNormal];
    }
}

-(void)arrayForOrder
{
    arrCartCheckout = [[NSMutableArray alloc] init];
   
    int walletMoney = 0;
    
    if (walletAmount>=50 && isWallet)
    {
        walletMoney = offerAmount + 50;
    }
    else
    {
        walletMoney = offerAmount;
    }
    
    
    int modeValue = walletMoney%[[AppDelegate sharedDelegate].arrCartItems count];
    
    if (modeValue > 0) {
        walletMoney = walletMoney - modeValue;
    }
    
    float reedemAmount = 0.0;

    for (int i=0; i<[[AppDelegate sharedDelegate].arrCartItems count]; i++)
    {
        CartItemData *cart = [[AppDelegate sharedDelegate].arrCartItems objectAtIndex:i];
        NSMutableDictionary *dict = [[NSMutableDictionary alloc] init];
        [dict setObject:cart.itemId forKey:@"cateId"];
        [dict setObject:cart.subItemId forKey:@"subCateId"];
        [dict setObject:cart.subChildItemId forKey:@"childId"];
        [dict setObject:cart.itemPrice forKey:@"cateRate"];
        [dict setObject:cart.itemComment forKey:@"userComment"];
        [dict setObject:@"1" forKey:@"status"];
        [dict setObject:cart.itemQty forKey:@"quantity"];
        [dict setObject:[NSString stringWithFormat:@"%d",i+1] forKey:@"cart_id"];

        
        
        if (walletMoney > 0) {
            
            reedemAmount = (walletMoney/[[AppDelegate sharedDelegate].arrCartItems count]);
            
            if (i == [[AppDelegate sharedDelegate].arrCartItems count] - 1) {
                reedemAmount = reedemAmount + modeValue;
            }
            
            [dict setObject:[NSString stringWithFormat:@"%.2f",reedemAmount] forKey:@"reedemAmount"];
            reedemAmount = 0.0;
            
        }else{
            [dict setObject:@"0" forKey:@"reedemAmount"];
        }
        
        
//        if (walletAmount>=50 && isWallet)
//        {
//            reedemAmount = (offerAmount+50)/[[AppDelegate sharedDelegate].arrCartItems count];
//        }
//        else
//        {
//            reedemAmount = (offerAmount)/[[AppDelegate sharedDelegate].arrCartItems count];
//        }
        
        //[dict setObject:[NSString stringWithFormat:@"%.2f",reedemAmount] forKey:@"reedemAmount"];

        NSString *str = cart.itemDate;
        // NSString *strDate = [NSString stringWithFormat:@"%@/%@/%@",[str substringWithRange:NSMakeRange(8, 2)],[str substringWithRange:NSMakeRange(5, 2)],[str substringWithRange:NSMakeRange(0, 4)]];
        
        [dict setObject:str forKey:@"orderServiceDate"];
        [dict setObject:cart.itemTime forKey:@"orderServiceTime"];
        [arrCartCheckout addObject:dict];
    }
}


-(IBAction)btnSaveAddressClicked:(id)sender
{
    if (!isDefaultAddress)
    {
        isDefaultAddress = TRUE;
        // // [btnSaveAddress setImage:[UIImage imageNamed:@"uncheck.png"] forState:UIControlStateNormal];
        [imgCheck setImage:[UIImage imageNamed:@"checkFill.png"]];
    }
    else
    {
        isDefaultAddress = FALSE;
        // // [btnSaveAddress setImage:[UIImage imageNamed:@"check.png"] forState:UIControlStateNormal];
        [imgCheck setImage:[UIImage imageNamed:@"check.png"]];
    }
}




#pragma mark CCAVENUE USER


- (void)openCCAvenue {
    
    NSLog(@"button clicked");
    
    CCWebViewController* controller = [[CCWebViewController alloc] initWithNibName:@"CCWebViewController" bundle:nil];
    controller.accessCode = ccAccessCode;
    controller.merchantId = ccMerchantId;
    controller.amount = [lblFinalAmount.text stringByReplacingOccurrencesOfString:@"₹" withString:@""];
    controller.currency = ccCurrency;
    controller.orderId = orderID;
    controller.redirectUrl = ccUrlRedirect;
    controller.cancelUrl = ccUrlCancel;
    controller.rsaKeyUrl = ccUrlRSA;
    controller.delegate  = self;
    controller.address = strAddress;
    
    if (walletAmount >=50 && isWallet){
        controller.wallet = [NSString stringWithFormat:@"%d",50];
    }else{
        controller.wallet = @"0";
    }
  
    controller.modalTransitionStyle = UIModalTransitionStyleCrossDissolve;
    [self presentViewController:controller animated:YES completion:nil];
}

-(void)onTransiction:(ccStatus)status tid:(NSString *)tid{
    [Helper removeOrderID];
    
    [[AppDelegate sharedDelegate].arrCartItems removeAllObjects];
    ThankYouVC *thank = [[ThankYouVC alloc] initWithNibName:@"ThankYouVC" bundle:nil];
    thank.strOrderId = orderID;
    thank.isOnline = isCCAvenue;
    thank.ccstatus = status;
    thank.trekingID = tid;
    
    [self.navigationController pushViewController:thank animated:YES];
}
-(void)onTransiction:(ccStatus)status {
    
    if(status == successCC){
                
     
    }
}

@end
