//
//  ServiceDetailVC.m
//  ToolsOnWheel
//
//  Created by Kapil Goyal on 31/12/17.
//  Copyright © 2017 Kapil Goyal. All rights reserved.
//

#import "ServiceDetailVC.h"


@interface ServiceDetailVC ()<DSLCalendarViewDelegate>
{
    CalPopUp *calView;
}


@property (nonatomic, weak) IBOutlet DSLCalendarView *calendarView;
@end

@implementation ServiceDetailVC
@synthesize strSubCatId,dictSelectedItem,dictMain;

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    [self performSelector:@selector(updateUI) withObject:nil afterDelay:0.01];
    txtViewComment.text = @"Additional Comment....";
    txtViewComment.delegate = self;
    
    arrServices = [[NSMutableArray alloc] init];
    arrTime = [[NSMutableArray alloc] init];
    arrDate = [[NSMutableArray alloc] init];
    arrServicesData = [[NSMutableArray alloc] init];
    arrDisableDate = [[NSMutableArray alloc] init];
    arrTimeSlot = [[NSMutableArray alloc] init];
    //   [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(getChagneDateService:) name:kDateChange object:nil];
    
   
    viewPopup.layer.cornerRadius = 5.0;
    [viewPopup setBackgroundColor:[UIColor whiteColor]];
    
    viewQty.layer.cornerRadius = 5.0;
    [viewQty.layer setBorderColor:[UIColor lightGrayColor].CGColor];
    [viewQty.layer setBorderWidth:1.0];
    
    viewComment.layer.cornerRadius = 5.0;
    [viewComment.layer setBorderColor:[UIColor lightGrayColor].CGColor];
    [viewComment.layer setBorderWidth:1.0];
    
    
    [scrlView setContentSize:CGSizeMake(scrlView.frame.size.width, scrlView.frame.size.height+20)];
    //   viewPopup.frame = CGRectMake(viewPopup.frame.origin.x, viewPopup.frame.origin.y, viewPopup.frame.size.width, 110);
    
    popUpHeight.constant = 110;
    
    cartItemBadge = [CustomBadge customBadgeWithString:@""
                                       withStringColor:[UIColor whiteColor]
                                        withInsetColor:kBadgeIconColor
                                        withBadgeFrame:YES
                                   withBadgeFrameColor:kBadgeIconColor
                                             withScale:0.7
                                           withShining:NO];
    [cartItemBadge setFrame:CGRectMake(WIDTH -cartItemBadge.frame.size.width-6, 27, cartItemBadge.frame.size.width, cartItemBadge.frame.size.height)];
    [cartItemBadge setHidden:YES];
    [self.view addSubview:cartItemBadge];
    [self addKeyboardControls];
    
    isCalender = false;
    isDropDown = false;
}


-(void)updateUI
{
    NSString *heading = [[dictSelectedItem objectForKey:@"name"] uppercaseString];
    [CustomNavigation addTarget:self backRequired:NO title:heading];
    for (UIView *v1 in self.view.subviews)
    {
        if ([v1 isKindOfClass:[CommonHeader class]])
        {
            for (UIView *viewTmp in v1.subviews)
            {
                if ([viewTmp isKindOfClass:[UIButton class]])
                {
                    if (viewTmp.tag==1)
                    {
                        viewTmp.hidden=YES;
                    }
                    else if(viewTmp.tag==106)
                    {
                        [(UIButton*)viewTmp addTarget:self action:@selector(btnBackClicked:) forControlEvents:UIControlEventTouchUpInside];
                    }
                    else if(viewTmp.tag==102)
                    {
                        [(UIButton*)viewTmp addTarget:self action:@selector(moveToCart:) forControlEvents:UIControlEventTouchUpInside];
                        [(UIButton*)viewTmp setImage:[UIImage imageNamed:@"cartW.png"] forState:UIControlStateNormal];

                    }
                }
                else if ([viewTmp isKindOfClass:[UIImageView class]])
                {
                    if (viewTmp.tag==101)
                    {
                        [(UIImageView*)viewTmp setImage:[UIImage imageNamed:@"back.png"]];
                    }
                }
                else if ([viewTmp isKindOfClass:[UILabel class]])
                {
                        [(UILabel*)viewTmp setTextColor:[UIColor whiteColor]];
                }
            }
        }
    }
    
    [self.view bringSubviewToFront:cartItemBadge];
}

-(void)viewWillAppear:(BOOL)animated
{
    [self receiveNotification:nil];
    [self.menuContainerViewController setPanMode:MFSideMenuPanModeNone];

    lblDate.text=@"";
    [lblTime setPlaceholder:@"Select Time"];
    [btnDate setUserInteractionEnabled:NO];
    [btnTime setUserInteractionEnabled:NO];
    [btnInfo setHidden:YES];
    [lblCharacters setHidden:YES];
    [lblStaticTotalAmount setHidden:YES];
    [lblDiscount setHidden:YES];
    [lblTotalAmount setHidden:YES];
    [lblStaticDiscount setHidden:YES];
    [lblDot1 setHidden:YES];
    [lblDot2 setHidden:YES];
    [lblPayment setHidden:YES];
    [viewCart setHidden:YES];

    
    [imgTop sd_setImageWithURL:[NSURL URLWithString:_strImgeUrl] placeholderImage:[UIImage imageNamed:@"banner1.png"]];
    

    [Helper showLoading];
    NSDictionary *param =@{@"subcatId":strSubCatId,
                           @"secureSignature": SECURE_SIGNATURE_API};
    
    [WebServiceCalls POST:kGetSubCategory parameter:param completionBlock:^(id JSON, WebServiceResult result)
     {
         [Helper stopLoading];
         @try
         {
            if([[JSON objectForKey:@"status"] boolValue])
             {
                 [self->arrServicesData removeAllObjects];
                 self->arrServicesData = [JSON objectForKey:@"result"];
                 [self->arrServices removeAllObjects];
                 // [self->arrServices addObject:@"Select Services"];
                 for (int i=0; i<[self->arrServicesData count]; i++) {
                     [self->arrServices addObject:[[self->arrServicesData objectAtIndex:i] objectForKey:@"name"]];
                 }
                 
                 [self apiDisableDate];
             }
             else if(![[JSON objectForKey:@"status"] boolValue])
             {
                 [Utils showAlertMessage:KMessageTitle Message:@"No Service"];
                 [self.navigationController popViewControllerAnimated:YES];
             }
         }
         @catch (NSException *exception) { }
         @finally {
          
         }
     }];
}

-(void)apiDisableDate {
    
    [Helper showLoading];
    [WebServiceCalls POST:kDisableDate parameter:nil completionBlock:^(id JSON, WebServiceResult result)
     {
         [Helper stopLoading];
         
         @try
         {
             
             if([[JSON objectForKey:@"status"] boolValue])
             {
                 [self->arrDisableDate removeAllObjects];
                 self->arrDisableDate = [JSON objectForKey:@"result"];
                 [self->arrDate removeAllObjects];
                 for (int i=0; i<[self->arrDisableDate count]; i++)
                 {
                     [self->arrDate addObject:[[self->arrDisableDate objectAtIndex:i] objectForKey:@"date"]];
                 }
             }
             else if(![[JSON objectForKey:@"status"] boolValue])
             {
                 [Utils showAlertMessage:KMessageTitle Message:[self->dictMain objectForKey:@"result"]];
             }
         }
         @catch (NSException *exception) { }
         @finally {
             
         }
     }];
}

-(IBAction)moveToCart:(id)sender
{

        MyCartViewController *vc = [[MyCartViewController alloc] initWithNibName:@"MyCartViewController" bundle:nil];
        [self.navigationController pushViewController:vc animated:YES];
}

-(void)btnBackClicked:(id)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}


#pragma mark Date Selection

-(void)onDateSelected:(NSString *)date {
 
    isCalender = false;
        [[NSUserDefaults standardUserDefaults] removeObjectForKey:@"datepopup"];
        [[NSUserDefaults standardUserDefaults] synchronize];
        strDate = date;
        lblDate.text = strDate;
    
        lblDate.text = [NSString stringWithFormat:@"%@/%@/%@",[strDate substringWithRange:NSMakeRange(8, 2)],[strDate substringWithRange:NSMakeRange(5, 2)],[strDate substringWithRange:NSMakeRange(0, 4)]];
    
    
    
    
    ///////////////////// ------------- working here
    strCheck = @"timeslot";

    
    [Helper showLoading];
    NSDictionary *param =@{@"selectDate":strDate,
                           @"cateId": [[NSUserDefaults standardUserDefaults] objectForKey:@"maincatid"]};
    
    [WebServiceCalls POST:kTimeSlotDate parameter:param completionBlock:^(id JSON, WebServiceResult result)
     {
         [Helper stopLoading];
         @try
         {
             NSDictionary * dictMain = JSON;
             if([[dictMain objectForKey:@"status"] boolValue])
             {
                 [self->viewTime setUserInteractionEnabled:TRUE];
                 [self->lblTime setText:@""];
                 [self->lblTime setPlaceholder:@"Select Time"];
                 [self->arrTimeSlot removeAllObjects];
                 [self->arrTime removeAllObjects];
                 
                 // [self->arrTimeSlot addObject:@"Select Time"];
                 self->arrTime = [dictMain objectForKey:@"timeSlot"];
                 for (int i=0; i<[self->arrTime count]; i++)
                 {
                     [self->arrTimeSlot addObject:[self->arrTime objectAtIndex:i] ];
                 }
             }
             else if(![[dictMain objectForKey:@"status"] boolValue])
             {
                 [self->viewTime setUserInteractionEnabled:FALSE];
                 [self->lblTime setText:@""];
                 [self->lblTime setPlaceholder:@"Select Time"];
                 [Utils showAlertMessage:KMessageTitle Message:@"Sorry! Booking are closed for today."];
             }
         }
         @catch (NSException *exception) { }
         @finally {
             
         }
     }];

}



#pragma mark - DropDown List methods
- (IBAction)generateDropDown:(UIButton*)sender
{
    END_KEY_VC
    if (isCalender) {
        return;
    }

    NSString *titlePicker = @"Select Service";
    NSArray *arrayPicker = arrServices;
    if( sender.tag == 300)
    {
        titlePicker = @"Select time slot";
        arrayPicker = arrTimeSlot;
        if (!(lblDate.text.length > 1)) {
            
            NSLog(@"not allowd");
            return;
        }
    }
    
    
    
    [ActionSheetStringPicker showPickerWithTitle:titlePicker
                                            rows:arrayPicker
                                initialSelection:0
                                       doneBlock:^(ActionSheetStringPicker *picker, NSInteger selectedIndex, id selectedValue) {
                                           NSLog(@"Picker: %@, Index: %ld, value: %@",
                                                 picker, (long)selectedIndex, selectedValue);
                                           
                                           NSLog(@"%ld",(long)sender.tag);
                                           
                                           [self onselected:sender :(int)selectedIndex];
                                           
                                           
                                       }
                                     cancelBlock:^(ActionSheetStringPicker *picker) {
                                         NSLog(@"Block Picker Canceled");
                                     }
                                          origin:sender];
    
}


-(void)onselected:(UIButton *)sender :(int)index{
    
    if (sender.tag==100)
    {
        NSArray *list = [NSArray arrayWithArray:arrServices];
        NSLog(@"selected data :%@",[list objectAtIndex:index]);
        serviveIndex = index+1;
        lblService.text = arrServices[index];
        if (![lblService.text isEqualToString:@"Select Services"])
        {
            //                viewPopup.frame = CGRectMake(viewPopup.frame.origin.x, viewPopup.frame.origin.y, viewPopup.frame.size.width, 251);
            [viewCart setHidden:YES];
            
            popUpHeight.constant = 288;
            [viewComment setHidden:NO];
            [viewQty setHidden:NO];
            [btnDate setUserInteractionEnabled:YES];
            [btnTime setUserInteractionEnabled:YES];
            [btnInfo setHidden:NO];
            
            [lblCharacters setHidden:NO];
            
            
            [lblStaticDiscount setHidden:NO];
            [lblDiscount setHidden:NO];
            [lblStaticTotalAmount setHidden:NO];
            [lblDot1 setHidden:NO];
            [lblDot2 setHidden:NO];
            
            [lblTotalAmount setHidden:NO];
            [lblPayment setHidden:NO];
            [viewCart setHidden:NO];
            lblQty.text = @"1";
            
            
            
            lblTotalAmount.attributedText = [[AppDelegate sharedDelegate] getStrikeOutColoredString:[UIColor orangeColor] string:[[arrServicesData objectAtIndex:serviveIndex-1] objectForKey:@"rate"]];
            strSubChildItemId = [[arrServicesData objectAtIndex:serviveIndex-1] objectForKey:@"id"];
            
            if ([[[arrServicesData objectAtIndex:serviveIndex-1] objectForKey:@"discount"]intValue] == 0)
            {
                
                lblPayment.attributedText =  [[AppDelegate sharedDelegate] getColoredString:[UIColor whiteColor] string:[[arrServicesData objectAtIndex:serviveIndex-1] objectForKey:@"rate"]];
                
            }
            else
            {
                
                lblPayment.attributedText =[[AppDelegate sharedDelegate] getColoredString:[UIColor whiteColor] string:[[arrServicesData objectAtIndex:serviveIndex-1] objectForKey:@"discount"]];
                
            }
            
            NSString *strDiscount = [NSString stringWithFormat:@"%d",[[[arrServicesData objectAtIndex:serviveIndex-1] objectForKey:@"rate"] intValue]-[[[arrServicesData objectAtIndex:serviveIndex-1] objectForKey:@"discount"]intValue]];
            
            [lblDiscount setAttributedText: [[AppDelegate sharedDelegate] getColoredString:[UIColor orangeColor] string:strDiscount]];
            
            
            
        }
        else{
            //                viewPopup.frame = CGRectMake(viewPopup.frame.origin.x, viewPopup.frame.origin.y, viewPopup.frame.size.width, 110);
            popUpHeight.constant = 110;
            [viewComment setHidden:YES];
            [viewQty setHidden:YES ];
            lblDate.text=@"";
            lblTime.text=@"";
            [lblTime setPlaceholder:@"Select Time"];
            [btnDate setUserInteractionEnabled:NO];
            [btnTime setUserInteractionEnabled:NO];
            [btnInfo setHidden:YES];
            [lblCharacters setHidden:YES];
            
            [lblStaticTotalAmount setHidden:YES];
            [lblDot1 setHidden:YES];
            [lblDot2 setHidden:YES];
            [lblDiscount setHidden:YES];
            [lblStaticDiscount setHidden:YES];
            [lblTotalAmount setHidden:YES];
            [lblPayment setHidden:YES];
            [viewCart setHidden:YES];
        }
    }else  if (sender.tag==300)
    {
        NSArray *list = [NSArray arrayWithArray:arrTimeSlot];
        NSLog(@"selected data :%@",[list objectAtIndex:index]);
        //          strLocationId = [arrLocationId objectAtIndex:index];
        lblTime.text = arrTimeSlot[index];
    }
}



-(void)showHide:(BOOL)isBool
{
    [lblCharacters setHidden:isBool];
    [lblStaticTotalAmount setHidden:isBool];
    [lblDiscount setHidden:isBool];
    [lblStaticDiscount setHidden:isBool];
    [lblTotalAmount setHidden:isBool];
    [lblPayment setHidden:isBool];
    [lblDot1 setHidden:isBool];
    [lblDot2 setHidden:isBool];
}

- (void)downloadDropDownData:(UIButton*)sender
{
    if (sender.tag==100)
    {
        NSArray *list = [NSArray arrayWithArray:arrServices];
        [self.dropDownView setDataArray:list];
    }
    else if (sender.tag==300)
    {
        NSArray *list = [NSArray arrayWithArray:arrTimeSlot];
        [self.dropDownView setDataArray:list];
    }
}


-(IBAction)btnPlusClicked:(id)sender
{
    int i= [[lblQty text] intValue];
    if (i<10)
    {
        [lblQty setText:[NSString stringWithFormat:@"%i",++i]];
        
        NSString *strPayment = [NSString stringWithFormat:@"%i",i*([[[arrServicesData objectAtIndex:serviveIndex-1] objectForKey:@"rate"] intValue])];

        
        lblTotalAmount.attributedText =  [[AppDelegate sharedDelegate] getStrikeOutColoredString:[UIColor orangeColor] string:strPayment];

        
        if ([[[arrServicesData objectAtIndex:serviveIndex-1] objectForKey:@"discount"] intValue]==0)
        {
            NSString *strPayment = [NSString stringWithFormat:@"%i",i*([[[arrServicesData objectAtIndex:serviveIndex-1] objectForKey:@"rate"] intValue])];
            
            lblPayment.attributedText =  [[AppDelegate sharedDelegate] getColoredString:[UIColor whiteColor] string:strPayment];

        }
        else
        {
            NSString *strPayment = [NSString stringWithFormat:@"%i",i*([[[arrServicesData objectAtIndex:serviveIndex-1] objectForKey:@"discount"] intValue])];

            lblPayment.attributedText =  [[AppDelegate sharedDelegate] getColoredString:[UIColor whiteColor] string:strPayment];
        }
        amount = [lblPayment.text intValue];
        
        NSString *strPayment1 = [NSString stringWithFormat:@"%d",i*[[[arrServicesData objectAtIndex:serviveIndex-1] objectForKey:@"rate"] intValue]-(i*[[[arrServicesData objectAtIndex:serviveIndex-1] objectForKey:@"discount"]intValue])];
        
        [lblDiscount setAttributedText:[[AppDelegate sharedDelegate] getColoredString:[UIColor orangeColor] string:strPayment1]];
    }
}

-(IBAction)btnMinusClicked:(id)sender
{
    int i= [[lblQty text] intValue];
    if(i>1)
    {
        [lblQty setText:[NSString stringWithFormat:@"%i",--i]];
        
        NSString *strPayment = [NSString stringWithFormat:@"%i",i*([[[arrServicesData objectAtIndex:serviveIndex-1] objectForKey:@"rate"] intValue])];
        lblTotalAmount.attributedText =  [[AppDelegate sharedDelegate] getStrikeOutColoredString:[UIColor orangeColor] string:strPayment];

        
        if (([[[arrServicesData objectAtIndex:serviveIndex-1] objectForKey:@"discount"] intValue])==0)
        {
            NSString *strPayment = [NSString stringWithFormat:@"%i",i*([[[arrServicesData objectAtIndex:serviveIndex-1] objectForKey:@"rate"] intValue])];
            
            lblPayment.attributedText =  [[AppDelegate sharedDelegate] getColoredString:[UIColor whiteColor] string:strPayment];

        }
        else
        {
            NSString *strPayment = [NSString stringWithFormat:@"%i",i*([[[arrServicesData objectAtIndex:serviveIndex-1] objectForKey:@"discount"] intValue])];

            lblPayment.attributedText =  [[AppDelegate sharedDelegate] getColoredString:[UIColor whiteColor] string:strPayment];

        }
        
        amount = [lblPayment.text intValue];

        NSString *strPayment1 = [NSString stringWithFormat:@"%d",i*[[[arrServicesData objectAtIndex:serviveIndex-1] objectForKey:@"rate"] intValue]-(i*[[[arrServicesData objectAtIndex:serviveIndex-1] objectForKey:@"discount"]intValue])];
        
        [lblDiscount setAttributedText:[[AppDelegate sharedDelegate] getColoredString:[UIColor orangeColor] string:strPayment1]];

    }
}

-(IBAction)btnAddToCartClicked:(id)sender
{
    if ([lblService.text isEqualToString:@"Select Services"])
    {
        [Utils showAlertMessage:KMessageTitle Message:@"Please select service"];
    }
    else if ([lblDate.text isEqualToString:@""])
    {
        [Utils showAlertMessage:KMessageTitle Message:@"Please select date"];
    }
    else if ([lblTime.text isEqualToString:@""] || [lblTime.text isEqualToString:@"Select Time"])
    {
        [Utils showAlertMessage:KMessageTitle Message:@"Please select time"];
    }
    else if ([txtViewComment.text length]>200)
    {
        [Utils showAlertMessage:KMessageTitle Message:@"Please enter comment max 200 characters"];
    }
    else
    {
        CartItemData *cart =[[CartItemData alloc] init];
        cart.itemQty = lblQty.text;
        cart.itemDate = lblDate.text;
        cart.itemTime = lblTime.text;
        
        NSString *strDateTime = [NSString stringWithFormat:@"%@ %@",strDate,[lblTime.text substringWithRange:NSMakeRange(0, 7)]];
        cart.itemDateTime = strDateTime;
        NSLog(@"cart.itemDateTime=%@",cart.itemDateTime);
        cart.itemPrice = [NSString stringWithFormat:@"%@",[lblPayment.text substringWithRange:NSMakeRange(1, lblPayment.text.length-1)]];
        if ([txtViewComment.text isEqualToString:@"Additional Comment...."])
        {
            cart.itemComment = @"";
        }
        else
        {
            cart.itemComment = txtViewComment.text;
        }
        cart.itemId = [dictMain objectForKey:@"id"];
        cart.subItemId = strSubCatId;
        cart.subChildItemId = strSubChildItemId;
        cart.itemName = [dictMain objectForKey:@"name"];
        cart.itemSubName = lblService.text;
        cart.itemImage = [dictMain objectForKey:@"image"];
        cart.itemStatus = @"1";
        [[AppDelegate sharedDelegate].arrCartItems addObject:cart];
        [self receiveNotification:nil];
      
        
//        MyCartViewController *vc = [[MyCartViewController alloc] initWithNibName:@"MyCartViewController" bundle:nil];
//        [self.navigationController pushViewController:vc animated:YES];
//        [[NSNotificationCenter defaultCenter] postNotificationName:@"UPDATE_CART" object:nil];
        [self putViewAddMore];
        
    }
}

-(void)putViewAddMore{
    ViewAddMore *view = [[[NSBundle mainBundle]loadNibNamed:@"ViewAddMore" owner:self options:nil]objectAtIndex:0];
    view.selfCon = self;
    view.frame = self.view.frame;
    [self.view addSubview:view];
    
}


-(IBAction)btnDateClicked
{
    END_KEY_VC
    if (isDropDown) {
        return;
    }
    isCalender = true;
    [[NSUserDefaults standardUserDefaults] setObject:@"service" forKey:@"datepopup"];
    [[NSUserDefaults standardUserDefaults] synchronize];
    
    if (!calView) {
        calView  = [[[NSBundle mainBundle] loadNibNamed:@"CalPopUp" owner:self options:nil] objectAtIndex:0];
        calView.frame = self.view.bounds;
        [calView UpdateView];
        calView.caldelegate = self;
    }
    calView.hidden = false;
    [self.view addSubview:calView];
   
}

- (IBAction)btnDonePopUpPressed:(id)sender
{
    [viewDetailPopup removeFromSuperview];
}

- (IBAction)btnInfoPressed:(id)sender
{
    [self.view addSubview:viewDetailPopup];
    viewDetailPopup.frame = self.view.bounds;
    viewInnerPopup.layer.cornerRadius = 8.0f;
//    btnDone.layer.cornerRadius = 15.0f;
    
    
    btnDone.layer.cornerRadius = 15.0f;
    btnDone.layer.borderColor = [UIColor orangeColor].CGColor;
    btnDone.layer.borderWidth = 2.0f;
    

    //create NSMutableAttributedString for adding attributes as per required
    NSMutableAttributedString *attrHTMLText = [[[NSAttributedString alloc] initWithData:[[[arrServicesData objectAtIndex:serviveIndex-1] objectForKey:@"desc"] dataUsingEncoding:NSUTF8StringEncoding] options:@{NSDocumentTypeDocumentAttribute: NSHTMLTextDocumentType,NSCharacterEncodingDocumentAttribute: @(NSUTF8StringEncoding)} documentAttributes:nil error:nil] mutableCopy];
    //set font here
    [attrHTMLText addAttribute:NSFontAttributeName value:lblFullServiceDescription.font range:NSMakeRange(0, attrHTMLText.length)];
    //set color here
    [attrHTMLText addAttribute:NSForegroundColorAttributeName value:[UIColor blackColor] range:NSMakeRange(0, attrHTMLText.length)];
    
    NSMutableParagraphStyle *paraStyle = [[NSMutableParagraphStyle alloc] init];
    paraStyle.alignment = NSTextAlignmentCenter;
    
    [attrHTMLText addAttribute:NSParagraphStyleAttributeName value:paraStyle range:NSMakeRange(0, attrHTMLText.length)];
    
    //Now set attribute text to your Label
    lblFullServiceDescription.attributedText = attrHTMLText;
    
    // lblFullServiceDescription.text = [NSString stringWithFormat:@"%@",([[arrServicesData objectAtIndex:serviveIndex-1] objectForKey:@"desc"])];
    
    
    //    lblFullServiceDescription.text = @"Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.";
}

#pragma mark - Animation
- (void) receiveNotification:(NSNotification *) notification
{
    if ([[AppDelegate sharedDelegate].arrCartItems count]==0)
    {
        [self hideBadge];
    }
    else
    {
        cartItemBadge.alpha = 0.0;
        [cartItemBadge autoBadgeSizeWithString:[NSString stringWithFormat:@"%lu",(unsigned long)[[AppDelegate sharedDelegate].arrCartItems count]]];
        [cartItemBadge setHidden:NO];
        [self showAnimation];
    }    //    }
    //    else
    //    {
    //        [cartItemBadge setHidden:YES];
    //    }
}

- (void)showAnimation
{
    [UIView animateWithDuration:0.2 animations:
     ^(void){
         cartItemBadge.transform = CGAffineTransformScale(CGAffineTransformIdentity,1.1f, 1.1f);
         cartItemBadge.alpha = 0.5;
     }
                     completion:^(BOOL finished){
                         [self bounceOutAnimationStoped];
                     }];
}
- (void)bounceOutAnimationStoped
{
    
    [UIView animateWithDuration:0.1 animations:
     ^(void){
         self->cartItemBadge.transform = CGAffineTransformScale(CGAffineTransformIdentity,0.9, 0.9);
         self->cartItemBadge.alpha = 0.8;
     }
                     completion:^(BOOL finished){
                         [self bounceInAnimationStoped];
                     }];
}
- (void)bounceInAnimationStoped
{
    [UIView animateWithDuration:0.1 animations:
     ^(void){
         self->cartItemBadge.transform = CGAffineTransformScale(CGAffineTransformIdentity,1, 1);
         self->cartItemBadge.alpha = 1.0;
     }
                     completion:^(BOOL finished)
     {
     }];
}
- (void)hideBadge
{
    cartItemBadge.alpha = 0;
    cartItemBadge.transform = CGAffineTransformScale(CGAffineTransformIdentity,0.6, 0.6);
}


#pragma mark BsKeyBoardContrrols delegate
-(void)addKeyboardControls
{
    // Initialize the keyboard controls
    keyboardControls = [[BSKeyboardControls alloc] init];
    
    // Set the delegate of the keyboard controls
    keyboardControls.delegate = self;
    
    // Add all text fields you want to be able to skip between to the keyboard controls
    // The order of thise text fields are important. The order is used when pressing "Previous" or "Next"
    
    keyboardControls.textFields = [NSArray arrayWithObjects:txtViewComment,nil];
    
    
    // Set the style of the bar. Default is UIBarStyleBlackTranslucent.
    // keyboardControls.barStyle = UIBarStyleBlackTranslucent;
    
    // Set the tint color of the "Previous" and "Next" button. Default is black.
    keyboardControls.previousNextTintColor = [UIColor blackColor];
    
    // Set the tint color of the done button. Default is a color which looks a lot like the original blue color for a "Done" butotn
    keyboardControls.doneTintColor = [UIColor colorWithRed:34.0/255.0 green:164.0/255.0 blue:255.0/255.0 alpha:1.0];
    
    // Set title for the "Previous" button. Default is "Previous".
    keyboardControls.previousTitle = @"Previous";
    
    // Set title for the "Next button". Default is "Next".
    keyboardControls.nextTitle = @"Next";
    
    // Add the keyboard control as accessory view for all of the text fields
    // Also set the delegate of all the text fields to self
    for (id textField in keyboardControls.textFields)
    {
       if([textField isKindOfClass:[UITextView class]])
        {
            ((UITextView *) textField).inputAccessoryView = keyboardControls;
            ((UITextView *) textField).delegate = self;
        }
    }
}

-(void)scrollViewToCenterOfScreen:(UIView *)theView
{
    CGFloat viewCenterY = theView.center.y;
    CGRect applicationFrame = [[UIScreen mainScreen] applicationFrame];
    CGFloat availableHeight = applicationFrame.size.height - 280; // Remove area covered by keyboard
    
    CGFloat y = viewCenterY - availableHeight / 2.0;
    if (y < 0) {
        y = 0;
    }
    [scrlView setContentOffset:CGPointMake(0, y) animated:YES];
    
}

- (void)keyboardControlsDonePressed:(BSKeyboardControls *)controls
{
    [controls.activeTextField resignFirstResponder];
    [scrlView setContentOffset:CGPointMake(0, 0) animated:YES];
}

- (void)keyboardControlsPreviousNextPressed:(BSKeyboardControls *)controls withDirection:(KeyboardControlsDirection)direction andActiveTextField:(id)textField
{
    [textField becomeFirstResponder];
    [self scrollViewToCenterOfScreen:textField];
}

-(BOOL)textViewShouldBeginEditing:(UITextView *)textView
{
    if (isDropDown || isCalender) {
        return NO;
    }
    
    if ([textView.text isEqualToString:@"Additional Comment...."])
    {
        txtViewComment.text = @"";
    }
//    txtViewComment.textColor = [UIColor blackColor];
    if ([keyboardControls.textFields containsObject:textView])
        keyboardControls.activeTextField = textView;
    
    [self scrollViewToCenterOfScreen:textView];
    return YES;
}

-(void) textViewDidChange:(UITextView *)textView
{
    if(txtViewComment.text.length == 0){
        //txtViewComment.textColor = [UIColor lightGrayColor];
        txtViewComment.text = @"Additional Comment....";
        [txtViewComment resignFirstResponder];
    }
}
- (void)textViewDidEndEditing:(UITextView *)textView
{
    if ([textView.text isEqualToString:@""]) {
        textView.text = @"Additional Comment";
        textView.textColor = [UIColor lightGrayColor]; //optional
    }
    [textView resignFirstResponder];
}

- (BOOL)textView:(UITextView *)textView shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text {
    
    if([text isEqualToString:@"\n"]) {
        [textView resignFirstResponder];
        return NO;
    }
    return YES;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}


@end
