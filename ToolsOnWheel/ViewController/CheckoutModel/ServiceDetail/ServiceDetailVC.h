//
//  ServiceDetailVC.h
//  ToolsOnWheel
//
//  Created by Kapil Goyal on 31/12/17.
//  Copyright © 2017 Kapil Goyal. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "DropDownView.h"
#import "UIImageView+WebCache.h"
#import "CustomBadge.h"
#import "BSKeyboardControls.h"
#import "KTTextView.h"
#import "TPKeyboardAvoidingScrollView.h"

@interface ServiceDetailVC : UIViewController<BSKeyboardControlsDelegate,UITextViewDelegate,calenderDelegate>
{
    BSKeyboardControls *keyboardControls;
    IBOutlet TPKeyboardAvoidingScrollView *scrlView;
    IBOutlet UIImageView *ImgView;
    
    IBOutlet UIView *viewComment;
    
    IBOutlet NSLayoutConstraint *popUpHeight;
    IBOutlet UITextField *lblService;
    IBOutlet UITextField *lblDate;
    IBOutlet UITextField *lblTime;
    
    IBOutlet UILabel *lblQty;
    IBOutlet UILabel *lblTotalAmount;
    IBOutlet UILabel *lblDiscount;
    
    IBOutlet UILabel *lblPayment;
    IBOutlet UIView *viewPopup;
    IBOutlet UIView *viewDate;

    IBOutlet UIImageView *imgTop;
    
    IBOutlet UIButton *btnDone;
    IBOutlet UILabel *lblFullServiceDescription;
    IBOutlet UIView *viewDetailPopup;
    
    IBOutlet UITextView *txtViewComment;
    NSString *strCheck;
    NSMutableArray *arrDate;
    NSMutableArray *arrTime;
    NSMutableArray *arrServices;
    NSMutableArray *arrServicesData;
    NSMutableArray *arrDisableDate;
    NSMutableArray *arrTimeSlot;

    int qty;
    IBOutlet UIButton *btnPlus;
    IBOutlet UIButton *btnMinus;
    NSString *strDate;
    NSInteger serviveIndex;
    IBOutlet UIView *viewQty;
    IBOutlet UILabel *lblCharacters;
    IBOutlet UIButton *btnDate;
    IBOutlet UIButton *btnTime;
    
    IBOutlet UILabel *lblStaticTotalAmount;
    IBOutlet UILabel *lblStaticDiscount;
    IBOutlet UIView *viewCart;
    BOOL boolDate;
    UITapGestureRecognizer *tapGesture;
    CustomBadge *cartItemBadge;
    int amount;
    IBOutlet UIButton *btnInfo;
    NSString *strSubChildItemId;
    IBOutlet UIView *viewInnerPopup;
    IBOutlet UILabel *lblDot1;
    IBOutlet UILabel *lblDot2;
    IBOutlet UIView *viewTime;
    
}
@property (retain,nonatomic)DropDownView *dropDownView;
@property(nonatomic,strong)NSString *strSubCatId;
@property(nonatomic,strong)NSString *strImgeUrl;
@property(nonatomic, strong)NSMutableDictionary *dictSelectedItem;
@property(nonatomic, strong)NSMutableDictionary *dictMain;

-(IBAction)btnPlusClicked:(id)sender;
-(IBAction)btnMinusClicked:(id)sender;
-(IBAction)btnAddToCartClicked:(id)sender;
-(IBAction)generateDropDown:(UIButton*)sender;
-(IBAction)btnDateClicked;

@end
