//
//  ViewController.m
//  ToolsOnWheel
//
//  Created by Kapil Goyal on 17/09/17.
//  Copyright © 2017 Kapil Goyal. All rights reserved.
//

#import "SubCategoryVC.h"
#import "CommonHeader.h"
#import "CustomNavigation.h"
#import "MFSideMenu.h"
#import "Config.h"


#import "ASIFormDataRequest.h"
#import "JSON.h"
#import "MBProgressHUD.h"
#import "AppDelegate.h"
#import "HomeVC.h"
#import "Utils.h"
#import "SubCategoryTVCell.h"
#import "ServiceDetailVC.h"
#import "MyCartViewController.h"

@interface SubCategoryVC ()
@property (nonatomic, weak) IBOutlet DSLCalendarView *calendarView;

@end

@implementation SubCategoryVC
@synthesize dictCat,arrSubCat,dictMain;
- (void)viewDidLoad
{
    [super viewDidLoad];
    
    cartItemBadge = [CustomBadge customBadgeWithString:@""
                                       withStringColor:[UIColor whiteColor]
                                        withInsetColor:kBadgeIconColor
                                        withBadgeFrame:YES
                                   withBadgeFrameColor:kBadgeIconColor
                                             withScale:0.7
                                           withShining:NO];
    [cartItemBadge setFrame:CGRectMake(WIDTH -cartItemBadge.frame.size.width-6, 27, cartItemBadge.frame.size.width, cartItemBadge.frame.size.height)];
    [cartItemBadge setHidden:YES];
    [self.view addSubview:cartItemBadge];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(receiveNotification:)
                                                 name:@"UPDATE_CART"
                                               object:nil];
    
    [self performSelector:@selector(updateUI) withObject:nil afterDelay:0.01];
    
    _calendarView.delegate = self;
    
    
    arrSubCat = [[NSMutableArray alloc] init];
    NSArray *arrTmp1 = [[dictCat objectForKey:@"names"] componentsSeparatedByString:@";"];
    NSArray *arrTmp2 = [[dictCat objectForKey:@"subCatId"] componentsSeparatedByString:@";"];
    NSArray *arrTmp3 = [[dictCat objectForKey:@"subCateImg"] componentsSeparatedByString:@";"];

    for (int i=0; i<[arrTmp1 count];i++)
    {
        NSMutableDictionary *dict = [NSMutableDictionary new];
        [dict setObject:[arrTmp1 objectAtIndex:i] forKey:@"name"];
        [dict setObject:[arrTmp2 objectAtIndex:i] forKey:@"subcatid"];
        [dict setObject:[arrTmp3 objectAtIndex:i] forKey:@"subcatimage"];
        [arrSubCat addObject:dict];
    }
    
    
}

-(void)updateUI
{
    [CustomNavigation addTarget:self backRequired:NO title:@"SUB CATEGORY"];
    for (UIView *v1 in self.view.subviews)
    {
        if ([v1 isKindOfClass:[CommonHeader class]])
        {
            for (UIView *viewTmp in v1.subviews)
            {
                if ([viewTmp isKindOfClass:[UIButton class]])
                {
                    if (viewTmp.tag==1)
                    {
                        viewTmp.hidden=YES;
                    }
                    else if(viewTmp.tag==106)
                    {
                        [(UIButton*)viewTmp addTarget:self action:@selector(btnBackClicked:) forControlEvents:UIControlEventTouchUpInside];
                    }
                    else if(viewTmp.tag==102)
                    {
                        [(UIButton*)viewTmp addTarget:self action:@selector(moveToCart:) forControlEvents:UIControlEventTouchUpInside];
                    }
                }
                else if ([viewTmp isKindOfClass:[UIImageView class]])
                {
                    if (viewTmp.tag==101)
                    {
                        [(UIImageView*)viewTmp setImage:[UIImage imageNamed:@"backP.png"]];
                    }
                }
            }
        }
    }
    [self.view bringSubviewToFront:cartItemBadge];

}


-(void)viewWillAppear:(BOOL)animated
{
    [self receiveNotification:nil];
    [self.menuContainerViewController setPanMode:MFSideMenuPanModeNone];
}

-(IBAction)moveToCart:(id)sender
{
 
        MyCartViewController *vc = [[MyCartViewController alloc] initWithNibName:@"MyCartViewController" bundle:nil];
        [self.navigationController pushViewController:vc animated:YES];
    
}


#pragma mark - DSLCalendarViewDelegate methods

- (void)calendarView:(DSLCalendarView *)calendarView didSelectRange:(DSLCalendarRange *)range {
    if (range != nil) {
        NSLog( @"Selected %ld/%ld - %ld/%ld", (long)range.startDay.day, (long)range.startDay.month, (long)range.endDay.day, (long)range.endDay.month);
    }
    else {
        NSLog( @"No selection" );
    }
}

- (DSLCalendarRange*)calendarView:(DSLCalendarView *)calendarView didDragToDay:(NSDateComponents *)day selectingRange:(DSLCalendarRange *)range {
    if (NO) { // Only select a single day
        return [[DSLCalendarRange alloc] initWithStartDay:day endDay:day];
    }
    else if (/* DISABLES CODE */ (NO)) { // Don't allow selections before today
        NSDateComponents *today = [[NSDate date] dslCalendarView_dayWithCalendar:calendarView.visibleMonth.calendar];
        
        NSDateComponents *startDate = range.startDay;
        NSDateComponents *endDate = range.endDay;
        
        if ([self day:startDate isBeforeDay:today] && [self day:endDate isBeforeDay:today]) {
            return nil;
        }
        else {
            if ([self day:startDate isBeforeDay:today]) {
                startDate = [today copy];
            }
            if ([self day:endDate isBeforeDay:today]) {
                endDate = [today copy];
            }
            
            return [[DSLCalendarRange alloc] initWithStartDay:startDate endDay:endDate];
        }
    }
    
    return range;
}

- (void)calendarView:(DSLCalendarView *)calendarView willChangeToVisibleMonth:(NSDateComponents *)month duration:(NSTimeInterval)duration {
    NSLog(@"Will show %@ in %.3f seconds", month, duration);
}

- (void)calendarView:(DSLCalendarView *)calendarView didChangeToVisibleMonth:(NSDateComponents *)month {
    NSLog(@"Now showing %@", month);
}

- (BOOL)day:(NSDateComponents*)day1 isBeforeDay:(NSDateComponents*)day2 {
    return ([day1.date compare:day2.date] == NSOrderedAscending);
}
 

-(void)btnBackClicked:(id)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}

#pragma mark - UITableView Delegates

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [arrSubCat count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"Cell";
    SubCategoryTVCell *cell = (SubCategoryTVCell *)[tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil)
    {
        UIViewController *view = [[UIViewController alloc]initWithNibName:@"SubCategoryTVCell" bundle:nil];
        cell = (SubCategoryTVCell *)view.view;
    }
    cell.viewContainer.layer.cornerRadius=15.0f;
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    cell.lblSubCategory.text =  [[arrSubCat objectAtIndex:indexPath.row] objectForKey:@"name"];
    cell.viewColor.backgroundColor = [UIColor orangeColor];
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if ([AppDelegate checkNetwork]) {
        
        [tableView deselectRowAtIndexPath:indexPath animated:YES];
        ServiceDetailVC *serviceDetail = [[ServiceDetailVC alloc] initWithNibName:@"ServiceDetailVC" bundle:nil];
        serviceDetail.strSubCatId = [[arrSubCat objectAtIndex:indexPath.row] objectForKey:@"subcatid"];
        serviceDetail.strImgeUrl = [[arrSubCat objectAtIndex:indexPath.row] objectForKey:@"subcatimage"];
        serviceDetail.dictSelectedItem = [arrSubCat objectAtIndex:indexPath.row];
        serviceDetail.dictMain = dictMain;
        [self.navigationController pushViewController:serviceDetail animated:YES];
    }
   
}

#pragma mark - Animation
- (void) receiveNotification:(NSNotification *) notification
{
    if ([[AppDelegate sharedDelegate].arrCartItems count]==0)
    {
        [self hideBadge];
    }
    else
    {
        cartItemBadge.alpha = 0.0;
        [cartItemBadge autoBadgeSizeWithString:[NSString stringWithFormat:@"%lu",(unsigned long)[[AppDelegate sharedDelegate].arrCartItems count]]];
        [cartItemBadge setHidden:NO];
        [self showAnimation];
    }    //    }
    //    else
    //    {
    //        [cartItemBadge setHidden:YES];
    //    }
}

- (void)showAnimation
{
    [UIView animateWithDuration:0.2 animations:
     ^(void){
         self->cartItemBadge.transform = CGAffineTransformScale(CGAffineTransformIdentity,1.1f, 1.1f);
         self->cartItemBadge.alpha = 0.5;
     }
                     completion:^(BOOL finished){
                         [self bounceOutAnimationStoped];
                     }];
}
- (void)bounceOutAnimationStoped
{
    
    [UIView animateWithDuration:0.1 animations:
     ^(void){
         self->cartItemBadge.transform = CGAffineTransformScale(CGAffineTransformIdentity,0.9, 0.9);
         self->cartItemBadge.alpha = 0.8;
     }
                     completion:^(BOOL finished){
                         [self bounceInAnimationStoped];
                     }];
}
- (void)bounceInAnimationStoped
{
    [UIView animateWithDuration:0.1 animations:
     ^(void){
         self->cartItemBadge.transform = CGAffineTransformScale(CGAffineTransformIdentity,1, 1);
         self->cartItemBadge.alpha = 1.0;
     }
                     completion:^(BOOL finished)
     {
     }];
}
- (void)hideBadge
{
    cartItemBadge.alpha = 0;
    cartItemBadge.transform = CGAffineTransformScale(CGAffineTransformIdentity,0.6, 0.6);
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

@end
