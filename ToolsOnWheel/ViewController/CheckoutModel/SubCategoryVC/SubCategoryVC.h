
//
//  ViewController.h
//  ToolsOnWheel
//
//  Created by Kapil Goyal on 17/09/17.
//  Copyright © 2017 Kapil Goyal. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "DSLCalendarView.h"
#import "MFSideMenu.h"
#import "CustomBadge.h"

@interface SubCategoryVC : UIViewController<UITableViewDelegate,UITableViewDataSource,DSLCalendarViewDelegate>
{
    CustomBadge *cartItemBadge;
    IBOutlet UITableView *tbleViewSubCategory;
}
@property(nonatomic,strong)NSMutableDictionary *dictCat;
@property(nonatomic,strong)NSMutableArray *arrSubCat;
@property(nonatomic,strong)NSMutableDictionary *dictMain;

@end

