//
//  LoginVC.h
//  ToolsOnWheel
//
//  Created by Kapil Goyal on 12/12/17.
//  Copyright © 2017 Kapil Goyal. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ThankYouVC : UIViewController
{
    IBOutlet UIButton *btnRateUs;
    IBOutlet UIButton *btnHome;
    IBOutlet UIButton *btnLikesUS;
    IBOutlet UILabel *lblOrderId;

    IBOutlet UILabel *lblTreckingHint;
    IBOutlet UILabel *lblTreckingID;
    IBOutlet UIImageView *imgIcon;
    
    IBOutlet UILabel *lblStausTitle;
    
    
    
}
@property(nonatomic,strong)NSString *strOrderId;
@property(nonatomic,strong)NSString *trekingID;

@property(nonatomic,readwrite)BOOL isOnline;
@property(nonatomic,readwrite)ccStatus ccstatus;


-(IBAction)btnLikesUsClicked:(id)sender;
-(IBAction)btnHomeClicked:(id)sender;
-(IBAction)btnRateUsClicked:(id)sender;

@end
