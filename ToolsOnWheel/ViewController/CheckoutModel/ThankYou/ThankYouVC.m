//
//  LoginVC.m
//  ToolsOnWheel
//
//  Created by Kapil Goyal on 12/12/17.
//  Copyright © 2017 Kapil Goyal. All rights reserved.
//

#import "ThankYouVC.h"
#import "AppDelegate.h"
#import "CustomNavigation.h"
#import "MFSideMenu.h"
#import "HomeVC.h"

@interface ThankYouVC ()

@end

@implementation ThankYouVC
@synthesize strOrderId;
- (void)viewDidLoad
{
    [super viewDidLoad];
  
    [self performSelector:@selector(updateUI) withObject:nil afterDelay:0.01];
    lblOrderId.text = [NSString stringWithFormat:@"%@",strOrderId];
 
    
    btnRateUs.layer.cornerRadius = 10.0f;
    
    
    if (_isOnline) {
        lblTreckingID.hidden = false;
        lblTreckingHint.hidden = false;
        btnRateUs.center = CGPointMake(btnRateUs.center.x
                                       , btnRateUs.center.y+20);
        lblTreckingID.text = _trekingID;
        
        if (_ccstatus == cancelCC) {
            imgIcon.image = [UIImage imageNamed:@"error_icon"];
            
            lblStausTitle.text = @"Our support team will contact you soon regarding.";
        }
    }
}

-(void)updateUI
{
    [CustomNavigation addTarget:self backRequired:NO title:@"ORDER SUCCESSFUL"];
    for (UIView *v1 in self.view.subviews)
    {
        if ([v1 isKindOfClass:[CommonHeader class]])
        {
            for (UIView *viewTmp in v1.subviews)
            {
                if ([viewTmp isKindOfClass:[UIButton class]])
                {
                    if (viewTmp.tag==1)
                    {
                        viewTmp.hidden=YES;
                    }
                    else if(viewTmp.tag==106)
                    {
                        [(UIButton*)viewTmp setHidden:true];
//                        [(UIButton*)viewTmp addTarget:self action:@selector(btnBackClicked:) forControlEvents:UIControlEventTouchUpInside];
                    }
                    else if(viewTmp.tag==102)
                    {
                        UIButton *btn = (UIButton *)viewTmp;
                        btn.hidden = true;
                    }
                    
                }
                else if ([viewTmp isKindOfClass:[UIImageView class]])
                {
                    if (viewTmp.tag==101)
                    {
                        [(UIImageView*)viewTmp setHidden:true];
//                        [(UIImageView*)viewTmp setImage:[UIImage imageNamed:@"back-button1.png"]];
                    }
                }
                else if ([viewTmp isKindOfClass:[UILabel class]])
                {
                    if (viewTmp.tag==110)
                    {
                        [(UILabel*)viewTmp setTextColor:[UIColor whiteColor]];
                    }
                }
            }
        }
    }
}


-(void)viewWillAppear:(BOOL)animated
{
    [self.menuContainerViewController setPanMode:MFSideMenuPanModeNone];
}

- (void)btnBackClicked
{
    [self.navigationController popViewControllerAnimated:YES];
}

-(IBAction)btnLikesUsClicked:(id)sender
{
    if ([sender tag]==0)
    {
        [[UIApplication sharedApplication] openURL:[NSURL URLWithString: @"https://m.facebook.com/toolsonwheel/"]];

    }
    else if([sender tag]==1)
    {
        [[UIApplication sharedApplication] openURL:[NSURL URLWithString: @"https://www.instagram.com/toolsonwheel/"]];

    }
    else if([sender tag]==2)
    {
        [[UIApplication sharedApplication] openURL:[NSURL URLWithString: @"https://mobile.twitter.com/toolsonwheel/"]];

    }
}

-(IBAction)btnHomeClicked:(id)sender
{
    NSArray *viewControllers = [[self navigationController] viewControllers];
    for( int i=0;i<[viewControllers count];i++){
        id obj=[viewControllers objectAtIndex:i];
        if([obj isKindOfClass:[HomeVC class]]){
            [[self navigationController] popToViewController:obj animated:YES];
        }
    }
}

-(IBAction)btnRateUsClicked:(id)sender
{
    
}


@end
