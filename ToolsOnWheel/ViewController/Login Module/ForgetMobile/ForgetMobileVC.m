//
//  LoginVC.m
//  ToolsOnWheel
//
//  Created by Kapil Goyal on 12/12/17.
//  Copyright © 2017 Kapil Goyal. All rights reserved.
//

#import "ForgetMobileVC.h"
#import "RegisterVC.h"
#import "ASIFormDataRequest.h"
#import "JSON.h"
#import "MBProgressHUD.h"
#import "AppDelegate.h"
#import "HomeVC.h"
#import "Utils.h"
#import "Config.h"
#import "ForgetEmailVC.h"

#import "CommonHeader.h"
#import "CustomNavigation.h"
#import "MFSideMenu.h"

@interface ForgetMobileVC ()

@end

@implementation ForgetMobileVC
@synthesize strUserId,strVerificationCode;
- (void)viewDidLoad
{
    [super viewDidLoad];
    [CustomNavigation addTarget:self backRequired:NO title:@""];
    for (UIView *v1 in self.view.subviews)
    {
        if ([v1 isKindOfClass:[CommonHeader class]])
        {
            for (UIView *viewTmp in v1.subviews)
            {
                if ([viewTmp isKindOfClass:[UIButton class]])
                {
                    if (viewTmp.tag==1)
                    {
                        viewTmp.hidden=YES;
                    }
                    else if(viewTmp.tag==106)
                    {
                        [(UIButton*)viewTmp addTarget:self action:@selector(btnBackClicked) forControlEvents:UIControlEventTouchUpInside];
                    }
                    else if(viewTmp.tag==102)
                    {
                        [(UIButton*)viewTmp setHidden:YES];
                    }
                }
                else if ([viewTmp isKindOfClass:[UIImageView class]])
                {
                    if (viewTmp.tag==101)
                    {
                        [(UIImageView*)viewTmp setImage:[UIImage imageNamed:@"back.png"]];
                    }
                }
            }
        }
    }
    txtFieldMobileNo.borderStyle = UITextBorderStyleRoundedRect;
    txtFieldMobileNo.layer.cornerRadius = 14.0;
    [txtFieldMobileNo setBackgroundColor:[UIColor clearColor]];
    [txtFieldMobileNo.layer setBorderColor:[UIColor whiteColor].CGColor];
    [txtFieldMobileNo.layer setBorderWidth:2.0];

    
    UIColor *color = [UIColor whiteColor];
    txtFieldMobileNo.attributedPlaceholder = [[NSAttributedString alloc] initWithString:@"Registered number" attributes:@{NSForegroundColorAttributeName: color}];
    
    
    
    btnNext.layer.cornerRadius = 22; // this value vary as per your desire
    btnNext.clipsToBounds = YES;
    // Do any additional setup after loading the view from its nib.
    [self addKeyboardControls];
}

-(void)viewWillAppear:(BOOL)animated
{
    [self.menuContainerViewController setPanMode:MFSideMenuPanModeNone];
}

- (void)btnBackClicked
{
    [self.navigationController popViewControllerAnimated:YES];
}

- (BOOL)forgetValidationTxtFields
{
    txtFieldMobileNo.text = [txtFieldMobileNo.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
    
    NSString *mobileNumberPattern = @"[6789][0-9]{9}";
    NSPredicate *mobileNumberPred = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", mobileNumberPattern];
    
    BOOL matched = [mobileNumberPred evaluateWithObject:txtFieldMobileNo.text];
    if(txtFieldMobileNo.text == NULL || [txtFieldMobileNo.text length] == 0)
    {
        [Utils showAlertMessage:KMessageTitle Message:@"Please enter mobile number"];
        [txtFieldMobileNo becomeFirstResponder];
        return false ;
    }
    else if (txtFieldMobileNo.text.length<10 || txtFieldMobileNo.text.length>10)
    {
        [Utils showAlertMessage:KMessageTitle Message:@"Please enter mobile number in 10 digits"];
        [txtFieldMobileNo becomeFirstResponder];
        return false;
    }
    else if (!matched)
    {
        [Utils showAlertMessage:KMessageTitle Message:@"Please enter valid number"];
        [txtFieldMobileNo becomeFirstResponder];
        return false ;
    }
    
    
    else
    {
        return true;
    }
}

- (BOOL) textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range
 replacementString:(NSString *)string
{
    if (textField == txtFieldMobileNo)
    {
        NSString *resultText = [txtFieldMobileNo.text stringByReplacingCharactersInRange:range
                                                                              withString:string];
        return resultText.length <= 10;
    }
    else
    {
        return YES;
    }
    
}

#pragma mark BsKeyBoardContrrols delegate
-(void)addKeyboardControls
{
    // Initialize the keyboard controls
    keyboardControls = [[BSKeyboardControls alloc] init];
    
    // Set the delegate of the keyboard controls
    keyboardControls.delegate = self;
    
    // Add all text fields you want to be able to skip between to the keyboard controls
    // The order of thise text fields are important. The order is used when pressing "Previous" or "Next"
    
    keyboardControls.textFields = [NSArray arrayWithObjects:txtFieldMobileNo,nil];
    
    
    // Set the style of the bar. Default is UIBarStyleBlackTranslucent.
    // keyboardControls.barStyle = UIBarStyleBlackTranslucent;
    
    // Set the tint color of the "Previous" and "Next" button. Default is black.
    keyboardControls.previousNextTintColor = [UIColor blackColor];
    
    // Set the tint color of the done button. Default is a color which looks a lot like the original blue color for a "Done" butotn
    keyboardControls.doneTintColor = [UIColor colorWithRed:34.0/255.0 green:164.0/255.0 blue:255.0/255.0 alpha:1.0];
    
    // Set title for the "Previous" button. Default is "Previous".
    keyboardControls.previousTitle = @"Previous";
    
    // Set title for the "Next button". Default is "Next".
    keyboardControls.nextTitle = @"Next";
    
    // Add the keyboard control as accessory view for all of the text fields
    // Also set the delegate of all the text fields to self
    for (id textField in keyboardControls.textFields)
    {
        if ([textField isKindOfClass:[UITextField class]])
        {
            ((UITextField *) textField).inputAccessoryView = keyboardControls;
            ((UITextField *) textField).delegate = self;
        }
        
    }
}

-(void)scrollViewToCenterOfScreen:(UIView *)theView
{
    CGFloat viewCenterY = theView.center.y;
    CGRect applicationFrame = [[UIScreen mainScreen] applicationFrame];
    CGFloat availableHeight = applicationFrame.size.height - 280; // Remove area covered by keyboard
    
    CGFloat y = viewCenterY - availableHeight / 2.0;
    if (y < 0) {
        y = 0;
    }
   // [scrlView setContentOffset:CGPointMake(0, y) animated:YES];
    
}

- (void)keyboardControlsDonePressed:(BSKeyboardControls *)controls
{
    [controls.activeTextField resignFirstResponder];
    //[scrlView setContentOffset:CGPointMake(0, 0) animated:YES];
}

- (void)keyboardControlsPreviousNextPressed:(BSKeyboardControls *)controls withDirection:(KeyboardControlsDirection)direction andActiveTextField:(id)textField
{
    [textField becomeFirstResponder];
    [self scrollViewToCenterOfScreen:textField];
}

#pragma mark UITextFieldDelegate
- (BOOL)textFieldShouldBeginEditing:(UITextField *)textField
{
    if ([keyboardControls.textFields containsObject:textField])
        keyboardControls.activeTextField = textField;
    
    [self scrollViewToCenterOfScreen:textField];
    return YES;
    
}

-(BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [textField resignFirstResponder];
    // [controls.activeTextField resignFirstResponder];
    //[scrlView setContentOffset:CGPointMake(0, 0) animated:YES];
    return YES;
}

-(IBAction)btnLoginIntoclicked:(id)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}

-(IBAction)btnNextclicked:(id)sender
{
    [self.view endEditing:true];
    if ([self forgetValidationTxtFields])
    {
        if ([AppDelegate checkNetwork])
        {
//            [MBProgressHUD showHUDAddedTo:[AppDelegate sharedDelegate].window animated:YES];
//            ASIFormDataRequest *request = [ASIFormDataRequest requestWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@%@",ServerAdd3,kForget_mob]]];
//            [request setDelegate:self];
//            [request setPostValue:txtFieldMobileNo.text forKey:@"mobNumber"];
//            [request setPostValue:[[NSUserDefaults standardUserDefaults] objectForKey:@"secureSignature"] forKey:@"secureSignature"];
//            [request startAsynchronous];
            
            NSDictionary *param =@{@"mobNumber":txtFieldMobileNo.text,
                                   @"secureSignature": [[NSUserDefaults standardUserDefaults] objectForKey:@"secureSignature"]};
            
            [Helper showLoading];
            [WebServiceCalls POST:kForget_mob parameter:param completionBlock:^(id JSON, WebServiceResult result)
             {
                 @try
                 {
                     [Helper stopLoading];

                     if (result == WebServiceResultSuccess)
                     {
                         NSDictionary *dictMain = JSON;
                         if ([dictMain[@"status"] integerValue] == 1)
                         {
                             ForgetEmailVC *objEmailVC = [[ForgetEmailVC alloc] init];
                             objEmailVC.strUserId = [dictMain objectForKey:@"userId"];
                             objEmailVC.strVerificationCode = [dictMain objectForKey:@"code"];
                             objEmailVC.strMobileNo = self->txtFieldMobileNo.text;
                             [self.navigationController pushViewController:objEmailVC animated:YES];
                             self->txtFieldMobileNo.text=@"";
                             [Utils showAlertMessage:KMessageTitle Message:[dictMain objectForKey:@"result"]];
                         }
                         else if(![[dictMain objectForKey:@"status"] boolValue])
                         {
                             [Utils showAlertMessage:KMessageTitle Message:[dictMain objectForKey:@"result"]];
                         }
                     }
                 }
                 @catch (NSException *exception) { }
                 @finally {
                     [Helper stopLoading];
                 }
             }];
        }
    }
}

/*- (void)requestFinished:(ASIHTTPRequest *)request
{
    [MBProgressHUD hideAllHUDsForView:[AppDelegate sharedDelegate].window animated:YES];
    NSString *receivedString = [request responseString];
    NSLog(@"str=%@",receivedString);
    NSDictionary *dictMain=[receivedString JSONValue];
    if([[dictMain objectForKey:@"status"] boolValue])
    {
        ForgetEmailVC *objEmailVC = [[ForgetEmailVC alloc] init];
        objEmailVC.strUserId = [dictMain objectForKey:@"userId"];
        objEmailVC.strVerificationCode = [dictMain objectForKey:@"code"];
        objEmailVC.strMobileNo = txtFieldMobileNo.text;
        [self.navigationController pushViewController:objEmailVC animated:YES];
        txtFieldMobileNo.text=@"";
        [Utils showAlertMessage:KMessageTitle Message:[dictMain objectForKey:@"result"]];
    }
    else if(![[dictMain objectForKey:@"status"] boolValue])
    {
        [Utils showAlertMessage:KMessageTitle Message:[dictMain objectForKey:@"result"]];
    }
}*/

//- (void)requestFailed:(ASIHTTPRequest *)request
//{
//    NSLog(@"Fail");
//    [MBProgressHUD hideAllHUDsForView:[AppDelegate sharedDelegate].window animated:YES];
//    // [Utils showAlertMessage:KMessageTitle Message:@"Network error"];
//}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
