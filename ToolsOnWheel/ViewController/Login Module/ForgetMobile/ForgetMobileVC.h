//
//  LoginVC.h
//  ToolsOnWheel
//
//  Created by Kapil Goyal on 12/12/17.
//  Copyright © 2017 Kapil Goyal. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BSKeyboardControls.h"

@interface ForgetMobileVC : UIViewController<BSKeyboardControlsDelegate>
{
    IBOutlet UITextField *txtFieldMobileNo;
    IBOutlet UIButton *btnNext;
      BSKeyboardControls *keyboardControls;
}
@property(nonatomic,strong)NSString *strUserId;
@property(nonatomic,strong)NSString *strVerificationCode;

-(IBAction)btnNextclicked:(id)sender;
-(IBAction)btnLoginIntoclicked:(id)sender;

@end
