//
//  LoginVC.h
//  ToolsOnWheel
//
//  Created by Kapil Goyal on 12/12/17.
//  Copyright © 2017 Kapil Goyal. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BSKeyboardControls.h"

@interface ForgetEmailVC : UIViewController<BSKeyboardControlsDelegate,UITextFieldDelegate>
{
    IBOutlet UITextField *txtFieldVerificationCode;
    IBOutlet UITextField *txtFieldPassword;
    IBOutlet UITextField *txtFieldConfirmPassword;

    IBOutlet UIButton *btnSubmit;
    NSString *strCheck;
    BSKeyboardControls *keyboardControls;
    IBOutlet UIScrollView *scrlView;
    NSString *strCode;
    
    IBOutlet UIView *viewReg;
    
    IBOutlet UIButton *btnNext;
    
    IBOutlet UIView *viewPass;
    IBOutlet UILabel *lblTitle;
}
@property(nonatomic,strong)NSString *strUserId;
@property(nonatomic,strong)NSString *strMobileNo;;

@property(nonatomic,strong)NSString *strVerificationCode;

-(IBAction)btnRegenerateCodeClicked:(id)sender;
-(IBAction)btnSubmitClicked:(id)sender;

@end
