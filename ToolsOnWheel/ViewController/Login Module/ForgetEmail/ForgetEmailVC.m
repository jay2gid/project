//
//  LoginVC.m
//  ToolsOnWheel
//
//  Created by Kapil Goyal on 12/12/17.
//  Copyright © 2017 Kapil Goyal. All rights reserved.
//

#import "ForgetEmailVC.h"
#import "RegisterVC.h"
#import "ASIFormDataRequest.h"
#import "JSON.h"
#import "MBProgressHUD.h"
#import "AppDelegate.h"
#import "HomeVC.h"
#import "Utils.h"
#import "Config.h"

#import "CommonHeader.h"
#import "CustomNavigation.h"
#import "MFSideMenu.h"
#import "LoginVC.h"

@interface ForgetEmailVC ()

@end

@implementation ForgetEmailVC
@synthesize strUserId,strVerificationCode,strMobileNo;
- (void)viewDidLoad
{
    [super viewDidLoad];
    [CustomNavigation addTarget:self backRequired:NO title:@""];
    for (UIView *v1 in self.view.subviews)
    {
        if ([v1 isKindOfClass:[CommonHeader class]])
        {
            for (UIView *viewTmp in v1.subviews)
            {
                if ([viewTmp isKindOfClass:[UIButton class]])
                {
                    if (viewTmp.tag==1)
                    {
                        viewTmp.hidden=YES;
                    }
                    else if(viewTmp.tag==106)
                    {
                        [(UIButton*)viewTmp addTarget:self action:@selector(btnBackClicked) forControlEvents:UIControlEventTouchUpInside];
                    }
                    else if(viewTmp.tag==102)
                    {
                        [(UIButton*)viewTmp setHidden:YES];
                    }
                }
                else if ([viewTmp isKindOfClass:[UIImageView class]])
                {
                    if (viewTmp.tag==101)
                    {
                        [(UIImageView*)viewTmp setImage:[UIImage imageNamed:@"back.png"]];
                    }
                }
            }
        }
    }
    txtFieldVerificationCode.borderStyle = UITextBorderStyleRoundedRect;
    txtFieldVerificationCode.layer.cornerRadius = 14.0;
    [txtFieldVerificationCode setBackgroundColor:[UIColor clearColor]];
    [txtFieldVerificationCode.layer setBorderColor:[UIColor whiteColor].CGColor];
    [txtFieldVerificationCode.layer setBorderWidth:2.0];

    
    UIColor *color = [UIColor whiteColor];
    txtFieldVerificationCode.attributedPlaceholder = [[NSAttributedString alloc] initWithString:@"Verification code" attributes:@{NSForegroundColorAttributeName: color}];
    
    
    txtFieldPassword.borderStyle = UITextBorderStyleRoundedRect;
    txtFieldPassword.layer.cornerRadius = 14.0;
    [txtFieldPassword setBackgroundColor:[UIColor clearColor]];
    [txtFieldPassword.layer setBorderColor:[UIColor whiteColor].CGColor];
    [txtFieldPassword.layer setBorderWidth:2.0];
    txtFieldPassword.attributedPlaceholder = [[NSAttributedString alloc] initWithString:@"New Password" attributes:@{NSForegroundColorAttributeName: color}];

    
    txtFieldConfirmPassword.borderStyle = UITextBorderStyleRoundedRect;
    txtFieldConfirmPassword.layer.cornerRadius = 14.0;
    [txtFieldConfirmPassword setBackgroundColor:[UIColor clearColor]];
    [txtFieldConfirmPassword.layer setBorderColor:[UIColor whiteColor].CGColor];
    [txtFieldConfirmPassword.layer setBorderWidth:2.0];
    txtFieldConfirmPassword.attributedPlaceholder = [[NSAttributedString alloc] initWithString:@"Confirm Password" attributes:@{NSForegroundColorAttributeName: color}];

    
    btnSubmit.layer.cornerRadius = 22; // this value vary as per your desire
    btnSubmit.clipsToBounds = YES;
    [self addKeyboardControls];
    // Do any additional setup after loading the view from its nib.
}

-(void)viewWillAppear:(BOOL)animated
{
    [self.menuContainerViewController setPanMode:MFSideMenuPanModeDefault];
}
- (void)btnBackClicked
{
    [self.navigationController popViewControllerAnimated:YES];
}

#pragma mark BsKeyBoardContrrols delegate
-(void)addKeyboardControls
{
    // Initialize the keyboard controls
    keyboardControls = [[BSKeyboardControls alloc] init];
    
    // Set the delegate of the keyboard controls
    keyboardControls.delegate = self;
    
    // Add all text fields you want to be able to skip between to the keyboard controls
    // The order of thise text fields are important. The order is used when pressing "Previous" or "Next"
    
    keyboardControls.textFields = [NSArray arrayWithObjects:txtFieldVerificationCode,txtFieldPassword,txtFieldConfirmPassword,nil];
    
    
    // Set the style of the bar. Default is UIBarStyleBlackTranslucent.
    // keyboardControls.barStyle = UIBarStyleBlackTranslucent;
    
    // Set the tint color of the "Previous" and "Next" button. Default is black.
    keyboardControls.previousNextTintColor = [UIColor blackColor];
    
    // Set the tint color of the done button. Default is a color which looks a lot like the original blue color for a "Done" butotn
    keyboardControls.doneTintColor = [UIColor colorWithRed:34.0/255.0 green:164.0/255.0 blue:255.0/255.0 alpha:1.0];
    
    // Set title for the "Previous" button. Default is "Previous".
    keyboardControls.previousTitle = @"Previous";
    
    // Set title for the "Next button". Default is "Next".
    keyboardControls.nextTitle = @"Next";
    
    // Add the keyboard control as accessory view for all of the text fields
    // Also set the delegate of all the text fields to self
    for (id textField in keyboardControls.textFields)
    {
        if ([textField isKindOfClass:[UITextField class]])
        {
            ((UITextField *) textField).inputAccessoryView = keyboardControls;
            ((UITextField *) textField).delegate = self;
        }
        
    }
}

-(void)scrollViewToCenterOfScreen:(UIView *)theView
{
    CGFloat viewCenterY = theView.center.y;
    CGRect applicationFrame = [[UIScreen mainScreen] applicationFrame];
    CGFloat availableHeight = applicationFrame.size.height - 280; // Remove area covered by keyboard
    
    CGFloat y = viewCenterY - availableHeight / 2.0;
    if (y < 0) {
        y = 0;
    }
     [scrlView setContentOffset:CGPointMake(0, y) animated:YES];
    
}

- (void)keyboardControlsDonePressed:(BSKeyboardControls *)controls
{
    [controls.activeTextField resignFirstResponder];
    [scrlView setContentOffset:CGPointMake(0, 0) animated:YES];
}

- (void)keyboardControlsPreviousNextPressed:(BSKeyboardControls *)controls withDirection:(KeyboardControlsDirection)direction andActiveTextField:(id)textField
{
    [textField becomeFirstResponder];
    [self scrollViewToCenterOfScreen:textField];
}

#pragma mark UITextFieldDelegate
- (BOOL)textFieldShouldBeginEditing:(UITextField *)textField
{
    if ([keyboardControls.textFields containsObject:textField])
        keyboardControls.activeTextField = textField;
    
    [self scrollViewToCenterOfScreen:textField];
    return YES;
    
}

-(BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [textField resignFirstResponder];
    // [controls.activeTextField resignFirstResponder];
    [scrlView setContentOffset:CGPointMake(0, 0) animated:YES];
    return YES;
}

- (BOOL)passwordValidationTxtFields
{
    txtFieldPassword.text=[txtFieldPassword.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
    txtFieldConfirmPassword.text=[txtFieldConfirmPassword.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
    txtFieldVerificationCode.text=[txtFieldVerificationCode.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
    
    if(txtFieldVerificationCode.text == NULL || [txtFieldVerificationCode.text length] == 0)
    {
        [Utils showAlertMessage:KMessageTitle Message:@"Please enter verification code"];
        [txtFieldVerificationCode becomeFirstResponder];
        return false;
    }
    else if(![txtFieldVerificationCode.text isEqualToString:strVerificationCode])
    {
        [Utils showAlertMessage:KMessageTitle Message:@"Incorrect verification code"];
        [txtFieldVerificationCode becomeFirstResponder];
        return false;
    }
    else if(txtFieldPassword.text == NULL || [txtFieldPassword.text length] == 0)
    {
        [Utils showAlertMessage:KMessageTitle Message:@"Please enter password"];
        [txtFieldPassword becomeFirstResponder];
        return false;
    }
    else if(txtFieldPassword.text.length<6)
    {
        [Utils showAlertMessage:KMessageTitle Message:@"Please enter password at least 6 characters"];
        [txtFieldPassword becomeFirstResponder];
        return false;
    }
    else if(txtFieldPassword.text.length>15)
    {
        [Utils showAlertMessage:KMessageTitle Message:@"Please enter password 6 to 15 characters"];
        [txtFieldPassword becomeFirstResponder];
        return false;
    }
    if(txtFieldConfirmPassword.text == NULL || [txtFieldConfirmPassword.text length] == 0)
    {
        [Utils showAlertMessage:KMessageTitle Message:@"Please enter password confirm password"];
        [txtFieldConfirmPassword becomeFirstResponder];
        return false;
    }
    else if(txtFieldConfirmPassword.text.length<6)
    {
        [Utils showAlertMessage:KMessageTitle Message:@"Please enter confirm password at least 6 characters"];
        [txtFieldConfirmPassword becomeFirstResponder];
        return false;
    }
    else if(txtFieldConfirmPassword.text.length>15)
    {
        [Utils showAlertMessage:KMessageTitle Message:@"Please enter confirm password 6 to 15 characters"];
        [txtFieldConfirmPassword becomeFirstResponder];
        return false;
    }
    else if(![txtFieldPassword.text isEqualToString:txtFieldConfirmPassword.text])
    {
        [Utils showAlertMessage:KMessageTitle Message:@"Confirm password does not match"];
        [txtFieldConfirmPassword becomeFirstResponder];
        return false;
    }
    else
    {
        return true;
    }
}

-(IBAction)btnLoginIntoclicked:(id)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}

- (BOOL)registrationValidationTxtFields
{
    
    txtFieldVerificationCode.text = [txtFieldVerificationCode.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
    txtFieldPassword.text =[txtFieldPassword.text  stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
    txtFieldConfirmPassword.text=[txtFieldConfirmPassword.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
    
    
  
   
    if( [txtFieldPassword.text length] == 0)
    {
        [Utils showAlertMessage:KMessageTitle Message:@"Please enter password"];
        [txtFieldPassword becomeFirstResponder];
        return false;
    }
    else if(txtFieldPassword.text.length<6)
    {
        [Utils showAlertMessage:KMessageTitle Message:@"Please enter password at least 6 characters"];
        [txtFieldPassword becomeFirstResponder];
        return false;
    }
    else if(txtFieldConfirmPassword.text.length<6)
    {
        [Utils showAlertMessage:KMessageTitle Message:@"Please enter confirm password at least 6 characters"];
        [txtFieldConfirmPassword becomeFirstResponder];
        return false;
    }
    else if(txtFieldConfirmPassword.text.length>15)
    {
        [Utils showAlertMessage:KMessageTitle Message:@"Please enter confirm password 6 to 15 characters"];
        [txtFieldConfirmPassword becomeFirstResponder];
        return false;
    }
    else if(![txtFieldPassword.text isEqualToString:txtFieldConfirmPassword.text])
    {
        [Utils showAlertMessage:KMessageTitle Message:@"Confirm password does not match"];
        [txtFieldConfirmPassword becomeFirstResponder];
        return false;
    }
    else
    {
        return true;
    }
}

-(IBAction)btnSubmitClicked:(id)sender
{
    if ([self registrationValidationTxtFields])
    {
        if ([AppDelegate checkNetwork])
        {
//            strCheck = @"submit";
//            [MBProgressHUD showHUDAddedTo:[AppDelegate sharedDelegate].window animated:YES];
//
//            ASIFormDataRequest *request = [ASIFormDataRequest requestWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@%@",ServerAdd3,kForget_Password]]];
//            [request setDelegate:self];
//            [request setPostValue:strUserId forKey:@"userId"];
//            [request setPostValue:txtFieldPassword.text forKey:@"newPassword"];
//            [request setPostValue:[[NSUserDefaults standardUserDefaults] objectForKey:@"secureSignature"] forKey:@"secureSignature"];
//            [request startAsynchronous];
            
            NSDictionary *param =@{@"userId":strUserId,
                                   @"newPassword":txtFieldPassword.text,
                                   @"secureSignature": [[NSUserDefaults standardUserDefaults] objectForKey:@"secureSignature"]};
            
            [Helper showLoading];
            [WebServiceCalls POST:kForget_Password parameter:param completionBlock:^(id JSON, WebServiceResult result)
             {
                 @try
                 {
                     [Helper stopLoading];
                     
                     if (result == WebServiceResultSuccess)
                     {
                         NSDictionary *dictMain = JSON;
                         if ([dictMain[@"status"] integerValue] == 1)
                         {
                             self->txtFieldVerificationCode.text = @"";
                             self->txtFieldPassword.text = @"";
                             self->txtFieldConfirmPassword.text = @"";
                             
                             NSArray *viewControllers = [[self navigationController] viewControllers];
                             id obj;
                             for( int i=0;i<[viewControllers count];i++)
                             {
                                 obj=[viewControllers objectAtIndex:i];
                                 if([obj isKindOfClass:[LoginVC class]])
                                 {
                                     [[self navigationController] popToViewController:obj animated:YES];
                                     break;
                                 }
                             }
                             [self.navigationController popViewControllerAnimated:YES];
                         }
                     }
                 }
                 @catch (NSException *exception) { }
                 @finally {
                     [Helper stopLoading];
                 }
             }];
        }
    }
}

//- (void)requestFinished:(ASIHTTPRequest *)request
//{
//    [MBProgressHUD hideAllHUDsForView:[AppDelegate sharedDelegate].window animated:YES];
//    NSString *receivedString = [request responseString];
//    NSLog(@"str=%@",receivedString);
//    NSDictionary *dictMain=[receivedString JSONValue];
//
//    if ([strCheck isEqualToString:@"verification"])
//    {
//        if([[dictMain objectForKey:@"status"] boolValue])
//        {
//            strVerificationCode = [NSString stringWithFormat:@"%@",[dictMain objectForKey:@"code"]];
//
//            //[Utils showAlertMessage:KMessageTitle Message:[dictMain objectForKey:@"result"]];
//        }
//    }
//    else if ([strCheck isEqualToString:@"submit"])
//    {
//        if([[dictMain objectForKey:@"status"] boolValue])
//        {
//            txtFieldVerificationCode.text = @"";
//            txtFieldPassword.text = @"";
//            txtFieldConfirmPassword.text = @"";
//
//            NSArray *viewControllers = [[self navigationController] viewControllers];
//            id obj;
//            for( int i=0;i<[viewControllers count];i++)
//            {
//                obj=[viewControllers objectAtIndex:i];
//                if([obj isKindOfClass:[LoginVC class]])
//                {
//                    [[self navigationController] popToViewController:obj animated:YES];
//                    break;
//                }
//            }
//
//
//            [self.navigationController popViewControllerAnimated:YES];
//            //[Utils showAlertMessage:KMessageTitle Message:[dictMain objectForKey:@"result"]];
//        }
//    }
//}
//
//
//- (void)requestFailed:(ASIHTTPRequest *)request
//{
//    NSLog(@"Fail");
//    [MBProgressHUD hideAllHUDsForView:[AppDelegate sharedDelegate].window animated:YES];
//    // [Utils showAlertMessage:KMessageTitle Message:@"Network error"];
//}

-(IBAction)btnRegenerateCodeClicked:(id)sender
{
    if ([AppDelegate checkNetwork])
    {
//        strCheck = @"verification";
//        [MBProgressHUD showHUDAddedTo:[AppDelegate sharedDelegate].window animated:YES];
//        ASIFormDataRequest *request = [ASIFormDataRequest requestWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@%@",ServerAdd3,kForget_mob]]];
//        [request setDelegate:self];
//        [request setPostValue:strMobileNo forKey:@"mobNumber"];
//        [request setPostValue:[[NSUserDefaults standardUserDefaults] objectForKey:@"secureSignature"] forKey:@"secureSignature"];
//        [request startAsynchronous];
    
    NSDictionary *param =@{@"mobNumber":strMobileNo,
                           @"secureSignature": [[NSUserDefaults standardUserDefaults] objectForKey:@"secureSignature"]};
    
    [Helper showLoading];
    [WebServiceCalls POST:kForget_mob parameter:param completionBlock:^(id JSON, WebServiceResult result)
     {
         @try
         {
             [Helper stopLoading];
             
             if (result == WebServiceResultSuccess)
             {
                 NSDictionary *dictMain = JSON;
                 if ([dictMain[@"status"] integerValue] == 1)
                 {
                     self->strVerificationCode = [NSString stringWithFormat:@"%@",[dictMain objectForKey:@"code"]];
                 }
                 else if(![[dictMain objectForKey:@"status"] boolValue])
                 {
                     [Utils showAlertMessage:KMessageTitle Message:[dictMain objectForKey:@"result"]];
                 }
             }
         }
         @catch (NSException *exception) { }
         @finally {
             [Helper stopLoading];
         }
     }];
    }
}



- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)tapNext:(id)sender {
    
    
    if(txtFieldVerificationCode.text.length == 0)
    {
        [Utils showAlertMessage:KMessageTitle Message:@"Please enter verification code"];
        [txtFieldPassword becomeFirstResponder];
        return ;
    }
    else if ([strVerificationCode integerValue] != [txtFieldVerificationCode.text integerValue])
    {
        [Utils showAlertMessage:KMessageTitle Message:@"Please enter vaild verification code"];
        [txtFieldVerificationCode becomeFirstResponder];
        return;
    }
    
    
    lblTitle.text = @"Enter New Password";
    viewPass.hidden = false;
    viewPass.alpha = 1;
    viewPass.backgroundColor = [UIColor clearColor];
    txtFieldVerificationCode.hidden = true;
    viewReg.hidden = true;
    btnNext.hidden = true;
    [self.view endEditing:true];
    [txtFieldPassword becomeFirstResponder];
    
}


@end
