//
//  LoginVC.m
//  ToolsOnWheel
//
//  Created by Kapil Goyal on 12/12/17.
//  Copyright © 2017 Kapil Goyal. All rights reserved.
//

#import "RegisterVC.h"
#import "VerificationVC.h"
#import "CommonHeader.h"
#import "CustomNavigation.h"
#import "MFSideMenu.h"
#import "Config.h"
#import "VerificationVC.h"

#import "ASIFormDataRequest.h"
#import "JSON.h"
#import "MBProgressHUD.h"
#import "AppDelegate.h"
#import "HomeVC.h"
#import "Utils.h"
#import "DHValidation.h"

@interface RegisterVC ()

@end

@implementation RegisterVC

- (void)viewDidLoad {
    
    
    [super viewDidLoad];
    [CustomNavigation addTarget:self backRequired:NO title:@""];
    for (UIView *v1 in self.view.subviews)
    {
        if ([v1 isKindOfClass:[CommonHeader class]])
        {
            for (UIView *viewTmp in v1.subviews)
            {
                if ([viewTmp isKindOfClass:[UIButton class]])
                {
                    if (viewTmp.tag==1)
                    {
                        viewTmp.hidden=YES;
                    }
                    else if(viewTmp.tag==106)
                    {
                        [(UIButton*)viewTmp addTarget:self action:@selector(btnBackClicked) forControlEvents:UIControlEventTouchUpInside];
                    }
                    else if(viewTmp.tag==102)
                    {
                        [(UIButton*)viewTmp setHidden:YES];
                    }
                }
                else if ([viewTmp isKindOfClass:[UIImageView class]])
                {
                    if (viewTmp.tag==101)
                    {
                        [(UIImageView*)viewTmp setImage:[UIImage imageNamed:@"back.png"]];
                    }
                }
            }
        }
    }
    
    lblHeader.text = @"Please fill up the details below to get registered";
    txtFieldEmail.borderStyle = UITextBorderStyleRoundedRect;
    txtFieldEmail.layer.cornerRadius = 14.0;
    [txtFieldEmail setBackgroundColor:[UIColor clearColor]];
    [txtFieldEmail.layer setBorderColor:[UIColor whiteColor].CGColor];
    [txtFieldEmail.layer setBorderWidth:2.0];

    
    UIColor *color = [UIColor whiteColor];
    txtFieldEmail.attributedPlaceholder = [[NSAttributedString alloc] initWithString:@"Email" attributes:@{NSForegroundColorAttributeName: color}];
    
    txtFieldPassword.borderStyle = UITextBorderStyleRoundedRect;
    txtFieldPassword.layer.cornerRadius = 14.0;
    [txtFieldPassword setBackgroundColor:[UIColor clearColor]];
    [txtFieldPassword.layer setBorderColor:[UIColor whiteColor].CGColor];
    [txtFieldPassword.layer setBorderWidth:2.0];
    txtFieldPassword.attributedPlaceholder = [[NSAttributedString alloc] initWithString:@"Password" attributes:@{NSForegroundColorAttributeName: color}];

    txtFieldMobileNumber.borderStyle = UITextBorderStyleRoundedRect;
    txtFieldMobileNumber.layer.cornerRadius = 14.0;
    [txtFieldMobileNumber setBackgroundColor:[UIColor clearColor]];
    [txtFieldMobileNumber.layer setBorderColor:[UIColor whiteColor].CGColor];
    [txtFieldMobileNumber.layer setBorderWidth:2.0];
    
    
    txtFieldMobileNumber.attributedPlaceholder = [[NSAttributedString alloc] initWithString:@"Mobile number" attributes:@{NSForegroundColorAttributeName: color}];

    
    txtFieldFullName.borderStyle = UITextBorderStyleRoundedRect;
    txtFieldFullName.layer.cornerRadius = 14.0;
    [txtFieldFullName setBackgroundColor:[UIColor clearColor]];
    [txtFieldFullName.layer setBorderColor:[UIColor whiteColor].CGColor];
    [txtFieldFullName.layer setBorderWidth:2.0];
    
    
    txtFieldFullName.attributedPlaceholder = [[NSAttributedString alloc] initWithString:@"Full name" attributes:@{NSForegroundColorAttributeName: color}];
    txtFieldFullName.delegate = self;
    
    txtFieldReferenceCode.borderStyle = UITextBorderStyleRoundedRect;
    txtFieldReferenceCode.layer.cornerRadius = 14.0;
    [txtFieldReferenceCode setBackgroundColor:[UIColor clearColor]];
    [txtFieldReferenceCode.layer setBorderColor:[UIColor whiteColor].CGColor];
    [txtFieldReferenceCode.layer setBorderWidth:2.0];
    
    
    txtFieldReferenceCode.attributedPlaceholder = [[NSAttributedString alloc] initWithString:@"Reference code (Optional)" attributes:@{NSForegroundColorAttributeName: color}];

    
    btnSignup.layer.cornerRadius = 22; // this value vary as per your desire
    btnSignup.clipsToBounds = YES;
    // Do any additional setup after loading the view from its nib.
    [self addKeyboardControls];
}

-(void)viewWillAppear:(BOOL)animated
{
    [self.menuContainerViewController setPanMode:MFSideMenuPanModeNone];
}

- (void)btnBackClicked
{
    [self.navigationController popViewControllerAnimated:YES];
}

#pragma mark BsKeyBoardContrrols delegate
-(void)addKeyboardControls
{
    // Initialize the keyboard controls
    keyboardControls = [[BSKeyboardControls alloc] init];
    
    // Set the delegate of the keyboard controls
    keyboardControls.delegate = self;
    
    // Add all text fields you want to be able to skip between to the keyboard controls
    // The order of thise text fields are important. The order is used when pressing "Previous" or "Next"
    
    keyboardControls.textFields = [NSArray arrayWithObjects:txtFieldMobileNumber,txtFieldFullName,txtFieldEmail,txtFieldPassword,txtFieldReferenceCode,nil];
    
    
    // Set the style of the bar. Default is UIBarStyleBlackTranslucent.
    // keyboardControls.barStyle = UIBarStyleBlackTranslucent;
    
    // Set the tint color of the "Previous" and "Next" button. Default is black.
    keyboardControls.previousNextTintColor = [UIColor blackColor];
    
    // Set the tint color of the done button. Default is a color which looks a lot like the original blue color for a "Done" butotn
    keyboardControls.doneTintColor = [UIColor colorWithRed:34.0/255.0 green:164.0/255.0 blue:255.0/255.0 alpha:1.0];
    
    // Set title for the "Previous" button. Default is "Previous".
    keyboardControls.previousTitle = @"Previous";
    
    // Set title for the "Next button". Default is "Next".
    keyboardControls.nextTitle = @"Next";
    
    // Add the keyboard control as accessory view for all of the text fields
    // Also set the delegate of all the text fields to self
    for (id textField in keyboardControls.textFields)
    {
        if ([textField isKindOfClass:[UITextField class]])
        {
            ((UITextField *) textField).inputAccessoryView = keyboardControls;
            ((UITextField *) textField).delegate = self;
        }
     
    }
}

-(void)scrollViewToCenterOfScreen:(UIView *)theView
{
    CGFloat viewCenterY = theView.center.y;
    CGRect applicationFrame = [[UIScreen mainScreen] applicationFrame];
    CGFloat availableHeight = applicationFrame.size.height - 240; // Remove area covered by keyboard
    
    CGFloat y = viewCenterY - availableHeight / 2.0;
    if (y < 0) {
        y = 0;
    }
    [scrlView setContentOffset:CGPointMake(0, y) animated:YES];
    
}

- (void)keyboardControlsDonePressed:(BSKeyboardControls *)controls
{
    [controls.activeTextField resignFirstResponder];
    [scrlView setContentOffset:CGPointMake(0, 0) animated:YES];
}

- (void)keyboardControlsPreviousNextPressed:(BSKeyboardControls *)controls withDirection:(KeyboardControlsDirection)direction andActiveTextField:(id)textField
{
    [textField becomeFirstResponder];
    [self scrollViewToCenterOfScreen:textField];
}

#pragma mark UITextField Delegate
- (BOOL)textFieldShouldBeginEditing:(UITextField *)textField
{
    if ([keyboardControls.textFields containsObject:textField])
        keyboardControls.activeTextField = textField;
    
    [self scrollViewToCenterOfScreen:textField];
    return YES;
    
}

#pragma mark - UITextField Delegates
-(BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [textField resignFirstResponder];
    // [controls.activeTextField resignFirstResponder];
    [scrlView setContentOffset:CGPointMake(0, 0) animated:YES];
    return YES;
}

- (BOOL)validateStringWithAlphabet:(NSString *)string {
    NSString *stringRegex = @"[A-Za-z]";
    NSPredicate *stringPredicate = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", stringRegex];
    
    return [stringPredicate evaluateWithObject:string];
}

- (BOOL)registrationValidationTxtFields
{
    txtFieldMobileNumber.text = [txtFieldMobileNumber.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
    txtFieldFullName.text =[txtFieldFullName.text  stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
    txtFieldEmail.text=[txtFieldEmail.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
    txtFieldPassword.text=[txtFieldPassword.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
    
    
    DHValidation *aValidation = [[DHValidation alloc]init];
    NSString *mobileNumberPattern = @"[6789][0-9]{9}";
    NSPredicate *mobileNumberPred = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", mobileNumberPattern];
    
    BOOL matched = [mobileNumberPred evaluateWithObject:txtFieldMobileNumber.text];
    
    BOOL flag = [aValidation validateEmail:txtFieldEmail.text];
    
    if(txtFieldMobileNumber.text == NULL || [txtFieldMobileNumber.text length] == 0)
    {
        [Utils showAlertMessage:KMessageTitle Message:@"Please enter mobile number"];
        [txtFieldMobileNumber becomeFirstResponder];
        return false ;
    }
    else if (txtFieldMobileNumber.text.length<10 || txtFieldMobileNumber.text.length>10)
    {
        [Utils showAlertMessage:KMessageTitle Message:@"Please enter mobile number in 10 digits"];
        [txtFieldMobileNumber becomeFirstResponder];
        return false;
    }
    else if (!matched)
    {
        [Utils showAlertMessage:KMessageTitle Message:@"Please enter valid number"];
        [txtFieldMobileNumber becomeFirstResponder];
        return false ;
    }
    else if(txtFieldFullName.text == NULL || [txtFieldFullName.text length] == 0)
    {
        [Utils showAlertMessage:KMessageTitle Message:@"Please enter full name"];
        [txtFieldFullName becomeFirstResponder];
        return false;
    }
    
    
//    else if(txtFieldEmail.text == NULL || [txtFieldEmail.text length] == 0)
//    {
//        [Utils showAlertMessage:KMessageTitle Message:@"Please enter email address"];
//        [txtFieldEmail becomeFirstResponder];
//        return false;
//    }
    else if ([txtFieldEmail.text length]>0  && !flag)
    {
        [Utils showAlertMessage:KMessageTitle Message:@"Please enter valid email address"];
        [txtFieldEmail becomeFirstResponder];
        return false;
    }
    else if(txtFieldPassword.text == NULL || [txtFieldPassword.text length] == 0)
    {
        [Utils showAlertMessage:KMessageTitle Message:@"Please enter password"];
        [txtFieldPassword becomeFirstResponder];
        return false;
    }
    else if(txtFieldPassword.text.length<6)
    {
        [Utils showAlertMessage:KMessageTitle Message:@"Please enter password at least 6 characters"];
        [txtFieldPassword becomeFirstResponder];
        return false;
    }
    else
    {
        return true;
    }
}

- (BOOL) textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range
 replacementString:(NSString *)string
{
    NSString *currentString = [textField.text stringByReplacingCharactersInRange:range withString:string];

    if (textField == txtFieldMobileNumber)
    {
        NSString *resultText = [txtFieldMobileNumber.text stringByReplacingCharactersInRange:range
                                                                                  withString:string];
        return resultText.length <= 10;
    }
    else  if (textField == txtFieldFullName) {
        
        NSCharacterSet *invalidCharSet = [[NSCharacterSet characterSetWithCharactersInString:@"ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz "] invertedSet];
        NSString *filtered = [[string componentsSeparatedByCharactersInSet:invalidCharSet] componentsJoinedByString:@""];
        return [string isEqualToString:filtered];
       // return [self  validateStringWithAlphabet:currentString];
    }
    else
    {
        return YES;
    }
    return YES;
}


-(IBAction)btnForgetPasswordClicked:(id)sender
{
    
}

-(IBAction)btnSignUpclicked:(id)sender;
{
    if ([self registrationValidationTxtFields])
    {
        if ([AppDelegate checkNetwork])
        {
//            strCheck = @"deviceid";
//            [MBProgressHUD showHUDAddedTo:[AppDelegate sharedDelegate].window animated:YES];
//            ASIFormDataRequest *request = [ASIFormDataRequest requestWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@%@",ServerAdd3,kCheckDeviceID]]];
//            [request setDelegate:self];
//            [request setPostValue:[[NSUserDefaults standardUserDefaults] objectForKey:@"deviceid"] forKey:@"deviceId"];
//            [request setPostValue:[[NSUserDefaults standardUserDefaults] objectForKey:@"secureSignature"] forKey:@"secureSignature"];
//            [request startAsynchronous];
            
            NSDictionary *param =@{@"deviceId":[[NSUserDefaults standardUserDefaults] objectForKey:@"deviceid"],
                                   @"secureSignature":[[NSUserDefaults standardUserDefaults] objectForKey:@"secureSignature"]};
            
            [Helper showLoading];
            [WebServiceCalls POST:kCheckDeviceID parameter:param completionBlock:^(id JSON, WebServiceResult result)
             {
                 @try
                 {
                     [Helper stopLoading];
                     
                     if (result == WebServiceResultSuccess)
                     {
                         NSDictionary *dictMain = JSON;
                         if ([dictMain[@"status"] integerValue] == 1)
                         {
                             [self signUpHud_withDict:dictMain];
                         }
                         else if(![[dictMain objectForKey:@"status"] boolValue])
                         {
                             [Helper stopLoading];
                         }
                     }
                 }
                 @catch (NSException *exception) { }
                 @finally {
                     [Helper stopLoading];
                 }
             }];
        }
    }
 }

-(void) signUpHud_withDict:(NSDictionary *)dict
{
    [[NSUserDefaults standardUserDefaults] setObject:txtFieldPassword.text forKey:@"password"];
    
    NSDictionary *param =@{@"mob":txtFieldMobileNumber.text,
                           @"name":txtFieldFullName.text,
                           @"email":txtFieldEmail.text,
                           @"password":txtFieldPassword.text,
                           @"refcode":txtFieldReferenceCode.text,
                           @"deviceCheck":[dict objectForKey:@"result"],
                           @"usertype":@"ios",
                           @"deviceId":[[NSUserDefaults standardUserDefaults] objectForKey:@"deviceid"],
                           @"secureSignature":[[NSUserDefaults standardUserDefaults] objectForKey:@"secureSignature"]};
    
    [Helper showLoading];
    [WebServiceCalls POST:kSignUp parameter:param completionBlock:^(id JSON, WebServiceResult result)
     {
         @try
         {
             [Helper stopLoading];
             
             if (result == WebServiceResultSuccess)
             {
                 NSDictionary *dictMain = JSON;
                 if ([dictMain[@"status"] integerValue] == 1)
                 {
                     VerificationVC *objVeri = [[VerificationVC alloc] init];
                     objVeri.strUserId = [dictMain objectForKey:@"userId"];
                     objVeri.strName = self->txtFieldFullName.text;
                     objVeri.strEmail = self->txtFieldEmail.text;
                     objVeri.strMobileNo = self->txtFieldMobileNumber.text;
                     objVeri.strReferenceCode = self->txtFieldReferenceCode.text;
                     objVeri.strVerificationCode = self->txtFieldReferenceCode.text;
                     [self.navigationController pushViewController:objVeri animated:YES];
                     //  [Utils showAlertMessage:KMessageTitle Message:[dictMain objectForKey:@"result"]];
                 }
                 else
                 {
                     [Utils showAlertMessage:KMessageTitle Message:[dictMain objectForKey:@"result"]];
                 }
             }
         }
         @catch (NSException *exception) { }
         @finally {
             [Helper stopLoading];
         }
     }];
}

//#pragma mark Request Finished
//- (void)requestFinished:(ASIHTTPRequest *)request
//{
//    [MBProgressHUD hideAllHUDsForView:[AppDelegate sharedDelegate].window animated:YES];
//    NSString *receivedString = [request responseString];
//    NSLog(@"str=%@",receivedString);
//    NSDictionary *dictMain=[receivedString JSONValue];
//        if ([strCheck isEqualToString:@"deviceid"])
//        {
//            if([[dictMain objectForKey:@"status"] boolValue])
//            {
//                if ([AppDelegate checkNetwork])
//                {
//                    strCheck = @"signup";
//                    [MBProgressHUD showHUDAddedTo:[AppDelegate sharedDelegate].window animated:YES];
//                    ASIFormDataRequest *request = [ASIFormDataRequest requestWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@%@",ServerAdd3,kSignUp]]];
//                    [request setDelegate:self];
//                    [request setPostValue:txtFieldMobileNumber.text forKey:@"mob"];
//                    [request setPostValue:txtFieldFullName.text forKey:@"name"];
//                    [request setPostValue:txtFieldEmail.text forKey:@"email"];
//                    [request setPostValue:txtFieldPassword.text forKey:@"password"];
//                    [[NSUserDefaults standardUserDefaults] setObject:txtFieldPassword.text forKey:@"password"];
//                    [request setPostValue:txtFieldReferenceCode.text forKey:@"refcode"];
//                    [request setPostValue:[dictMain objectForKey:@"result"] forKey:@"deviceCheck"];
//                    [request setPostValue:@"ios" forKey:@"usertype"];
//
//                    [request setPostValue:[[NSUserDefaults standardUserDefaults] objectForKey:@"deviceid"] forKey:@"deviceId"];
//                    [request setPostValue:[[NSUserDefaults standardUserDefaults] objectForKey:@"secureSignature"] forKey:@"secureSignature"];
//                    [request startAsynchronous];
//                }
//
//            }
//        }
//    else if ([strCheck isEqualToString:@"signup"])
//    {
//        if([[dictMain objectForKey:@"status"] boolValue])
//        {
//            VerificationVC *objVeri = [[VerificationVC alloc] init];
//            objVeri.strUserId = [dictMain objectForKey:@"userId"];
//            objVeri.strName = txtFieldFullName.text;
//            objVeri.strEmail = txtFieldEmail.text;
//            objVeri.strMobileNo = txtFieldMobileNumber.text;
//            objVeri.strReferenceCode = txtFieldReferenceCode.text;
//            objVeri.strVerificationCode = strVerificationCode;
//            [self.navigationController pushViewController:objVeri animated:YES];
//          //  [Utils showAlertMessage:KMessageTitle Message:[dictMain objectForKey:@"result"]];
//        }
//        else
//        {
//            [Utils showAlertMessage:KMessageTitle Message:[dictMain objectForKey:@"result"]];
//        }
//    }
//}
//
//- (void)requestFailed:(ASIHTTPRequest *)request
//{
//    NSLog(@"Fail");
//    [MBProgressHUD hideAllHUDsForView:[AppDelegate sharedDelegate].window animated:YES];
//    // [Utils showAlertMessage:KMessageTitle Message:@"Network error"];
//}


-(IBAction)btnSigninclicked:(id)sender;
{
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
@end
