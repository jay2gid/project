//
//  LoginVC.h
//  ToolsOnWheel
//
//  Created by Kapil Goyal on 12/12/17.
//  Copyright © 2017 Kapil Goyal. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BSKeyboardControls.h"

@interface RegisterVC : UIViewController<UITextFieldDelegate,BSKeyboardControlsDelegate>
{
    IBOutlet UITextField *txtFieldEmail;
    IBOutlet UITextField *txtFieldPassword;
    IBOutlet UITextField *txtFieldMobileNumber;
    IBOutlet UITextField *txtFieldFullName;
    IBOutlet UITextField *txtFieldReferenceCode;
    BSKeyboardControls *keyboardControls;
    IBOutlet UIScrollView *scrlView;
    IBOutlet UIButton *btnSignup;
    NSString *strCheck;
    NSString *strVerificationCode;
    IBOutlet UILabel *lblHeader;
}
-(IBAction)btnSigninclicked:(id)sender;
-(IBAction)btnSignUpclicked:(id)sender;

@end
