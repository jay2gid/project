//
//  LoginVC.m
//  ToolsOnWheel
//
//  Created by Kapil Goyal on 12/12/17.
//  Copyright © 2017 Kapil Goyal. All rights reserved.
//

#import "LoginVC.h"
#import "RegisterVC.h"
#import "ASIFormDataRequest.h"
#import "JSON.h"
#import "MBProgressHUD.h"
#import "AppDelegate.h"
#import "HomeVC.h"
#import "Utils.h"
#import "Config.h"
#import "ForgetMobileVC.h"

#import "CommonHeader.h"
#import "CustomNavigation.h"
#import "MFSideMenu.h"


@interface LoginVC ()

@end

@implementation LoginVC
{
    NSString *imageUrl;
}
- (void)viewDidLoad
{
    [super viewDidLoad];
    
    [CustomNavigation addTarget:self backRequired:NO title:@""];
    for (UIView *v1 in self.view.subviews)
    {
        if ([v1 isKindOfClass:[CommonHeader class]])
        {
            for (UIView *viewTmp in v1.subviews)
            {
                if ([viewTmp isKindOfClass:[UIButton class]])
                {
                    if (viewTmp.tag==1)
                    {
                        viewTmp.hidden=YES;
                    }
                    else if(viewTmp.tag==106)
                    {
                        [(UIButton*)viewTmp addTarget:self action:@selector(revealLeftSidebar:) forControlEvents:UIControlEventTouchUpInside];
                    }
                    else if(viewTmp.tag==102)
                    {
                        // [(UIButton*)viewTmp addTarget:self action:@selector(moveToCart) forControlEvents:UIControlEventTouchUpInside];
                        [(UIButton*)viewTmp setHidden:YES];

                    }
                }
                else if ([viewTmp isKindOfClass:[UIImageView class]])
                {
                    if (viewTmp.tag==101)
                    {
                        [(UIImageView*)viewTmp setImage:[UIImage imageNamed:@"back.png"]];
                    }
                }
            }
        }
    }
    
    txtFieldMobile.borderStyle = UITextBorderStyleRoundedRect;
    txtFieldMobile.layer.cornerRadius = 14.0;
    [txtFieldMobile setBackgroundColor:[UIColor clearColor]];
    [txtFieldMobile.layer setBorderColor:[UIColor whiteColor].CGColor];
    [txtFieldMobile.layer setBorderWidth:2.0];

    
    UIColor *color = [UIColor whiteColor];
    txtFieldMobile.attributedPlaceholder = [[NSAttributedString alloc] initWithString:@"Mobile number" attributes:@{NSForegroundColorAttributeName: color}];
    
    txtFieldPassword.borderStyle = UITextBorderStyleRoundedRect;
    txtFieldPassword.layer.cornerRadius = 14.0;
    [txtFieldPassword setBackgroundColor:[UIColor clearColor]];
    [txtFieldPassword.layer setBorderColor:[UIColor whiteColor].CGColor];
    [txtFieldPassword.layer setBorderWidth:2.0];
    txtFieldPassword.attributedPlaceholder = [[NSAttributedString alloc] initWithString:@"Password" attributes:@{NSForegroundColorAttributeName: color}];

    
    btnSignIn.layer.cornerRadius = 22; // this value vary as per your desire
    btnSignIn.clipsToBounds = YES;
    [self addKeyboardControls];


    // Do any additional setup after loading the view from its nib.
}

-(void)viewWillAppear:(BOOL)animated
{
    [self.menuContainerViewController setPanMode:MFSideMenuPanModeDefault];
}

#pragma mark iPhone
#pragma mark MFSidebarDelegate
- (void)leftSideMenuButtonPressed:(id)sender
{
    [self.menuContainerViewController toggleLeftSideMenuCompletion:^{
    }];
}

- (void)revealLeftSidebar:(id)sender
{
    NSArray *viewControllers = [[self navigationController] viewControllers];
    for( int i=0;i<[viewControllers count];i++){
        id obj=[viewControllers objectAtIndex:i];
        if([obj isKindOfClass:[HomeVC class]]){
            [[self navigationController] popToViewController:obj animated:YES];
        }
    }
    
}


- (void)btnBackClicked
{
    [self.navigationController popViewControllerAnimated:YES];
}

#pragma mark BsKeyBoardContrrols delegate
-(void)addKeyboardControls
{
    // Initialize the keyboard controls
    keyboardControls = [[BSKeyboardControls alloc] init];
    
    // Set the delegate of the keyboard controls
    keyboardControls.delegate = self;
    
    // Add all text fields you want to be able to skip between to the keyboard controls
    // The order of thise text fields are important. The order is used when pressing "Previous" or "Next"
    
    keyboardControls.textFields = [NSArray arrayWithObjects:txtFieldMobile,txtFieldPassword,nil];
    
    
    // Set the style of the bar. Default is UIBarStyleBlackTranslucent.
    // keyboardControls.barStyle = UIBarStyleBlackTranslucent;
    
    // Set the tint color of the "Previous" and "Next" button. Default is black.
    keyboardControls.previousNextTintColor = [UIColor blackColor];
    
    // Set the tint color of the done button. Default is a color which looks a lot like the original blue color for a "Done" butotn
    keyboardControls.doneTintColor = [UIColor colorWithRed:34.0/255.0 green:164.0/255.0 blue:255.0/255.0 alpha:1.0];
    
    // Set title for the "Previous" button. Default is "Previous".
    keyboardControls.previousTitle = @"Previous";
    
    // Set title for the "Next button". Default is "Next".
    keyboardControls.nextTitle = @"Next";
    
    // Add the keyboard control as accessory view for all of the text fields
    // Also set the delegate of all the text fields to self
    for (id textField in keyboardControls.textFields)
    {
        if ([textField isKindOfClass:[UITextField class]])
        {
            ((UITextField *) textField).inputAccessoryView = keyboardControls;
            ((UITextField *) textField).delegate = self;
        }
        
    }
}

-(void)scrollViewToCenterOfScreen:(UIView *)theView
{
    CGFloat viewCenterY = theView.center.y;
    CGRect applicationFrame = [[UIScreen mainScreen] applicationFrame];
    CGFloat availableHeight = applicationFrame.size.height - 280; // Remove area covered by keyboard
    
    CGFloat y = viewCenterY - availableHeight / 2.0;
    if (y < 0) {
        y = 0;
    }
    [scrlView setContentOffset:CGPointMake(0, y) animated:YES];
    
}

- (void)keyboardControlsDonePressed:(BSKeyboardControls *)controls
{
    [controls.activeTextField resignFirstResponder];
    [scrlView setContentOffset:CGPointMake(0, 0) animated:YES];
}

- (void)keyboardControlsPreviousNextPressed:(BSKeyboardControls *)controls withDirection:(KeyboardControlsDirection)direction andActiveTextField:(id)textField
{
    [textField becomeFirstResponder];
    [self scrollViewToCenterOfScreen:textField];
}

#pragma mark UITextFieldDelegate
- (BOOL)textFieldShouldBeginEditing:(UITextField *)textField
{
    if ([keyboardControls.textFields containsObject:textField])
        keyboardControls.activeTextField = textField;
    
    [self scrollViewToCenterOfScreen:textField];
    return YES;
    
}

-(BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [textField resignFirstResponder];
    // [controls.activeTextField resignFirstResponder];
    [scrlView setContentOffset:CGPointMake(0, 0) animated:YES];
    return YES;
}

-(IBAction)btnForgetPasswordClicked:(id)sender
{
    ForgetMobileVC *objForget = [[ForgetMobileVC alloc] init];
    [self.navigationController pushViewController:objForget animated:YES];
}

-(IBAction)btnSignUpclicked:(id)sender
{
    RegisterVC *objReg = [[RegisterVC alloc] init];
    [self.navigationController pushViewController:objReg animated:YES];
}

- (BOOL)loginValidationTxtFields
{
    txtFieldMobile.text = [txtFieldMobile.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
    txtFieldPassword.text=[txtFieldPassword.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
    
    NSString *mobileNumberPattern = @"[6789][0-9]{9}";
    NSPredicate *mobileNumberPred = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", mobileNumberPattern];
    
    BOOL matched = [mobileNumberPred evaluateWithObject:txtFieldMobile.text];
    
    
    if(txtFieldMobile.text == NULL || [txtFieldMobile.text length] == 0)
    {
        [Utils showAlertMessage:KMessageTitle Message:@"Please enter mobile number"];
        [txtFieldMobile becomeFirstResponder];
        return false ;
    }
    else if (txtFieldMobile.text.length<10 || txtFieldMobile.text.length>10)
    {
        [Utils showAlertMessage:KMessageTitle Message:@"Please enter mobile number in 10 digits"];
        [txtFieldMobile becomeFirstResponder];
        return false;
    }
    
    else if (!matched)
    {
        [Utils showAlertMessage:KMessageTitle Message:@"Please enter valid number"];
        [txtFieldMobile becomeFirstResponder];
        return false ;
    }
    else if(txtFieldPassword.text == NULL || [txtFieldPassword.text length] == 0)
    {
        [Utils showAlertMessage:KMessageTitle Message:@"Please enter password"];
        [txtFieldPassword becomeFirstResponder];
        return false;
    }
    else if(txtFieldPassword.text.length<6)
    {
        [Utils showAlertMessage:KMessageTitle Message:@"Please enter password at least 6 characters"];
        [txtFieldPassword becomeFirstResponder];
        return false;
    }
    else
    {
        return true;
    }
}

- (BOOL) textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range
 replacementString:(NSString *)string
{
    if (textField == txtFieldMobile)
    {
        NSString *resultText = [txtFieldMobile.text stringByReplacingCharactersInRange:range
                                                                            withString:string];
        return resultText.length <= 10;
    }
    else
    {
        return YES;
    }
    
}

-(IBAction)btnSigninclicked:(id)sender
{
    if ([self loginValidationTxtFields])
    {
        if ([AppDelegate checkNetwork])
        {
//            [MBProgressHUD showHUDAddedTo:[AppDelegate sharedDelegate].window animated:YES];
//            ASIFormDataRequest *request = [ASIFormDataRequest requestWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@%@",ServerAdd3,kLogin_user]]];
//            [request setDelegate:self];
//            [request setPostValue:txtFieldMobile.text forKey:@"userMob_login"];
//            [request setPostValue:txtFieldPassword.text forKey:@"userPassword_login"];
//            [request setPostValue:[[NSUserDefaults standardUserDefaults] objectForKey:@"secureSignature"] forKey:@"secureSignature"];
//            [request startAsynchronous];
            
            NSDictionary *param =@{@"userMob_login":txtFieldMobile.text,
                                   @"userPassword_login":txtFieldPassword.text,
                                   @"secureSignature":[[NSUserDefaults standardUserDefaults] objectForKey:@"secureSignature"]};
            
            [Helper showLoading];
            [WebServiceCalls POST:kLogin_user parameter:param completionBlock:^(id JSON, WebServiceResult result)
             {
                 @try
                 {
                     [Helper stopLoading];
                     
                     if (result == WebServiceResultSuccess)
                     {
                         NSDictionary *dictMain = JSON;
                         if ([dictMain[@"status"] integerValue] == 1)
                         {
                             [self.navigationController popViewControllerAnimated:YES];
                             [[NSUserDefaults standardUserDefaults] setObject:[dictMain objectForKey:@"userId"] forKey:@"userid"];
                             [[NSUserDefaults standardUserDefaults] setObject:[dictMain objectForKey:@"email"] forKey:@"email"];
                             [[NSUserDefaults standardUserDefaults] setObject:[dictMain objectForKey:@"phone"] forKey:@"phone"];
                             [[NSUserDefaults standardUserDefaults] setObject:[dictMain objectForKey:@"name"] forKey:@"name"];
                             [[NSUserDefaults standardUserDefaults] setObject:[dictMain objectForKey:@"refCode"] forKey:@"refcode"];
                             [[NSUserDefaults standardUserDefaults] setObject:self->txtFieldPassword.text forKey:@"password"];
                             
                             [Helper saveUserImage:[Helper getString:dictMain[@"img"]]];
                             
                             
                             [[NSUserDefaults standardUserDefaults] setObject:[dictMain objectForKey:@"ratingStatus"] forKey:@"ratingStatus"];
                             
                             NSArray *arr = [[dictMain objectForKey:@"address"] componentsSeparatedByString:@"/n/nLandmark:"];
                             if ([arr count]>0)
                             {
                                 for (int i=0; i<[arr count]; i++)
                                 {
                                     if(i==0)
                                         [[NSUserDefaults standardUserDefaults] setObject:[arr objectAtIndex:i] forKey:@"address"];
                                     else
                                         [[NSUserDefaults standardUserDefaults] setObject:[arr objectAtIndex:i] forKey:@"areaName"];
                                 }
                             }
                             // [[NSUserDefaults standardUserDefaults] setObject:[dictMain objectForKey:@"areaName"] forKey:@"areaName"];
                             [[NSUserDefaults standardUserDefaults] synchronize];
                             
                             
                             
                             //        UINavigationController *navi = [AppDelegate sharedDelegate].container.centerViewController;
                             //        NSArray *viewControllers = [navi viewControllers];
                             //        for( int i=0;i<[viewControllers count];i++)
                             //        {
                             //            id obj=[viewControllers objectAtIndex:i];
                             //            if([obj isKindOfClass:[HomeVC class]])
                             //            {
                             //                [[self navigationController] popToViewController:obj animated:YES];
                             //                break;
                             //            }
                             //        }
                             
                             NSMutableArray *viewControllers = [[[self navigationController] viewControllers] mutableCopy];
                             if ([viewControllers count]==2)
                             {
                                 [viewControllers removeObjectAtIndex:0];
                             }
                             for( int i=0;i<[viewControllers count];i++)
                             {
                                 id obj=[viewControllers objectAtIndex:i];
                                 if([obj isKindOfClass:[HomeVC class]])
                                 {
                                     [[self navigationController] popToViewController:obj animated:YES];
                                     break;
                                 }
                             }
                             
                             [[NSNotificationCenter defaultCenter] postNotificationName:@"login_logout" object:nil];
                         }
                         else if(![[dictMain objectForKey:@"status"] boolValue])
                         {
                             [Utils showAlertMessage:KMessageTitle Message:[dictMain objectForKey:@"result"]];
                         }
                     }
                 }
                 @catch (NSException *exception) { }
                 @finally {
                     [Helper stopLoading];
                 }
             }];
        }
    }
}

//#pragma mark Request Finished
//
//- (void)requestFinished:(ASIHTTPRequest *)request
//{
//    [MBProgressHUD hideAllHUDsForView:[AppDelegate sharedDelegate].window animated:YES];
//    NSString *receivedString = [request responseString];
//    NSLog(@"str=%@",receivedString);
//    NSDictionary *dictMain=[receivedString JSONValue];
//    if([[dictMain objectForKey:@"status"] boolValue])
//    {
//        [self.navigationController popViewControllerAnimated:YES];
//        [[NSUserDefaults standardUserDefaults] setObject:[dictMain objectForKey:@"userId"] forKey:@"userid"];
//        [[NSUserDefaults standardUserDefaults] setObject:[dictMain objectForKey:@"email"] forKey:@"email"];
//        [[NSUserDefaults standardUserDefaults] setObject:[dictMain objectForKey:@"phone"] forKey:@"phone"];
//        [[NSUserDefaults standardUserDefaults] setObject:[dictMain objectForKey:@"name"] forKey:@"name"];
//        [[NSUserDefaults standardUserDefaults] setObject:[dictMain objectForKey:@"refCode"] forKey:@"refcode"];
//        [[NSUserDefaults standardUserDefaults] setObject:txtFieldPassword.text forKey:@"password"];
//
//        [Helper saveUserImage:[Helper getString:dictMain[@"img"]]];
//
//
//        [[NSUserDefaults standardUserDefaults] setObject:[dictMain objectForKey:@"ratingStatus"] forKey:@"ratingStatus"];
//
//        NSArray *arr = [[dictMain objectForKey:@"address"] componentsSeparatedByString:@"/n/nLandmark:"];
//        if ([arr count]>0)
//        {
//            for (int i=0; i<[arr count]; i++)
//            {
//                if(i==0)
//                    [[NSUserDefaults standardUserDefaults] setObject:[arr objectAtIndex:i] forKey:@"address"];
//                else
//                    [[NSUserDefaults standardUserDefaults] setObject:[arr objectAtIndex:i] forKey:@"areaName"];
//            }
//        }
//        // [[NSUserDefaults standardUserDefaults] setObject:[dictMain objectForKey:@"areaName"] forKey:@"areaName"];
//        [[NSUserDefaults standardUserDefaults] synchronize];
//
//
//
////        UINavigationController *navi = [AppDelegate sharedDelegate].container.centerViewController;
////        NSArray *viewControllers = [navi viewControllers];
////        for( int i=0;i<[viewControllers count];i++)
////        {
////            id obj=[viewControllers objectAtIndex:i];
////            if([obj isKindOfClass:[HomeVC class]])
////            {
////                [[self navigationController] popToViewController:obj animated:YES];
////                break;
////            }
////        }
//
//        NSMutableArray *viewControllers = [[[self navigationController] viewControllers] mutableCopy];
//        if ([viewControllers count]==2)
//        {
//            [viewControllers removeObjectAtIndex:0];
//        }
//        for( int i=0;i<[viewControllers count];i++)
//        {
//            id obj=[viewControllers objectAtIndex:i];
//            if([obj isKindOfClass:[HomeVC class]])
//            {
//                [[self navigationController] popToViewController:obj animated:YES];
//                break;
//            }
//        }
//
//
//        [[NSNotificationCenter defaultCenter] postNotificationName:@"login_logout" object:nil];
//
//    }
//    else if(![[dictMain objectForKey:@"status"] boolValue])
//    {
//        [Utils showAlertMessage:KMessageTitle Message:[dictMain objectForKey:@"result"]];
//    }
//}
//
//
//
//- (void)requestFailed:(ASIHTTPRequest *)request
//{
//    NSLog(@"Fail");
//    [MBProgressHUD hideAllHUDsForView:[AppDelegate sharedDelegate].window animated:YES];
//    // [Utils showAlertMessage:KMessageTitle Message:@"Network error"];
//}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];

}


@end
