//
//  LoginVC.h
//  ToolsOnWheel
//
//  Created by Kapil Goyal on 12/12/17.
//  Copyright © 2017 Kapil Goyal. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BSKeyboardControls.h"

@interface LoginVC : UIViewController<UITextFieldDelegate,BSKeyboardControlsDelegate>
{
    BSKeyboardControls *keyboardControls;
    IBOutlet UITextField *txtFieldMobile;
    IBOutlet UITextField *txtFieldPassword;
    IBOutlet UIButton *btnSignIn;
    IBOutlet UIScrollView *scrlView;
}
-(IBAction)btnForgetPasswordClicked:(id)sender;
-(IBAction)btnSigninclicked:(id)sender;
-(IBAction)btnSignUpclicked:(id)sender;

@end
