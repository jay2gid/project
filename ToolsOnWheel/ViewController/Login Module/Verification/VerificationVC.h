//
//  LoginVC.h
//  ToolsOnWheel
//
//  Created by Kapil Goyal on 12/12/17.
//  Copyright © 2017 Kapil Goyal. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BSKeyboardControls.h"

@interface VerificationVC : UIViewController<BSKeyboardControlsDelegate,UITextFieldDelegate>
{
    IBOutlet UITextField *txtFieldVerification;
    IBOutlet UIButton *btnSubmit;
    NSString *strCheck;
    BSKeyboardControls *keyboardControls;

}
@property(nonatomic,strong)NSString *strUserId;
@property(nonatomic,strong)NSString *strVerificationCode;
@property(nonatomic,strong)NSString *strName;
@property(nonatomic,strong)NSString *strEmail;
@property(nonatomic,strong)NSString *strMobileNo;
@property(nonatomic,strong)NSString *strReferenceCode;

-(IBAction)btnRegenerateCodeclicked:(id)sender;
-(IBAction)btnSubmitCodeclicked:(id)sender;

@end
