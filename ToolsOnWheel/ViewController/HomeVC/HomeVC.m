//
//  ViewController.m
//  ToolsOnWheel
//
//  Created by Kapil Goyal on 17/09/17.
//  Copyright © 2017 Kapil Goyal. All rights reserved.
//

#import "HomeVC.h"
#import "CommonHeader.h"
#import "CustomNavigation.h"
#import "MFSideMenu.h"
#import "Config.h"

#import "RateOurServiceVC.h"

#import "ASIFormDataRequest.h"
#import "JSON.h"
#import "MBProgressHUD.h"
#import "AppDelegate.h"
#import "HomeVC.h"
#import "Utils.h"
#import "SubCategory.h"
#import "SubCategoryVC.h"
#import "MyCartViewController.h"
#import "ViewRateService.h"

@interface HomeVC ()<mainDelegate> {
    PromoBannerView *bannerView;
}

@end

@implementation HomeVC

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    cartItemBadge = [CustomBadge customBadgeWithString:@""
                                       withStringColor:[UIColor whiteColor]
                                        withInsetColor:kBadgeIconColor
                                        withBadgeFrame:YES
                                   withBadgeFrameColor:kBadgeIconColor
                                             withScale:0.6
                                           withShining:NO];
    
    [cartItemBadge setFrame:CGRectMake([UIScreen mainScreen].bounds.size.width -cartItemBadge.frame.size.width-8, 27, cartItemBadge.frame.size.width, cartItemBadge.frame.size.height)];
    [cartItemBadge setHidden:YES];
    [self.view addSubview:cartItemBadge];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(receiveNotification:)
                                                 name:@"UPDATE_CART"
                                               object:nil];
    
    [self performSelector:@selector(updateUI) withObject:nil afterDelay:0.01];
    
    page = 0;
    
    [self loginAPICalled];
    
    [AppDelegate sharedDelegate].mainDelegate = self;
}

-(void)didBecomeActive {
 
    [self loginAPICalled];
}

-(void)updateUI
{
    [CustomNavigation addTarget:self backRequired:NO title:@"HOME"];
    for (UIView *v1 in self.view.subviews)
    {
        if ([v1 isKindOfClass:[CommonHeader class]])
        {
            for (UIView *viewTmp in v1.subviews)
            {
                if ([viewTmp isKindOfClass:[UIButton class]])
                {
                    if (viewTmp.tag==1)
                    {
                        viewTmp.hidden=YES;
                    }
                    else if(viewTmp.tag==106)
                    {
                        [(UIButton*)viewTmp addTarget:self action:@selector(revealLeftSidebar:) forControlEvents:UIControlEventTouchUpInside];
                    }
                    else if(viewTmp.tag==102)
                    {
                        [(UIButton*)viewTmp addTarget:self action:@selector(moveToCart:) forControlEvents:UIControlEventTouchUpInside];
                    }
                    
                }
                else if ([viewTmp isKindOfClass:[UIImageView class]])
                {
                    if (viewTmp.tag==101)
                    {
                        [(UIImageView*)viewTmp setImage:[UIImage imageNamed:@"menu.png"]];
                    }
                }
            }
        }
    }
    
    arrSliderImage = [GlobalDataPersistance sharedGlobalDataPersistence].arrSliderImage;
    arrSubcategory = [GlobalDataPersistance sharedGlobalDataPersistence].arrSubcategory;

    bannerView = [self bannerView];
    bannerView.arrSlides = [NSMutableArray arrayWithArray:arrSliderImage];
    [bannerView reloadData];
    
    [tbleViewCategory reloadData];
}

-(IBAction)moveToCart:(id)sender
{
        MyCartViewController *vc = [[MyCartViewController alloc] initWithNibName:@"MyCartViewController" bundle:nil];
        [self.navigationController pushViewController:vc animated:YES];
   
}
-(void)viewWillAppear:(BOOL)animated
{
    [self receiveNotification:nil];
    [self.menuContainerViewController setPanMode:MFSideMenuPanModeDefault];
    if ([[NSUserDefaults standardUserDefaults] objectForKey:@"userid"])
    {
        [btnCall setHidden:NO];
    }
    else
    {
        [btnCall setHidden:YES];
    }
    
    
}

#pragma mark - Custom Views

-(PromoBannerView *)bannerView
{
    bannerView = nil;
    NSArray *topLevelObjects = [[NSBundle mainBundle] loadNibNamed:@"PromoBannerView" owner:nil options:nil];
    
    for(id currentObject in topLevelObjects)
    {
        if([currentObject isKindOfClass:[PromoBannerView class]])
        {
            bannerView = (PromoBannerView*)currentObject;
            break;
        }
    }
    
    return bannerView;
}

#pragma mark iPhone
#pragma mark MFSidebarDelegate
- (void)leftSideMenuButtonPressed:(id)sender
{
    [self.menuContainerViewController toggleLeftSideMenuCompletion:^{
    }];
}

- (void)revealLeftSidebar:(id)sender
{
    [self leftSideMenuButtonPressed:nil];
}

-(IBAction)btnImgClicked:(id)sender
{
    UIButton *btn = sender;
    SubCategoryVC *subCat = [[SubCategoryVC alloc] initWithNibName:@"SubCategoryVC" bundle:nil];
    subCat.dictCat = [[arrSubcategory objectAtIndex:btn.tag] objectForKey:@"subcategory"];
    subCat.dictMain = [arrSubcategory objectAtIndex:btn.tag] ;
    [[NSUserDefaults standardUserDefaults] setObject:[[arrSubcategory objectAtIndex:btn.tag] objectForKey:@"id"] forKey:@"maincatid"];
    [[NSUserDefaults standardUserDefaults] synchronize];
    [self.navigationController pushViewController:subCat animated:YES];
    
//        RateOurServiceVC *subCat = [[RateOurServiceVC alloc] initWithNibName:@"RateOurServiceVC" bundle:nil];
//        [self.navigationController pushViewController:subCat animated:YES];
    
}
- (IBAction)btnCallPressed:(id)sender
{
    NSString *phoneNumber = [@"tel:" stringByAppendingString:@"180030002605"];
    
    UIAlertController * alert = [UIAlertController
                                 alertControllerWithTitle:@"Call"
                                 message:@"1800 3000 2605"
                                 preferredStyle:UIAlertControllerStyleAlert];
    
    //Add Buttons
    
    UIAlertAction* yesButton = [UIAlertAction
                                actionWithTitle:@"Yes"
                                style:UIAlertActionStyleDefault
                                handler:^(UIAlertAction * action)
                                {
                                    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:phoneNumber]];

                                }];
    
    UIAlertAction* noButton = [UIAlertAction
                               actionWithTitle:@"No"
                               style:UIAlertActionStyleDefault
                               handler:^(UIAlertAction * action) {
                                   //Handle no, thanks button
                               }];
    
    //Add your buttons to alert controller
    
    [alert addAction:yesButton];
    [alert addAction:noButton];
    
    [self presentViewController:alert animated:YES completion:nil];
}

#pragma mark - UITableView Delegates

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [arrSubcategory count];
}

-(UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    return bannerView;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    if(section == 0)
    {
        return 210;
    }
    return 0;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{

    static NSString *CellIdentifier = @"Cell";
    SubCategory *cell = (SubCategory *)[tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil)
    {
        UIViewController *view = [[UIViewController alloc]initWithNibName:@"SubCategory" bundle:nil];
        cell = (SubCategory *)view.view;
    }
        
    
        
    cell.imgBig.layer.cornerRadius = 15; // this value vary as per your desire
    cell.imgBig.clipsToBounds = YES;
    
    [cell.imgBig sd_setImageWithURL:[NSURL URLWithString:[[arrSubcategory objectAtIndex:indexPath.row] objectForKey:@"cateBigimage"]] placeholderImage:[UIImage imageNamed:@"banner1.png"]];
    [cell.imgSmall sd_setImageWithURL:[NSURL URLWithString:[[arrSubcategory objectAtIndex:indexPath.row] objectForKey:@"image"]] placeholderImage:[UIImage imageNamed:@"banner1.png"]];

    cell.lblSubCategory.text =  [[arrSubcategory objectAtIndex:indexPath.row] objectForKey:@"name"];
    cell.viewContainer.layer.cornerRadius = 10.0;
    cell.btnImg.tag = indexPath.row;
    [cell.btnImg addTarget:self action:@selector(btnImgClicked:) forControlEvents:UIControlEventTouchUpInside];
        return cell;

}



- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
}

#pragma mark - Animation
- (void) receiveNotification:(NSNotification *) notification
{
    if ([[AppDelegate sharedDelegate].arrCartItems count]==0)
    {
        [self hideBadge];
    }
    else
    {
        cartItemBadge.alpha = 0.0;
        [cartItemBadge autoBadgeSizeWithString:[NSString stringWithFormat:@"%lu",(unsigned long)[[AppDelegate sharedDelegate].arrCartItems count]]];
        [cartItemBadge setHidden:NO];
        [self showAnimation];
    }
}

- (void)showAnimation
{
    [UIView animateWithDuration:0.2 animations:
     ^(void){
         self->cartItemBadge.transform = CGAffineTransformScale(CGAffineTransformIdentity,1.1f, 1.1f);
         self->cartItemBadge.alpha = 0.5;
     }
                     completion:^(BOOL finished){
                         [self bounceOutAnimationStoped];
                     }];
}
- (void)bounceOutAnimationStoped
{
    
    
    [UIView animateWithDuration:0.1 animations:^{
        self->cartItemBadge.transform = CGAffineTransformScale(CGAffineTransformIdentity,0.9, 0.9);
        self->cartItemBadge.alpha = 0.8;
    } completion:^(BOOL finished) {
        [self bounceInAnimationStoped];

    }];
    
    
}
- (void)bounceInAnimationStoped
{
    
    
    [UIView animateWithDuration:0.1 animations:^{
        self->cartItemBadge.transform = CGAffineTransformScale(CGAffineTransformIdentity,1, 1);
        self->cartItemBadge.alpha = 1.0;
    } completion:^(BOOL finished) {
        [self bounceInAnimationStoped];
        
    }];
    
}
- (void)hideBadge
{
    cartItemBadge.alpha = 0;
    cartItemBadge.transform = CGAffineTransformScale(CGAffineTransformIdentity,0.6, 0.6);
}

-(IBAction)btnPhoneClicked:(id)sender;
{
  
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}



-(void)loginAPICalled{
    
    [AppDelegate sharedDelegate].isHome = true;
    
    if (USER_ID) {
        
        NSDictionary *param =@{@"userMob_login":USER_PHONE,
                               @"userPassword_login":USER_PASSWORD,
                               @"secureSignature": SECURE_SIGNATURE_API
                               };
        
        [WebServiceCalls POST:kLogin_user parameter:param completionBlock:^(id JSON, WebServiceResult result)
         {
             @try{
                 
                 if (result == WebServiceResultSuccess) {
                     
                     if ([JSON[@"status"] integerValue] == 1) {
                         
                         [self.navigationController popViewControllerAnimated:YES];
                         [[NSUserDefaults standardUserDefaults] setObject:[JSON objectForKey:@"userId"] forKey:@"userid"];
                         [[NSUserDefaults standardUserDefaults] setObject:[JSON objectForKey:@"email"] forKey:@"email"];
                         [[NSUserDefaults standardUserDefaults] setObject:[JSON objectForKey:@"phone"] forKey:@"phone"];
                         [[NSUserDefaults standardUserDefaults] setObject:[JSON objectForKey:@"name"] forKey:@"name"];
                         [[NSUserDefaults standardUserDefaults] setObject:[JSON objectForKey:@"refCode"] forKey:@"refcode"];
                         
                         [Helper saveUserImage:[Helper getString:JSON[@"img"]]];
                         
                         if ([JSON[@"ratingStatus"] integerValue] == 1) {
                             [self callService];
                         }
                         
                         NSArray *arr = [[JSON objectForKey:@"address"] componentsSeparatedByString:@"/n/nLandmark:"];
                         if ([arr count]>0)
                         {
                             for (int i=0; i<[arr count]; i++)
                             {
                                 if(i==0)
                                     [[NSUserDefaults standardUserDefaults] setObject:[arr objectAtIndex:i] forKey:@"address"];
                                 else
                                     [[NSUserDefaults standardUserDefaults] setObject:[arr objectAtIndex:i] forKey:@"areaName"];
                             }
                         }
                         [[NSUserDefaults standardUserDefaults] synchronize];
                         
                         
                     }else {
                         
                     }
                 }
             }
             @catch (NSException *exception) { }
             @finally { }
         }];
    }
}


-(void)callService{
    
    NSString *urlString = [NSString stringWithFormat:@"lastCompleteorder&userId=%@",USER_ID];
    
    [WebServiceCalls GET:urlString parameter:nil completionBlock:^(id JSON, WebServiceResult result)
     {
         @try{
             NSLog(@"%@", JSON);
             if (result == WebServiceResultSuccess) {
                 
                 if ([JSON[@"status"] integerValue] == 1) {
                     
                     if ([Helper checkID:[Helper getString:JSON[@"result"][0][@"serviceId"]]] == false ){
                         [self rateServiceView:JSON[@"result"][0]];
                     }
                     
                     
                 }else {
                     
                 }
             }
         }
         @catch (NSException *exception) { }
         @finally { }
     }];
}


-(void)rateServiceView:(NSDictionary *)info{
    
    if (!isRatingPopop) {
        
        [[NSUserDefaults standardUserDefaults]setBool:true forKey:@"ratingPopup"];
                
        ViewRateService *view = [[[NSBundle mainBundle]loadNibNamed:@"ViewRateService" owner:self options:nil]objectAtIndex:0];
        view.frame = self.view.frame;
        view.center = CGPointMake(view.center.x,view.center.y+view.frame.size.height);
        view.info = info;
        [view setData];
        [[AppDelegate sharedDelegate].navigationController.view addSubview:view];
        
        
        [UIView animateWithDuration:0.3 animations:^{
            view.center = CGPointMake(view.center.x,view.center.y-view.frame.size.height);
        }];
    }
}

@end
