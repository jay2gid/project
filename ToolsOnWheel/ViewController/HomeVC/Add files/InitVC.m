//
//  InitVC.m
//  ToolsOnWheel
//
//  Created by Shikhar Khanna on 06/03/18.
//  Copyright © 2018 Kapil Goyal. All rights reserved.
//

#import "InitVC.h"
#import "HomeVC.h"
#import "CommonHeader.h"
#import "CustomNavigation.h"
#import "MFSideMenu.h"
#import "Config.h"
#import "GlobalDataPersistance.h"

#import "RateOurServiceVC.h"

#import "ASIFormDataRequest.h"
#import "JSON.h"
#import "AppDelegate.h"
#import "HomeVC.h"
#import "Utils.h"
#import "SubCategory.h"
#import "SubCategoryVC.h"
#import "MyCartViewController.h"

@interface InitVC ()
{
    IBOutlet UIView *veiwRetry;
    NSString *strCheck;
    NSMutableArray *arrSliderImage;
    NSMutableArray *arrSubcategory;
}

@end

@implementation InitVC

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    [self getSlider];
    //  veiwRetry.hidden = true;
}

- (IBAction)tapRetry:(id)sender {

    [UIView animateWithDuration:0.2 animations:^{
        self->veiwRetry.frame = CGRectMake(0, self.view.frame.size.height,WIDTH, 35);
    }];
    [self getSlider];
    
}

-(void)getSlider
{
    if ([AppDelegate checkNetwork])
    {
        strCheck = @"slider";
        NSDictionary *param = @{@"version":@"8",
                               @"secureSignature":SECURE_SIGNATURE};
        
        [WebServiceCalls POST:kSlider parameter:param completionBlock:^(id JSON, WebServiceResult result)
         {
             @try{
                 NSDictionary *dictMain =JSON;
                 if([[dictMain objectForKey:@"status"] boolValue])
                 {
                     NSMutableArray *arrSlider = [dictMain objectForKey:@"result"];
                     if ([arrSlider count]>0) {
                         
                         self->arrSliderImage = [[NSMutableArray alloc] init];
                         for (int i=0; i<[arrSlider count]; i++)
                         {
                             NSMutableDictionary *dict = [NSMutableDictionary dictionary];
                             [dict setObject:[[arrSlider objectAtIndex:i] objectForKey:@"sliderImg"] forKey:@"image"];
                             [dict setObject:[[arrSlider objectAtIndex:i] objectForKey:@"sliderCaptation"] forKey:@"name"];
                             
                             [self->arrSliderImage addObject:dict];
                         }
                         [self getHomePage];
                     }
                 }
             }
             @catch (NSException *exception) { }
             @finally { }
         }];
        
        
        
        
        
//        ASIFormDataRequest *request = [ASIFormDataRequest requestWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@%@",ServerAdd3,kSlider]]];
//        [request setPostValue:[[NSUserDefaults standardUserDefaults] objectForKey:@"secureSignature"] forKey:@"secureSignature"];
//        [request setPostValue:@"8" forKey:@"version"];
//
//        [request setDelegate:self];
//            [request startAsynchronous];
        
    }else{
        
        veiwRetry.hidden = false;

        [UIView animateWithDuration:0.2 animations:^{

            self->veiwRetry.frame = CGRectMake(0, self.view.frame.size.height-35,WIDTH, 35);

        }];
    }
}

-(void)getHomePage
{
    strCheck = @"category";
//    ASIFormDataRequest *request = [ASIFormDataRequest requestWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@%@",ServerAdd,kHome]]];
//    [request setDelegate:self];
//    [request startAsynchronous];

    
    NSString *urlString = [NSString stringWithFormat:@"%@%@",ServerAdd,kHome];
    [WebServiceCalls POST_WITH_URL:urlString parameter:nil completionBlock:^(id JSON, WebServiceResult result)
     {
         @try{
             
             NSDictionary *dictMain =JSON;
             
             if([[dictMain objectForKey:@"status"] boolValue])
             {
                 NSLog(@"dictMain=%@",dictMain);
                 NSMutableArray *arrCategoryTmp = [dictMain objectForKey:@"result"];
                 self->arrSubcategory = [[NSMutableArray alloc] init];
                 for (int i=0; i<[arrCategoryTmp count]; i++)
                 {
                     NSMutableDictionary *dict = [NSMutableDictionary dictionary];
                     [dict setObject:[[arrCategoryTmp objectAtIndex:i] objectForKey:@"id"] forKey:@"id"];
                     [dict setObject:[[arrCategoryTmp objectAtIndex:i] objectForKey:@"name"] forKey:@"name"];
                     [dict setObject:[[arrCategoryTmp objectAtIndex:i] objectForKey:@"desc"] forKey:@"desc"];
                     [dict setObject:[[arrCategoryTmp objectAtIndex:i] objectForKey:@"image"] forKey:@"image"];
                     [dict setObject:[[arrCategoryTmp objectAtIndex:i] objectForKey:@"cateBigimage"] forKey:@"cateBigimage"];
                     [dict setObject:[[arrCategoryTmp objectAtIndex:i] objectForKey:@"subcategory"] forKey:@"subcategory"];
                     [self->arrSubcategory addObject:dict];
                 }
                 [[NSUserDefaults standardUserDefaults] setObject:[dictMain objectForKey:@"secureSignature"] forKey:@"secureSignature"];
                 [[NSUserDefaults standardUserDefaults] synchronize];
                 [self getAboutUs];
             }
             
         }
         @catch (NSException *exception) { }
         @finally { }
     }];
    
    
    
    
    
    
}

-(void)getAboutUs
{
//    strCheck = @"aboutus";
//    ASIFormDataRequest *request = [ASIFormDataRequest requestWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@%@",ServerAdd3,kABoutUSContent]]];
//    [request setDelegate:self];
//    [request startAsynchronous];
    
    
    [WebServiceCalls POST:kABoutUSContent parameter:nil completionBlock:^(id JSON, WebServiceResult result)
     {
         @try{
             
             NSDictionary *dictMain =JSON;
            
             if([[dictMain objectForKey:@"status"] boolValue])
             {
                 NSLog(@"dictMain12=%@",dictMain);
                 NSArray *arr=[dictMain objectForKey:@"result"];
                 [[NSUserDefaults standardUserDefaults] setObject:[[arr objectAtIndex:0] objectForKey:@"aboutDesc"] forKey:@"aboutDesc"];
                 [[NSUserDefaults standardUserDefaults] setObject:[[arr objectAtIndex:0] objectForKey:@"faqDesc"] forKey:@"faqDesc"];
                 [[NSUserDefaults standardUserDefaults] setObject:[[arr objectAtIndex:0] objectForKey:@"privacyDesc"] forKey:@"privacyDesc"];
                 [[NSUserDefaults standardUserDefaults] synchronize];
             }
             
             GlobalDataPersistance *GDP = [GlobalDataPersistance sharedGlobalDataPersistence];
             GDP.arrSliderImage = [NSMutableArray arrayWithArray:self->arrSliderImage];
             GDP.arrSubcategory = [NSMutableArray arrayWithArray:self->arrSubcategory];
             AppDelegate *appDel = App_Delegate;
             [appDel setRootViewController];
             
         }
         @catch (NSException *exception) { }
         @finally { }
     }];
    
}

#pragma mark Request Finished
- (void)requestFinished:(ASIHTTPRequest *)request
{
    NSString *receivedString = [request responseString];
    NSLog(@"str=%@",receivedString);
    NSDictionary *dictMain=[receivedString JSONValue];
    
    if ([strCheck isEqualToString:@"slider"])
    {
        if([[dictMain objectForKey:@"status"] boolValue])
        {
            NSMutableArray *arrSlider = [dictMain objectForKey:@"result"];
            if ([arrSlider count]>0)
            {
                arrSliderImage = [[NSMutableArray alloc] init];
                for (int i=0; i<[arrSlider count]; i++)
                {
                    NSMutableDictionary *dict = [NSMutableDictionary dictionary];
                    [dict setObject:[[arrSlider objectAtIndex:i] objectForKey:@"sliderImg"] forKey:@"image"];
                    [dict setObject:[[arrSlider objectAtIndex:i] objectForKey:@"sliderCaptation"] forKey:@"name"];
                    
                    [arrSliderImage addObject:dict];
                }
                
                [self getHomePage];
            }
            
        }
    }
    else if([strCheck isEqualToString:@"category"])
    {
        if([[dictMain objectForKey:@"status"] boolValue])
        {
            NSLog(@"dictMain=%@",dictMain);
            NSMutableArray *arrCategoryTmp = [dictMain objectForKey:@"result"];
            arrSubcategory = [[NSMutableArray alloc] init];
            for (int i=0; i<[arrCategoryTmp count]; i++)
            {
                NSMutableDictionary *dict = [NSMutableDictionary dictionary];
                [dict setObject:[[arrCategoryTmp objectAtIndex:i] objectForKey:@"id"] forKey:@"id"];
                [dict setObject:[[arrCategoryTmp objectAtIndex:i] objectForKey:@"name"] forKey:@"name"];
                [dict setObject:[[arrCategoryTmp objectAtIndex:i] objectForKey:@"desc"] forKey:@"desc"];
                [dict setObject:[[arrCategoryTmp objectAtIndex:i] objectForKey:@"image"] forKey:@"image"];
                [dict setObject:[[arrCategoryTmp objectAtIndex:i] objectForKey:@"cateBigimage"] forKey:@"cateBigimage"];
                [dict setObject:[[arrCategoryTmp objectAtIndex:i] objectForKey:@"subcategory"] forKey:@"subcategory"];
                [arrSubcategory addObject:dict];
            }
            [[NSUserDefaults standardUserDefaults] setObject:[dictMain objectForKey:@"secureSignature"] forKey:@"secureSignature"];
            [[NSUserDefaults standardUserDefaults] synchronize];
            [self getAboutUs];
        }
    }
    else if([strCheck isEqualToString:@"aboutus"])
    {
        if([[dictMain objectForKey:@"status"] boolValue])
        {
            NSLog(@"dictMain12=%@",dictMain);
            NSArray *arr=[dictMain objectForKey:@"result"];
            [[NSUserDefaults standardUserDefaults] setObject:[[arr objectAtIndex:0] objectForKey:@"aboutDesc"] forKey:@"aboutDesc"];
            [[NSUserDefaults standardUserDefaults] setObject:[[arr objectAtIndex:0] objectForKey:@"faqDesc"] forKey:@"faqDesc"];
            [[NSUserDefaults standardUserDefaults] setObject:[[arr objectAtIndex:0] objectForKey:@"privacyDesc"] forKey:@"privacyDesc"];
            [[NSUserDefaults standardUserDefaults] synchronize];
        }
        
        GlobalDataPersistance *GDP = [GlobalDataPersistance sharedGlobalDataPersistence];
        GDP.arrSliderImage = [NSMutableArray arrayWithArray:arrSliderImage];
        GDP.arrSubcategory = [NSMutableArray arrayWithArray:arrSubcategory];
        AppDelegate *appDel = App_Delegate;
        [appDel setRootViewController];
    }
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
