
//
//  ViewController.h
//  ToolsOnWheel
//
//  Created by Kapil Goyal on 17/09/17.
//  Copyright © 2017 Kapil Goyal. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CustomNavigation.h"
#import "CommonHeader.h"
#import "MFSideMenu.h"
#import "UIImageView+WebCache.h"
#import "CustomBadge.h"
#import "PromoBannerView.h"
#import "GlobalDataPersistance.h"
#import "AppDelegate.h"

@interface HomeVC : UIViewController<UIScrollViewDelegate,UITableViewDelegate,UITableViewDataSource>
{
    NSString *strHome;
    BOOL pageControlUsed;
    IBOutlet UIScrollView *scrlViewPicture;
    IBOutlet UIPageControl *pageControl;
    int num,xPosition;
    UIImageView *imgSlider;
    int page;
    NSTimer *timer;
    CustomBadge *cartItemBadge;
    
    BOOL pageControlUsed1;
    IBOutlet UIScrollView *scrlViewBrand;
    IBOutlet UIPageControl *pageControlBrand;
    int num1,xPosition1;
    UIImageView *imgSlider1;
    int page1;
    NSTimer *timer1;
    /****Custom badge*****/
    int intCartCount;
    NSMutableArray *arrSliderImage;
    NSMutableArray *arrSubcategory;
    IBOutlet UILabel *lblSlider;
    NSString *strCheck;
    IBOutlet UITableView *tbleViewCategory;
    IBOutlet UIButton *btnCall;
}
-(IBAction)btnPhoneClicked:(id)sender;

@end

