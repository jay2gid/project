//
//  PromoBannerView.m
//  ToolsOnWheel
//
//  Created by Shikhar Khanna on 07/02/18.
//  Copyright © 2018 Kapil Goyal. All rights reserved.
//

#import "PromoBannerView.h"
#import "AppDelegate.h"

@implementation PromoBannerView
{
    NSInteger scrollIndex;
    AppDelegate *appDelegateObj;
}
@synthesize arrSlides;

-(void)reloadData
{
    appDelegateObj = (AppDelegate *)[[UIApplication sharedApplication] delegate];

    viewCarousel.dataSource = self;
    viewCarousel.delegate = self;
    
    viewCarousel.type = iCarouselTypeLinear;
    viewCarousel.pagingEnabled = TRUE;

    [self addTimer];
    [viewCarousel reloadData];
    
}

#pragma mark - Add Timer

-(void)addTimer
{
//    self.pageControlForPromo.numberOfPages = promoData.arrMedia.count;
    if (appDelegateObj.scrollTimer)
    {
        [appDelegateObj.scrollTimer invalidate];
        appDelegateObj.scrollTimer = nil;
        scrollIndex = 0;
    }
    
    appDelegateObj.scrollTimer = [NSTimer scheduledTimerWithTimeInterval:3.0 target:self selector:@selector(onTimer) userInfo:nil repeats:YES];
    [[NSRunLoop currentRunLoop] addTimer:appDelegateObj.scrollTimer forMode:NSRunLoopCommonModes];
}

- (void) onTimer
{
    [viewCarousel scrollToItemAtIndex:scrollIndex animated:YES];
    if (scrollIndex == arrSlides.count)
    {
        scrollIndex = 0;
    }
    else
    {
        scrollIndex = scrollIndex+1;
    }
}

-(NSInteger)numberOfItemsInCarousel:(iCarousel *)carousel
{
    return arrSlides.count;
}

-(UIView *)carousel:(iCarousel *)carousel viewForItemAtIndex:(NSInteger)index reusingView:(UIView *)view
{
    PromoComponentView *viewPromo = nil;
    NSArray *topLevelObjects = [[NSBundle mainBundle] loadNibNamed:@"PromoComponentView" owner:nil options:nil];
    
    for(id currentObject in topLevelObjects)
    {
        if([currentObject isKindOfClass:[PromoComponentView class]])
        {
            viewPromo = (PromoComponentView*)currentObject;
            break;
        }
    }
    
    viewPromo.layer.cornerRadius = 10.0;
    viewPromo.imgViewBanner.layer.cornerRadius = 10.0;
    NSDictionary *dict = [arrSlides objectAtIndex:index];
    
    [viewPromo.imgViewBanner sd_setImageWithURL:[NSURL URLWithString:[dict objectForKey:@"image"]] placeholderImage:[UIImage imageNamed:@"banner1.png"]];
    viewPromo.lblTitle.text = [dict objectForKey:@"name"];
    viewPromo.lblTitle.textAlignment = NSTextAlignmentCenter;
    viewPromo.lblTitle.textColor = [UIColor colorWithRed:107/255.0 green:150/255.0 blue:191/255.0 alpha:1.0];

    return viewPromo;
}

- (CGFloat)carousel:(iCarousel *)carousel valueForOption:(iCarouselOption)option withDefault:(CGFloat)value
{
    if (option == iCarouselOptionSpacing)
    {
        return value * 1.05;
    }
    return value;
}
@end
