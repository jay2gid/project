//
//  PromoComponentView.h
//  ToolsOnWheel
//
//  Created by Shikhar Khanna on 07/02/18.
//  Copyright © 2018 Kapil Goyal. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PromoComponentView : UIView

@property (strong, nonatomic) IBOutlet UIImageView *imgViewBanner;
@property (strong, nonatomic) IBOutlet UILabel *lblTitle;
@property (strong, nonatomic) IBOutlet UILabel *lblSubTitle;
@end
