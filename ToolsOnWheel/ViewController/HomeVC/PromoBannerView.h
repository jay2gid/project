//
//  PromoBannerView.h
//  ToolsOnWheel
//
//  Created by Shikhar Khanna on 07/02/18.
//  Copyright © 2018 Kapil Goyal. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "iCarousel.h"
#import "PromoComponentView.h"
#import "UIImageView+WebCache.h"


@interface PromoBannerView : UIView<iCarouselDataSource,iCarouselDelegate>
{
    IBOutlet iCarousel *viewCarousel;
    
}

@property(nonatomic, strong)NSMutableArray *arrSlides;
-(void)reloadData;
@end
