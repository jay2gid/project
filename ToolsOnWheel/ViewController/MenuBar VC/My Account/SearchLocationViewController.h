//
//  SearchLocationViewController.h
//  Qboy
//
//  Created by Shikhar Khanna on 16/03/17.
//  Copyright © 2017 Mac. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SearchLocationTVCell.h"
#import <MapKit/MapKit.h>


@protocol SearchLocationDelegate <NSObject>

-(void)didSelectLocation:(MKMapItem *)locationItem;

@end

@interface SearchLocationViewController : UIViewController<UISearchBarDelegate>
{
    IBOutlet UISearchBar *searchBarLocation;
    IBOutlet UITableView *tblLocation;
    
}

@property(nonatomic,strong)id <SearchLocationDelegate>delegate;

@end
