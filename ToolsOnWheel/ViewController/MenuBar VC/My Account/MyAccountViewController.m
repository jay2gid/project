//
//  MyAccountViewController.m
//  ToolsOnWheel
//
//  Created by Shikhar Khanna on 08/01/18.
//  Copyright © 2018 Kapil Goyal. All rights reserved.
//

#import "MyAccountViewController.h"
#import "CommonHeader.h"
#import "CustomNavigation.h"
#import "MFSideMenu.h"
#import "HomeVC.h"
#import "MBProgressHUD.h"
#import "Utils.h"
#import "Config.h"
#import "JSON.h"
#import "DHValidation.h"
#import "AppDelegate.h"
#import "ASIFormDataRequest.h"

@interface MyAccountViewController ()
{
    NSString *imageName;
}
@end

@implementation MyAccountViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    [self performSelector:@selector(updateUI) withObject:nil afterDelay:0.01];
    viewContainer.layer.cornerRadius = 8.0f;
    btnUpdate.layer.cornerRadius = 20.0f;
    imgViewProfile.layer.cornerRadius = 60;
    
    [self addKeyboardControls];
 
    [self setUserData];
}

-(void)setUserData {
    
    [imgViewProfile sd_setImageWithURL:[NSURL URLWithString:[Helper getUserImage]] placeholderImage:[UIImage imageNamed:@"man1.png"]];

    txtFName.text = [[NSUserDefaults standardUserDefaults] objectForKey:@"name"];
    txtFPhone.text = [[NSUserDefaults standardUserDefaults] objectForKey:@"phone"];
    txtFEmail.text = [[NSUserDefaults standardUserDefaults] objectForKey:@"email"];
    txtFLocation.text = [[NSUserDefaults standardUserDefaults] objectForKey:@"address"];
    txtLandmark.text = [[NSUserDefaults standardUserDefaults] objectForKey:@"areaName"];
}


-(void)viewWillAppear:(BOOL)animated {
   

}

//-(void)uploadImageToServerWithParams:(NSString )urlString PostDataDictonery :(NSMutableDictionary )Dictionery
//{
//
//    if ([AppDelegate checkNetwork] && ![UIApplication sharedApplication].isNetworkActivityIndicatorVisible)
//    {
//        NSString *JsonString=nil;
//        NSError *error;
//        NSData *jsonData = [NSJSONSerialization dataWithJSONObject:Dictionery
//                                                           options:NSJSONWritingPrettyPrinted // Pass 0 if you don't care about the readability of the generated string
//                                                             error:&error];
//
//        if (! jsonData) {
//            NSLog(@"Got an error: %@", error);
//        }
//        else
//        {
//            //NSLog(@"ASICallSyncToServerWithFunctionName activity %d",![UIApplication sharedApplication].isNetworkActivityIndicatorVisible);
//            JsonString = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
//            [UIApplication sharedApplication].networkActivityIndicatorVisible = YES;
//
//            NSLog(@"requestjson-->> %@",JsonString);
//
//            NSLog(@"ServerAdd-->> %@",urlString);
//            NSURL *aUrl=[[NSURL alloc]initWithString:urlString]];
//
//            ASIFormDataRequest *anASIReq = [ASIFormDataRequest requestWithURL:aUrl];
//
//            anASIReq.methodName=@"";
//
//            for (NSString *values  in Dictionery)
//            {
//                if (![value isEqualToString:@"image"])
//                {
//                    [anASIReq setPostValue:[Dictionery objectForKey:values]  forKey:values];
//
//                }
//
//            }
//
//
//            if (Dictionery objectForKey@"image")
//            {
//                NSString *filePath = [NSString stringWithFormat:@"%@",[Dictionery objectForKey:@"image"]];
//                if(filePath.length)
//                    [anASIReq setFile:[NSData dataWithContentsOfFile:[Dictionery objectForKey:@"image"]] withFileName:[NSString stringWithFormat:@"%ld",(long)[NSDate timeIntervalSinceReferenceDate]] andContentType:@"image/png" forKey:@"image"];
//            }
//
//            //            [anASIReq setPostValue:JsonString  forKey:@"json"];
//
//            [anASIReq addRequestHeader:@"Accept" value:@"application/json"];
//            [anASIReq addRequestHeader:@"content-type" value:@"application/x-www-form-urlencoded"];
//
//            anASIReq.delegate = self;
//            anASIReq.didFailSelector = @selector(dataDownloadFail:);
//            anASIReq.didFinishSelector = @selector(dataDidFinishDowloading:);
//            anASIReq.requestMethod = @"POST";
//
//            [anASIReq startAsynchronous];
//        }
//    }
//}

-(void)updateUI
{
    [CustomNavigation addTarget:self backRequired:NO title:@"MY ACCOUNT"];
    for (UIView *v1 in self.view.subviews)
    {
        if ([v1 isKindOfClass:[CommonHeader class]])
        {
            for (UIView *viewTmp in v1.subviews)
            {
                if ([viewTmp isKindOfClass:[UIButton class]])
                {
                    if (viewTmp.tag==1)
                    {
                        viewTmp.hidden=YES;
                    }
                    else if(viewTmp.tag==106)
                    {
                        [(UIButton*)viewTmp addTarget:self action:@selector(revealLeftSidebar:) forControlEvents:UIControlEventTouchUpInside];
                    }
                    else if(viewTmp.tag==102)
                    {
                        // [(UIButton*)viewTmp addTarget:self action:@selector(moveToCart) forControlEvents:UIControlEventTouchUpInside];
                        [(UIButton*)viewTmp setHidden:YES];

                    }
                    
                }
                else if ([viewTmp isKindOfClass:[UIImageView class]])
                {
                    if (viewTmp.tag==101)
                    {
                        [(UIImageView*)viewTmp setImage:[UIImage imageNamed:@"back.png"]];
                    }
                }
                else if ([viewTmp isKindOfClass:[UILabel class]])
                {
                    if (viewTmp.tag==110)
                    {
                        [(UILabel*)viewTmp setTextColor:[UIColor whiteColor]];
                    }
                }
            }
        }
    }
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(BOOL)valiateMyAccount
{
    txtFName.text = [txtFName.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
    txtFEmail.text =[txtFEmail.text  stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
    txtFLocation.text=[txtFLocation.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
    txtLandmark.text=[txtLandmark.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
    
    
    DHValidation *aValidation = [[DHValidation alloc]init];
    
    
    BOOL flag = [aValidation validateEmail:txtFEmail.text];
    
    
    
    if(txtFName.text == NULL || [txtFName.text length] == 0)
    {
        [Utils showAlertMessage:KMessageTitle Message:@"Please enter name"];
        [txtFName becomeFirstResponder];
        return false;
    }
    else if ([txtFEmail.text length]>0  && !flag)
    {
        [Utils showAlertMessage:KMessageTitle Message:@"Please enter valid email address"];
        [txtFEmail becomeFirstResponder];
        return false;
    }
    else if ([txtFLocation.text length]>0)
    {
        if ([txtFLocation.text length]<20)
        {
            [Utils showAlertMessage:KMessageTitle Message:@"Address should be of atleast 20 characters."];
            [txtFLocation becomeFirstResponder];
            return false;
        }
        else
        {
            return true;

        }
    }
    else
    {
        return true;
    }
}

- (IBAction)btnUpdatePressed:(id)sender
{
//    if ([self valiateMyAccount])
//    {
//        //http://toolsonwheel.com/tools/apidata.php?action=update_user_info&image=TEST.png&userId=31&name=hello&email=hello@test.com&userAddress=ashok nagar&mob=369852145
//        if ([AppDelegate checkNetwork])
//        {
//            [MBProgressHUD showHUDAddedTo:[AppDelegate sharedDelegate].window animated:YES];
//            ASIFormDataRequest *request = [ASIFormDataRequest requestWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@%@",ServerAdd3,kUpdateUserInfo]]];
//            [request setDelegate:self];
//            [request setPostValue:[[NSUserDefaults standardUserDefaults] objectForKey:@"userid"] forKey:@"userId"];
//            [request setPostValue:txtFName.text forKey:@"name"];
//            [request setPostValue:txtFPhone.text forKey:@"mob"];
//
//            NSData *imageData1=UIImageJPEGRepresentation(imgViewProfile.image, 1.0);
//            imageName = [NSString stringWithFormat:@"%@.jpg",[self dateConstant]];
//
//            if(imageData1 != nil)
//                [request setFile:imageData1 withFileName:imageName andContentType:@"image/png" forKey:@"image"];
//
//            txtLandmark.text=[txtLandmark.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
//            if (txtLandmark.text.length>0)
//            {
//                [request setPostValue:[NSString stringWithFormat:@"%@/n/nLandmark:%@",txtFLocation.text,txtLandmark.text] forKey:@"userAddress"];
//            }
//            else if (txtFLocation.text.length==0)
//            {
//                [request setPostValue:[NSString stringWithFormat:@"/n/nLandmark:%@",txtLandmark.text] forKey:@"userAddress"];
//            }
//            else
//            {
//                [request setPostValue:txtFLocation.text forKey:@"userAddress"];
//            }
//            [request startAsynchronous];
//        }
//    }
    
    if ([self valiateMyAccount])
    {
        if ([AppDelegate checkNetwork])
        {
            NSData *imageData1=UIImageJPEGRepresentation(imgViewProfile.image, 1.0);
            
            txtLandmark.text=[txtLandmark.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
            
            NSString *address;
            
            if (txtLandmark.text.length > 0)
            {
                address = [NSString stringWithFormat:@"%@/n/nLandmark:%@",txtFLocation.text,txtLandmark.text];
            }
            else if (txtFLocation.text.length == 0)
            {
                address = [NSString stringWithFormat:@"/n/nLandmark:%@",txtLandmark.text];
            }
            else
            {
                address = txtFLocation.text;
            }

            NSDictionary *param =@{@"userId" : [[NSUserDefaults standardUserDefaults] objectForKey:@"userid"],
                                   @"name" : txtFName.text,
                                   @"mob" : txtFPhone.text,
                                   @"userAddress" : address};
            
            [Helper showLoading];
            [WebServiceCalls POST:kUpdateUserInfo parameter:param imageData:imageData1 completionBlock:^(id JSON, WebServiceResult result)
             {
                 @try
                 {
                     [Helper stopLoading];
                     
                     if (result == WebServiceResultSuccess)
                     {
                         NSDictionary *dictMain = JSON;
                         if ([dictMain[@"status"] integerValue] == 1)
                         {
                             [[NSUserDefaults standardUserDefaults] setObject:self->txtFName.text forKey:@"name"];
                             [[NSUserDefaults standardUserDefaults] setObject:self->txtFLocation.text forKey:@"address"];
                             [[NSUserDefaults standardUserDefaults] setObject:self->txtLandmark.text forKey:@"areaName"];
                             [[NSUserDefaults standardUserDefaults] setObject:self->txtFEmail.text forKey:@"email"];
                             [[NSUserDefaults standardUserDefaults] synchronize];
                             
                             [Utils showAlertMessage:KMessageTitle Message:[dictMain objectForKey:@"message"]];
                             
                             [Helper saveUserImage:self->imageName];
                             [[NSNotificationCenter defaultCenter] postNotificationName:@"login_logout" object:nil];
                         }
                         else
                         {
                             [Utils showAlertMessage:KMessageTitle Message:[dictMain objectForKey:@"result"]];
                         }
                     }
                 }
                 @catch (NSException *exception) { }
                 @finally {
                     [Helper stopLoading];
                 }
             }];
        }
    }
}

//- (void)requestFinished:(ASIHTTPRequest *)request
//{
//    [MBProgressHUD hideAllHUDsForView:[AppDelegate sharedDelegate].window animated:YES];
//    NSString *receivedString = [request responseString];
//    NSLog(@"str=%@",receivedString);
//    NSDictionary *dictMain=[receivedString JSONValue];
//   
//        if([[dictMain objectForKey:@"status"] boolValue])
//        {
//            if (imgViewProfile.image) {
//                /// [Helper saveUserImage:imgViewProfile.image];
//            }
//            
//            [[NSUserDefaults standardUserDefaults] setObject:txtFName.text forKey:@"name"];
//            [[NSUserDefaults standardUserDefaults] setObject:txtFLocation.text forKey:@"address"];
//            [[NSUserDefaults standardUserDefaults] setObject:txtLandmark.text forKey:@"areaName"];
//            [[NSUserDefaults standardUserDefaults] setObject:txtFEmail.text forKey:@"email"];
//            [[NSUserDefaults standardUserDefaults] synchronize];
//
//            [Utils showAlertMessage:KMessageTitle Message:[dictMain objectForKey:@"message"]];
//
//            [Helper saveUserImage:imageName];
//            [[NSNotificationCenter defaultCenter] postNotificationName:@"login_logout" object:nil];
//
//        }
//        else
//        {
//            [Utils showAlertMessage:KMessageTitle Message:[dictMain objectForKey:@"result"]];
//        }
//}
//
//- (void)requestFailed:(ASIHTTPRequest *)request
//{
//    NSLog(@"Fail");
//    [MBProgressHUD hideAllHUDsForView:[AppDelegate sharedDelegate].window animated:YES];
//    // [Utils showAlertMessage:KMessageTitle Message:@"Network error"];
//}

- (IBAction)btnUploadImagePressed:(id)sender
{
    UIAlertController *actionSheet = [UIAlertController alertControllerWithTitle:@"Select Option" message:nil preferredStyle:UIAlertControllerStyleActionSheet];
    
    [actionSheet addAction:[UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleCancel handler:^(UIAlertAction *action) {
        
        // Cancel button tappped.
        [self dismissViewControllerAnimated:YES completion:^{
        }];
    }]];
    
    [actionSheet addAction:[UIAlertAction actionWithTitle:@"Camera" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action)
    {
        UIImagePickerController *picker = [[UIImagePickerController alloc]init];
        picker.allowsEditing = YES;
        picker.sourceType = UIImagePickerControllerSourceTypeCamera;
        picker.delegate = self;
        
        if (IS_IPAD)
        {
            [[NSOperationQueue mainQueue] addOperationWithBlock:^
             {
                 [self presentViewController:picker animated:YES completion:nil];
             }];
        }
        else
        {
            [self presentViewController:picker animated:YES completion:nil];
        }
    }]];
    
    [actionSheet addAction:[UIAlertAction actionWithTitle:@"Gallery" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action)
    {
        UIImagePickerController *picker = [[UIImagePickerController alloc]init];
        picker.allowsEditing = YES;
        picker.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
        picker.delegate = self;
        
        if (IS_IPAD)
        {
            [[NSOperationQueue mainQueue] addOperationWithBlock:^
             {
                 [self presentViewController:picker animated:YES completion:nil];
             }];
        }
        else
        {
            [self presentViewController:picker animated:YES completion:nil];
        }
        // OK button tapped.
       
    }]];
    
    // Present action sheet.
    [self presentViewController:actionSheet animated:YES completion:nil];
}



-(void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info
{
    UIImage *imgProfile = (UIImage *)[info objectForKey:UIImagePickerControllerEditedImage];
    imgViewProfile.image = [self cropImge:imgProfile];
    [picker dismissViewControllerAnimated:YES completion:nil];
}

-(UIImage*)cropImge:(UIImage*)image{
    
    CGSize newSize = CGSizeMake(500,500);
    UIGraphicsBeginImageContext(newSize);
    [image drawInRect:CGRectMake(0,0,newSize.width,newSize.height)];
    UIImage *newImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    return newImage;
}

- (IBAction)btnLocationPressed:(id)sender
{
    SearchLocationViewController *vc = [[SearchLocationViewController alloc] initWithNibName:@"SearchLocationViewController" bundle:nil];
    vc.delegate = self;
    [self presentViewController:vc animated:true completion:nil];
}


-(void)addKeyboardControls
{
    // Initialize the keyboard controls
    keyboardControls = [[BSKeyboardControls alloc] init];
    
    // Set the delegate of the keyboard controls
    keyboardControls.delegate = self;
    
    // Add all text fields you want to be able to skip between to the keyboard controls
    // The order of thise text fields are important. The order is used when pressing "Previous" or "Next"
    
    keyboardControls.textFields = [NSArray arrayWithObjects:txtFName,txtFEmail,txtFLocation,txtLandmark,nil];
    
    
    // Set the style of the bar. Default is UIBarStyleBlackTranslucent.
    // keyboardControls.barStyle = UIBarStyleBlackTranslucent;
    
    // Set the tint color of the "Previous" and "Next" button. Default is black.
    keyboardControls.previousNextTintColor = [UIColor blackColor];
    
    // Set the tint color of the done button. Default is a color which looks a lot like the original blue color for a "Done" butotn
    keyboardControls.doneTintColor = [UIColor colorWithRed:34.0/255.0 green:164.0/255.0 blue:255.0/255.0 alpha:1.0];
    
    // Set title for the "Previous" button. Default is "Previous".
    keyboardControls.previousTitle = @"Previous";
    
    // Set title for the "Next button". Default is "Next".
    keyboardControls.nextTitle = @"Next";
    
    // Add the keyboard control as accessory view for all of the text fields
    // Also set the delegate of all the text fields to self
    for (id textField in keyboardControls.textFields)
    {
        if ([textField isKindOfClass:[UITextField class]])
        {
            ((UITextField *) textField).inputAccessoryView = keyboardControls;
            ((UITextField *) textField).delegate = self;
        }
        
    }
}

-(void)scrollViewToCenterOfScreen:(UIView *)theView
{
    CGFloat viewCenterY = theView.center.y;
    CGRect applicationFrame = [[UIScreen mainScreen] applicationFrame];
    CGFloat availableHeight = applicationFrame.size.height - 280; // Remove area covered by keyboard
    
    CGFloat y = viewCenterY - availableHeight / 2.0;
    if (y < 0) {
        y = 0;
    }
    [scrlView setContentOffset:CGPointMake(0, y) animated:YES];
    
}

- (void)keyboardControlsDonePressed:(BSKeyboardControls *)controls
{
    [controls.activeTextField resignFirstResponder];
    [scrlView setContentOffset:CGPointMake(0, 0) animated:YES];
}

- (void)keyboardControlsPreviousNextPressed:(BSKeyboardControls *)controls withDirection:(KeyboardControlsDirection)direction andActiveTextField:(id)textField
{
    [textField becomeFirstResponder];
    [self scrollViewToCenterOfScreen:textField];
}

#pragma mark UITextFieldDelegate
- (BOOL)textFieldShouldBeginEditing:(UITextField *)textField
{
    if ([keyboardControls.textFields containsObject:textField])
        keyboardControls.activeTextField = textField;
    
    [self scrollViewToCenterOfScreen:textField];
    return YES;
    
}

-(BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [textField resignFirstResponder];
    // [controls.activeTextField resignFirstResponder];
    [scrlView setContentOffset:CGPointMake(0, 0) animated:YES];
    return YES;
}


#pragma mark iPhone
#pragma mark MFSidebarDelegate
- (void)leftSideMenuButtonPressed:(id)sender
{
    [self.menuContainerViewController toggleLeftSideMenuCompletion:^{
    }];
}

- (void)revealLeftSidebar:(id)sender
{
    NSArray *viewControllers = [[self navigationController] viewControllers];
    for( int i=0;i<[viewControllers count];i++){
        id obj=[viewControllers objectAtIndex:i];
        if([obj isKindOfClass:[HomeVC class]]){
            [[self navigationController] popToViewController:obj animated:YES];
        }
    }
    
}




-(NSString *)dateConstant
{
    NSDateFormatter* dateFormatter = [[NSDateFormatter alloc] init];
    dateFormatter.dateFormat = @"yyyyMMddHHmmss";
    return [dateFormatter stringFromDate:[NSDate date]];
}


- (void)didSelectLocation:(MKMapItem *)locationItem {
    
}


@end
