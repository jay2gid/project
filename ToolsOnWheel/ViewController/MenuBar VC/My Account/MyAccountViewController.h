//
//  MyAccountViewController.h
//  ToolsOnWheel
//
//  Created by Shikhar Khanna on 08/01/18.
//  Copyright © 2018 Kapil Goyal. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SearchLocationViewController.h"
#import "BSKeyboardControls.h"
#import "TPKeyboardAvoidingScrollView.h"

#define IS_IPAD (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)

@interface MyAccountViewController : UIViewController<UITextFieldDelegate,SearchLocationDelegate,UIImagePickerControllerDelegate,UINavigationControllerDelegate,BSKeyboardControlsDelegate,UIScrollViewDelegate>
{
    
    IBOutlet UIImageView *imgViewProfile;
    IBOutlet UITextField *txtFLocation;
    IBOutlet UITextField *txtFPhone;
    IBOutlet UITextField *txtFEmail;
    IBOutlet UITextField *txtFName;
    IBOutlet UITextField *txtLandmark;

    IBOutlet UIButton *btnUpdate;
    IBOutlet UIView *viewContainer;
    BSKeyboardControls *keyboardControls;
    UIScrollView *scrlView;
}
@end
