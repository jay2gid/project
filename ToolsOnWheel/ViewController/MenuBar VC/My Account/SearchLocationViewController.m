//
//  SearchLocationViewController.m
//  Qboy
//
//  Created by Shikhar Khanna on 16/03/17.
//  Copyright © 2017 Mac. All rights reserved.
//

#import "SearchLocationViewController.h"

@interface SearchLocationViewController ()
{
    NSMutableArray *arrLocations;
}

@end

@implementation SearchLocationViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    searchBarLocation.delegate = self;
    // Do any additional setup after loading the view from its nib.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)searchBar:(UISearchBar *)searchBar textDidChange:(NSString *)searchText
{
    NSLog(@"search Text :%@",searchText);
    
    MKLocalSearchRequest *searchRequest = [[MKLocalSearchRequest alloc] init];
    [searchRequest setNaturalLanguageQuery:searchText];
    
    // Create the local search to perform the search
    MKLocalSearch *localSearch = [[MKLocalSearch alloc] initWithRequest:searchRequest];
    [localSearch startWithCompletionHandler:^(MKLocalSearchResponse *response, NSError *error) {
        if (!error)
        {
            arrLocations = [NSMutableArray arrayWithArray:response.mapItems];
            [tblLocation reloadData];
//            for (MKMapItem *mapItem in [response mapItems]) {
//                NSLog(@"Name: %@, Placemark title: %@", [mapItem name], [[mapItem placemark] title]);
//            }
        } else {
            NSLog(@"Search Request Error: %@", [error localizedDescription]);
        }
    }];
}


-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return arrLocations.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    static NSString *CellIdentifier= @"SearchLocationTVCell";
    
    SearchLocationTVCell* cell = nil;
    cell = (SearchLocationTVCell*)[tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil)
    {
        NSString *nibNameOrNil= @"SearchLocationTVCell";
        
        NSArray *topLevelObjects = [[NSBundle mainBundle] loadNibNamed:nibNameOrNil owner:nil options:nil];
        
        for(id currentObject in topLevelObjects)
        {
            if([currentObject isKindOfClass:[SearchLocationTVCell class]])
            {
                cell = (SearchLocationTVCell*)currentObject;
                break;
            }
        }
        
        cell.contentView.layer.borderWidth = 1.0;
        cell.contentView.layer.borderColor = [UIColor colorWithRed:235/255.0 green:235/255.0 blue:235/255.0 alpha:1.0f].CGColor;
        
    }
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    
    MKMapItem *item = (MKMapItem *)[arrLocations objectAtIndex:indexPath.row];
    cell.lblLocation.text = [NSString stringWithFormat:@"%@",[item name]];
    
    return cell;
    
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    MKMapItem *item = (MKMapItem *)[arrLocations objectAtIndex:indexPath.row];

    if (self.delegate && [self.delegate respondsToSelector:@selector(didSelectLocation:)])
    {
        [self dismissViewControllerAnimated:true completion:^{
            [self.delegate didSelectLocation:item];
        }];
    }
}


- (IBAction)backBtnPressed:(id)sender
{
    [self dismissViewControllerAnimated:true completion:nil];
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
