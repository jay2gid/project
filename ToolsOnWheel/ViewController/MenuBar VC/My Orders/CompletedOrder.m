//
//  MyOrdersVC.m
//  ToolsOnWheel
//
//  Created by Shikhar Khanna on 06/01/18.
//  Copyright © 2018 Kapil Goyal. All rights reserved.
//

#import "CompletedOrder.h"
#import "CustomNavigation.h"
#import "CommonHeader.h"
#import "MyOrdersTVCell.h"
#import "HCSStarRatingView.h"
#import "Config.h"
#import "HomeVC.h"
#import "Utils.h"
#import "CompletedOrdersTVCell.h"
#import "ComplaintOrderView.h"

@interface CompletedOrder ()
{
    RatingOrder *ratingOrderView;
    ComplaintOrderView *complaintView;
    NSInteger oldRating;
    StarRatingControl *tempSRating;
}
@end

@implementation CompletedOrder
@synthesize arrCompletedOrders;


-(void)updateView
{
    if (arrCompletedOrders.count > 0)
    {
        viewNoOrders.hidden = true;
        tblViewComplete.hidden = false;
    }
    else
    {
        viewNoOrders.hidden = false;
        tblViewComplete.hidden = true;
    }
    [tblViewComplete reloadData];
    
}

#pragma mark - UITableViewDataSource
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSDateFormatter *dateFormat;
    if (!dateFormat) {
        dateFormat = [[NSDateFormatter alloc] init];
        [dateFormat setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
    }
    NSDate *workEndDate = [dateFormat dateFromString:[[arrCompletedOrders objectAtIndex:indexPath.row] objectForKey:@"workEndTime"]];
    workEndDate = [workEndDate dateByAddingTimeInterval:19800];
    
    NSDate *sixDaysAfter = [workEndDate dateByAddingTimeInterval:6*24*60*60];
    dateFormat = nil;
    if ([[NSDate date] compare:sixDaysAfter] == NSOrderedDescending)
    {
        return 350;
    }
    else
    {
        return 400;
    }
}


-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [self.arrCompletedOrders count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier= @"CompletedOrdersTVCell";
    CompletedOrdersTVCell* cell = nil;
    cell = (CompletedOrdersTVCell*)[tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil)
    {
        NSString *nibNameOrNil= @"CompletedOrdersTVCell";
        NSArray *topLevelObjects = [[NSBundle mainBundle] loadNibNamed:nibNameOrNil owner:nil options:nil];
        for(id currentObject in topLevelObjects)
        {
            if([currentObject isKindOfClass:[CompletedOrdersTVCell class]])
            {
                cell = (CompletedOrdersTVCell*)currentObject;
                break;
            }
        }
    }
    
    cell.imgBackground.layer.cornerRadius = 40.0f;
    cell.imgBackground.image = [UIImage imageNamed:@"circle.png"];
    
    if ([[arrCompletedOrders objectAtIndex:indexPath.row] objectForKey:@"rating"] == (id)[NSNull null] || [[[arrCompletedOrders objectAtIndex:indexPath.row] objectForKey:@"rating"] length] == 0 )
    {
//        cell.starView.maximumValue = 5;
//        cell.starView.minimumValue = 0;
        cell.starView.rating = 0;
        cell.starView.tintColor = [UIColor redColor];
//        cell.starView.emptyStarImage = [UIImage imageNamed:@"starG.png"];
//        cell.starView.filledStarImage = [UIImage imageNamed:@"star.png"];
        cell.starView.tag = indexPath.row;
        cell.starView.delegate = self;
        [cell.starView setUserInteractionEnabled:true];

    }
    else
    {
//        cell.starView.minimumValue = [[[arrCompletedOrders objectAtIndex:indexPath.row] objectForKey:@"rating"] floatValue];
        cell.starView.rating = [[[arrCompletedOrders objectAtIndex:indexPath.row] objectForKey:@"rating"] integerValue];
        cell.starView.tintColor = [UIColor redColor];
//        cell.starView.emptyStarImage = [UIImage imageNamed:@"starG.png"];
//        cell.starView.filledStarImage = [UIImage imageNamed:@"star.png"];
        cell.starView.tag = indexPath.row;
        [cell.starView setUserInteractionEnabled:FALSE];
        
    }
    
   
    
    NSDateFormatter *dateFormat;
    if (!dateFormat) {
         dateFormat = [[NSDateFormatter alloc] init];
        [dateFormat setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
    }
    NSDate *workEndDate = [dateFormat dateFromString:[[arrCompletedOrders objectAtIndex:indexPath.row] objectForKey:@"workEndTime"]];
    workEndDate = [workEndDate dateByAddingTimeInterval:19800];

    NSDate *sixDaysAfter = [workEndDate dateByAddingTimeInterval:6*24*60*60];
    dateFormat = nil;
    if ([[NSDate date] compare:sixDaysAfter] == NSOrderedDescending)
    {
        cell.bottomHeight.constant = 32;
        [cell.btnComplaint setHidden:YES];
    }
    else
    {
         cell.bottomHeight.constant = 84;
        [cell.btnComplaint setHidden:NO];
    }
    [cell.contentView setBackgroundColor:[UIColor clearColor]];
    [cell setBackgroundColor:[UIColor clearColor]];
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
  
    cell.viewContainer.layer.cornerRadius = 5.0f;
    cell.viewImage.layer.cornerRadius = 40.0f;

    [cell.btnCancel setHidden:YES];
    [cell.btnUpdate setHidden:YES];
   
    if ([[arrCompletedOrders objectAtIndex:indexPath.row] objectForKey:@"orderId"] != (id)[NSNull null] || [[[arrCompletedOrders objectAtIndex:indexPath.row] objectForKey:@"orderId"] length] != 0 )
    {
        cell.lblOrderId.text = [NSString stringWithFormat:@"Order Id: %@",[[arrCompletedOrders objectAtIndex:indexPath.row] objectForKey:@"orderId"]];
    }
    
    if ([[arrCompletedOrders objectAtIndex:indexPath.row] objectForKey:@"serviceId"] != (id)[NSNull null] || [[[arrCompletedOrders objectAtIndex:indexPath.row] objectForKey:@"serviceId"] length] != 0 )
    {
        cell.lblServiceId.text = [[arrCompletedOrders objectAtIndex:indexPath.row] objectForKey:@"serviceId"];

    }
    if( (([[arrCompletedOrders objectAtIndex:indexPath.row] objectForKey:@"cateName"] != (id)[NSNull null] || [[[arrCompletedOrders objectAtIndex:indexPath.row] objectForKey:@"cateName"] length] != 0 )) && (([[arrCompletedOrders objectAtIndex:indexPath.row] objectForKey:@"serviceName"] != (id)[NSNull null] || [[[arrCompletedOrders objectAtIndex:indexPath.row] objectForKey:@"serviceName"] length] != 0 )))
    {
        cell.lblTitle.text = [NSString stringWithFormat:@"%@: %@",[[arrCompletedOrders objectAtIndex:indexPath.row] objectForKey:@"cateName"],[[arrCompletedOrders objectAtIndex:indexPath.row] objectForKey:@"serviceName"]];
    }
    
    if ([[arrCompletedOrders objectAtIndex:indexPath.row] objectForKey:@"rates"] != (id)[NSNull null] || [[[arrCompletedOrders objectAtIndex:indexPath.row] objectForKey:@"rates"] length] != 0 )
    {
        cell.lblPrice.text = [NSString stringWithFormat:@"₹%@",[[arrCompletedOrders objectAtIndex:indexPath.row] objectForKey:@"rates"]];
    }
    
    if ([[arrCompletedOrders objectAtIndex:indexPath.row] objectForKey:@"serviceDate"] != (id)[NSNull null] || [[[arrCompletedOrders objectAtIndex:indexPath.row] objectForKey:@"serviceDate"] length] != 0 )
    {
        cell.lblServiceDate.text = [[arrCompletedOrders objectAtIndex:indexPath.row] objectForKey:@"serviceDate"];
    }
    if ([[arrCompletedOrders objectAtIndex:indexPath.row] objectForKey:@"serviceTime"] != (id)[NSNull null] || [[[arrCompletedOrders objectAtIndex:indexPath.row] objectForKey:@"serviceTime"] length] != 0 )
    {
        cell.lblServiceTime.text = [[arrCompletedOrders objectAtIndex:indexPath.row] objectForKey:@"serviceTime"];
    }
    if ([[arrCompletedOrders objectAtIndex:indexPath.row] objectForKey:@"quantity"] != (id)[NSNull null] || [[[arrCompletedOrders objectAtIndex:indexPath.row] objectForKey:@"quantity"] length] != 0 )
    {
        cell.lblQty.text = [NSString stringWithFormat:@"%@",[[arrCompletedOrders objectAtIndex:indexPath.row] objectForKey:@"quantity"]];
    }
    
    if ([[arrCompletedOrders objectAtIndex:indexPath.row] objectForKey:@"cateImgWhite"] != (id)[NSNull null] || [[[arrCompletedOrders objectAtIndex:indexPath.row] objectForKey:@"cateImgWhite"] length] != 0 )
    {
        [cell.imgViewLogo sd_setImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@",[[arrCompletedOrders objectAtIndex:indexPath.row] objectForKey:@"cateImgWhite"]]]];
    }
    
    cell.lblPrice.layer.cornerRadius = 10.0f;
    cell.lblPrice.layer.borderColor = [UIColor orangeColor].CGColor;
    cell.lblPrice.layer.borderWidth = 2.0f;
    
    
    cell.btnComplaint.layer.cornerRadius = 15.0f;
    
    cell.btnComplaint.tag = indexPath.row;
    [cell.btnComplaint addTarget:self action:@selector(btnComplaintPressed:) forControlEvents:UIControlEventTouchUpInside];
    
    return cell;
    
}


-(void)starRatingControl:(StarRatingControl *)control didUpdateRating:(NSUInteger)rating{
    
  
    
    int tag = (int)control.tag;

    if (rating > 3)
    {
        if ([AppDelegate checkNetwork])
        {
//            [MBProgressHUD showHUDAddedTo:[AppDelegate sharedDelegate].window animated:YES];
//            ASIFormDataRequest *request = [ASIFormDataRequest requestWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@%@",ServerAdd3,kOrderRating]]];
//            [request setDelegate:self];
//            [request setPostValue:[[arrCompletedOrders objectAtIndex:tag] objectForKey:@"serviceId"] forKey:@"orderMatrixId"];
//            [request setPostValue:[NSString stringWithFormat:@"%lu",(unsigned long)rating] forKey:@"rating"];
//            [request setPostValue:@"" forKey:@"ratingComment"];
//            [request startAsynchronous];
            
            NSDictionary *dict = @{@"orderMatrixId":[[arrCompletedOrders objectAtIndex:tag] objectForKey:@"serviceId"],
                                   @"rating":[NSString stringWithFormat:@"%lu",(unsigned long)rating],
                                   @"ratingComment":@""};
            [self ratingHud_withParam:dict];
        }
       
    }
    else if (rating > 0)
    {
        ratingOrderView = [self ratingOrderView];
        [[UIApplication sharedApplication].keyWindow addSubview:ratingOrderView];
        ratingOrderView.strServiceId = [[arrCompletedOrders objectAtIndex:tag] objectForKey:@"serviceId"];
        ratingOrderView.strRating = [NSString stringWithFormat:@"%lu",(unsigned long)rating];
        [ratingOrderView updateView];
        ratingOrderView.frame = [UIApplication sharedApplication].keyWindow.bounds;
        ratingOrderView.lblTitle.text  = @"Are you not satisfy with our service? Please let us know What issue you are facing in our service ?";
        ratingOrderView.delegate = self;
    }
}

-(void) ratingHud_withParam:(NSDictionary *)dict
{
    NSDictionary *param = dict;
    
    [Helper showLoading];
    [WebServiceCalls POST:kOrderRating parameter:param completionBlock:^(id JSON, WebServiceResult result)
     {
         @try
         {
             [Helper stopLoading];
             
             if (result == WebServiceResultSuccess)
             {
                 NSDictionary *dictMain = JSON;
                 if ([dictMain[@"status"] integerValue] == 1)
                 {
                     [Utils showAlertMessage:KMessageTitle Message:[dictMain objectForKey:@"result"]];
                     [[NSNotificationCenter defaultCenter] postNotificationName:@"MYORDER_RELOAD" object:@"2"];
                 }
                 else if(![[dictMain objectForKey:@"status"] boolValue])
                 {
                     [Utils showAlertMessage:KMessageTitle Message:[dictMain objectForKey:@"result"]];
                 }
             }
         }
         @catch (NSException *exception) { }
         @finally {
             [Helper stopLoading];
         }
     }];
}

-(void)starRatingControl:(StarRatingControl *)control willUpdateRating:(NSUInteger)rating{
    
    tempSRating =control;
    oldRating = rating;
}


//- (void)requestFinished:(ASIHTTPRequest *)request
//{
//    [MBProgressHUD hideAllHUDsForView:[AppDelegate sharedDelegate].window animated:YES];
//    NSString *receivedString = [request responseString];
//    NSLog(@"str=%@",receivedString);
//    NSDictionary *dictMain=[receivedString JSONValue];
//    if([[dictMain objectForKey:@"status"] boolValue])
//    {
//        [Utils showAlertMessage:KMessageTitle Message:[dictMain objectForKey:@"result"]];
//        [[NSNotificationCenter defaultCenter] postNotificationName:@"MYORDER_RELOAD" object:@"2"];
//    }
//    else if(![[dictMain objectForKey:@"status"] boolValue])
//    {
//        [Utils showAlertMessage:KMessageTitle Message:[dictMain objectForKey:@"result"]];
//    }
//}
//
//- (void)requestFailed:(ASIHTTPRequest *)request
//{
//    NSLog(@"Fail");
//    [MBProgressHUD hideAllHUDsForView:[AppDelegate sharedDelegate].window animated:YES];
//    // [Utils showAlertMessage:KMessageTitle Message:@"Network error"];
//}

-(void)btnComplaintPressed:(UIButton *)sender
{
    if ([[arrCompletedOrders objectAtIndex:sender.tag] objectForKey:@"complainStatus"] == (id)[NSNull null] || [[[arrCompletedOrders objectAtIndex:sender.tag] objectForKey:@"complainStatus"] length] == 0 )
    {
        complaintView = [self complaintView];
        [[UIApplication sharedApplication].keyWindow addSubview:complaintView];
        complaintView.frame = [[UIApplication sharedApplication].keyWindow bounds];
        complaintView.strServiceId = [[arrCompletedOrders objectAtIndex:sender.tag] objectForKey:@"serviceId"];
        
        [complaintView updateView];
    }
    else
    {
        [Utils showAlertMessage:KMessageTitle Message:@"Please contact customer care"];
    }
}


-(RatingOrder *)ratingOrderView
{
    NSArray *arrNibs = [NSArray arrayWithArray:[[NSBundle mainBundle] loadNibNamed:NSStringFromClass([RatingOrder class]) owner:self options:nil]];
    
    if (ratingOrderView!=nil)
    {
        [ratingOrderView removeFromSuperview];
        ratingOrderView = nil;
    }
    
    ratingOrderView = [arrNibs firstObject];
    ratingOrderView.controller = self.hostView;
    return ratingOrderView;
}

-(ComplaintOrderView *)complaintView
{
    NSArray *arrNibs = [NSArray arrayWithArray:[[NSBundle mainBundle] loadNibNamed:NSStringFromClass([ComplaintOrderView class]) owner:self options:nil]];
    
    if (complaintView!=nil)
    {
        [complaintView removeFromSuperview];
        complaintView = nil;
    }
    
    complaintView = [arrNibs firstObject];
    complaintView.controller = self.hostView;

    return complaintView;
}



- (void)exitRatingPopup {
    tempSRating.rating = [[Helper getStringORZero:arrCompletedOrders[tempSRating.tag][@"rating"]] integerValue];
}

- (void)ratingCompleted {
    
}


@end
