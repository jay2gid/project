//
//  UpdateOrderView.h
//  ToolsOnWheel
//
//  Created by Shikhar Khanna on 10/01/18.
//  Copyright © 2018 Kapil Goyal. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ASIFormDataRequest.h"
#import "JSON.h"
#import "MBProgressHUD.h"
#import "AppDelegate.h"
#import "DropDownView.h"

@interface UpdateOrderView : UIView<calenderDelegate>
{
    
    IBOutlet UIView *viewContainer;
    IBOutlet UITextField *txtDate;
    IBOutlet UITextField *txtFTime;
    IBOutlet UIButton *btnUpdate;
    IBOutlet UIButton *btnCancel;
    NSMutableArray *arrTimeSlot;
    NSMutableArray *arrDate;

    NSString *strCheck;
    IBOutlet UIButton *btnDropDown;
    UIButton *btnSender;
    IBOutlet UIView *viewTime;
    CalPopUp *calView;

}

-(void)updateView;
@property(retain, nonatomic)NSString  *strServiceId;
@property(retain, nonatomic)NSString  *strOrderDate;
@property(retain, nonatomic)NSString  *strOrderTime;

@property(retain, nonatomic)NSString  *catId;


@property(retain, nonatomic)UIViewController *controller;
@property (retain,nonatomic)DropDownView *dropDownView;

@end
