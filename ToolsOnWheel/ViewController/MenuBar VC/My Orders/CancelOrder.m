//
//  MyOrdersVC.m
//  ToolsOnWheel
//
//  Created by Shikhar Khanna on 06/01/18.
//  Copyright © 2018 Kapil Goyal. All rights reserved.
//

#import "CancelOrder.h"
#import "MyOrdersTVCell.h"
#import "Config.h"
#import "Utils.h"
#import "HomeVC.h"

@interface CancelOrder ()
{
}
@end

@implementation CancelOrder
@synthesize arrCancelOrders;


-(void)updateView
{
    if (arrCancelOrders.count > 0)
    {
        viewNoOrders.hidden = true;
        tblViewCancel.hidden = false;
    }
    else
    {
        viewNoOrders.hidden = false;
        tblViewCancel.hidden = true;
    }
    [tblViewCancel reloadData];
    
}


#pragma mark - UITableViewDataSource
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [self.arrCancelOrders count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier= @"MyOrdersTVCell";
    MyOrdersTVCell* cell = nil;
    cell = (MyOrdersTVCell*)[tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil)
    {
        NSString *nibNameOrNil= @"MyOrdersTVCell";
        
        NSArray *topLevelObjects = [[NSBundle mainBundle] loadNibNamed:nibNameOrNil owner:nil options:nil];
        
        for(id currentObject in topLevelObjects)
        {
            if([currentObject isKindOfClass:[MyOrdersTVCell class]])
            {
                cell = (MyOrdersTVCell*)currentObject;
                break;
            }
        }
    }
    
    [cell.contentView setBackgroundColor:[UIColor clearColor]];
    [cell setBackgroundColor:[UIColor clearColor]];
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
  
    cell.viewContainer.layer.cornerRadius = 5.0f;
    cell.viewImage.layer.cornerRadius = 40.0f;
    cell.imgBackground.layer.cornerRadius = 40.0f;
    cell.imgBackground.image = [UIImage imageNamed:@"circle.png"];

    
    [cell.btnCancel setHidden:YES];
    [cell.btnUpdate setHidden:YES];
    
    if ([[arrCancelOrders objectAtIndex:indexPath.row] objectForKey:@"orderId"] != (id)[NSNull null] || [[[arrCancelOrders objectAtIndex:indexPath.row] objectForKey:@"orderId"] length] != 0 )
    {
        cell.lblOrderId.text = [NSString stringWithFormat:@"Order Id: %@",[[arrCancelOrders objectAtIndex:indexPath.row] objectForKey:@"orderId"]];
    }
    
    if ([[arrCancelOrders objectAtIndex:indexPath.row] objectForKey:@"serviceId"] != (id)[NSNull null] || [[[arrCancelOrders objectAtIndex:indexPath.row] objectForKey:@"serviceId"] length] != 0 )
    {
        cell.lblServiceId.text = [[arrCancelOrders objectAtIndex:indexPath.row] objectForKey:@"serviceId"];

    }
    if( (([[arrCancelOrders objectAtIndex:indexPath.row] objectForKey:@"cateName"] != (id)[NSNull null] || [[[arrCancelOrders objectAtIndex:indexPath.row] objectForKey:@"cateName"] length] != 0 )) && (([[arrCancelOrders objectAtIndex:indexPath.row] objectForKey:@"serviceName"] != (id)[NSNull null] || [[[arrCancelOrders objectAtIndex:indexPath.row] objectForKey:@"serviceName"] length] != 0 )))
    {
        cell.lblTitle.text = [NSString stringWithFormat:@"%@: %@",[[arrCancelOrders objectAtIndex:indexPath.row] objectForKey:@"cateName"],[[arrCancelOrders objectAtIndex:indexPath.row] objectForKey:@"serviceName"]];
    }
    
    if ([[arrCancelOrders objectAtIndex:indexPath.row] objectForKey:@"rates"] != (id)[NSNull null] || [[[arrCancelOrders objectAtIndex:indexPath.row] objectForKey:@"rates"] length] != 0 )
    {
        cell.lblPrice.text = [NSString stringWithFormat:@"₹%@",[[arrCancelOrders objectAtIndex:indexPath.row] objectForKey:@"rates"]];
    }
    
    if ([[arrCancelOrders objectAtIndex:indexPath.row] objectForKey:@"serviceDate"] != (id)[NSNull null] || [[[arrCancelOrders objectAtIndex:indexPath.row] objectForKey:@"serviceDate"] length] != 0 )
    {
        cell.lblServiceDate.text = [[arrCancelOrders objectAtIndex:indexPath.row] objectForKey:@"serviceDate"];
    }
    if ([[arrCancelOrders objectAtIndex:indexPath.row] objectForKey:@"serviceTime"] != (id)[NSNull null] || [[[arrCancelOrders objectAtIndex:indexPath.row] objectForKey:@"serviceTime"] length] != 0 )
    {
        cell.lblServiceTime.text = [[arrCancelOrders objectAtIndex:indexPath.row] objectForKey:@"serviceTime"];
    }
    if ([[arrCancelOrders objectAtIndex:indexPath.row] objectForKey:@"quantity"] != (id)[NSNull null] || [[[arrCancelOrders objectAtIndex:indexPath.row] objectForKey:@"quantity"] length] != 0 )
    {
        cell.lblQty.text = [NSString stringWithFormat:@"%@",[[arrCancelOrders objectAtIndex:indexPath.row] objectForKey:@"quantity"]];
    }
   
    if ([[arrCancelOrders objectAtIndex:indexPath.row] objectForKey:@"cateImgWhite"] != (id)[NSNull null] || [[[arrCancelOrders objectAtIndex:indexPath.row] objectForKey:@"cateImgWhite"] length] != 0 )
    {
        [cell.imgViewLogo sd_setImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@",[[arrCancelOrders objectAtIndex:indexPath.row] objectForKey:@"cateImgWhite"]]]];
    }
    
    
    cell.bottomHeight.constant = 0;
    
    cell.lblPrice.layer.cornerRadius = 10.0f;
    cell.lblPrice.layer.borderColor = [UIColor orangeColor].CGColor;
    cell.lblPrice.layer.borderWidth = 2.0f;
    
    return cell;
}




@end
