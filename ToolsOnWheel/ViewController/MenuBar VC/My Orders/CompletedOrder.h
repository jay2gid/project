//
//  MyOrdersVC.h
//  ToolsOnWheel
//
//  Created by Shikhar Khanna on 06/01/18.
//  Copyright © 2018 Kapil Goyal. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ASIFormDataRequest.h"
#import "JSON.h"
#import "MBProgressHUD.h"
#import "AppDelegate.h"
#import "RatingOrder.h"


@interface CompletedOrder : UIView<UITableViewDataSource,UITableViewDelegate,StarRatingDelegate,ratingDelegaeComplete >
{
    IBOutlet UITableView *tblViewComplete;

    IBOutlet UIView *viewNoOrders;
}
@property(nonatomic,strong)id hostView;
@property(nonatomic,strong) NSMutableArray *arrCompletedOrders;
-(void)updateView;
@end
