//
//  CancelOrderView.m
//  ToolsOnWheel
//
//  Created by Shikhar Khanna on 10/01/18.
//  Copyright © 2018 Kapil Goyal. All rights reserved.
//

#import "RatingOrder.h"

#define kContainerHeight 274
#define kcommentHeight 93
#define kcommentTop 13

@implementation RatingOrder

-(void)updateView
{
    btnCancel.layer.cornerRadius = 20.0f;
    btnSubmit.layer.cornerRadius = 20.0f;
    viewContainer.layer.cornerRadius = 10.0f;
    [self resetContainer];

    arrCancelResaon = [[NSArray alloc] initWithObjects:@"Service",@"Professiomalism",@"Punctuality",@"Toolsonwheel app",@"Other", nil];
    txtViewComment.text = @"Comment";
    txtViewComment.delegate = self;
    
    viewComment.layer.borderWidth = 1.0f;
    viewComment.layer.borderColor = [UIColor colorWithRed:235/255.0 green:235/255.0 blue:235/255.0 alpha:1.0f].CGColor;
    viewComment.layer.cornerRadius = 5.0f;
}

-(void)resetContainer
{
    commentHeight.constant = 0;
    commentTop.constant = 0;
    containerHeight.constant = 171;
}

-(void)showCommentView
{
    commentHeight.constant = kcommentHeight;
    commentTop.constant = kcommentTop;
    containerHeight.constant = kContainerHeight;
}

- (BOOL)textViewShouldBeginEditing:(UITextView *)textView
{
    if (isCalender || isDropDown) {
        return NO;
    }
    
    if ([txtViewComment.text isEqualToString:@"Comment"])
    {
        txtViewComment.text = @"";
    }
    txtViewComment.textColor = [UIColor blackColor];
    return YES;
}

-(void) textViewDidChange:(UITextView *)textView
{
    
    if(txtViewComment.text.length == 0)
    {
        txtViewComment.textColor = [UIColor lightGrayColor];
        txtViewComment.text = @"Comment";
        [txtViewComment resignFirstResponder];
    }
}

#pragma mark - UIButton Actions
- (IBAction)btnSubmitPressed:(id)sender
{
    
    [self endEditing:true];
    
    NSString *strReason;
    if ([txtFReason.text isEqualToString:@"Other"])
    {
        txtViewComment.text=[txtViewComment.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
        
        if(txtViewComment.text == NULL || [txtViewComment.text length] == 0  || [txtViewComment.text.lowercaseString isEqualToString:@"comment"])
        {
            [Utils showAlertMessage:KMessageTitle Message:@"Please enter reason"];
            [txtViewComment becomeFirstResponder];
            return;
        }
        else
        {
            strReason = txtViewComment.text;
        }
    }
    else if([txtFReason.text isEqualToString:@"Select Reason"])
    {
        [Utils showAlertMessage:KMessageTitle Message:@"Please choose reason"];
        return;

    }
    else
    {
        strReason = txtFReason.text;
    }
    //update your values here
    if ([AppDelegate checkNetwork])
    {
//        [MBProgressHUD showHUDAddedTo:[AppDelegate sharedDelegate].window animated:YES];
//        ASIFormDataRequest *request = [ASIFormDataRequest requestWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@%@",ServerAdd3,kOrderRating]]];
//        [request setDelegate:self];
//        [request setPostValue:self.strServiceId forKey:@"orderMatrixId"];
//        [request setPostValue:self.strRating forKey:@"rating"];
//        [request setPostValue:strReason forKey:@"ratingComment"];
//        [request startAsynchronous];
        
        NSDictionary *param =@{@"orderMatrixId":self.strServiceId,
                               @"rating":self.strRating,
                               @"ratingComment":strReason};
        
        [Helper showLoading];
        [WebServiceCalls POST:kOrderRating parameter:param completionBlock:^(id JSON, WebServiceResult result)
         {
             @try
             {
                 [Helper stopLoading];
                 
                 if (result == WebServiceResultSuccess)
                 {
                     NSDictionary *dictMain = JSON;
                     if ([dictMain[@"status"] integerValue] == 1)
                     {
                         [self.delegate ratingCompleted];
                         [[NSNotificationCenter defaultCenter] postNotificationName:@"MYORDER_RELOAD" object:@"2"];
                         [Utils showAlertMessage:KMessageTitle Message:[dictMain objectForKey:@"result"]];
                         [self removeFromSuperview];
                     }
                     else if(![[dictMain objectForKey:@"status"] boolValue])
                     {
                         [Utils showAlertMessage:KMessageTitle Message:[dictMain objectForKey:@"result"]];
                     }
                 }
             }
             @catch (NSException *exception) { }
             @finally {
                 [Helper stopLoading];
             }
         }];
    }
}

//- (void)requestFinished:(ASIHTTPRequest *)request
//{
//    [MBProgressHUD hideAllHUDsForView:[AppDelegate sharedDelegate].window animated:YES];
//    NSString *receivedString = [request responseString];
//    NSLog(@"str=%@",receivedString);
//    NSDictionary *dictMain=[receivedString JSONValue];
//    if([[dictMain objectForKey:@"status"] boolValue])
//    {
//        [self.delegate ratingCompleted];
//        [[NSNotificationCenter defaultCenter] postNotificationName:@"MYORDER_RELOAD" object:@"2"];
//        [Utils showAlertMessage:KMessageTitle Message:[dictMain objectForKey:@"result"]];
//        [self removeFromSuperview];
//    }
//    else if(![[dictMain objectForKey:@"status"] boolValue])
//    {
//        [Utils showAlertMessage:KMessageTitle Message:[dictMain objectForKey:@"result"]];
//    }
//}
//
//- (void)requestFailed:(ASIHTTPRequest *)request
//{
//    NSLog(@"Fail");
//    [MBProgressHUD hideAllHUDsForView:[AppDelegate sharedDelegate].window animated:YES];
//    // [Utils showAlertMessage:KMessageTitle Message:@"Network error"];
//}

- (BOOL)textView:(UITextView *)textView shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text {
    
    if([text isEqualToString:@"\n"]) {
        [textView resignFirstResponder];
        return NO;
    }
    
    return YES;
}

- (IBAction)btnCancelPressed:(id)sender
{
    [_delegate exitRatingPopup];
    [self removeFromSuperview];
}

#pragma mark - DropDown delegate
- (IBAction)generateDropDown:(UIButton*)sender
{
    if (self.dropDownView)
    {
        [_dropDownView removeFromSuperview];
        _dropDownView = nil;
    }
    
    CGRect rect = [sender convertRect:sender.bounds toView:self];
    self.dropDownView = [[DropDownView alloc] initWithFrame:CGRectMake(rect.origin.x-5, rect.origin.y, rect.size.width+8, rect.size.height) target:self];
    [self.dropDownView setBackgroundColor:[UIColor clearColor]];
    [self.dropDownView setTag:sender.tag];
    
    [self downloadDropDownData:sender];
    
    if ([self.dropDownView.myDataarray count])
    {
        [self addSubview:self.dropDownView];
    }
    
    [self.dropDownView performSelector:@selector(openDropDown) withObject:nil afterDelay:0.1];
}

-(void)didSelectIndex:(int)index ForDropDown:(DropDownView*)dropdown
{
    if (dropdown.strDropDownValue)
    {
        NSArray *list = [NSArray arrayWithArray:arrCancelResaon];
        NSLog(@"selected data :%@",[list objectAtIndex:index]);
        txtFReason.text = dropdown.strDropDownValue;
        if ([txtFReason.text isEqualToString:@"Other"])
        {
            [self showCommentView];
        }
        else
        {
            [self resetContainer];
        }
    }
    [dropdown.myLabel setText:@""];
    [self.dropDownView removeFromSuperview];
}

- (void)downloadDropDownData:(UIButton*)sender
{
    NSArray *list = [NSArray arrayWithArray:arrCancelResaon];
    [self.dropDownView setDataArray:list];
}

@end
