//
//  CancelOrderView.m
//  ToolsOnWheel
//
//  Created by Shikhar Khanna on 10/01/18.
//  Copyright © 2018 Kapil Goyal. All rights reserved.
//

#import "CancelOrderView.h"
#import "Config.h"
#import "Utils.h"

#define kContainerHeight 274
#define kcommentHeight 93
#define kcommentTop 13

@implementation CancelOrderView

-(void)updateView
{
    btnCancel.layer.cornerRadius = 20.0f;
    btnSubmit.layer.cornerRadius = 20.0f;
    viewContainer.layer.cornerRadius = 10.0f;
    arrCancelResaon = [[NSArray alloc] initWithObjects:@"Change my mind",@"Service not required",@"Already done",@"Book by mistake",@"Other", nil];
   
    txtViewComment.text = @"Reason";
    txtViewComment.delegate = self;
    
    [self resetContainer];
    
    viewComment.layer.borderWidth = 1.0f;
    viewComment.layer.borderColor = [UIColor colorWithRed:235/255.0 green:235/255.0 blue:235/255.0 alpha:1.0f].CGColor;
    viewComment.layer.cornerRadius = 5.0f;
}

- (BOOL)textView:(UITextView *)textView shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text {
    
    if([text isEqualToString:@"\n"]) {
        [textView resignFirstResponder];
        return NO;
    }
    
    return YES;
}

-(void)resetContainer
{
    commentHeight.constant = 0;
    commentTop.constant = 0;
    containerHeight.constant = 171;
}

-(void)showCommentView
{
    commentHeight.constant = kcommentHeight;
    commentTop.constant = kcommentTop;
    containerHeight.constant = kContainerHeight;
}

- (BOOL) textViewShouldBeginEditing:(UITextView *)textView
{
    if ([txtViewComment.text isEqualToString:@"Reason"])
    {
        txtViewComment.text = @"";
    }
    return YES;
}

-(void) textViewDidChange:(UITextView *)textView
{
    
    if(txtViewComment.text.length == 0)
    {
        txtViewComment.text = @"Reason";
        [txtViewComment resignFirstResponder];
    }
}

#pragma mark - UIButton Actions
- (IBAction)btnSubmitPressed:(id)sender
{
    NSString *strReason;
    if ([txtFReason.text isEqualToString:@"Other"])
    {
        txtViewComment.text=[txtViewComment.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
        if(txtViewComment.text == NULL || [txtViewComment.text length] == 0 || [txtViewComment.text isEqualToString:@"Reason"])
        {
            [Utils showAlertMessage:KMessageTitle Message:@"Please enter reason"];
            [txtViewComment becomeFirstResponder];
            return;
        }
        else
        {
            strReason = txtViewComment.text;
        }
    }
    else if([txtFReason.text isEqualToString:@"Select Reason"])
    {
        [Utils showAlertMessage:KMessageTitle Message:@"Please choose reason"];
        return;

    }
    else
    {
        strReason = txtFReason.text;
    }
    //update your values here
    if ([AppDelegate checkNetwork])
    {
//        [MBProgressHUD showHUDAddedTo:[AppDelegate sharedDelegate].window animated:YES];
//        ASIFormDataRequest *request = [ASIFormDataRequest requestWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@%@",ServerAdd3,kOrderCancel]]];
//        [request setDelegate:self];
//        [request setPostValue:self.strServiceId forKey:@"orderMatrixId"];
//        [request setPostValue:@"0" forKey:@"orderStatus"];
//        [request setPostValue:strReason forKey:@"cancelReason"];
//        [request startAsynchronous];
        
        NSDictionary *param =@{@"orderMatrixId":self.strServiceId,
                               @"orderStatus":@"0",
                               @"cancelReason":strReason};
        
        [Helper showLoading];
        [WebServiceCalls POST:kOrderCancel parameter:param completionBlock:^(id JSON, WebServiceResult result)
         {
             @try
             {
                 [Helper stopLoading];
                 
                 if (result == WebServiceResultSuccess)
                 {
                     NSDictionary *dictMain = JSON;
                     if ([dictMain[@"status"] integerValue] == 1)
                     {
                         [[NSNotificationCenter defaultCenter] postNotificationName:@"MYORDER_RELOAD" object:@"3"];
                         [self performSelector:@selector(delayNotification) withObject:nil afterDelay:0.2];
                     }
                     else if(![[dictMain objectForKey:@"status"] boolValue])
                     {
                         [Utils showAlertMessage:KMessageTitle Message:[dictMain objectForKey:@"result"]];
                     }
                 }
             }
             @catch (NSException *exception) { }
             @finally {
                 [Helper stopLoading];
             }
         }];
    }
}

//- (void)requestFinished:(ASIHTTPRequest *)request
//{
//    [MBProgressHUD hideAllHUDsForView:[AppDelegate sharedDelegate].window animated:YES];
//    NSString *receivedString = [request responseString];
//    NSLog(@"str=%@",receivedString);
//    NSDictionary *dictMain=[receivedString JSONValue];
//    if([[dictMain objectForKey:@"status"] boolValue])
//    {
//        [[NSNotificationCenter defaultCenter] postNotificationName:@"MYORDER_RELOAD" object:@"3"];
//        [self performSelector:@selector(delayNotification) withObject:nil afterDelay:0.2];
//    }
//    else if(![[dictMain objectForKey:@"status"] boolValue])
//    {
//        [Utils showAlertMessage:KMessageTitle Message:[dictMain objectForKey:@"result"]];
//    }
//}
//
//- (void)requestFailed:(ASIHTTPRequest *)request
//{
//    NSLog(@"Fail");
//    [MBProgressHUD hideAllHUDsForView:[AppDelegate sharedDelegate].window animated:YES];
//    // [Utils showAlertMessage:KMessageTitle Message:@"Network error"];
//}

-(void)delayNotification
{
    [self removeFromSuperview];
}

- (IBAction)btnCancelPressed:(id)sender
{
    [self removeFromSuperview];
}

#pragma mark - DropDown delegate
- (IBAction)generateDropDown:(UIButton*)sender
{
//    if (self.dropDownView)
//    {
//        [_dropDownView removeFromSuperview];
//        _dropDownView = nil;
//    }
//
//    CGRect rect = [sender convertRect:sender.bounds toView:self];
//    self.dropDownView = [[DropDownView alloc] initWithFrame:CGRectMake(rect.origin.x-5, rect.origin.y, rect.size.width+8, rect.size.height) target:self];
//    [self.dropDownView setBackgroundColor:[UIColor clearColor]];
//    [self.dropDownView setTag:sender.tag];
//
//    [self downloadDropDownData:sender];
//
//    if ([self.dropDownView.myDataarray count])
//    {
//        [self addSubview:self.dropDownView];
//    }
//
//    [self.dropDownView performSelector:@selector(openDropDown) withObject:nil afterDelay:0.1];
    
    
    [ActionSheetStringPicker showPickerWithTitle:@"Select Reason"
                                            rows:arrCancelResaon
                                initialSelection:0
                                       doneBlock:^(ActionSheetStringPicker *picker, NSInteger selectedIndex, id selectedValue) {
                                           NSLog(@"Picker: %@, Index: %ld, value: %@",
                                                 picker, (long)selectedIndex, selectedValue);
                                           
                                           NSLog(@"%ld",(long)sender.tag);
                                           
                                           
                                           NSArray *list = [NSArray arrayWithArray:self->arrCancelResaon];
                                           NSLog(@"selected data :%@",[list objectAtIndex:selectedIndex]);
                                           self->txtFReason.text = [list objectAtIndex:selectedIndex];
                                           
                                           if ([self->txtFReason.text isEqualToString:@"Other"])
                                           {
                                               [self showCommentView];
                                           }
                                           else
                                           {
                                               [self resetContainer];
                                           }
                                       }
                                     cancelBlock:^(ActionSheetStringPicker *picker) {
                                         NSLog(@"Block Picker Canceled");
                                     }
                                          origin:sender];
}

//
//-(void)didSelectIndex:(int)index ForDropDown:(DropDownView*)dropdown
//{
//    if (dropdown.strDropDownValue)
//    {
//        NSArray *list = [NSArray arrayWithArray:arrCancelResaon];
//        NSLog(@"selected data :%@",[list objectAtIndex:index]);
//        txtFReason.text = dropdown.strDropDownValue;
//        if ([txtFReason.text isEqualToString:@"Other"])
//        {
//            [self showCommentView];
//        }
//        else
//        {
//            [self resetContainer];
//        }
//    }
//    [dropdown.myLabel setText:@""];
//    [self.dropDownView removeFromSuperview];
//}

//- (void)downloadDropDownData:(UIButton*)sender
//{
//    NSArray *list = [NSArray arrayWithArray:arrCancelResaon];
//    [self.dropDownView setDataArray:list];
//}

@end
