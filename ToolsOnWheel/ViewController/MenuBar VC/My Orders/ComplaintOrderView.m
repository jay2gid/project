//
//  ComplaintOrderView.m
//  ToolsOnWheel
//
//  Created by Shikhar Khanna on 10/01/18.
//  Copyright © 2018 Kapil Goyal. All rights reserved.
//

#import "ComplaintOrderView.h"
#import "Config.h"
#import "Utils.h"

#define kContainerHeight 274
#define kcommentHeight 93
#define kcommentTop 13

@implementation ComplaintOrderView
@synthesize strServiceId;
-(void)updateView
{
    btnCancel.layer.cornerRadius = 20.0f;
    btnSubmit.layer.cornerRadius = 20.0f;
    viewContainer.layer.cornerRadius = 10.0f;
   
    txtViewComment.text = @"Reason";
    txtViewComment.delegate = self;
    
    
    viewComment.layer.borderWidth = 1.0f;
    viewComment.layer.borderColor = [UIColor colorWithRed:235/255.0 green:235/255.0 blue:235/255.0 alpha:1.0f].CGColor;
    viewComment.layer.cornerRadius = 5.0f;
}


- (BOOL) textViewShouldBeginEditing:(UITextView *)textView
{
    if ([txtViewComment.text isEqualToString:@"Reason"])
    {
        txtViewComment.text = @"";
    }
    return YES;
}

-(void) textViewDidChange:(UITextView *)textView
{
    
    if(txtViewComment.text.length == 0)
    {
        txtViewComment.text = @"Reason";
        [txtViewComment resignFirstResponder];
    }
}

#pragma mark - UIButton Actions
- (IBAction)btnSubmitPressed:(id)sender
{
    [self endEditing:true];
    txtViewComment.text=[txtViewComment.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
    if(txtViewComment.text == NULL || [txtViewComment.text length] == 0 || [txtViewComment.text isEqualToString:@"Reason"])
    {
        [Utils showAlertMessage:KMessageTitle Message:@"Please enter reason"];
        [txtViewComment becomeFirstResponder];
        return;
    }
    else
    {
        //update your values here
        if ([AppDelegate checkNetwork])
        {
//            [MBProgressHUD showHUDAddedTo:[AppDelegate sharedDelegate].window animated:YES];
//            ASIFormDataRequest *request = [ASIFormDataRequest requestWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@%@",ServerAdd3,kOrderComplaint]]];
//            [request setDelegate:self];
//            [request setPostValue:self.strServiceId forKey:@"orderMatrixId"];
//            [request setPostValue:[[NSUserDefaults standardUserDefaults] objectForKey:@"userid"] forKey:@"userId"];
//            [request setPostValue:txtViewComment.text forKey:@"comment"];
//            [request startAsynchronous];
            
            NSDictionary *param =@{@"orderMatrixId":self.strServiceId,
                                   @"userId":[[NSUserDefaults standardUserDefaults] objectForKey:@"userid"],
                                   @"comment":txtViewComment.text};
            
            [Helper showLoading];
            [WebServiceCalls POST:kOrderComplaint parameter:param completionBlock:^(id JSON, WebServiceResult result)
             {
                 @try
                 {
                     [self endEditing:true];

                     [Helper stopLoading];
                     
                     if (result == WebServiceResultSuccess)
                     {
                         NSDictionary *dictMain = JSON;
                         if ([dictMain[@"status"] integerValue] == 1)
                         {
                             [Utils showAlertMessage:KMessageTitle Message:[dictMain objectForKey:@"result"]];
                             [[NSNotificationCenter defaultCenter] postNotificationName:@"MYORDER_RELOAD" object:@"2"];
                             [self performSelector:@selector(delayNotification) withObject:nil afterDelay:0.2];
                         }
                         else if(![[dictMain objectForKey:@"status"] boolValue])
                         {
                             [Utils showAlertMessage:KMessageTitle Message:[dictMain objectForKey:@"result"]];
                         }
                     }
                 }
                 @catch (NSException *exception) { }
                 @finally {
                     [Helper stopLoading];
                 }
             }];
        }
    }
}

//- (void)requestFinished:(ASIHTTPRequest *)request
//{
//    [self endEditing:true];
//
//    [MBProgressHUD hideAllHUDsForView:[AppDelegate sharedDelegate].window animated:YES];
//    NSString *receivedString = [request responseString];
//    NSLog(@"str=%@",receivedString);
//    NSDictionary *dictMain=[receivedString JSONValue];
//    if([[dictMain objectForKey:@"status"] boolValue])
//    {
//        [Utils showAlertMessage:KMessageTitle Message:[dictMain objectForKey:@"result"]];
//        [[NSNotificationCenter defaultCenter] postNotificationName:@"MYORDER_RELOAD" object:@"2"];
//        [self performSelector:@selector(delayNotification) withObject:nil afterDelay:0.2];
//    }
//    else if(![[dictMain objectForKey:@"status"] boolValue])
//    {
//        [Utils showAlertMessage:KMessageTitle Message:[dictMain objectForKey:@"result"]];
//    }
//}
//
//- (void)requestFailed:(ASIHTTPRequest *)request
//{
//    NSLog(@"Fail");
//    [MBProgressHUD hideAllHUDsForView:[AppDelegate sharedDelegate].window animated:YES];
//    // [Utils showAlertMessage:KMessageTitle Message:@"Network error"];
//}

-(void)delayNotification
{
    [self removeFromSuperview];
}

- (BOOL)textView:(UITextView *)textView shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text {
    
    if([text isEqualToString:@"\n"]) {
        [textView resignFirstResponder];
        return NO;
    }
    
    return YES;
}

- (IBAction)btnCancelPressed:(id)sender
{
    [self removeFromSuperview];
}


@end
