//
//  CancelOrderView.h
//  ToolsOnWheel
//
//  Created by Shikhar Khanna on 10/01/18.
//  Copyright © 2018 Kapil Goyal. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ASIFormDataRequest.h"
#import "JSON.h"
#import "MBProgressHUD.h"
#import "AppDelegate.h"
#import "DropDownView.h"


@protocol ratingDelegaeComplete <NSObject>

-(void)ratingCompleted;
-(void)exitRatingPopup;

@end

@interface RatingOrder : UIView<UITextViewDelegate>
{
    IBOutlet NSLayoutConstraint *containerHeight;
    IBOutlet NSLayoutConstraint *commentHeight;
    IBOutlet NSLayoutConstraint *commentTop;
    IBOutlet UIView *viewComment;
    IBOutlet UIView *viewContainer;
    IBOutlet UITextField *txtFReason;
    IBOutlet UIButton *btnSubmit;
    IBOutlet UIButton *btnCancel;
    NSArray *arrCancelResaon;
    IBOutlet UITextView *txtViewComment;
}
@property (strong, nonatomic) IBOutlet UILabel *lblTitle;

@property (strong, nonatomic) id<ratingDelegaeComplete> delegate;

-(void)updateView;
@property(retain, nonatomic)NSString  *strServiceId;
@property(retain, nonatomic)NSString  *strRating;

@property(retain, nonatomic)UIViewController *controller;
@property (retain,nonatomic)DropDownView *dropDownView;

@end
//- (void)selectTabWithNumberFive
//{
//    int tabSelect=0;
//    if (tab==0 || tab==2)
//    {
//        tabSelect = 1;
//    }
//    else if (tab==1)
//    {
//        tabSelect = 0;
//    }
//    if (tab==3)
//    {
//        tabSelect = 2;
//    }
//    [self selectTabAtIndex:tabSelect];
//}
//- (void)loadContent {
//    self.numberOfTabs = 3;
//    //    [self selectTabAtIndex:1];
//    [self selectTabWithNumberFive];
//}

