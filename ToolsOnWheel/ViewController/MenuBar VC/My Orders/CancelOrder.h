//
//  MyOrdersVC.h
//  ToolsOnWheel
//
//  Created by Shikhar Khanna on 06/01/18.
//  Copyright © 2018 Kapil Goyal. All rights reserved.
//

#import <UIKit/UIKit.h>


@interface CancelOrder : UIView<UITableViewDelegate,UITableViewDataSource>
{
    IBOutlet UIView *viewNoOrders;
    IBOutlet UITableView *tblViewCancel;
}
@property(nonatomic,strong)id hostView;
@property(nonatomic,strong) NSMutableArray *arrCancelOrders;
-(void)updateView;
@end
