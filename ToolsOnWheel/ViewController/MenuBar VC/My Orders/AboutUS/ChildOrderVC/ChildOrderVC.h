//
//  LoginSignUpVC.h
//  Demo Project
//
//  Created by Shikhar Khanna on 06/08/15.
//  Copyright (c) 2015 Shikhar Khanna. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "HomeVC.h"
#import "Config.h"

#import "ViewPagerController.h"

@interface ChildOrderVC : ViewPagerController<ViewPagerDataSource,ViewPagerDelegate,mainDelegate,UIScrollViewDelegate>
{
    NSMutableArray *arrUpcomingOrders;
    NSMutableArray *arrCompletedOrders;
    NSMutableArray *arrCancelOrders;
    
    NSString *strOrderStatus;
    int tab;
}
-(void)update;
@property (nonatomic) NSUInteger numberOfTabs;
@property(nonatomic,strong)id hostView1;

@end
