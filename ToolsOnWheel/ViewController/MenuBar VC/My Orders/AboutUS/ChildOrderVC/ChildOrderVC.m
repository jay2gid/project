//
//  LoginSignUpVC.m
//  Demo Project
//
//  Created by Shikhar Khanna on 06/08/15.
//  Copyright (c) 2015 Shikhar Khanna. All rights reserved.
//

#import "ChildOrderVC.h"
#import "UpComingOrderVC.h"
#import "CompletedOrder.h"
#import "CancelOrder.h"

#import "CommonHeader.h"
#import "CustomNavigation.h"
#import "MFSideMenu.h"
#import "Utils.h"

@interface ChildOrderVC ()
{
    UpComingOrderVC *upcomingOrder;
    CompletedOrder *completeOrder;
    CancelOrder *cancelOrder;
    
    IBOutlet UIView *viewSelector;
    IBOutlet UIScrollView *mainScrol;
    IBOutlet UIButton *btnUpcomming;
    IBOutlet UIButton *btnComplete;
    IBOutlet UIButton *btnCancel;
    
    float scrollHeight;
}

@end


@implementation ChildOrderVC
@synthesize hostView1;
 
- (void)viewDidLoad
{
    [super viewDidLoad];

    arrCancelOrders = [[NSMutableArray alloc] init];
    arrUpcomingOrders = [[NSMutableArray alloc] init];
    arrCompletedOrders  = [[NSMutableArray alloc] init];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(myorder_reload:) name:@"MYORDER_RELOAD" object:nil];

    [self update];

    [AppDelegate sharedDelegate].mainDelegate = self;
    
    scrollHeight =mainScrol.frame.size.height;

    mainScrol.delegate = self;
    mainScrol.contentSize = CGSizeMake(self.view.frame.size.width*3, scrollHeight);
    mainScrol.contentOffset = CGPointMake(WIDTH, 0);
    viewSelector.frame = btnComplete.frame;
    
//    cancelOrder = [self orderCancel];
//    upcomingOrder = [self orderUpcoming];
//    completeOrder = [self orderComplete];
}

-(void)refreshOrders{
    cancelOrder = [self orderCancel];
    upcomingOrder = [self orderUpcoming];
    completeOrder = [self orderComplete];
}

-(void)update{
    if ([AppDelegate checkNetwork])
    {
//        [MBProgressHUD showHUDAddedTo:[AppDelegate sharedDelegate].window animated:YES];
//        ASIFormDataRequest *request = [ASIFormDataRequest requestWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@%@",ServerAdd3,kListOfOrders]]];
//        [request setDelegate:self];
//        [request setPostValue:[[NSUserDefaults standardUserDefaults] objectForKey:@"userid"] forKey:@"userId"];
//        [request startAsynchronous];
        
        [self listOfOrdersHud];
    }
}

//-(void)viewDidAppear:(BOOL)animated{
//
////    [super viewDidAppear:true];
////
////    if ([AppDelegate checkNetwork])
////    {
////        [MBProgressHUD showHUDAddedTo:[AppDelegate sharedDelegate].window animated:YES];
////        ASIFormDataRequest *request = [ASIFormDataRequest requestWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@%@",ServerAdd3,kListOfOrders]]];
////        [request setDelegate:self];
////        [request setPostValue:[[NSUserDefaults standardUserDefaults] objectForKey:@"userid"] forKey:@"userId"];
////        [request startAsynchronous];
////    }
//}

-(void)viewWillAppear:(BOOL)animated{
    
    if ([AppDelegate checkNetwork])
    {
//        [MBProgressHUD showHUDAddedTo:[AppDelegate sharedDelegate].window animated:YES];
//        ASIFormDataRequest *request = [ASIFormDataRequest requestWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@%@",ServerAdd3,kListOfOrders]]];
//        [request setDelegate:self];
//        [request setPostValue:[[NSUserDefaults standardUserDefaults] objectForKey:@"userid"] forKey:@"userId"];
//        [request startAsynchronous];
        
        [self listOfOrdersHud];
    }
}

-(void)didBecomeActive{
    
    if ([AppDelegate checkNetwork])
    {
//        [MBProgressHUD showHUDAddedTo:[AppDelegate sharedDelegate].window animated:YES];
//        ASIFormDataRequest *request = [ASIFormDataRequest requestWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@%@",ServerAdd3,kListOfOrders]]];
//        [request setDelegate:self];
//        [request setPostValue:[[NSUserDefaults standardUserDefaults] objectForKey:@"userid"] forKey:@"userId"];
//        [request startAsynchronous];
        
        [self listOfOrdersHud];
    }
}

-(void)myorder_reload:(NSNotification*)objNoti
{
    NSString *strtab = [objNoti object];
    tab = [strtab intValue];
    arrCancelOrders = [[NSMutableArray alloc] init];
    arrUpcomingOrders = [[NSMutableArray alloc] init];
    arrCompletedOrders  = [[NSMutableArray alloc] init];
    if ([AppDelegate checkNetwork])
    {
//        [MBProgressHUD showHUDAddedTo:[AppDelegate sharedDelegate].window animated:YES];
//        ASIFormDataRequest *request = [ASIFormDataRequest requestWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@%@",ServerAdd3,kListOfOrders]]];
//        [request setDelegate:self];
//        [request setPostValue:[[NSUserDefaults standardUserDefaults] objectForKey:@"userid"] forKey:@"userId"];
//        [request startAsynchronous];
        
        [self listOfOrdersHud];
    }
   
}

-(void) listOfOrdersHud
{
    NSDictionary *param =@{@"userId":[[NSUserDefaults standardUserDefaults] objectForKey:@"userid"]};
    
    [Helper showLoading];
    [WebServiceCalls POST:kListOfOrders parameter:param completionBlock:^(id JSON, WebServiceResult result)
     {
         @try
         {
             [Helper stopLoading];
             
             if (result == WebServiceResultSuccess)
             {
                 NSDictionary *dictMain = JSON;
                 if ([dictMain[@"status"] integerValue] == 1)
                 {
                     
                     NSMutableArray *arrOrders = [dictMain objectForKey:@"result"];
                     self->arrUpcomingOrders = [NSMutableArray array];
                     self->arrCancelOrders = [NSMutableArray array];
                     self->arrCompletedOrders = [NSMutableArray array];
                     
                     for (int i=0; i<[arrOrders count]; i++)
                     {
                         id str = [[arrOrders objectAtIndex:i] objectForKey:@"itemStatus"];
                         if (str != (id)[NSNull null])
                         {
                             if ([[[arrOrders objectAtIndex:i] objectForKey:@"itemStatus"] isEqualToString:@"1"])
                             {
                                 [self->arrUpcomingOrders addObject:[arrOrders objectAtIndex:i] ];
                             }
                             else if ([[[arrOrders objectAtIndex:i] objectForKey:@"itemStatus"] isEqualToString:@"0"])
                             {
                                 [self->arrCancelOrders addObject:[arrOrders objectAtIndex:i] ];
                             }
                             else if ([[[arrOrders objectAtIndex:i] objectForKey:@"itemStatus"] isEqualToString:@"2"])
                             {
                                 [self->arrCompletedOrders addObject:[arrOrders objectAtIndex:i] ];
                             }
                         }
                     }
                 }
                 else if(![[dictMain objectForKey:@"status"] boolValue])
                 {
                     //[Utils showAlertMessage:KMessageTitle Message:[dictMain objectForKey:@"result"]];
                 }
                 self.dataSource = self;
                 self.delegate = self;
                 
                 [self refreshOrders];
             }
         }
         @catch (NSException *exception) { }
         @finally {
             [Helper stopLoading];
         }
     }];
}

//- (void)requestFinished:(ASIHTTPRequest *)request
//{
//    [MBProgressHUD hideAllHUDsForView:[AppDelegate sharedDelegate].window animated:YES];
//    NSString *receivedString = [request responseString];
//    NSLog(@"str=%@",receivedString);
//    NSDictionary *dictMain=[receivedString JSONValue];
//    if([[dictMain objectForKey:@"status"] boolValue])
//    {
//
//        NSMutableArray *arrOrders = [dictMain objectForKey:@"result"];
//        arrUpcomingOrders = [NSMutableArray array];
//        arrCancelOrders = [NSMutableArray array];
//        arrCompletedOrders = [NSMutableArray array];
//
//        for (int i=0; i<[arrOrders count]; i++)
//        {
//            id str = [[arrOrders objectAtIndex:i] objectForKey:@"itemStatus"];
//            if (str != (id)[NSNull null])
//            {
//                if ([[[arrOrders objectAtIndex:i] objectForKey:@"itemStatus"] isEqualToString:@"1"])
//                {
//                    [arrUpcomingOrders addObject:[arrOrders objectAtIndex:i] ];
//                }
//                else if ([[[arrOrders objectAtIndex:i] objectForKey:@"itemStatus"] isEqualToString:@"0"])
//                {
//                    [arrCancelOrders addObject:[arrOrders objectAtIndex:i] ];
//                }
//                else if ([[[arrOrders objectAtIndex:i] objectForKey:@"itemStatus"] isEqualToString:@"2"])
//                {
//                    [arrCompletedOrders addObject:[arrOrders objectAtIndex:i] ];
//                }
//            }
//
//        }
//
//    }
//    else if(![[dictMain objectForKey:@"status"] boolValue])
//    {
//        //[Utils showAlertMessage:KMessageTitle Message:[dictMain objectForKey:@"result"]];
//    }
//    self.dataSource = self;
//    self.delegate = self;
//
//    [self refreshOrders];
//
//    // [self performSelector:@selector(loadContent) withObject:nil afterDelay:0.01];
//
/////    BOOL tempFlag;
//
//}
//
//- (void)requestFailed:(ASIHTTPRequest *)request
//{
//    NSLog(@"Fail");
//    [MBProgressHUD hideAllHUDsForView:[AppDelegate sharedDelegate].window animated:YES];
//    // [Utils showAlertMessage:KMessageTitle Message:@"Network error"];
//}

-(UpComingOrderVC *)orderUpcoming
{
    upcomingOrder = nil;
    NSArray *topLevelObjects = [[NSBundle mainBundle] loadNibNamed:@"UpComingOrderVC" owner:nil options:nil];
    
    for(id currentObject in topLevelObjects)
    {
        if([currentObject isKindOfClass:[UpComingOrderVC class]])
        {
            upcomingOrder = (UpComingOrderVC*)currentObject;
            break;
        }
    }
   
    upcomingOrder.hostView = self.hostView1;
    [upcomingOrder setFrame:CGRectMake(0, 0, WIDTH, scrollHeight)];
    upcomingOrder.arrUpcomingOrders = arrUpcomingOrders;
    [upcomingOrder.tblViewUpcoming reloadData];
    [upcomingOrder updateView];
    [mainScrol addSubview:upcomingOrder];
    
    
    return upcomingOrder;
}

-(CompletedOrder *)orderComplete
{
    completeOrder = nil;
    NSArray *topLevelObjects = [[NSBundle mainBundle] loadNibNamed:@"CompletedOrder" owner:nil options:nil];
    
    for(id currentObject in topLevelObjects)
    {
        if([currentObject isKindOfClass:[CompletedOrder class]])
        {
            completeOrder = (CompletedOrder*)currentObject;
            break;
        }
    }
    
    completeOrder.hostView = self.hostView1;
    [completeOrder setFrame:CGRectMake(WIDTH, 0,WIDTH, scrollHeight)];
    completeOrder.arrCompletedOrders = arrCompletedOrders;
    
    [mainScrol addSubview:completeOrder];
    [completeOrder updateView];

    return completeOrder;
}


-(CancelOrder *)orderCancel
{
    cancelOrder = nil;
    NSArray *topLevelObjects = [[NSBundle mainBundle] loadNibNamed:@"CancelOrder" owner:nil options:nil];
    for(id currentObject in topLevelObjects)
    {
        if([currentObject isKindOfClass:[CancelOrder class]])
        {
            cancelOrder = (CancelOrder*)currentObject;
            break;
        }
    }
    cancelOrder.hostView = self.hostView1;
    [cancelOrder  setFrame:CGRectMake(WIDTH*2, 0, WIDTH, scrollHeight)];
    cancelOrder.arrCancelOrders = arrCancelOrders;
    [mainScrol addSubview:cancelOrder];
    
    [cancelOrder updateView];
    return cancelOrder;
}


#pragma mark - Setters
//- (void)setNumberOfTabs:(NSUInteger)numberOfTabs
//{
//    // Set numberOfTabs
//    _numberOfTabs = 3;
//    // Reload data
//    [self reloadData];
//}
//
//#pragma mark - Helpers
//- (void)selectTabWithNumberFive
//{
//    int tabSelect=0;
//    if (tab==0 || tab==2)
//    {
//        tabSelect = 1;
//    }
//    else if (tab==1)
//    {
//        tabSelect = 0;
//    }
//    if (tab==3)
//    {
//        tabSelect = 2;
//    }
//    [self selectTabAtIndex:tabSelect];
//}
//
//
//- (void)loadContent {
//    self.numberOfTabs = 3;
//    //    [self selectTabAtIndex:1];
//    [self selectTabWithNumberFive];
//}
//
//#pragma mark - Interface Orientation Changes
//- (void)willRotateToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation duration:(NSTimeInterval)duration {
//
//    // Update changes after screen rotates
//    [self performSelector:@selector(setNeedsReloadOptions) withObject:nil afterDelay:duration];
//}
//
//#pragma mark - ViewPagerDataSource
//- (NSUInteger)numberOfTabsForViewPager:(ViewPagerController *)viewPager {
//    return self.numberOfTabs;
//}
//- (UIView *)viewPager:(ViewPagerController *)viewPager viewForTabAtIndex:(NSUInteger)index
//{
//
//    NSArray *arrTabs = [NSArray arrayWithObjects:@"UPCOMING",@"COMPLETED",@"CANCELLED",nil];
//    UILabel *label = [UILabel new];
//    CGRect rect = label.frame;
//    if (IS_IPAD)
//    {
//        rect.size.width = 384;
//        rect.size.height = 70.0;
////        label.font = [UIFont fontWithName:@"JosefinSans" size:30.0];
//    }
//    else
//    {
//        rect.size.width = 180;
//        rect.size.height = 35.0;
////        label.font = [UIFont fontWithName:@"JosefinSans" size:15.0];
//    }
//
//    [label setFrame:rect];
//    [label setNumberOfLines:3];
//    [label setLineBreakMode:NSLineBreakByWordWrapping];
//    label.font = [UIFont fontWithName:@"GothamMedium" size:14];
//    label.text = [arrTabs objectAtIndex:index];
//    label.textAlignment = NSTextAlignmentCenter;
//    label.textColor = [UIColor whiteColor];
//    return label;
//}
//
//- (UIViewController *)viewPager:(ViewPagerController *)viewPager contentViewControllerForTabAtIndex:(NSUInteger)index {
//
//    NSArray *arrViewSelectors = @[@"orderUpcoming",@"orderComplete",@"orderCancel"];
//    UIViewController *contentVC = [[UIViewController alloc] init];
//
//    SEL sel = NSSelectorFromString([arrViewSelectors objectAtIndex:index]);
//    UIView *aView = [self performSelector:sel withObject:nil];
//    [contentVC.view setBackgroundColor:[UIColor clearColor]];
//    // [aView setBackgroundColor:[UIColor greenColor]];
//
//    //[aView setBackgroundColor:[UIColor greenColor]];
//    [contentVC.view setFrame:aView.frame];
//    [contentVC.view addSubview:aView];
//    return contentVC;
//}
//
//#pragma mark - ViewPagerDelegate
//- (void)viewPager:(ViewPagerController *)viewPager didChangeTabToIndex:(NSUInteger)index {
//        NSLog(@"index=%lu",(unsigned long)index);
//}
//
//- (CGFloat)viewPager:(ViewPagerController *)viewPager valueForOption:(ViewPagerOption)option withDefault:(CGFloat)value {
//
//    NSLog(@"%s",__PRETTY_FUNCTION__);
//    switch (option) {
//        case ViewPagerOptionStartFromSecondTab:
//            return 0.0;
//        case ViewPagerOptionCenterCurrentTab:
//            return 1.0;
//        case ViewPagerOptionTabLocation:
//            return 1.0;
//        case ViewPagerOptionTabHeight:
//            if (IS_IPAD)
//                return 70.0;
//            else
//                return 35.0;
//        case ViewPagerOptionTabOffset:
//            return 36.0;
//        case ViewPagerOptionTabWidth:
//            if (IS_IPAD)
//                return 384;
//            else
//                return [UIScreen mainScreen].bounds.size.width/3;
//        case ViewPagerOptionFixFormerTabsPositions:
//            return 0.0;
//        case ViewPagerOptionFixLatterTabsPositions:
//            return 1.0;
//        default:
//            return value;
//    }
//}
//
//- (UIColor *)viewPager:(ViewPagerController *)viewPager colorForComponent:(ViewPagerComponent)component withDefault:(UIColor *)color {
//
//    switch (component) {
//        case ViewPagerIndicator:
//            return [UIColor clearColor];
//        case ViewPagerTabsView:
//            return [UIColor colorWithRed:152/255.0 green:186/255.0 blue:214/255.0 alpha:1.0];
//        case ViewPagerContent:
//            return [UIColor clearColor];
//        default:
//            return color;
//    }
//}


- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

- (IBAction)tapHeaderButton:(UIButton *)sender {
    [mainScrol setContentOffset:CGPointMake(self.view.frame.size.width * sender.tag, 0) animated:true];
}

-(void)scrollViewDidScroll:(UIScrollView *)scrollView{
    viewSelector.frame = CGRectMake(scrollView.contentOffset.x/3, 0, btnCancel.frame.size.width, 39);
}
@end
