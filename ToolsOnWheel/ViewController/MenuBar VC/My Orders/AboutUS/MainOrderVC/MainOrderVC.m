//
//  ContentViewController.m
//  Scoo Talks
//
//  Created by Sheetal on 4/30/15.
//  Copyright (c) 2015 Dotsquares. All rights reserved.
//

#import "MainOrderVC.h"

#import "ASIFormDataRequest.h"
#import "JSON.h"
#import "MBProgressHUD.h"
#import "AppDelegate.h"
#import "HomeVC.h"
#import "Utils.h"
#import "Config.h"


#import "CommonHeader.h"
#import "CustomNavigation.h"
#import "MFSideMenu.h"
#import "ChildOrderVC.h"


#import "UpComingOrderVC.h"
#import "CompletedOrder.h"
#import "CancelOrder.h"

#define IsRunningTallPhone() ([UIDevice currentDevice].userInterfaceIdiom == UIUserInterfaceIdiomPhone && [UIScreen mainScreen].bounds.size.height == 568)


@interface MainOrderVC ()
{
    //ChildOrderVC *parentController;
    IBOutlet UIView *viewSelector;
    IBOutlet UIScrollView *mainScrol;
    IBOutlet UIButton *btnUpcomming;
    IBOutlet UIButton *btnComplete;
    IBOutlet UIButton *btnCancel;
    
    float scrollHeight;
    
    NSMutableArray *arrUpcomingOrders;
    NSMutableArray *arrCompletedOrders;
    NSMutableArray *arrCancelOrders;
    
    NSString *strOrderStatus;
    
    UpComingOrderVC *upcomingOrder;
    CompletedOrder *completeOrder;
    CancelOrder *cancelOrder;
    
}

@end

@implementation MainOrderVC

- (void)viewDidLoad
{
    [super viewDidLoad];
    [self performSelector:@selector(updateUI) withObject:nil afterDelay:0.01];
//
//    parentController = [[ChildOrderVC alloc] initWithNibName:@"ChildOrderVC" bundle:nil];
//    parentController.hostView1 = self;
//    CGRect rect = parentController.view.frame;
//    rect.origin.y = 64;
//    parentController.view.frame = rect;
//    [self.view addSubview:parentController.view];
    
    [self didBecomeActive];
    
    [AppDelegate sharedDelegate].mainDelegate = self;
    
    
    mainScrol.delegate = self;
    mainScrol.contentSize = CGSizeMake(WIDTH*3, scrollHeight);
    mainScrol.contentOffset = CGPointMake(WIDTH, 0);
    viewSelector.frame = btnComplete.frame;
    
    scrollHeight = HEIGHT - 64 ;

    isCalender = false;
    isDropDown = false;
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(myorder_reload:) name:@"MYORDER_RELOAD" object:nil];

}
-(void)myorder_reload:(NSNotification*)objNoti{
   
    NSString *strtab = [objNoti object];
    
    int tabValue = [strtab intValue];
    
    
    [mainScrol setContentOffset:CGPointMake(WIDTH*(tabValue-1), 0) animated:YES];
    [self didBecomeActive];

}

-(void)viewWillAppear:(BOOL)animated
{
//    [self.menuContainerViewController setPanMode:MFSideMenuPanModeNone];
//
//    if (parentController){
//        [parentController update];
//    }
    
}

-(void)updateUI
{
    [CustomNavigation addTarget:self backRequired:NO title:@"MY ORDERS"];
    for (UIView *v1 in self.view.subviews)
    {
        if ([v1 isKindOfClass:[CommonHeader class]])
        {
            [v1 setBackgroundColor:[UIColor whiteColor]];
            for (UIView *viewTmp in v1.subviews)
            {
                if ([viewTmp isKindOfClass:[UIButton class]])
                {
                    if (viewTmp.tag==1)
                    {
                        viewTmp.hidden=YES;
                    }
                    else if(viewTmp.tag==106)
                    {
                        [(UIButton*)viewTmp addTarget:self action:@selector(revealLeftSidebar:) forControlEvents:UIControlEventTouchUpInside];
                    }
                    else if(viewTmp.tag==102)
                    {
                       [(UIImageView*)viewTmp setHidden:YES];
                    }
                }
                else if ([viewTmp isKindOfClass:[UIImageView class]])
                {
                    if (viewTmp.tag==101)
                    {
                        [(UIImageView*)viewTmp setImage:[UIImage imageNamed:@"backP.png"]];
                    }
                }
            }
        }
    }
}


#pragma mark MFSidebarDelegate
- (void)leftSideMenuButtonPressed:(id)sender
{
    [self.menuContainerViewController toggleLeftSideMenuCompletion:^{
    }];
}

- (void)revealLeftSidebar:(id)sender
{
    NSArray *viewControllers = [[self navigationController] viewControllers];
    for( int i=0;i<[viewControllers count];i++){
        id obj=[viewControllers objectAtIndex:i];
        if([obj isKindOfClass:[HomeVC class]]){
            [[self navigationController] popToViewController:obj animated:YES];
        }
    }
}

- (void)didBecomeActive
{
    if ([AppDelegate checkNetwork])
    {
//        [MBProgressHUD showHUDAddedTo:[AppDelegate sharedDelegate].window animated:YES];
//        ASIFormDataRequest *request = [ASIFormDataRequest requestWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@%@",ServerAdd3,kListOfOrders]]];
//        [request setDelegate:self];
//        [request setPostValue:[[NSUserDefaults standardUserDefaults] objectForKey:@"userid"] forKey:@"userId"];
//        [request startAsynchronous];
        
        NSDictionary *param =@{@"userId":[[NSUserDefaults standardUserDefaults] objectForKey:@"userid"]};
        
        [Helper showLoading];
        [WebServiceCalls POST:kListOfOrders parameter:param completionBlock:^(id JSON, WebServiceResult result)
         {
             @try
             {
                 [Helper stopLoading];
                 
                 if (result == WebServiceResultSuccess)
                 {
                     NSDictionary *dictMain = JSON;
                     if ([dictMain[@"status"] integerValue] == 1)
                     {
                         
                         NSMutableArray *arrOrders = [dictMain objectForKey:@"result"];
                         self->arrUpcomingOrders = [NSMutableArray array];
                         self->arrCancelOrders = [NSMutableArray array];
                         self->arrCompletedOrders = [NSMutableArray array];
                         
                         for (int i=0; i<[arrOrders count]; i++)
                         {
                             id str = [[arrOrders objectAtIndex:i] objectForKey:@"itemStatus"];
                             if (str != (id)[NSNull null])
                             {
                                 if ([[[arrOrders objectAtIndex:i] objectForKey:@"itemStatus"] isEqualToString:@"1"])
                                 {
                                     [self->arrUpcomingOrders addObject:[arrOrders objectAtIndex:i] ];
                                 }
                                 else if ([[[arrOrders objectAtIndex:i] objectForKey:@"itemStatus"] isEqualToString:@"0"])
                                 {
                                     [self->arrCancelOrders addObject:[arrOrders objectAtIndex:i] ];
                                 }
                                 else if ([[[arrOrders objectAtIndex:i] objectForKey:@"itemStatus"] isEqualToString:@"2"])
                                 {
                                     [self->arrCompletedOrders addObject:[arrOrders objectAtIndex:i] ];
                                 }
                             }
                             
                         }
                         
                     }
                     else if(![[dictMain objectForKey:@"status"] boolValue])
                     {
                         //[Utils showAlertMessage:KMessageTitle Message:[dictMain objectForKey:@"result"]];
                     }
                     
                     [self refreshOrders];
                 }
             }
             @catch (NSException *exception) { }
             @finally {
                 [Helper stopLoading];
             }
         }];
    }
}

//- (void)requestFinished:(ASIHTTPRequest *)request
//{
//    [MBProgressHUD hideAllHUDsForView:[AppDelegate sharedDelegate].window animated:YES];
//    NSString *receivedString = [request responseString];
//    NSLog(@"str=%@",receivedString);
//    NSDictionary *dictMain=[receivedString JSONValue];
//    if([[dictMain objectForKey:@"status"] boolValue])
//    {
//
//        NSMutableArray *arrOrders = [dictMain objectForKey:@"result"];
//        arrUpcomingOrders = [NSMutableArray array];
//        arrCancelOrders = [NSMutableArray array];
//        arrCompletedOrders = [NSMutableArray array];
//
//        for (int i=0; i<[arrOrders count]; i++)
//        {
//            id str = [[arrOrders objectAtIndex:i] objectForKey:@"itemStatus"];
//            if (str != (id)[NSNull null])
//            {
//                if ([[[arrOrders objectAtIndex:i] objectForKey:@"itemStatus"] isEqualToString:@"1"])
//                {
//                    [arrUpcomingOrders addObject:[arrOrders objectAtIndex:i] ];
//                }
//                else if ([[[arrOrders objectAtIndex:i] objectForKey:@"itemStatus"] isEqualToString:@"0"])
//                {
//                    [arrCancelOrders addObject:[arrOrders objectAtIndex:i] ];
//                }
//                else if ([[[arrOrders objectAtIndex:i] objectForKey:@"itemStatus"] isEqualToString:@"2"])
//                {
//                    [arrCompletedOrders addObject:[arrOrders objectAtIndex:i] ];
//                }
//            }
//
//        }
//
//    }
//    else if(![[dictMain objectForKey:@"status"] boolValue])
//    {
//        //[Utils showAlertMessage:KMessageTitle Message:[dictMain objectForKey:@"result"]];
//    }
//
//
//    [self refreshOrders];
//}
//
//- (void)requestFailed:(ASIHTTPRequest *)request
//{
//    NSLog(@"Fail");
//    [MBProgressHUD hideAllHUDsForView:[AppDelegate sharedDelegate].window animated:YES];
//}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

- (IBAction)backBtnPressed:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}

-(void)refreshOrders{
    cancelOrder = [self orderCancel];
    upcomingOrder = [self orderUpcoming];
    completeOrder = [self orderComplete];
}

-(UpComingOrderVC *)orderUpcoming
{
    [upcomingOrder removeFromSuperview];

    upcomingOrder = nil;
    NSArray *topLevelObjects = [[NSBundle mainBundle] loadNibNamed:@"UpComingOrderVC" owner:nil options:nil];
    
    for(id currentObject in topLevelObjects)
    {
        if([currentObject isKindOfClass:[UpComingOrderVC class]])
        {
            upcomingOrder = (UpComingOrderVC*)currentObject;
            break;
        }
    }
    
    upcomingOrder.hostView = self;
    [upcomingOrder setFrame:CGRectMake(0, 0, WIDTH, scrollHeight)];
    upcomingOrder.arrUpcomingOrders = arrUpcomingOrders;
    [upcomingOrder.tblViewUpcoming reloadData];
    [upcomingOrder updateView];
    [mainScrol addSubview:upcomingOrder];
    
    
    return upcomingOrder;
}

-(CompletedOrder *)orderComplete
{
    [completeOrder removeFromSuperview];
    completeOrder = nil;
    NSArray *topLevelObjects = [[NSBundle mainBundle] loadNibNamed:@"CompletedOrder" owner:nil options:nil];
    
    for(id currentObject in topLevelObjects)
    {
        if([currentObject isKindOfClass:[CompletedOrder class]])
        {
            completeOrder = (CompletedOrder*)currentObject;
            break;
        }
    }
    
    completeOrder.hostView = self;
    [completeOrder setFrame:CGRectMake(WIDTH, 0,WIDTH, scrollHeight )];
    completeOrder.arrCompletedOrders = arrCompletedOrders;
    
    [mainScrol addSubview:completeOrder];
    [completeOrder updateView];
    
    return completeOrder;
}


-(CancelOrder *)orderCancel
{
    [cancelOrder removeFromSuperview];

    cancelOrder = nil;
    NSArray *topLevelObjects = [[NSBundle mainBundle] loadNibNamed:@"CancelOrder" owner:nil options:nil];
    for(id currentObject in topLevelObjects)
    {
        if([currentObject isKindOfClass:[CancelOrder class]])
        {
            cancelOrder = (CancelOrder*)currentObject;
            break;
        }
    }
    cancelOrder.hostView = self;
    [cancelOrder  setFrame:CGRectMake(WIDTH*2, 0, WIDTH, scrollHeight)];
    cancelOrder.arrCancelOrders = arrCancelOrders;
    [mainScrol addSubview:cancelOrder];
    
    [cancelOrder updateView];
    return cancelOrder;
}



- (IBAction)tapHeaderButton:(UIButton *)sender {
    [mainScrol setContentOffset:CGPointMake(self.view.frame.size.width * sender.tag, 0) animated:true];
}

-(void)scrollViewDidScroll:(UIScrollView *)scrollView{
    viewSelector.frame = CGRectMake(scrollView.contentOffset.x/3, 0, btnCancel.frame.size.width, 39);
}




@end
