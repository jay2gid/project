//
//  UpdateOrderView.m
//  ToolsOnWheel
//
//  Created by Shikhar Khanna on 10/01/18.
//  Copyright © 2018 Kapil Goyal. All rights reserved.
//

#import "UpdateOrderView.h"
#import "Config.h"
#import "Utils.h"

@implementation UpdateOrderView
{
    BOOL firstTime;
}
-(void)updateView
{
    // [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(getChagneDateOrder:) name:kDateChange object:nil];

    btnCancel.layer.cornerRadius = 20.0f;
    btnUpdate.layer.cornerRadius = 20.0f;
    viewContainer.layer.cornerRadius = 10.0f;
    txtDate.text = self.strOrderDate;
    txtFTime.text = self.strOrderTime;
    arrDate = [[NSMutableArray alloc] init];
    arrTimeSlot = [[NSMutableArray alloc] init];
    
    isCalender = false;
    isDropDown = false;
}

-(void)getChagneDateOrder:(NSNotification*)notification
{
    if ([[[NSUserDefaults standardUserDefaults] objectForKey:@"datepopup"] isEqualToString:@"order"])
    {
        [[NSUserDefaults standardUserDefaults] removeObjectForKey:@"datepopup"];
        [[NSUserDefaults standardUserDefaults] synchronize];
        NSString *strDate = [notification object];
        strCheck = @"timeslot";
    
        txtDate.text = [NSString stringWithFormat:@"%@/%@/%@",[strDate substringWithRange:NSMakeRange(8, 2)],[strDate substringWithRange:NSMakeRange(5, 2)],[strDate substringWithRange:NSMakeRange(0, 4)]];

        if ([AppDelegate checkNetwork])
        {
//            [MBProgressHUD showHUDAddedTo:[AppDelegate sharedDelegate].window animated:YES];
//            ASIFormDataRequest *request = [ASIFormDataRequest requestWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@%@",ServerAdd3,kTimeSlotDate]]];
//            [request setDelegate:self];
//            [request setPostValue:_catId forKey:@"cateId"];
//            [request setPostValue:strDate forKey:@"selectDate"];
//            [request startAsynchronous];
            
            NSDictionary *dict =@{@"cateId":_catId,
                                   @"selectDate":strDate};
            
            [self getChagneDateOrder_onDateSelected_HudWithDict:dict];
        }
    }
}

-(void)onDateSelected:(NSString *)date{
    
    
    dispatch_async(dispatch_get_main_queue(), ^{
    
        isCalender = false;
        [[NSUserDefaults standardUserDefaults] removeObjectForKey:@"datepopup"];
        [[NSUserDefaults standardUserDefaults] synchronize];
        NSString *strDate = date;
        self->strCheck = @"timeslot";
        
        self->txtDate.text = [NSString stringWithFormat:@"%@/%@/%@",[strDate substringWithRange:NSMakeRange(8, 2)],[strDate substringWithRange:NSMakeRange(5, 2)],[strDate substringWithRange:NSMakeRange(0, 4)]];
        
        if ([AppDelegate checkNetwork]) {
            
//            [MBProgressHUD showHUDAddedTo:[AppDelegate sharedDelegate].window animated:YES];
//            ASIFormDataRequest *request = [ASIFormDataRequest requestWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@%@",ServerAdd3,kTimeSlotDate]]];
//            [request setDelegate:self];
//            [request setPostValue:self->_catId forKey:@"cateId"];
//            [request setPostValue:strDate forKey:@"selectDate"];
//            [request startAsynchronous];
            
            NSDictionary *dict =@{@"cateId":self->_catId,
                                  @"selectDate":strDate};
            
            [self getChagneDateOrder_onDateSelected_HudWithDict:dict];
        }
    });
}

-(void) getChagneDateOrder_onDateSelected_HudWithDict:(NSDictionary *)dict
{
    NSDictionary *param = dict;
    
    [Helper showLoading];
    [WebServiceCalls POST:kTimeSlotDate parameter:param completionBlock:^(id JSON, WebServiceResult result)
     {
         @try
         {
             [Helper stopLoading];
             
             if (result == WebServiceResultSuccess)
             {
                 NSDictionary *dictMain = JSON;
                 if ([dictMain[@"status"] integerValue] == 1)
                 {
                     [self->arrTimeSlot removeAllObjects];
                     // [self->arrTimeSlot addObject:@"Select Time"];
                     [self->viewTime setUserInteractionEnabled:YES];
                     self->txtFTime.text = @"Select Time";
                     
                     NSMutableArray *arrTime = [dictMain objectForKey:@"timeSlot"];
                     for (int i=0; i<[arrTime count]; i++)
                     {
                         [self->arrTimeSlot addObject:[arrTime objectAtIndex:i] ];
                     }
                 }
                 else if(![[dictMain objectForKey:@"status"] boolValue])
                 {
                     [self->viewTime setUserInteractionEnabled:NO];
                     self->txtFTime.text = @"Select Time";
                     [Utils showAlertMessage:KMessageTitle Message:@"Sorry! Booking are closed for today."];
                 }
             }
         }
         @catch (NSException *exception) { }
         @finally {
             [Helper stopLoading];
         }
     }];
}

//- (void)requestFinished:(ASIHTTPRequest *)request
//{
//    [MBProgressHUD hideAllHUDsForView:[AppDelegate sharedDelegate].window animated:YES];
//    NSString *receivedString = [request responseString];
//    NSLog(@"str=%@",receivedString);
//    NSDictionary *dictMain=[receivedString JSONValue];
//
//    if ([strCheck isEqualToString:@"disabledate"])
//    {
//        if([[dictMain objectForKey:@"status"] boolValue])
//        {
//            NSMutableArray *arrDisableDate = [dictMain objectForKey:@"result"];
//            [arrDate removeAllObjects];
//            for (int i=0; i<[arrDisableDate count]; i++)
//            {
//                [arrDate addObject:[[arrDisableDate objectAtIndex:i] objectForKey:@"date"]];
//            }
//        }
//        else if(![[dictMain objectForKey:@"status"] boolValue])
//        {
//            [Utils showAlertMessage:KMessageTitle Message:[dictMain objectForKey:@"result"]];
//        }
//    }
//    else if ([strCheck isEqualToString:@"timeslot"])
//    {
//        if([[dictMain objectForKey:@"status"] boolValue])
//        {
//            [arrTimeSlot removeAllObjects];
//            [arrTimeSlot addObject:@"Select Time"];
//            [viewTime setUserInteractionEnabled:YES];
//            txtFTime.text = @"Select Time";
//
//            NSMutableArray *arrTime = [dictMain objectForKey:@"timeSlot"];
//            for (int i=0; i<[arrTime count]; i++)
//            {
//                [arrTimeSlot addObject:[arrTime objectAtIndex:i] ];
//            }
//        }
//        else if(![[dictMain objectForKey:@"status"] boolValue])
//        {
//            [viewTime setUserInteractionEnabled:NO];
//            txtFTime.text = @"Select Time";
//            [Utils showAlertMessage:KMessageTitle Message:@"Sorry! Booking are closed for today."];
//        }
//    }
//    else if ([strCheck isEqualToString:@"update"])
//    {
//
//        self.strOrderDate = txtDate.text;
//        if([[dictMain objectForKey:@"status"] boolValue])
//        {
//            [[NSNotificationCenter defaultCenter] postNotificationName:@"MYORDER_RELOAD" object:@"1"];
//            [Utils showAlertMessage:KMessageTitle Message:[dictMain objectForKey:@"result"]];
//            [self removeFromSuperview];
//
//        }
//        else if(![[dictMain objectForKey:@"status"] boolValue])
//        {
//            [Utils showAlertMessage:KMessageTitle Message:[dictMain objectForKey:@"result"]];
//        }
//    }
//    else if ([strCheck isEqualToString:@"onlytime"])
//    {
//        if([[dictMain objectForKey:@"status"] boolValue])
//        {
//            [arrTimeSlot removeAllObjects];
//            [arrTimeSlot addObject:@"Select Time"];
//            //[viewTime setUserInteractionEnabled:NO];
//            txtFTime.text = @"Select Time";
//            NSMutableArray *arrTime = [dictMain objectForKey:@"timeSlot"];
//            for (int i=0; i<[arrTime count]; i++)
//            {
//                [arrTimeSlot addObject:[arrTime objectAtIndex:i] ];
//            }
//
//            if (self.dropDownView)
//            {
//                [_dropDownView removeFromSuperview];
//                _dropDownView = nil;
//            }
//
//            CGRect rect = [btnSender convertRect:btnSender.bounds toView:self];
//            self.dropDownView = [[DropDownView alloc] initWithFrame:CGRectMake(rect.origin.x-5, rect.origin.y-40, rect.size.width, rect.size.height) target:self];
//            [self.dropDownView setBackgroundColor:[UIColor clearColor]];
//            [self.dropDownView setTag:btnSender.tag];
//
//            [self downloadDropDownData:btnSender];
//
//            if ([self.dropDownView.myDataarray count])
//            {
//                [self addSubview:self.dropDownView];
//            }
//
//            [self.dropDownView performSelector:@selector(openDropDown) withObject:nil afterDelay:0.1];
//        }
//        else if(![[dictMain objectForKey:@"status"] boolValue])
//        {
//            [Utils showAlertMessage:KMessageTitle Message:@"Sorry! Booking are closed for today."];
//        }
//
//    }
//}

#pragma mark - UIButton Actions

- (IBAction)btnUpdatePressed:(id)sender
{
    if ([txtFTime.text isEqualToString:@"Select Time"])
    {
        [Utils showAlertMessage:KMessageTitle Message:@"please select time"];
    }
    else
    {
        if ([AppDelegate checkNetwork])
        {
            strCheck = @"update";
//            [MBProgressHUD showHUDAddedTo:[AppDelegate sharedDelegate].window animated:YES];
//            ASIFormDataRequest *request = [ASIFormDataRequest requestWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@%@",ServerAdd3,kOrderUpdate]]];
//            [request setDelegate:self];
//            [request setPostValue:self.strServiceId forKey:@"orderId"];
//            //    NSString *strDateSelect = [NSString stringWithFormat:@"%@/%@/%@",[txtFDate.text substringWithRange:NSMakeRange(8, 2)],[txtFDate.text substringWithRange:NSMakeRange(5, 2)],[txtFDate.text substringWithRange:NSMakeRange(0, 4)]];
//
//            [request setPostValue:txtDate.text forKey:@"orderServiceDate"];
//            [request setPostValue:txtFTime.text forKey:@"orderServiceTime"];
//            [request startAsynchronous];
            
            NSDictionary *dict =@{@"orderId":self.strServiceId,
                                  @"orderServiceDate":txtDate.text,
                                  @"orderServiceTime":txtFTime.text};
            
            [self OrderUpdate_HudWithDict:dict];
        }
    }
}

-(void) OrderUpdate_HudWithDict:(NSDictionary *)dict
{
    NSDictionary *param = dict;
    
    [Helper showLoading];
    [WebServiceCalls POST:kOrderUpdate parameter:param completionBlock:^(id JSON, WebServiceResult result)
     {
         @try
         {
             [Helper stopLoading];
             
             if (result == WebServiceResultSuccess)
             {
                 self.strOrderDate = self->txtDate.text;

                 NSDictionary *dictMain = JSON;
                 if ([dictMain[@"status"] integerValue] == 1)
                 {
                     [[NSNotificationCenter defaultCenter] postNotificationName:@"MYORDER_RELOAD" object:@"1"];
                     [Utils showAlertMessage:KMessageTitle Message:[dictMain objectForKey:@"result"]];
                     [self removeFromSuperview];
                     
                 }
                 else if(![[dictMain objectForKey:@"status"] boolValue])
                 {
                     [Utils showAlertMessage:KMessageTitle Message:[dictMain objectForKey:@"result"]];
                 }
             }
         }
         @catch (NSException *exception) { }
         @finally {
             [Helper stopLoading];
         }
     }];
}

- (IBAction)btnCancelPressed:(id)sender
{
    [self removeFromSuperview];
}


#pragma mark - UIButton Actions
-(IBAction)btnDateClicked:(id)sender
{
    END_KEY_VIEW
    if (!isDropDown) {
        isCalender = true;
        [[NSUserDefaults standardUserDefaults] setObject:@"order" forKey:@"datepopup"];
        [[NSUserDefaults standardUserDefaults] synchronize];
        strCheck = @"disabledate";
        
        if ([AppDelegate checkNetwork])
        {

//        [MBProgressHUD showHUDAddedTo:[AppDelegate sharedDelegate].window animated:YES];
//        ASIFormDataRequest *request = [ASIFormDataRequest requestWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@%@",ServerAdd3,kDisableDate]]];
//            [request setDelegate:self];
//            [request startAsynchronous];
            
            [self disabledate_Hud];
            
            if (!calView) {
                calView  = [[[NSBundle mainBundle] loadNibNamed:@"CalPopUp" owner:self options:nil] objectAtIndex:0];
                calView.frame = self.bounds;
                [calView UpdateView];
                calView.caldelegate = self;
            }
            calView.hidden = false;
            [self addSubview:calView];
        }
    }
}

-(void) disabledate_Hud
{
//    NSDictionary *param = dict;
    
    [Helper showLoading];
    [WebServiceCalls GET:kDisableDate parameter:nil completionBlock:^(id JSON, WebServiceResult result)
     {
         @try
         {
             [Helper stopLoading];
             
             if (result == WebServiceResultSuccess)
             {
                 NSDictionary *dictMain = JSON;
                 if ([dictMain[@"status"] integerValue] == 1)
                 {
                     NSMutableArray *arrDisableDate = [dictMain objectForKey:@"result"];
                     [self->arrDate removeAllObjects];
                     for (int i=0; i<[arrDisableDate count]; i++) {
                         [self->arrDate addObject:[[arrDisableDate objectAtIndex:i] objectForKey:@"date"]];
                     }
                 }
                 else if(![[dictMain objectForKey:@"status"] boolValue])
                 {
                     [Utils showAlertMessage:KMessageTitle Message:[dictMain objectForKey:@"result"]];
                 }
             }
         }
         @catch (NSException *exception) { }
         @finally {
             [Helper stopLoading];
         }
     }];
}

#pragma mark - DropDown delegate

- (IBAction)generateDropDown:(UIButton*)sender
{
        END_KEY_VIEW
        btnSender = sender;
        if (firstTime == true)
        {
            if (!isCalender) {
//                if (self.dropDownView)
//                {
//                    [_dropDownView removeFromSuperview];
//                    _dropDownView = nil;
//                }
//
//                CGRect rect = [sender convertRect:sender.bounds toView:self];
//                self.dropDownView = [[DropDownView alloc] initWithFrame:CGRectMake(rect.origin.x-5, rect.origin.y-40, rect.size.width+12, rect.size.height) target:self];
//                [self.dropDownView setBackgroundColor:[UIColor clearColor]];
//                [self.dropDownView setTag:sender.tag];
//
//                [self downloadDropDownData:btnSender];
//
//                if ([self.dropDownView.myDataarray count])
//                {
//                    isDropDown = true;
//                    [self addSubview:self.dropDownView];
//                }
//
//                [self.dropDownView performSelector:@selector(openDropDown) withObject:nil afterDelay:0.1];
                
                [self openTimePicker];
            }
           
        }
        else
        {
            if (firstTime == NO ) {
                firstTime  = true;
                
                // txtFTime.text = @"Select Time";
                
                strCheck = @"onlytime";
                NSString *strDate = [NSString stringWithFormat:@"%@-%@-%@",[txtDate.text substringWithRange:NSMakeRange(6, 4)],[txtDate.text substringWithRange:NSMakeRange(3, 2)],[txtDate.text substringWithRange:NSMakeRange(0, 2)]];
                // strDate  2018-03-28   31/03/2018
                
                if ([AppDelegate checkNetwork])
                {
//                    [MBProgressHUD showHUDAddedTo:[AppDelegate sharedDelegate].window animated:YES];
//                    ASIFormDataRequest *request = [ASIFormDataRequest requestWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@%@",ServerAdd3,kTimeSlotDate]]];
//                    [request setDelegate:self];
//                    [request setPostValue:_catId forKey:@"cateId"];
//                    [request setPostValue:strDate forKey:@"selectDate"];
//                    [request startAsynchronous];
                    
                    NSDictionary *dict =@{@"cateId":_catId,
                                          @"selectDate":strDate};

                    [self onlytime_HudWithDict:dict];
                }
            }
        }
}




-(void)openTimePicker{
    
    [ActionSheetStringPicker showPickerWithTitle:@"Select time slot"
                                            rows:arrTimeSlot
                                initialSelection:0
                                       doneBlock:^(ActionSheetStringPicker *picker, NSInteger selectedIndex, id selectedValue) {
                                           NSLog(@"Picker: %@, Index: %ld, value: %@",
                                                 picker, (long)selectedIndex, selectedValue);
                                           
                                           // NSLog(@"%ld",(long)self->btnSender.tag);
                                           self->txtFTime.text = self->arrTimeSlot[selectedIndex];
                                           
                                       }
                                     cancelBlock:^(ActionSheetStringPicker *picker) {
                                         NSLog(@"Block Picker Canceled");
                                     }
                                          origin:btnSender];
    
}


-(void)onlytime_HudWithDict:(NSDictionary *)dict
{
    NSDictionary *param = dict;
    
    [Helper showLoading];
    [WebServiceCalls POST:kTimeSlotDate parameter:param completionBlock:^(id JSON, WebServiceResult result)
     {
         @try
         {
             [Helper stopLoading];
             
             if (result == WebServiceResultSuccess)
             {
                 NSDictionary *dictMain = JSON;
                 if ([dictMain[@"status"] integerValue] == 1)
                 {
                     [self->arrTimeSlot removeAllObjects];
                     //[self->arrTimeSlot addObject:@"Select Time"];
                     //  [viewTime setUserInteractionEnabled:NO];
                     self->txtFTime.text = @"Select Time";
                     NSMutableArray *arrTime = [dictMain objectForKey:@"timeSlot"];
                     for (int i=0; i<[arrTime count]; i++)
                     {
                         [self->arrTimeSlot addObject:[arrTime objectAtIndex:i] ];
                     }
                     
                     [self openTimePicker];
                 }
                 else if(![[dictMain objectForKey:@"status"] boolValue])
                 {
                     [Utils showAlertMessage:KMessageTitle Message:@"Sorry! Booking are closed for today."];
                 }
             }
         }
         @catch (NSException *exception) { }
         @finally {
             [Helper stopLoading];
         }
     }];
}

-(void)didSelectIndex:(int)index ForDropDown:(DropDownView*)dropdown
{
    isCalender = false;
    if (dropdown.strDropDownValue) {
        NSArray *list = [NSArray arrayWithArray:arrTimeSlot];
        NSLog(@"selected data :%@",[list objectAtIndex:index]);
        txtFTime.text = dropdown.strDropDownValue;
    }
    
    [dropdown.myLabel setText:@""];
    [self.dropDownView removeFromSuperview];
    _dropDownView = nil;
}

- (void)downloadDropDownData:(UIButton*)sender
{
    NSArray *list = [NSArray arrayWithArray:arrTimeSlot];
    [self.dropDownView setDataArray:list];
}

@end
