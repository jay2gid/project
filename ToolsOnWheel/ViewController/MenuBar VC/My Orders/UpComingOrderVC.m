//
//  MyOrdersVC.m
//  ToolsOnWheel
//
//  Created by Shikhar Khanna on 06/01/18.
//  Copyright © 2018 Kapil Goyal. All rights reserved.
//

#import "UpComingOrderVC.h"
#import "CustomNavigation.h"
#import "CommonHeader.h"
#import "MyOrdersTVCell.h"

#import "MFSideMenu.h"
#import "Config.h"
#import "Utils.h"
#import "UpdateOrderView.h"
#import "CancelOrderView.h"
#import "HomeVC.h"

@interface UpComingOrderVC ()
{
    UpdateOrderView *updateCustomView;
    CancelOrderView *cancelView;
}
@end

@implementation UpComingOrderVC
@synthesize arrUpcomingOrders,tblViewUpcoming;

-(void)updateView
{
    if (arrUpcomingOrders.count > 0)
    {
        viewNoOrders.hidden = true;
        tblViewUpcoming.hidden = false;
    }
    else
    {
        viewNoOrders.hidden = false;
        tblViewUpcoming.hidden = true;
    }
    [tblViewUpcoming reloadData];

}

-(UpdateOrderView *)updateCustomView
{
    NSArray *arrNibs = [NSArray arrayWithArray:[[NSBundle mainBundle] loadNibNamed:NSStringFromClass([UpdateOrderView class]) owner:self options:nil]];
    
    if (updateCustomView!=nil)
    {
        [updateCustomView removeFromSuperview];
        updateCustomView = nil;
    }
    
    updateCustomView = [arrNibs firstObject];
    updateCustomView.controller = self;
    return updateCustomView;
    
}

-(CancelOrderView *)cancelView
{
    NSArray *arrNibs = [NSArray arrayWithArray:[[NSBundle mainBundle] loadNibNamed:NSStringFromClass([CancelOrderView class]) owner:self options:nil]];
    
    if (cancelView!=nil)
    {
        [cancelView removeFromSuperview];
        cancelView = nil;
    }
    
    cancelView = [arrNibs firstObject];
    cancelView.controller = self;
    return cancelView;
    
}




#pragma mark - UITableViewDataSource
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
     BOOL isFlag = [self checkDateTime:(int)indexPath.row];
    if (isFlag)
    {
        return 360.0;
    }
    else
    {
        return 307.0;
    }
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
   return [arrUpcomingOrders count];
   
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    static NSString *CellIdentifier= @"MyOrdersTVCell";
    
    MyOrdersTVCell* cell = nil;
    cell = (MyOrdersTVCell*)[tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil)
    {
        NSString *nibNameOrNil= @"MyOrdersTVCell";
        
        NSArray *topLevelObjects = [[NSBundle mainBundle] loadNibNamed:nibNameOrNil owner:nil options:nil];
        
        for(id currentObject in topLevelObjects)
        {
            if([currentObject isKindOfClass:[MyOrdersTVCell class]])
            {
                cell = (MyOrdersTVCell*)currentObject;
                break;
            }
        }
    }
    
    [cell.contentView setBackgroundColor:[UIColor clearColor]];
    [cell setBackgroundColor:[UIColor clearColor]];
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
  
    cell.viewContainer.layer.cornerRadius = 5.0f;
    cell.viewImage.layer.cornerRadius = 40.0f;
    cell.imgBackground.layer.cornerRadius = 40.0f;
    cell.imgBackground.image = [UIImage imageNamed:@"circle.png"];
        BOOL isFlag = [self checkDateTime:(int)indexPath.row];
        if (isFlag)
        {
            [cell.btnCancel setHidden:NO];
            [cell.btnUpdate setHidden:NO];
            [cell.btnUpdate setTag:indexPath.row];
            [cell.btnUpdate addTarget:self action:@selector(btnUpdatePressed:) forControlEvents:UIControlEventTouchUpInside];
            [cell.btnCancel setTag:indexPath.row];
            [cell.btnCancel addTarget:self action:@selector(btnCancelPressed:) forControlEvents:UIControlEventTouchUpInside];
        }
        else
        {
            cell.bottomHeight.constant = 0;
        }

    
    if ([[arrUpcomingOrders objectAtIndex:indexPath.row] objectForKey:@"orderId"] != (id)[NSNull null] || [[[arrUpcomingOrders objectAtIndex:indexPath.row] objectForKey:@"orderId"] length] != 0 )
    {
        cell.lblOrderId.text = [NSString stringWithFormat:@"Order Id: %@",[[arrUpcomingOrders objectAtIndex:indexPath.row] objectForKey:@"orderId"]];
    }
    
    if ([[arrUpcomingOrders objectAtIndex:indexPath.row] objectForKey:@"cateImgWhite"] != (id)[NSNull null] || [[[arrUpcomingOrders objectAtIndex:indexPath.row] objectForKey:@"cateImgWhite"] length] != 0 )
    {
        [cell.imgViewLogo sd_setImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@",[[arrUpcomingOrders objectAtIndex:indexPath.row] objectForKey:@"cateImgWhite"]]]];
    }
    
    if ([[arrUpcomingOrders objectAtIndex:indexPath.row] objectForKey:@"serviceId"] != (id)[NSNull null] || [[[arrUpcomingOrders objectAtIndex:indexPath.row] objectForKey:@"serviceId"] length] != 0 )
    {
        cell.lblServiceId.text = [[arrUpcomingOrders objectAtIndex:indexPath.row] objectForKey:@"serviceId"];

    }
    if( (([[arrUpcomingOrders objectAtIndex:indexPath.row] objectForKey:@"cateName"] != (id)[NSNull null] || [[[arrUpcomingOrders objectAtIndex:indexPath.row] objectForKey:@"cateName"] length] != 0 )) && (([[arrUpcomingOrders objectAtIndex:indexPath.row] objectForKey:@"serviceName"] != (id)[NSNull null] || [[[arrUpcomingOrders objectAtIndex:indexPath.row] objectForKey:@"serviceName"] length] != 0 )))
    {
        cell.lblTitle.text = [NSString stringWithFormat:@"%@: %@",[[arrUpcomingOrders objectAtIndex:indexPath.row] objectForKey:@"cateName"],[[arrUpcomingOrders objectAtIndex:indexPath.row] objectForKey:@"serviceName"]];
    }
    
    if ([[arrUpcomingOrders objectAtIndex:indexPath.row] objectForKey:@"rates"] != (id)[NSNull null] || [[[arrUpcomingOrders objectAtIndex:indexPath.row] objectForKey:@"rates"] length] != 0 )
    {
        cell.lblPrice.text = [NSString stringWithFormat:@"₹%@",[[arrUpcomingOrders objectAtIndex:indexPath.row] objectForKey:@"rates"]];
    }
    
    if ([[arrUpcomingOrders objectAtIndex:indexPath.row] objectForKey:@"serviceDate"] != (id)[NSNull null] || [[[arrUpcomingOrders objectAtIndex:indexPath.row] objectForKey:@"serviceDate"] length] != 0 )
    {
        cell.lblServiceDate.text = [[arrUpcomingOrders objectAtIndex:indexPath.row] objectForKey:@"serviceDate"];
    }
    if ([[arrUpcomingOrders objectAtIndex:indexPath.row] objectForKey:@"serviceTime"] != (id)[NSNull null] || [[[arrUpcomingOrders objectAtIndex:indexPath.row] objectForKey:@"serviceTime"] length] != 0 )
    {
        cell.lblServiceTime.text = [[arrUpcomingOrders objectAtIndex:indexPath.row] objectForKey:@"serviceTime"];
    }
    if ([[arrUpcomingOrders objectAtIndex:indexPath.row] objectForKey:@"quantity"] != (id)[NSNull null] || [[[arrUpcomingOrders objectAtIndex:indexPath.row] objectForKey:@"quantity"] length] != 0 )
    {
        cell.lblQty.text = [NSString stringWithFormat:@"%@",[[arrUpcomingOrders objectAtIndex:indexPath.row] objectForKey:@"quantity"]];
    }
   
    cell.lblPrice.layer.cornerRadius = 15.0f;
    cell.lblPrice.layer.borderColor = [UIColor orangeColor].CGColor;
    cell.lblPrice.layer.borderWidth = 2.0f;
    
    cell.btnUpdate.layer.cornerRadius = 15.0f;
    cell.btnUpdate.layer.borderColor = [UIColor orangeColor].CGColor;
    
    cell.btnCancel.layer.cornerRadius = 15.0f;
    cell.btnCancel.layer.borderColor = [UIColor orangeColor].CGColor;
    
    return cell;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    
}


-(void)btnUpdatePressed:(UIButton *)sender
{
    BOOL isFlag = [self checkDateTime:(int)sender.tag];
    if (!isFlag)
    {
        [Utils showAlertMessage:KMessageTitle Message:@"Time limit has been crossed"];

    }
    else
    {
        updateCustomView = [self updateCustomView];
        [[UIApplication sharedApplication].keyWindow addSubview:updateCustomView];
        updateCustomView.strServiceId = [[arrUpcomingOrders objectAtIndex:sender.tag] objectForKey:@"serviceId"];
        updateCustomView.strOrderDate = [[arrUpcomingOrders objectAtIndex:sender.tag] objectForKey:@"serviceDate"];
        updateCustomView.strOrderTime = [[arrUpcomingOrders objectAtIndex:sender.tag] objectForKey:@"serviceTime"];
        updateCustomView.catId = [Helper getString:arrUpcomingOrders[sender.tag][@"cateId"]];
        [updateCustomView updateView];
        updateCustomView.frame = [UIApplication sharedApplication].keyWindow.bounds;
    }

}

-(BOOL) checkTimeInterval:(NSTimeInterval)timeInterval
{
    NSInteger ti = timeInterval;
    //  int ms = ((interval % 1) * 1000)
    
    //  int seconds = ti % 60;
    int minutes = (ti / 60) % 60;
    int hours = (int)(ti / 3600);
    if ((hours<=0) || ((hours==1) &&(minutes<30)) )
    {
        return FALSE;
    }
    else
        return TRUE;
}

-(BOOL)checkDateTime:(int)tag
{
    NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
    [dateFormat setDateFormat:@"yyyy-MM-dd hh:mma"];
    NSString *str = [[arrUpcomingOrders objectAtIndex:tag] objectForKey:@"serviceDate"];
    NSString *strDate = [NSString stringWithFormat:@"%@-%@-%@",[str substringWithRange:NSMakeRange(6, 4)],[str substringWithRange:NSMakeRange(3, 2)],[str substringWithRange:NSMakeRange(0, 2)]];
    
    //  NSDate *date1 = [dateFormat dateFromString:@"2018-01-28 05:44PM"];
    NSString *strDateTime = [NSString stringWithFormat:@"%@ %@",strDate,[[[arrUpcomingOrders objectAtIndex:tag] objectForKey:@"serviceTime"] substringWithRange:NSMakeRange(0, 7)]];
    NSLog(@"[NSDate date]=%@",[NSDate date]);
    NSDate *date = [dateFormat dateFromString:strDateTime];
    //  NSLog(@"date=%@",date);
    NSTimeInterval timeInterval = [date timeIntervalSinceDate:[NSDate date]];
    NSLog(@"timeInterval=%f",timeInterval);
    BOOL isFlag = [self checkTimeInterval:timeInterval];
    return isFlag;
}
-(void)btnCancelPressed:(UIButton *)sender
{
    BOOL isFlag = [self checkDateTime:(int)sender.tag];
    if (!isFlag) {
        [Utils showAlertMessage:KMessageTitle Message:@"Time limit has been crossed"];
    }else {
        
        cancelView = [self cancelView];
        [[UIApplication sharedApplication].keyWindow addSubview:cancelView];
        cancelView.strServiceId = [[arrUpcomingOrders objectAtIndex:sender.tag] objectForKey:@"serviceId"];
        
        [cancelView updateView];
        cancelView.frame = [UIApplication sharedApplication].keyWindow.bounds;
    }
}

@end
