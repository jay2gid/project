//
//  CancelOrderView.h
//  ToolsOnWheel
//
//  Created by Shikhar Khanna on 10/01/18.
//  Copyright © 2018 Kapil Goyal. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ASIFormDataRequest.h"
#import "JSON.h"
#import "MBProgressHUD.h"
#import "AppDelegate.h"
#import "DropDownView.h"


@interface CancelOrderView : UIView<UITextViewDelegate>
{
    
    IBOutlet NSLayoutConstraint *containerHeight;
    IBOutlet NSLayoutConstraint *commentHeight;
    IBOutlet NSLayoutConstraint *commentTop;
    IBOutlet UIView *viewComment;
    IBOutlet UIView *viewContainer;
    IBOutlet UITextField *txtFReason;
    IBOutlet UIButton *btnSubmit;
    IBOutlet UIButton *btnCancel;
    NSArray *arrCancelResaon;
    IBOutlet UITextView *txtViewComment;
}
-(void)updateView;
@property(retain, nonatomic)NSString  *strServiceId;
@property(retain, nonatomic)UIViewController *controller;
@property (retain,nonatomic)DropDownView *dropDownView;

@end
