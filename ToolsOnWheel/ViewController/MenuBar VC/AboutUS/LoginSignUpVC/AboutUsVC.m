//
//  LoginSignUpVC.m
//  Demo Project
//
//  Created by Shikhar Khanna on 06/08/15.
//  Copyright (c) 2015 Shikhar Khanna. All rights reserved.
//

#import "AboutUsVC.h"
#import "AboutVC.h"
#import "FaqVC.h"
#import "PrivacyVC.h"

#import "CommonHeader.h"
#import "CustomNavigation.h"
#import "MFSideMenu.h"

@interface AboutUsVC ()
{
    AboutVC *viewAbout;
    FaqVC *viewFaq;
    PrivacyVC *viewPrivacy;
}

@end

@implementation AboutUsVC
@synthesize hostView1;

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.dataSource = self;
    self.delegate = self;
    
    
    [self performSelector:@selector(loadContent) withObject:nil afterDelay:0.01];
}



-(AboutVC *)aboutVC
{
    viewAbout = nil;
    NSArray *topLevelObjects = [[NSBundle mainBundle] loadNibNamed:@"AboutVC" owner:nil options:nil];
    
    for(id currentObject in topLevelObjects)
    {
        if([currentObject isKindOfClass:[AboutVC class]])
        {
            viewAbout = (AboutVC*)currentObject;
            break;
        }
    }
   
    viewAbout.hostView = self.hostView1;
    [viewAbout setFrame:CGRectMake(0, 44, [UIScreen mainScreen].bounds.size.width,[UIScreen mainScreen].bounds.size.height-99)];
    [self.view addSubview:viewAbout];
    [viewAbout.webView loadHTMLString:[[NSUserDefaults standardUserDefaults] objectForKey:@"aboutDesc"]  baseURL:nil];
    // viewAbout.textView.text = [[NSUserDefaults standardUserDefaults] objectForKey:@"aboutDesc"];
    return viewAbout;
}

-(FaqVC *)faqVC
{
    viewFaq = nil;
    NSArray *topLevelObjects = [[NSBundle mainBundle] loadNibNamed:@"FaqVC" owner:nil options:nil];
    
    for(id currentObject in topLevelObjects)
    {
        if([currentObject isKindOfClass:[FaqVC class]])
        {
            viewFaq = (FaqVC*)currentObject;
            break;
        }
    }
    
    viewFaq.hostView = self.hostView1;
    [viewFaq setFrame:CGRectMake(0, 44, [UIScreen mainScreen].bounds.size.width,[UIScreen mainScreen].bounds.size.height-99)];
    [self.view addSubview:viewFaq];
    [viewFaq.webView loadHTMLString:[[NSUserDefaults standardUserDefaults] objectForKey:@"faqDesc"]  baseURL:nil];
    return viewFaq;
}


-(PrivacyVC *)privacyVC
{
    viewPrivacy = nil;
    NSArray *topLevelObjects = [[NSBundle mainBundle] loadNibNamed:@"FaqVC" owner:nil options:nil];
    for(id currentObject in topLevelObjects)
    {
        if([currentObject isKindOfClass:[FaqVC class]])
        {
            viewPrivacy = (PrivacyVC*)currentObject;
            break;
        }
    }
    viewPrivacy.hostView = self.hostView1;
    [viewPrivacy setFrame:CGRectMake(0, 44, [UIScreen mainScreen].bounds.size.width,[UIScreen mainScreen].bounds.size.height-99)];
    [self.view addSubview:viewPrivacy];
    [viewPrivacy.webView loadHTMLString:[[NSUserDefaults standardUserDefaults] objectForKey:@"privacyDesc"]  baseURL:nil];
    return viewPrivacy;
}


#pragma mark - Setters
- (void)setNumberOfTabs:(NSUInteger)numberOfTabs
{
    // Set numberOfTabs
    _numberOfTabs = 3;
    // Reload data
    [self reloadData];
}

#pragma mark - Helpers
- (void)selectTabWithNumberFive
{
    [self selectTabAtIndex:0];
}
- (void)loadContent
{
    self.numberOfTabs = 3;
    [self selectTabAtIndex:0];
    
}

#pragma mark - Interface Orientation Changes

-(void)willRotateToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation duration:(NSTimeInterval)duration {    
    // Update changes after screen rotates
    [self performSelector:@selector(setNeedsReloadOptions) withObject:nil afterDelay:duration];
}

#pragma mark - ViewPagerDataSource
- (NSUInteger)numberOfTabsForViewPager:(ViewPagerController *)viewPager {
    return self.numberOfTabs;
}
- (UIView *)viewPager:(ViewPagerController *)viewPager viewForTabAtIndex:(NSUInteger)index
{
    NSArray *arrTabs = [NSArray arrayWithObjects:@"About Us",@"FAQ",@"Privacy",nil];
    UILabel *label = [UILabel new];
    CGRect rect = label.frame;
    if (IS_IPAD)
    {
        rect.size.width = 384;
        rect.size.height = 70.0;
//        label.font = [UIFont fontWithName:@"JosefinSans" size:30.0];
    }
    else
    {
        rect.size.width = 180;
        rect.size.height = 40.0;
//        label.font = [UIFont fontWithName:@"JosefinSans" size:15.0];
    }
    
    [label setFrame:rect];
    [label setNumberOfLines:3];
    [label setLineBreakMode:NSLineBreakByWordWrapping];
    label.text = [arrTabs objectAtIndex:index];
    label.textAlignment = NSTextAlignmentCenter;
    label.font = [UIFont fontWithName:@"GothamMedium" size:17.0f];
    label.textColor = [UIColor whiteColor];
    return label;
}

- (UIViewController *)viewPager:(ViewPagerController *)viewPager contentViewControllerForTabAtIndex:(NSUInteger)index {
    
    
    NSArray *arrViewSelectors = @[@"aboutVC",@"faqVC",@"privacyVC"];
    UIViewController *contentVC = [[UIViewController alloc] init];
    
    SEL sel = NSSelectorFromString([arrViewSelectors objectAtIndex:index]);
    UIView *aView = [self performSelector:sel withObject:nil];
    [contentVC.view setBackgroundColor:[UIColor clearColor]];
    [aView setBackgroundColor:[UIColor clearColor]];
    
    //[aView setBackgroundColor:[UIColor greenColor]];
    [contentVC.view setFrame:aView.frame];
    [contentVC.view addSubview:aView];
    return contentVC;
}

#pragma mark - ViewPagerDelegate
- (void)viewPager:(ViewPagerController *)viewPager didChangeTabToIndex:(NSUInteger)index
{
    NSLog(@"index=%lu",(unsigned long)index);
}

- (CGFloat)viewPager:(ViewPagerController *)viewPager valueForOption:(ViewPagerOption)option withDefault:(CGFloat)value {
    
    NSLog(@"%s",__PRETTY_FUNCTION__);
    switch (option) {
        case ViewPagerOptionStartFromSecondTab:
            return 0.0;
        case ViewPagerOptionCenterCurrentTab:
            return 1.0;
        case ViewPagerOptionTabLocation:
            return 1.0;
        case ViewPagerOptionTabHeight:
            if (IS_IPAD)
                return 70.0;
            else
                return 35.0;
        case ViewPagerOptionTabOffset:
            return 36.0;
        case ViewPagerOptionTabWidth:
            if (IS_IPAD)
                return 384;
            else
                return [UIScreen mainScreen].bounds.size.width/3;
        case ViewPagerOptionFixFormerTabsPositions:
            return 0.0;
        case ViewPagerOptionFixLatterTabsPositions:
            return 1.0;
        default:
            return value;
    }
}

- (UIColor *)viewPager:(ViewPagerController *)viewPager colorForComponent:(ViewPagerComponent)component withDefault:(UIColor *)color {
    
    switch (component) {
        case ViewPagerIndicator:
            return [UIColor clearColor];
        case ViewPagerTabsView:
            return [UIColor colorWithRed:152/255.0 green:186/255.0 blue:214/255.0 alpha:1.0];
        case ViewPagerContent:
            return [UIColor whiteColor];
        default:
            return color;
    }
}


- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

@end
