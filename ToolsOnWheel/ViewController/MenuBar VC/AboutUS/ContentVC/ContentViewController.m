//
//  ContentViewController.m
//  Scoo Talks
//
//  Created by Sheetal on 4/30/15.
//  Copyright (c) 2015 Dotsquares. All rights reserved.
//

#import "ContentViewController.h"
#import "AboutUsVC.h"

#import "ASIFormDataRequest.h"
#import "JSON.h"
#import "MBProgressHUD.h"
#import "AppDelegate.h"
#import "HomeVC.h"
#import "Utils.h"
#import "Config.h"


#import "CommonHeader.h"
#import "CustomNavigation.h"
#import "MFSideMenu.h"

#define IsRunningTallPhone() ([UIDevice currentDevice].userInterfaceIdiom == UIUserInterfaceIdiomPhone && [UIScreen mainScreen].bounds.size.height == 568)


@interface ContentViewController ()
{
    AboutUsVC *parentController;
}

@end

@implementation ContentViewController
@synthesize webView;

- (void)viewDidLoad
{
    [super viewDidLoad];
    [self performSelector:@selector(updateUI) withObject:nil afterDelay:0.01];
    parentController = [[AboutUsVC alloc] initWithNibName:@"AboutUsVC" bundle:nil];
    parentController.hostView1 = self;
    CGRect rect = parentController.view.frame;
    rect.origin.y = 64;
    parentController.view.frame = rect;
    [self.view addSubview:parentController.view];
}

-(void)updateUI
{
    [CustomNavigation addTarget:self backRequired:NO title:@"ABOUT US"];
    for (UIView *v1 in self.view.subviews)
    {
        if ([v1 isKindOfClass:[CommonHeader class]])
        {
            for (UIView *viewTmp in v1.subviews)
            {
                if ([viewTmp isKindOfClass:[UIButton class]])
                {
                    if (viewTmp.tag==1)
                    {
                        viewTmp.hidden=YES;
                    }
                    else if(viewTmp.tag==106)
                    {
                        [(UIButton*)viewTmp addTarget:self action:@selector(revealLeftSidebar:) forControlEvents:UIControlEventTouchUpInside];
                    }
                    else if(viewTmp.tag==102)
                    {
                       [(UIImageView*)viewTmp setHidden:YES];
                    }
                }
                else if ([viewTmp isKindOfClass:[UIImageView class]])
                {
                    if (viewTmp.tag==101)
                    {
                        [(UIImageView*)viewTmp setImage:[UIImage imageNamed:@"backP.png"]];
                    }
                }
            }
        }
    }
}


#pragma mark MFSidebarDelegate
- (void)leftSideMenuButtonPressed:(id)sender
{
    [self.menuContainerViewController toggleLeftSideMenuCompletion:^{
    }];
}

- (void)revealLeftSidebar:(id)sender
{
    NSArray *viewControllers = [[self navigationController] viewControllers];
    for( int i=0;i<[viewControllers count];i++){
        id obj=[viewControllers objectAtIndex:i];
        if([obj isKindOfClass:[HomeVC class]]){
            [[self navigationController] popToViewController:obj animated:YES];
        }
    }
}

-(IBAction)AboutUsClicked:(UIButton *)sender
{
    btnAboutUs.selected = false;
    btnFaq.selected = false;
    btnPrivacy.selected = false;
    
    sender.selected = true;
    if (sender.tag==0)
    {
        [webView loadHTMLString:[[NSUserDefaults standardUserDefaults] objectForKey:@"aboutDesc"]  baseURL:nil];
    }
    else if (sender.tag==1)
    {
        [webView loadHTMLString:[[NSUserDefaults standardUserDefaults] objectForKey:@"faqDesc"]  baseURL:nil];
    }
    else
    {
        [webView loadHTMLString:[[NSUserDefaults standardUserDefaults] objectForKey:@"privacyDesc"]  baseURL:nil];
    }
}

- (void)requestFinished:(ASIHTTPRequest *)request
{
}

- (void)requestFailed:(ASIHTTPRequest *)request
{
    NSLog(@"Fail");
    [MBProgressHUD hideAllHUDsForView:[AppDelegate sharedDelegate].window animated:YES];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

- (IBAction)backBtnPressed:(id)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}

@end
