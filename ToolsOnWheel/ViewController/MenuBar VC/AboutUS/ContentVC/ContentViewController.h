//
//  ContentViewController.h
//  Scoo Talks
//
//  Created by Sheetal on 4/30/15.
//  Copyright (c) 2015 Dotsquares. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ContentViewController : UIViewController
{
    IBOutlet UIButton *btnAboutUs;
    
    IBOutlet UIButton *btnPrivacy;
    IBOutlet UIButton *btnFaq;
}

@property(nonatomic,strong)IBOutlet UIWebView *webView;
@property (nonatomic,retain) NSIndexPath *indexPath;
@end
