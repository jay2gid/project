//
//  OfferVC.m
//  ToolsOnWheel
//
//  Created by Kapil Goyal on 04/02/18.
//  Copyright © 2018 Kapil Goyal. All rights reserved.
//

#import "OfferVC.h"
#import "RegisterVC.h"
#import "ASIFormDataRequest.h"
#import "JSON.h"
#import "MBProgressHUD.h"
#import "AppDelegate.h"
#import "HomeVC.h"
#import "Utils.h"
#import "Config.h"
#import "ForgetMobileVC.h"

#import "CommonHeader.h"
#import "CustomNavigation.h"
#import "MFSideMenu.h"
#import "OfferTVCell.h"

@interface OfferVC ()

@end

@implementation OfferVC

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    arrOffer = [[NSMutableArray alloc] init];
    [self performSelector:@selector(updateUI) withObject:nil afterDelay:0.01];
}

-(void)updateUI
{
    [CustomNavigation addTarget:self backRequired:TRUE title:@"OFFERS"];
    for (UIView *v1 in self.view.subviews)
    {
        if ([v1 isKindOfClass:[CommonHeader class]])
        {
            for (UIView *viewTmp in v1.subviews)
            {
                if ([viewTmp isKindOfClass:[UIButton class]])
                {
                    if (viewTmp.tag==1)
                    {
                        viewTmp.hidden=YES;
                    }
                    else if(viewTmp.tag==106)
                    {
                        [(UIButton*)viewTmp addTarget:self action:@selector(revealLeftSidebar:) forControlEvents:UIControlEventTouchUpInside];
                    }
                    else if(viewTmp.tag==102)
                    {
                        [(UIImageView*)viewTmp setHidden:YES];
                    }
                }
                else if ([viewTmp isKindOfClass:[UIImageView class]])
                {
                    if (viewTmp.tag==101)
                    {
                        [(UIImageView*)viewTmp setImage:[UIImage imageNamed:@"back_white.png"]];
                    }
                }
                else if ([viewTmp isKindOfClass:[UILabel class]])
                {
                    if (viewTmp.tag==110)
                    {
                        [(UILabel*)viewTmp setTextColor:[UIColor whiteColor]];
                    }
                }
            }
        }
    }
}

-(void)viewWillAppear:(BOOL)animated
{
    [arrOffer removeAllObjects];
    if ([AppDelegate checkNetwork])
    {
//        [MBProgressHUD showHUDAddedTo:[AppDelegate sharedDelegate].window animated:YES];
//        ASIFormDataRequest *request = [ASIFormDataRequest requestWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@%@",ServerAdd3,kListOfOffer]]];
//        [request setDelegate:self];
//        [request setPostValue:[[NSUserDefaults standardUserDefaults] objectForKey:@"userid"] forKey:@"userId"];
//        [request setPostValue:[[NSUserDefaults standardUserDefaults] objectForKey:@"secureSignature"] forKey:@"secureSignature"];
//        [request startAsynchronous];
        
        NSDictionary *param =@{@"userId":[[NSUserDefaults standardUserDefaults] objectForKey:@"userid"],
                               @"secureSignature":[[NSUserDefaults standardUserDefaults] objectForKey:@"secureSignature"]};
        
        [Helper showLoading];
        [WebServiceCalls POST:kListOfOffer parameter:param completionBlock:^(id JSON, WebServiceResult result)
         {
             @try
             {
                 [Helper stopLoading];
                 
                 if (result == WebServiceResultSuccess)
                 {
                     NSDictionary *dictMain = JSON;
                     if ([dictMain[@"status"] integerValue] == 1)
                     {
                         if ([[dictMain objectForKey:@"amount"] isEqualToString:@""])
                         {
                             self->tblViewOffer.hidden = true;
                             self->viewNoOffer.hidden = false;
                         }
                         else
                         {
                             NSString *strCode = [[[dictMain objectForKey:@"result"] objectForKey:@"offerData"] objectForKey:@"codeNames"];
                             NSString *strDate = [[[dictMain objectForKey:@"result"] objectForKey:@"offerData"] objectForKey:@"endDate"];
                             NSString *strPrice = [[[dictMain objectForKey:@"result"] objectForKey:@"offerData"] objectForKey:@"price"];
                             
                             NSArray *arrCode = [strCode componentsSeparatedByString:@";"];
                             NSArray *arrDate = [strDate componentsSeparatedByString:@";"];
                             NSArray *arrPrice = [strPrice componentsSeparatedByString:@";"];
                             
                             for (int i=0; i<[arrCode count]; i++)
                             {
                                 NSMutableDictionary *dict = [NSMutableDictionary new];
                                 [dict setObject:[arrCode objectAtIndex:i] forKey:@"code"];
                                 [dict setObject:[arrDate objectAtIndex:i] forKey:@"date"];
                                 [dict setObject:[arrPrice objectAtIndex:i] forKey:@"price"];
                                 [self->arrOffer addObject:dict];
                             }
                             
                             if (self->arrOffer.count>0)
                             {
                                 self->tblViewOffer.hidden = false;
                                 self->viewNoOffer.hidden = true;
                             }
                             else{
                                 self->tblViewOffer.hidden = true;
                                 self->viewNoOffer.hidden = false;
                             }
                             
                             [self->tblViewOffer reloadData];
                         }
                         
                     }
                     else if(![[dictMain objectForKey:@"status"] boolValue])
                     {
                         // [Utils showAlertMessage:KMessageTitle Message:[dictMain objectForKey:@"result"]];
                         self->tblViewOffer.hidden = true;
                         self->viewNoOffer.hidden = false;
                     }
                 }
             }
             @catch (NSException *exception) { }
             @finally {
                 [Helper stopLoading];
             }
         }];
    }
    
}

#pragma mark iPhone
#pragma mark MFSidebarDelegate
- (void)leftSideMenuButtonPressed:(id)sender
{
    [self.menuContainerViewController toggleLeftSideMenuCompletion:^{
    }];
}

- (void)revealLeftSidebar:(id)sender
{
    NSArray *viewControllers = [[self navigationController] viewControllers];
    for( int i=0;i<[viewControllers count];i++){
        id obj=[viewControllers objectAtIndex:i];
        if([obj isKindOfClass:[HomeVC class]]){
            [[self navigationController] popToViewController:obj animated:YES];
        }
    }}



//#pragma mark Request Finished
//- (void)requestFinished:(ASIHTTPRequest *)request
//{
//    [MBProgressHUD hideAllHUDsForView:[AppDelegate sharedDelegate].window animated:YES];
//    NSString *receivedString = [request responseString];
//    NSLog(@"str=%@",receivedString);
//    NSDictionary *dictMain=[receivedString JSONValue];
//    if([[dictMain objectForKey:@"status"] boolValue])
//    {
//        if ([[dictMain objectForKey:@"amount"] isEqualToString:@""])
//        {
//            tblViewOffer.hidden = true;
//            viewNoOffer.hidden = false;
//        }
//        else
//        {
//            NSString *strCode = [[[dictMain objectForKey:@"result"] objectForKey:@"offerData"] objectForKey:@"codeNames"];
//            NSString *strDate = [[[dictMain objectForKey:@"result"] objectForKey:@"offerData"] objectForKey:@"endDate"];
//            NSString *strPrice = [[[dictMain objectForKey:@"result"] objectForKey:@"offerData"] objectForKey:@"price"];
//
//            NSArray *arrCode = [strCode componentsSeparatedByString:@";"];
//            NSArray *arrDate = [strDate componentsSeparatedByString:@";"];
//            NSArray *arrPrice = [strPrice componentsSeparatedByString:@";"];
//
//            for (int i=0; i<[arrCode count]; i++)
//            {
//                NSMutableDictionary *dict = [NSMutableDictionary new];
//                [dict setObject:[arrCode objectAtIndex:i] forKey:@"code"];
//                [dict setObject:[arrDate objectAtIndex:i] forKey:@"date"];
//                [dict setObject:[arrPrice objectAtIndex:i] forKey:@"price"];
//                [arrOffer addObject:dict];
//            }
//
//            if (arrOffer.count>0)
//            {
//                tblViewOffer.hidden = false;
//                viewNoOffer.hidden = true;
//            }
//            else{
//                tblViewOffer.hidden = true;
//                viewNoOffer.hidden = false;
//            }
//
//            [tblViewOffer reloadData];
//        }
//
//    }
//    else if(![[dictMain objectForKey:@"status"] boolValue])
//    {
//       // [Utils showAlertMessage:KMessageTitle Message:[dictMain objectForKey:@"result"]];
//        tblViewOffer.hidden = true;
//        viewNoOffer.hidden = false;
//    }
//}
//
//- (void)requestFailed:(ASIHTTPRequest *)request
//{
//    NSLog(@"Fail");
//    [MBProgressHUD hideAllHUDsForView:[AppDelegate sharedDelegate].window animated:YES];
//}


#pragma mark - UITableViewDataSource
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [arrOffer count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    static NSString *CellIdentifier= @"OfferTVCell";
    
    OfferTVCell* cell = nil;
    cell = (OfferTVCell*)[tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil)
    {
        NSString *nibNameOrNil= @"OfferTVCell";
        
        NSArray *topLevelObjects = [[NSBundle mainBundle] loadNibNamed:nibNameOrNil owner:nil options:nil];
        
        for(id currentObject in topLevelObjects)
        {
            if([currentObject isKindOfClass:[OfferTVCell class]])
            {
                cell = (OfferTVCell*)currentObject;
                break;
            }
        }
    }
   
    
    [cell.contentView setBackgroundColor:[UIColor clearColor]];
    [cell setBackgroundColor:[UIColor clearColor]];
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    
    
    cell.viewContainer.layer.cornerRadius = 5.0f;
    cell.lblFlat.text = [NSString stringWithFormat:@"FLAT Rs.%@",[[arrOffer objectAtIndex:indexPath.row] objectForKey:@"price"]];
    cell.lblCode.text = [[arrOffer objectAtIndex:indexPath.row] objectForKey:@"code"];
    cell.lblDate.text = [NSString stringWithFormat:@"*valid till %@",[[arrOffer objectAtIndex:indexPath.row] objectForKey:@"date"]];

    return cell;
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
