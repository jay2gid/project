//
//  OfferVC.h
//  ToolsOnWheel
//
//  Created by Kapil Goyal on 04/02/18.
//  Copyright © 2018 Kapil Goyal. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface OfferVC : UIViewController<UITableViewDelegate,UITableViewDataSource>
{
    IBOutlet UITableView *tblViewOffer;
    __weak IBOutlet UIView *viewNoOffer;
    NSMutableArray *arrOffer;
}
@end
