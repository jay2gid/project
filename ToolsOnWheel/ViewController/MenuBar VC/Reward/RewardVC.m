//
//  LoginVC.m
//  ToolsOnWheel
//
//  Created by Kapil Goyal on 12/12/17.
//  Copyright © 2017 Kapil Goyal. All rights reserved.
//

#import "RewardVC.h"
#import "RegisterVC.h"
#import "ASIFormDataRequest.h"
#import "JSON.h"
#import "MBProgressHUD.h"
#import "AppDelegate.h"
#import "HomeVC.h"
#import "Utils.h"
#import "Config.h"

#import "CommonHeader.h"
#import "CustomNavigation.h"
#import "MFSideMenu.h"

@interface RewardVC ()
@end


@implementation RewardVC
- (void)viewDidLoad
{
    [super viewDidLoad];
    [CustomNavigation addTarget:self backRequired:NO title:@"REWARD POINTS"];
    for (UIView *v1 in self.view.subviews)
    {
        if ([v1 isKindOfClass:[CommonHeader class]])
        {
            for (UIView *viewTmp in v1.subviews)
            {
                if ([viewTmp isKindOfClass:[UIButton class]])
                {
                    if (viewTmp.tag==1)
                    {
                        viewTmp.hidden=YES;
                    }
                    else if(viewTmp.tag==106)
                    {
                        [(UIButton*)viewTmp addTarget:self action:@selector(revealLeftSidebar:) forControlEvents:UIControlEventTouchUpInside];
                    }
                    else if(viewTmp.tag==102)
                    {
                        // [(UIButton*)viewTmp addTarget:self action:@selector(moveToCart) forControlEvents:UIControlEventTouchUpInside];
                        [(UIButton*)viewTmp setHidden:YES];
                    }
                }
                else if ([viewTmp isKindOfClass:[UIImageView class]])
                {
                    if (viewTmp.tag==101)
                    {
                        [(UIImageView*)viewTmp setImage:[UIImage imageNamed:@"back.png"]];
                    }
                }
                else if ([viewTmp isKindOfClass:[UILabel class]])
                {
                    
                        [(UILabel*)viewTmp setTextColor:[UIColor whiteColor]];
                    
                }
            }
        }
    }
}

-(void)viewWillAppear:(BOOL)animated
{
    [self.menuContainerViewController setPanMode:MFSideMenuPanModeNone];
    if ([AppDelegate checkNetwork])
    {
//        [MBProgressHUD showHUDAddedTo:[AppDelegate sharedDelegate].window animated:YES];
//        ASIFormDataRequest *request = [ASIFormDataRequest requestWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@%@",ServerAdd3,kWalletAmount]]];
//        [request setDelegate:self];
//        [request setPostValue:[[NSUserDefaults standardUserDefaults] objectForKey:@"userid"] forKey:@"userId"];
//        [request startAsynchronous];
        
        NSDictionary *param =@{@"userId":[[NSUserDefaults standardUserDefaults] objectForKey:@"userid"]};
        
        [Helper showLoading];
        [WebServiceCalls POST:kWalletAmount parameter:param completionBlock:^(id JSON, WebServiceResult result)
         {
             @try
             {
                 [Helper stopLoading];
                 
                 if (result == WebServiceResultSuccess)
                 {
                     NSDictionary *dictMain = JSON;
                     
                     if (![[NSUserDefaults standardUserDefaults] objectForKey:@"userid"])
                     {
                         self->lblRewardPoints.text = @"0";
                     }
                     else if ([dictMain[@"status"] integerValue] == 1)
                     {
                         if([[dictMain objectForKey:@"amount"] isEqualToString:@""])
                         {
                             self->lblRewardPoints.text = @"0";
                         }
                         else
                         {
                             self->lblRewardPoints.text = [dictMain objectForKey:@"amount"];
                         }
                     }
                     else if(![[dictMain objectForKey:@"status"] boolValue])
                     {
                         [Utils showAlertMessage:KMessageTitle Message:[dictMain objectForKey:@"result"]];
                     }
                 }
             }
             @catch (NSException *exception) { }
             @finally {
                 [Helper stopLoading];
             }
         }];
    }
    
}

//- (void)requestFinished:(ASIHTTPRequest *)request
//{
//    [MBProgressHUD hideAllHUDsForView:[AppDelegate sharedDelegate].window animated:YES];
//    NSString *receivedString = [request responseString];
//    NSLog(@"str=%@",receivedString);
//    NSDictionary *dictMain=[receivedString JSONValue];
//
//    if (![[NSUserDefaults standardUserDefaults] objectForKey:@"userid"])
//    {
//        lblRewardPoints.text = @"0";
//    }
//      else if([[dictMain objectForKey:@"status"] boolValue])
//        {
//            if([[dictMain objectForKey:@"amount"] isEqualToString:@""])
//            {
//                lblRewardPoints.text = @"0";
//
//            }
//            else
//            {
//                lblRewardPoints.text = [dictMain objectForKey:@"amount"];
//
//            }
//        }
//        else if(![[dictMain objectForKey:@"status"] boolValue])
//        {
//            [Utils showAlertMessage:KMessageTitle Message:[dictMain objectForKey:@"result"]];
//        }
//
//}
//
//
//- (void)requestFailed:(ASIHTTPRequest *) request
//{
//    NSLog(@"Fail");
//    [MBProgressHUD hideAllHUDsForView:[AppDelegate sharedDelegate].window animated:YES];
//    // [Utils showAlertMessage:KMessageTitle Message:@"Network error"];
//}

- (void)btnBackClicked
{
    [self.navigationController popViewControllerAnimated:YES];
}

#pragma mark MFSidebarDelegate
- (void)leftSideMenuButtonPressed:(id)sender
{
    [self.menuContainerViewController toggleLeftSideMenuCompletion:^{
    }];
}

- (void)revealLeftSidebar:(id)sender
{
    NSArray *viewControllers = [[self navigationController] viewControllers];
    for( int i=0;i<[viewControllers count];i++){
        id obj=[viewControllers objectAtIndex:i];
        if([obj isKindOfClass:[HomeVC class]]){
            [[self navigationController] popToViewController:obj animated:YES];
        }
    }
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
