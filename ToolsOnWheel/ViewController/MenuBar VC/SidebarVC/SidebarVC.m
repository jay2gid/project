//
//  SidebarVC.m
//  ToolsOnWheel
//
//  Created by Kapil Goyal on 17/09/17.
//  Copyright © 2017 Kapil Goyal. All rights reserved.
//

#import "SidebarVC.h"
#import "CatTVCell.h"
#import "LoginVC.h"
#import "ContentViewController.h"
#import "ContentViewController.h"
#import "AboutUsVC.h"
#import "RewardVC.h"
#import "ReferEarn.h"
#import "MainOrderVC.h"
#import "MyAccountViewController.h"
#import "Config.h"
#import "GlobalDataPersistance.h"
#import "OfferVC.h"
#import "AppDelegate.h"

@interface SidebarVC ()
{
}

@end

@implementation SidebarVC

- (void)viewDidLoad
{
    [super viewDidLoad];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(loginLogout:) name:@"login_logout" object:nil];
   
    imgViewProfile.layer.cornerRadius = 45.0f;
    [tblViewData setTableHeaderView:viewHeaderTable];
}


-(void)viewWillAppear:(BOOL)animated
{
    if ([[NSUserDefaults standardUserDefaults] objectForKey:@"userid"])
    {
        arrName = [[NSMutableArray alloc] initWithObjects:@"My Account",@"Reward Points",@"My Orders",@"Offers",@"Refer and Earn",@"About Us",@"Logout", nil];
        
        arrImage = [[NSMutableArray alloc] initWithObjects:@"account.png",@"reward.png",@"My_Orders.png",@"offers.png",@"earn.png",@"about.png",@"logout.png", nil];
        
        [lblName setText:[NSString stringWithFormat:@"Welcome, %@",[[NSUserDefaults standardUserDefaults] objectForKey:@"name"]]];
       
        [imgViewProfile sd_setImageWithURL:[NSURL URLWithString:[Helper getUserImage]] placeholderImage:[UIImage imageNamed:@"man1.png"]];
        
    } else {
        arrName = [[NSMutableArray alloc] initWithObjects:@"My Account",@"Reward Points",@"My Orders",@"Offers",@"Refer and Earn",@"About Us",@"Login", nil];
        
        arrImage = [[NSMutableArray alloc] initWithObjects:@"account.png",@"reward.png",@"My_Orders.png",@"offers.png",@"earn.png",@"about.png",@"logout.png", nil];
        lblName.text = @"Welcome, Guest";
    }
}

-(void)loginLogout:(NSNotification*)objNote
{
    arrName = [[NSMutableArray alloc] initWithObjects:@"My Account",@"Reward Points",@"My Orders",@"Offers",@"Refer and Earn",@"About Us",@"Logout", nil];
    
    arrImage = [[NSMutableArray alloc] initWithObjects:@"account.png",@"reward.png",@"My_Orders.png",@"offers.png",@"earn.png",@"about.png",@"logout.png", nil];
    [lblName setText:[NSString stringWithFormat:@"Welcome, %@",[[NSUserDefaults standardUserDefaults] objectForKey:@"name"]]];
    [tblViewData reloadData];
 
    [imgViewProfile sd_setImageWithURL:[NSURL URLWithString:[Helper getUserImage]] placeholderImage:[UIImage imageNamed:@"man1.png"]];

    
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return 7;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier= @"CatTVCell";
    
    CatTVCell* cell = nil;
    cell = (CatTVCell*)[tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil)
    {
        NSString *nibNameOrNil= @"CatTVCell";
        
        NSArray *topLevelObjects = [[NSBundle mainBundle] loadNibNamed:nibNameOrNil owner:nil options:nil];
        
        for(id currentObject in topLevelObjects)
        {
            if([currentObject isKindOfClass:[CatTVCell class]])
            {
                cell = (CatTVCell*)currentObject;
                break;
            }
        }
        
//        cell.selectionStyle = UITableViewCellSelectionStyleNone;
    }
    
    if ([GlobalDataPersistance sharedGlobalDataPersistence].selectedMenuIndex == indexPath.row)
    {
        if ([GlobalDataPersistance sharedGlobalDataPersistence].selectedMenuIndex==0)
        {
            cell.imgCat.image = [UIImage imageNamed:@"account.png"];
        }
        else if ([GlobalDataPersistance sharedGlobalDataPersistence].selectedMenuIndex==1)
        {
            cell.imgCat.image = [UIImage imageNamed:@"rewardP.png"];
        }
        else if ([GlobalDataPersistance sharedGlobalDataPersistence].selectedMenuIndex==2)
        {
            cell.imgCat.image = [UIImage imageNamed:@"myorderP.png"];
        }
        else if ([GlobalDataPersistance sharedGlobalDataPersistence].selectedMenuIndex==3)
        {
            cell.imgCat.image = [UIImage imageNamed:@"offersP.png"];
        }
        else if ([GlobalDataPersistance sharedGlobalDataPersistence].selectedMenuIndex==4)
        {
            cell.imgCat.image = [UIImage imageNamed:@"earnP.png"];
        }
        else if ([GlobalDataPersistance sharedGlobalDataPersistence].selectedMenuIndex==5)
        {
            cell.imgCat.image = [UIImage imageNamed:@"aboutp.png"];
        }
        else if ([GlobalDataPersistance sharedGlobalDataPersistence].selectedMenuIndex==6)
        {
            cell.imgCat.image = [UIImage imageNamed:@"logoutP.png"];
        }
        
        [cell setBackgroundColor:[UIColor whiteColor]];
        cell.lblCat.textColor = [UIColor colorWithRed:240/255.0 green:152/255.0 blue:24/155.0 alpha:1.0f];
    }
    else
    {
        cell.backgroundColor = [UIColor clearColor];
        cell.lblCat.textColor = [UIColor whiteColor];
        if (indexPath.row==0)
        {
            cell.imgCat.image = [UIImage imageNamed:@"accountW.png"];
        }
        else if (indexPath.row==1)
        {
            cell.imgCat.image = [UIImage imageNamed:@"reward.png"];
        }
        else if (indexPath.row==2)
        {
            cell.imgCat.image = [UIImage imageNamed:@"My_Orders.png"];
        }
        else if (indexPath.row==3)
        {
            cell.imgCat.image = [UIImage imageNamed:@"offers.png"];
        }
        else if (indexPath.row==4)
        {
            cell.imgCat.image = [UIImage imageNamed:@"earnW.png"];
        }
        else if (indexPath.row==5)
        {
            cell.imgCat.image = [UIImage imageNamed:@"about.png"];
        }
        else if (indexPath.row==6)
        {
            cell.imgCat.image = [UIImage imageNamed:@"logout.png"];
        }
        
    }
    NSString *name = [NSString stringWithFormat:@"%@",[arrName objectAtIndex:indexPath.row]];
        [cell.lblCat setText:name.uppercaseString];
        //[cell.imgCat setImage:[UIImage imageNamed:[arrImage objectAtIndex:indexPath.row]]];
        [cell.btnCat setTag:indexPath.row];
    
    // [cell.btnCat addTarget:self action:@selector(btnSubCatPressed:) forControlEvents:UIControlEventTouchUpInside];
    return cell;
    
}


-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [GlobalDataPersistance sharedGlobalDataPersistence].selectedMenuIndex = indexPath.row;
    [tblViewData reloadData];
//    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    //CatTVCell *cell = [tblViewData cellForRowAtIndexPath:indexPath];
    //cell.lblCat.textColor = [UIColor colorWithRed:1 green:0 blue:0 alpha:0.7];
    if (indexPath.row==0)
    {
        //        ContentViewController *content = [[ContentViewController alloc] initWithNibName:@"ContentViewController" bundle:nil];
        if (![[NSUserDefaults standardUserDefaults] objectForKey:@"userid"])
        {
            LoginVC *objLogin = [[LoginVC alloc] initWithNibName:@"LoginVC" bundle:nil];
            UINavigationController *navigationController = self.menuContainerViewController.centerViewController;
            //        NSArray *controllers = [NSArray arrayWithObject:objLogin];
            //        navigationController.viewControllers = controllers;
            [navigationController pushViewController:objLogin animated:NO];
            
            [self.menuContainerViewController setMenuState:MFSideMenuStateClosed];
        }
        else
        {
            MyAccountViewController *objReward = [[MyAccountViewController alloc] initWithNibName:@"MyAccountViewController" bundle:nil];
            UINavigationController *navigationController = self.menuContainerViewController.centerViewController;
            //        NSArray *controllers = [NSArray arrayWithObject:objReward];
            //        navigationController.viewControllers = controllers;
            [navigationController pushViewController:objReward animated:NO];
            
            [self.menuContainerViewController setMenuState:MFSideMenuStateClosed];
        }
    }
    
    if (indexPath.row==1)
    {
        //        ContentViewController *content = [[ContentViewController alloc] initWithNibName:@"ContentViewController" bundle:nil];
        if (![[NSUserDefaults standardUserDefaults] objectForKey:@"userid"])
        {
            LoginVC *objLogin = [[LoginVC alloc] initWithNibName:@"LoginVC" bundle:nil];
            UINavigationController *navigationController = self.menuContainerViewController.centerViewController;
            //        NSArray *controllers = [NSArray arrayWithObject:objLogin];
            //        navigationController.viewControllers = controllers;
            [navigationController pushViewController:objLogin animated:NO];
            
            [self.menuContainerViewController setMenuState:MFSideMenuStateClosed];
        }
        else
        {
            RewardVC *objReward = [[RewardVC alloc] initWithNibName:@"RewardVC" bundle:nil];
            UINavigationController *navigationController = self.menuContainerViewController.centerViewController;
            //        NSArray *controllers = [NSArray arrayWithObject:objReward];
            //        navigationController.viewControllers = controllers;
            [navigationController pushViewController:objReward animated:NO];
            
            [self.menuContainerViewController setMenuState:MFSideMenuStateClosed];
        }
       
        
    }
    if (indexPath.row==2)
    {
        //        ContentViewController *content = [[ContentViewController alloc] initWithNibName:@"ContentViewController" bundle:nil];
        if (![[NSUserDefaults standardUserDefaults] objectForKey:@"userid"])
        {
            LoginVC *objLogin = [[LoginVC alloc] initWithNibName:@"LoginVC" bundle:nil];
            UINavigationController *navigationController = self.menuContainerViewController.centerViewController;
            //        NSArray *controllers = [NSArray arrayWithObject:objLogin];
            //        navigationController.viewControllers = controllers;
            [navigationController pushViewController:objLogin animated:NO];
            
            [self.menuContainerViewController setMenuState:MFSideMenuStateClosed];
        }
        else
        {
        MainOrderVC *objReward = [[MainOrderVC alloc] initWithNibName:@"MainOrderVC" bundle:nil];
        UINavigationController *navigationController = self.menuContainerViewController.centerViewController;
        //        NSArray *controllers = [NSArray arrayWithObject:objReward];
        //        navigationController.viewControllers = controllers;
        [navigationController pushViewController:objReward animated:NO];
        
        [self.menuContainerViewController setMenuState:MFSideMenuStateClosed];
        }
        
    }
    
    else if (indexPath.row==3)
    {
        //        ContentViewController *content = [[ContentViewController alloc] initWithNibName:@"ContentViewController" bundle:nil];
        if (![[NSUserDefaults standardUserDefaults] objectForKey:@"userid"])
        {
            LoginVC *objLogin = [[LoginVC alloc] initWithNibName:@"LoginVC" bundle:nil];
            UINavigationController *navigationController = self.menuContainerViewController.centerViewController;
            //        NSArray *controllers = [NSArray arrayWithObject:objLogin];
            //        navigationController.viewControllers = controllers;
            [navigationController pushViewController:objLogin animated:NO];
            
            [self.menuContainerViewController setMenuState:MFSideMenuStateClosed];
        }
        else
        {
            OfferVC *objReward = [[OfferVC alloc] initWithNibName:@"OfferVC" bundle:nil];
            UINavigationController *navigationController = self.menuContainerViewController.centerViewController;
            //        NSArray *controllers = [NSArray arrayWithObject:objReward];
            //        navigationController.viewControllers = controllers;
            [navigationController pushViewController:objReward animated:NO];
            
            [self.menuContainerViewController setMenuState:MFSideMenuStateClosed];
        }
        
    }
    else if (indexPath.row==4)
    {
        if (![[NSUserDefaults standardUserDefaults] objectForKey:@"userid"])
        {
            LoginVC *objLogin = [[LoginVC alloc] initWithNibName:@"LoginVC" bundle:nil];
            UINavigationController *navigationController = self.menuContainerViewController.centerViewController;
            //        NSArray *controllers = [NSArray arrayWithObject:objLogin];
            //        navigationController.viewControllers = controllers;
            [navigationController pushViewController:objLogin animated:NO];
            
            [self.menuContainerViewController setMenuState:MFSideMenuStateClosed];
        }
        else
        {
        ReferEarn *objEarn = [[ReferEarn alloc] initWithNibName:@"ReferEarn" bundle:nil];
        UINavigationController *navigationController = self.menuContainerViewController.centerViewController;
//        NSArray *controllers = [NSArray arrayWithObject:objEarn];
//        navigationController.viewControllers = controllers;
        [navigationController pushViewController:objEarn animated:NO];

        [self.menuContainerViewController setMenuState:MFSideMenuStateClosed];
        }
    }
    else if (indexPath.row==5)
    {
        ContentViewController *content = [[ContentViewController alloc] initWithNibName:@"ContentViewController" bundle:nil];
        UINavigationController *navigationController = self.menuContainerViewController.centerViewController;
//        NSArray *controllers = [NSArray arrayWithObject:content];
//        navigationController.viewControllers = controllers;
        [navigationController pushViewController:content animated:NO];

        [self.menuContainerViewController setMenuState:MFSideMenuStateClosed];
    }
    
    else if (indexPath.row==6)
    {
        NSString *str = [arrName objectAtIndex:indexPath.row];
        if ([str isEqualToString:@"Logout"])
        {
            UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:KMessageTitle message:@"Are you sure you want to logout?" delegate:self cancelButtonTitle:@"No" otherButtonTitles:@"Yes", nil];
            [alertView show];
        }
        else
        {
            LoginVC *objLogin = [[LoginVC alloc] initWithNibName:@"LoginVC" bundle:nil];
            UINavigationController *navigationController = self.menuContainerViewController.centerViewController;
            //        NSArray *controllers = [NSArray arrayWithObject:objLogin];
            //        navigationController.viewControllers = controllers;
            [navigationController pushViewController:objLogin animated:NO];
            
            [self.menuContainerViewController setMenuState:MFSideMenuStateClosed];
        }
        
    }
    
    //UINavigationController *navi = [[UINavigationController alloc] initWithRootViewController:content];
    //    navi.navigationBarHidden = YES;
    //    [navi pushViewController:content animated:YES];
    // [self presentViewController:navi animated:YES completion:nil];
    
}

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex NS_DEPRECATED_IOS(2_0, 9_0);
{
    if(buttonIndex==1)
    {
        [[NSUserDefaults standardUserDefaults] removeObjectForKey:@"userid"];
        [[NSUserDefaults standardUserDefaults] removeObjectForKey:@"name"];
        [[NSUserDefaults standardUserDefaults] removeObjectForKey:@"email"];
        [[NSUserDefaults standardUserDefaults] removeObjectForKey:@"phone"];
        [[NSUserDefaults standardUserDefaults] removeObjectForKey:@"address"];
        [[NSUserDefaults standardUserDefaults] removeObjectForKey:@"areaName"];
        [[NSUserDefaults standardUserDefaults] removeObjectForKey:@"user_image"];

        [imgViewProfile sd_setImageWithURL:[NSURL URLWithString:[Helper getUserImage]] placeholderImage:[UIImage imageNamed:@"man1.png"]];

        
        lblName.text = @"Welcome, Guest";
        [[NSUserDefaults standardUserDefaults] synchronize];
        [[AppDelegate sharedDelegate].arrCartItems removeAllObjects];
        
        arrName = [[NSMutableArray alloc] initWithObjects:@"My Account",@"Reward Points",@"My Orders",@"Offers",@"Refer and Earn",@"About Us",@"Login", nil];
        
        arrImage = [[NSMutableArray alloc] initWithObjects:@"account.png",@"reward.png",@"My_Orders.png",@"offers.png",@"earn.png",@"about.png",@"logout.png", nil];
        [tblViewData reloadData];
        
        HomeVC *objLogin = [[HomeVC alloc] initWithNibName:@"HomeVC" bundle:nil];
        UINavigationController *navigationController = self.menuContainerViewController.centerViewController;
        //        NSArray *controllers = [NSArray arrayWithObject:objLogin];
        //        navigationController.viewControllers = controllers;
        [navigationController pushViewController:objLogin animated:NO];
        
        [self.menuContainerViewController setMenuState:MFSideMenuStateClosed];
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}



@end
