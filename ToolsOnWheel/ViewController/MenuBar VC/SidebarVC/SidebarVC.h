//
//  SidebarVC.h
//  ToolsOnWheel
//
//  Created by Kapil Goyal on 17/09/17.
//  Copyright © 2017 Kapil Goyal. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SidebarVC : UIViewController<UITableViewDelegate,UITableViewDataSource,UIAlertViewDelegate>
{
    NSMutableArray *arrName;
    NSMutableArray *arrImage;
    IBOutlet UITableView *tblViewData;
    IBOutlet UIView *viewHeaderTable;
    IBOutlet UIImageView *imgViewProfile;
    IBOutlet UILabel *lblName;

}
@end
