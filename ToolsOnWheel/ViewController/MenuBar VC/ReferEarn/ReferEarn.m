//
//  LoginVC.m
//  ToolsOnWheel
//
//  Created by Kapil Goyal on 12/12/17.
//  Copyright © 2017 Kapil Goyal. All rights reserved.
//

#import "ReferEarn.h"
#import "RegisterVC.h"
#import "ASIFormDataRequest.h"
#import "JSON.h"
#import "MBProgressHUD.h"
#import "AppDelegate.h"
#import "HomeVC.h"
#import "Utils.h"
#import "Config.h"

#import "CommonHeader.h"
#import "CustomNavigation.h"
#import "MFSideMenu.h"

#define IsRunningTallPhone() ([UIDevice currentDevice].userInterfaceIdiom == UIUserInterfaceIdiomPhone && [UIScreen mainScreen].bounds.size.height == 568)



@interface ReferEarn ()

@end

@implementation ReferEarn
- (void)viewDidLoad
{
    [super viewDidLoad];
    [CustomNavigation addTarget:self backRequired:NO title:@"REFER AND EARN"];
    for (UIView *v1 in self.view.subviews)
    {
        if ([v1 isKindOfClass:[CommonHeader class]])
        {
            for (UIView *viewTmp in v1.subviews)
            {
                if ([viewTmp isKindOfClass:[UIButton class]])
                {
                    if (viewTmp.tag==1)
                    {
                        viewTmp.hidden=YES;
                    }
                    else if(viewTmp.tag==106)
                    {
                        [(UIButton*)viewTmp addTarget:self action:@selector(revealLeftSidebar:) forControlEvents:UIControlEventTouchUpInside];
                    }
                    else if(viewTmp.tag==102)
                    {
                        // [(UIButton*)viewTmp addTarget:self action:@selector(moveToCart) forControlEvents:UIControlEventTouchUpInside];
                        [(UIButton*)viewTmp setHidden:YES];
                    }
                }
                else if ([viewTmp isKindOfClass:[UIImageView class]])
                {
                    if (viewTmp.tag==101)
                    {
                        [(UIImageView*)viewTmp setImage:[UIImage imageNamed:@"back.png"]];
                    }
                }
                else if ([viewTmp isKindOfClass:[UILabel class]])
                {
                    
                    [(UILabel*)viewTmp setTextColor:[UIColor whiteColor]];
                    
                }
            }
        }
    }
    
    if (IsRunningTallPhone())
    {
        viewShare.frame = CGRectMake(viewShare.frame.origin.x, viewShare.frame.origin.y-20, viewShare.frame.size.width, viewShare.frame.size.height);
        [scrlView setContentSize:CGSizeMake(scrlView.frame.size.width, scrlView.frame.size.height)];
    }
    else
    {
        [scrlView setContentSize:CGSizeMake(scrlView.frame.size.width, scrlView.frame.size.height+90)];
    }
    
    lblRewardPoints.text = [[NSUserDefaults standardUserDefaults] objectForKey:@"refcode"];
}


-(void)viewWillAppear:(BOOL)animated
{
    strMessage = [NSString stringWithFormat:@"Hi,\n I am giving you 50 Reward points by using my refer code \"'%@\" for signup in TOOLSONWHEEL and get 50 points redeemed on your first service. One stop solution for AC & Appliances repair, Plumbing, Car wash, Carpentry & Gardening, Download the app now.",[[NSUserDefaults standardUserDefaults] objectForKey:@"refcode"]];
    [self.menuContainerViewController setPanMode:MFSideMenuPanModeNone];
}
- (void)btnBackClicked
{
    [self.navigationController popViewControllerAnimated:YES];
}

#pragma mark MFSidebarDelegate
- (void)leftSideMenuButtonPressed:(id)sender
{
    [self.menuContainerViewController toggleLeftSideMenuCompletion:^{
    }];
}

- (void)revealLeftSidebar:(id)sender
{
    NSArray *viewControllers = [[self navigationController] viewControllers];
    for( int i=0;i<[viewControllers count];i++){
        id obj=[viewControllers objectAtIndex:i];
        if([obj isKindOfClass:[HomeVC class]]){
            [[self navigationController] popToViewController:obj animated:YES];
        }
    }
    
}

-(IBAction)btnFBClicked:(id)sender
{
    NSArray *items = @[strMessage];
    
    // build an activity view controller
    UIActivityViewController *controller = [[UIActivityViewController alloc]initWithActivityItems:items applicationActivities:nil];
    
    // and present it
    [self presentActivityController:controller];
}

-(IBAction)btnWhatupsClicked:(id)sender
{
    NSArray *items = @[strMessage];
    
    // build an activity view controller
    UIActivityViewController *controller = [[UIActivityViewController alloc]initWithActivityItems:items applicationActivities:nil];
    
    // and present it
    [self presentActivityController:controller];
}

-(IBAction)btnTwitterClicked:(id)sender
{
    NSArray *items = @[strMessage];
    
    // build an activity view controller
    UIActivityViewController *controller = [[UIActivityViewController alloc]initWithActivityItems:items applicationActivities:nil];
    
    // and present it
    [self presentActivityController:controller];
}

-(IBAction)btnMoreClicked:(id)sender
{
    NSArray *items = @[strMessage];
    
    // build an activity view controller
    UIActivityViewController *controller = [[UIActivityViewController alloc]initWithActivityItems:items applicationActivities:nil];
    
    // and present it
    [self presentActivityController:controller];
}


- (void)presentActivityController:(UIActivityViewController *)controller {
    
    // for iPad: make the presentation a Popover
    controller.modalPresentationStyle = UIModalPresentationPopover;
    [self presentViewController:controller animated:YES completion:nil];
    
    UIPopoverPresentationController *popController = [controller popoverPresentationController];
    popController.permittedArrowDirections = UIPopoverArrowDirectionAny;
    popController.barButtonItem = self.navigationItem.leftBarButtonItem;
    
    // access the completion handler
    controller.completionWithItemsHandler = ^(NSString *activityType,
                                              BOOL completed,
                                              NSArray *returnedItems,
                                              NSError *error){
        // react to the completion
        if (completed) {
            
            // user shared an item
            NSLog(@"We used activity type%@", activityType);
            
        } else {
            
            // user cancelled
            NSLog(@"We didn't want to share anything after all.");
        }
        
        if (error) {
            NSLog(@"An Error occured: %@, %@", error.localizedDescription, error.localizedFailureReason);
        }
    };
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
