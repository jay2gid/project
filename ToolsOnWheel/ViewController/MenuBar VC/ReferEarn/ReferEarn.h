//
//  LoginVC.h
//  ToolsOnWheel
//
//  Created by Kapil Goyal on 12/12/17.
//  Copyright © 2017 Kapil Goyal. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ReferEarn : UIViewController
{
    IBOutlet UILabel *lblRewardPoints;
    IBOutlet UIView *viewShare;
    IBOutlet UIScrollView *scrlView;
    NSString *strMessage;
}

-(IBAction)btnFBClicked:(id)sender;
-(IBAction)btnWhatupsClicked:(id)sender;
-(IBAction)btnTwitterClicked:(id)sender;
-(IBAction)btnMoreClicked:(id)sender;

@end
