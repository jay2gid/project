//
//  RateOurServiceVC.h
//  ToolsOnWheel
//
//  Created by Shikhar Khanna on 04/03/18.
//  Copyright © 2018 Kapil Goyal. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "HCSStarRatingView.h"

@interface RateOurServiceVC : UIView
{
    IBOutlet UIImageView *imgViewLogo;
    IBOutlet UIView *viewImage;
    IBOutlet UIView *viewContainer;
    IBOutlet UILabel *lblOrderId;
    IBOutlet UILabel *lblTitle;
    IBOutlet UILabel *lblPrice;
    IBOutlet HCSStarRatingView *starView;
    IBOutlet UILabel *lblQty;
    IBOutlet UILabel *lblServiceId;
    IBOutlet UILabel *lblServiceDate;
    IBOutlet UILabel *lblServiceTime;
    IBOutlet NSLayoutConstraint *bottomHeight;
    IBOutlet UIView *viewBottom;
    IBOutlet UIButton *btnSubmit;
    IBOutlet UIButton *btnCancel;
}

@end
