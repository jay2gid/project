//
//  DropDownButton.h
//  My Safe
//
//  Created by Sheetal on 10/3/14.
//  Copyright (c) 2014 Dotsquares. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface DropDownButton : UIButton
@property (nonatomic , assign) int tagValue;
@property (nonatomic , assign) int romNumber;
@property (nonatomic , assign) int tagParent;
@end
