//
//  CartItemData.h
//  CoffeeApp
//
//  Created by Sheetal on 1/27/15.
//  Copyright (c) 2015 Dotsquares. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface CartItemData : NSObject<NSCoding>

@property(retain,nonatomic) NSString *itemId;
@property(retain,nonatomic) NSString *subItemId;
@property(retain,nonatomic) NSString *subChildItemId;

@property(retain,nonatomic) NSString *cartId;
@property(retain,nonatomic) NSString *itemName;
@property(retain,nonatomic) NSString *itemSubName;
@property(retain,nonatomic) NSString *itemPrice;
@property(retain,nonatomic) NSString *itemQty;
@property(retain,nonatomic) NSString *itemDate;
@property(retain,nonatomic) NSString *itemTime;
@property(retain,nonatomic) NSString *itemComment;
@property(retain,nonatomic) NSString *itemDateTime;
@property(retain,nonatomic) NSString *itemStatus;
@property(retain,nonatomic) NSString *itemAvailable;

@property(retain,nonatomic) NSString *itemImage;




@end
