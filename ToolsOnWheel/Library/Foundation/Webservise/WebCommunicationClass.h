
#import <Foundation/Foundation.h>
#import "ASINetworkQueue.h"
#import "ASIHTTPRequest.h"
#import "Config.h"
//#import "NSString+URLEncoding.h" 
@class UserInfo;
@class DDAppDelegate;

@interface WebCommunicationClass : NSObject {

	DDAppDelegate *AnAppDelegatObj;
	NSString *MethoodName;
	id aCaller;
	
}

@property(nonatomic,retain)	id aCaller;
@property(nonatomic,assign)	BOOL isCancelAllRequest;


-(void)user_login:(NSString*)strEmail strPassword:(NSString*)strPassword;
-(void)user_add: (NSString*)strpefix strFName:(NSString*)strFName strLName:(NSString*)strLName strmail:(NSString*)strEmail strPassword:(NSString*)strPassword strDOB:(NSString*)strDOB strGender:(NSString*)strGender;
//=========Login with facebook and google+
-(void)social_login:(NSString *)fName lName:(NSString *)lName gender:(NSString *)gender mail:(NSString *)mail login_type:(NSString *)login_type dob:(NSString *)dob;

-(void)categoryProduct:(NSString *)categoryId limit:(NSString *)limit pageno:(NSString *)pageno sort:(NSString *)sort sortdirection:(NSString *)sortdirection;

-(void)productDetail:(NSString*)productId;
-(void)searchProducts:(NSString*)searchtext;
-(void)submitInventory:(NSMutableArray*)arrInventory;
-(void)ViewAllAddress:(NSString*)strCustomerId;

//-(void)forgotPassword:(NSMutableDictionary*)userData;


-(void)sendOrder:(NSMutableArray*)arrProduct shipping:(NSString *)shipping payment:(NSString *)payment email:(NSString *)email billingAddress:(NSString *)billingAddress shippingAddress:(NSString *)shippingAddress  ;


-(void)DashBoard;
-(void)getMyPropertyList:(NSString*)user_id page_number:(NSString *)page_number keyword:(NSString *)keyword;
-(void)getUserIncidentList:(NSString*)user_id page_number:(NSString *)page_number keyword:(NSString *)keyword;
-(void)getMyPropertyDetails:(NSString*)property_id;
-(void)getIncidentDetails:(NSString*)incident_id;
-(void)AddNotes:(NSMutableDictionary *)note_dictionary;
-(void)DeleteNoteFile:(NSString *)note_id property_id:(NSString*)property_id;
-(void)DeleteIncidentfile:(NSString *)incident_file_id incident_id:(NSString*)incident_id;
-(void)getpropertyandAsbestosItems;
-(void)getRemedialProperty;
-(void)getPropertyAsbestosItem:(NSString*)property_id;
-(void)getAirmonitoringtype;
-(void)addNonlicensedRemedialProject:(NSMutableDictionary *)remedial_dictionary;
-(void)addIncident:(NSMutableDictionary *)incident_dictionary;
-(void)GetNonLicensedRemedialProjects:(NSString *)page_number;
-(void)NonLicensedRemedialProjectsAccept:(NSString *)project_id;
-(void)SiteWorkComplete:(NSMutableDictionary *)sitework_dictionary;
-(void)getpropertyRoom:(NSString *)property_id;
-(void)getpropertyAsbestosData:(NSString *)property_id;
-(void)addPropertyAsbestoSurvey:(NSMutableDictionary *)dictionary;
-(void)getRemedialList:(NSString*)user_id page_number:(NSString *)page_number keyword:(NSString *)keyword;
-(void)getRemedialDetails:(NSString*)remedial_id;
-(void)getSurveyListings:(NSString*)user_id page_number:(NSString *)page_number keyword:(NSString *)keyword;
-(void)getSurveyDetail:(NSString*)survey_id;
-(void)getFiberTypeList;
-(void)getRoomSurvey:(NSString *)room_id;
-(void)getaddAsbestosinfo;
-(void)addPropertyAsbesto:(NSMutableDictionary *)dictionary;
-(void)getPropertyAsbestoSurvey:(NSString*)survey_id;
-(void)getHazardsRisk;

+ (NSString *)urlEncodeValue:(NSString *)str;
+ (NSString *)urlDecodeValue:(NSString *)str;
-(void)ASICallSyncToServerWithFunctionName:(NSString *)FunctionName PostDataDictonery :(NSMutableDictionary *)Dictionery;
-(void) dataDownloadFail:(ASIHTTPRequest*)aReq;
-(void) dataDidFinishDowloading:(ASIHTTPRequest*)aReq;
//-(NSDictionary * )getUTCFormateDate:(NSString *)aDate:(NSString *)aTimeStr;


//Knight Frank Functions
-(void)Getarea;

-(void)getreports:(NSString*)strYear;
-(void)user_add:(NSString*)strEmail strPassword:(NSString*)strPassword strFName:(NSString*)strFName strLName:(NSString*)strLName strPhone:(NSString*)strPhone;
-(void)mySavedproperty:(NSString*)strUserId;
-(void)deleteSavedproperty:(NSString*)strUserId arrPropertyId:(NSMutableArray*)arrPropertyIds;
-(void)Getaboutusdetail;
-(void)GetAasbanner;
-(void)SendMail:(NSString*)to from:(NSString*)from subject:(NSString*)subject message:(NSString*)message propertyid:(NSString*)property_id;
-(void)GetSalesHistory:(NSString*)user_id  propertyid:(NSString*)property_id year:(NSString*)year;
-(void)Getedit:(NSString*)user_id first_name:(NSString*)first_name last_name:(NSString*)last_name phone:(NSString*)phone password:(NSString*)password;
-(void)ForgotPassword:(NSString*)email;
-(void)user_Subscribe:(NSString*)strUserID strRecipt:(NSString*)strRecipt;
@end


@protocol WebCommunicationClassDelegate
-(void) dataDidFinishDowloading:(ASIHTTPRequest*)aReq withMethood:(NSString *)MethoodName withOBJ:(WebCommunicationClass *)aObj;
-(void) dataDownloadFail:(ASIHTTPRequest*)aReq  withMethood:(NSString *)MethoodName;



@end
