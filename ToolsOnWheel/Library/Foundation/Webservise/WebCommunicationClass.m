

#import "WebCommunicationClass.h"
#import "ASIHTTPRequest.h"
#import "ASIFormDataRequest.h"
#import <UIKit/UIKit.h>
#import "AppDelegate.h"
#import "Config.h"
#import "Macors.h"
#import "Utils.h"
#import "MBProgressHUD.h"

@implementation WebCommunicationClass

@synthesize aCaller;

- (id)init {
    self = [super init];
    if (self)
	{
		AnAppDelegatObj=(DDAppDelegate *)[[UIApplication sharedApplication] delegate];
		
    }
    return self;
}
- (void)dealloc
{
//    NSLog(@"%s",__PRETTY_FUNCTION__);
    [aCaller release];
    aCaller = nil;
    [super dealloc];
}

#pragma mark -
#pragma mark - User Defined Methods
-(void)CategoryDrawer
{
    NSMutableDictionary *aUserInfo= [NSMutableDictionary dictionary];
    [self ASICallSyncToServerWithFunctionName:categoryDrawer PostDataDictonery:aUserInfo];
}

-(void)DashBoard
{
    NSMutableDictionary *aUserInfo= [NSMutableDictionary dictionary];
    [self ASICallSyncToServerWithFunctionName:HomePage PostDataDictonery:aUserInfo];
}

-(void)user_login:(NSString*)strEmail strPassword:(NSString*)strPassword
{
    NSMutableDictionary *aUserInfo= [NSMutableDictionary dictionary];
    [aUserInfo setValue:strEmail forKey:@"email"];
    [aUserInfo setValue:strPassword forKey:@"password"];
    [self ASICallSyncToServerWithFunctionName:Login PostDataDictonery:aUserInfo];
}

-(void)user_add: (NSString*)strpefix strFName:(NSString*)strFName strLName:(NSString*)strLName strmail:(NSString*)strEmail strPassword:(NSString*)strPassword strDOB:(NSString*)strDOB strGender:(NSString*)strGender
{
    NSMutableDictionary *aUserInfo= [NSMutableDictionary dictionary];
    [aUserInfo setValue:strpefix forKey:@"prefix"];
    [aUserInfo setValue:strFName forKey:@"first_name"];
    [aUserInfo setValue:strLName forKey:@"last_name"];
    [aUserInfo setValue:strDOB forKey:@"dob"];
    [aUserInfo setValue:strEmail forKey:@"email"];
    [aUserInfo setValue:strGender forKey:@"gender"];
    [aUserInfo setValue:strPassword forKey:@"password"];
    [self ASICallSyncToServerWithFunctionName:Registration PostDataDictonery:aUserInfo];
}

-(void)social_login:(NSString *)fName lName:(NSString *)lName gender:(NSString *)gender mail:(NSString *)mail login_type:(NSString *)login_type dob:(NSString *)dob;
{
    NSMutableDictionary *aUserInfo= [NSMutableDictionary dictionary];
    http://blueheaven.sunnyeyewear.com/json_webservice_api/socialLogin.php?first_name=Vishesh&last_name=shahani&email=doticonic91@gmail.com&dob=07/29/2014&gender=1&type=fb
    [aUserInfo setValue:fName forKey:@"first_name"];
    [aUserInfo setValue:lName forKey:@"last_name"];
    [aUserInfo setValue:gender forKey:@"gender"];
    [aUserInfo setValue:mail forKey:@"email"];
    [aUserInfo setValue:login_type forKey:@"type"];
    [aUserInfo setValue:dob forKey:@"dob"];
    [self ASICallSyncToServerWithFunctionName:SocialLogin PostDataDictonery:aUserInfo];
}



-(void)categoryProduct:(NSString *)categoryId limit:(NSString *)limit pageno:(NSString *)pageno sort:(NSString *)sort sortdirection:(NSString *)sortdirection
{
    NSMutableDictionary *aUserInfo= [NSMutableDictionary dictionary];
    [aUserInfo setValue:categoryId forKey:@"categoryID"];
    [aUserInfo setValue:limit forKey:@"limit"];
    [aUserInfo setValue:pageno forKey:@"pageNo"];
    [aUserInfo setValue:sort forKey:@"sort"];
    [aUserInfo setValue:sortdirection forKey:@"sortDir"];
    [self ASICallSyncToServerWithFunctionName:CategoryProduct PostDataDictonery:aUserInfo];
}

-(void)productDetail:(NSString*)productId
{
    NSMutableDictionary *aUserInfo= [NSMutableDictionary dictionary];
    [aUserInfo setValue:productId forKey:@"productID"];
    [self ASICallSyncToServerWithFunctionName:ProductDetail PostDataDictonery:aUserInfo];
}

-(void)searchProducts:(NSString*)searchtext
{
    NSMutableDictionary *aUserInfo= [NSMutableDictionary dictionary];
    [aUserInfo setValue:searchtext forKey:@"search"];
    [self ASICallSyncToServerWithFunctionName:SearchText PostDataDictonery:aUserInfo];
}


-(void)ViewAllAddress:(NSString*)strCustomerId
{
    NSMutableDictionary *aUserInfo= [NSMutableDictionary dictionary];
    [aUserInfo setValue:[[NSUserDefaults standardUserDefaults] objectForKey:kUserId] forKey:@"customerID"];
    [self ASICallSyncToServerWithFunctionName:ViewAddress PostDataDictonery:aUserInfo];
}


-(void)sendOrder:(NSMutableArray*)arrProduct shipping:(NSString *)shipping payment:(NSString *)payment email:(NSString *)email billingAddress:(NSString *)billingAddress shippingAddress:(NSString *)shippingAddress
{
    NSMutableDictionary *aUserInfo= [NSMutableDictionary dictionary];
    [aUserInfo setValue:arrProduct forKey:@"productID"];
    [aUserInfo setValue:@"flatrate_flatrate" forKey:@"shipping"];
    [aUserInfo setValue:@"cashondelivery" forKey:@"payment"];

    [aUserInfo setValue:email forKey:@"email"];
    [aUserInfo setValue:@"43" forKey:@"billingAddressID"];
    [aUserInfo setValue:@"43" forKey:@"shippingAddressID"];

    [self ASICallSyncToServerWithFunctionName:OrderCreate PostDataDictonery:aUserInfo];
}

-(void)savedproperty:(NSString*)strUserId propertyId:(NSString*)strPropertyId;
{
    NSMutableDictionary *aUserInfo= [NSMutableDictionary dictionary];
    [aUserInfo setValue:strUserId forKey:@"user_id"];
    [aUserInfo setValue:strPropertyId forKey:@"property_id"];
    [self ASICallSyncToServerWithFunctionName:SavedProperty PostDataDictonery:aUserInfo];
}

-(void)mySavedproperty:(NSString*)strUserId
{
    NSMutableDictionary *aUserInfo= [NSMutableDictionary dictionary];
    [aUserInfo setValue:strUserId forKey:@"user_id"];
    [self ASICallSyncToServerWithFunctionName:mySavedProperty PostDataDictonery:aUserInfo];
}

-(void)deleteSavedproperty:(NSString*)strUserId arrPropertyId:(NSMutableArray*)arrPropertyIds;
{
    NSMutableDictionary *aUserInfo= [NSMutableDictionary dictionary];
    [aUserInfo setValue:strUserId forKey:@"user_id"];
    [aUserInfo setValue:arrPropertyIds forKey:@"property_id"];
    [self ASICallSyncToServerWithFunctionName:deleteSavedProperty PostDataDictonery:aUserInfo];
}

-(void)Getaboutusdetail
{
    NSMutableDictionary *aUserInfo= [NSMutableDictionary dictionary];
    [self ASICallSyncToServerWithFunctionName:Getcontact PostDataDictonery:aUserInfo];
}
-(void)GetAasbanner
{
    //[LetterProgressView showHUDAddedTo:[AppDelegate sharedDelegate].window animated:YES];
    NSMutableDictionary *aUserInfo= [NSMutableDictionary dictionary];
    [self ASICallSyncToServerWithFunctionName:Getadvertisement PostDataDictonery:aUserInfo];
}
-(void)SendMail:(NSString*)to from:(NSString*)from subject:(NSString*)subject message:(NSString*)message propertyid:(NSString*)property_id;
{
    NSMutableDictionary *aUserInfo= [NSMutableDictionary dictionary];
    [aUserInfo setValue:to forKey:@"to"];
    [aUserInfo setValue:from forKey:@"from"];
    [aUserInfo setValue:subject forKey:@"subject"];
    [aUserInfo setValue:message forKey:@"message"];
    [aUserInfo setValue:property_id forKey:@"property_id"];
    [self ASICallSyncToServerWithFunctionName:Sendmail PostDataDictonery:aUserInfo];
}

-(void)GetSalesHistory:(NSString*)user_id  propertyid:(NSString*)property_id year:(NSString*)year
{
    NSMutableDictionary *aUserInfo= [NSMutableDictionary dictionary];
    [aUserInfo setValue:user_id forKey:@"user_id"];
    [aUserInfo setValue:property_id forKey:@"property_id"];
    [aUserInfo setValue:year forKey:@"date_sold"];
    [self ASICallSyncToServerWithFunctionName:GetSaleshistory PostDataDictonery:aUserInfo];
}
-(void)Getedit:(NSString*)user_id first_name:(NSString*)first_name last_name:(NSString*)last_name phone:(NSString*)phone password:(NSString*)password
{
    NSMutableDictionary *aUserInfo= [NSMutableDictionary dictionary];
    [aUserInfo setValue:user_id forKey:@"user_id"];
    [aUserInfo setValue:first_name forKey:@"first_name"];
    [aUserInfo setValue:last_name forKey:@"last_name"];
    [aUserInfo setValue:phone forKey:@"phone"];
    [aUserInfo setValue:password forKey:@"password"];
    [self ASICallSyncToServerWithFunctionName:Editprofile PostDataDictonery:aUserInfo];
}
-(void)ForgotPassword:(NSString*)email
{
    NSMutableDictionary *aUserInfo= [NSMutableDictionary dictionary];
    [aUserInfo setValue:email forKey:@"email"];
    [self ASICallSyncToServerWithFunctionName:Getforgot_password PostDataDictonery:aUserInfo];
}
-(void)user_Subscribe:(NSString*)strUserID strRecipt:(NSString*)strRecipt
{
    NSMutableDictionary *aUserInfo= [NSMutableDictionary dictionary];
    [aUserInfo setValue:strUserID forKey:@"user_id"];
    [aUserInfo setValue:strRecipt forKey:@"reciept_data"];
    [self ASICallSyncToServerWithFunctionName:Subscription PostDataDictonery:aUserInfo];
}
#pragma mark - Class Methods
+ (NSString *)urlEncodeValue:(NSString *)str
{
	NSString *result = (NSString *) CFURLCreateStringByAddingPercentEscapes(kCFAllocatorDefault, (CFStringRef)str, NULL, CFSTR("?=&+"), kCFStringEncodingUTF8);
	return [result autorelease];
}
+ (NSString *)urlDecodeValue:(NSString *)str
{
	//NSString *result = (NSString *) CFURLCreateStringByReplacingPercentEscapes(kCFAllocatorDefault, (CFStringRef)str, NULL, CFSTR("?=&+"), kCFStringEncodingUTF8);
	NSString *result = (NSString *) CFURLCreateStringByReplacingPercentEscapesUsingEncoding(kCFAllocatorDefault, (CFStringRef)str,  CFSTR("?=&+"), kCFStringEncodingUTF8);
	return [result autorelease];
}


-(void)ASICallSyncToServerWithFunctionName:(NSString *)FunctionName PostDataDictonery :(NSMutableDictionary *)Dictionery
{
    if ([AppDelegate checkNetwork])
    {
        [self retain];
        NSString *JsonString=nil;
        NSError *error;
        NSData *jsonData = [NSJSONSerialization dataWithJSONObject:Dictionery
                                                           options:NSJSONWritingPrettyPrinted // Pass 0 if you don't care about the readability of the generated string
                                                             error:&error];
        
        if (! jsonData) {
            NSLog(@"Got an error: %@", error);
        }
        else
        {
            JsonString = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
            [UIApplication sharedApplication].networkActivityIndicatorVisible = YES;
            
            NSLog(@"FunctionName-->> %@",FunctionName);
            NSLog(@"requestjson-->> %@",JsonString);
            
            MethoodName=FunctionName;
            NSString *urlString = [NSString stringWithFormat:@"%@%@",ServerAdd,FunctionName];
            NSLog(@"ServerAdd-->> %@",ServerAdd);
            NSURL *aUrl=[[[NSURL alloc]initWithString:urlString]autorelease];
            
            ASIFormDataRequest *anASIReq = [ASIFormDataRequest requestWithURL:aUrl];
            
            //anASIReq.methodName=FunctionName;
            
            

//            [anASIReq setPostValue:FunctionName forKey:@"action"];
            [anASIReq setPostValue:JsonString forKey:@"json"];
            
          
            
            anASIReq.delegate = self;
            anASIReq.didFailSelector = @selector(dataDownloadFail:);
            anASIReq.didFinishSelector = @selector(dataDidFinishDowloading:);
            anASIReq.requestMethod = @"POST";
            
            [JsonString release];
            [anASIReq startAsynchronous];
        }
    }
//    else
//    {
//        //[SVProgressHUD dismiss];
//    }
}

-(void)submitInventory:(NSMutableArray*)arrInventory
{
    NSMutableDictionary *aUserInfo= [NSMutableDictionary dictionary];
    [aUserInfo setValue:arrInventory forKey:@"product"];
    [self ASICallSyncToServerWithFunctionName:kInsertInventory PostDataDictonery:aUserInfo];
}

#pragma mark - Response Methods
-(void) dataDidFinishDowloading:(ASIHTTPRequest*)aReq
{
    NSString *aStrResult=[[[NSString alloc] initWithData:[aReq responseData] encoding:NSUTF8StringEncoding] autorelease];
    NSLog( @"%@",aStrResult);
    [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
    
    NSArray *arrMethodNames = [NSArray arrayWithObjects:kInsertInventory,
                               HomePage,categoryDrawer,GetSearchResult,SocialLogin,CategoryProduct,ProductDetail,SearchText,getReports,Login,Registration,ViewAddress,SavedProperty,mySavedProperty,deleteSavedProperty,Getcontact,Getadvertisement,Sendmail,GetSaleshistory,Getforgot_password,Editprofile,Subscription,OrderCreate,
                               nil];

    if ([arrMethodNames containsObject:MethoodName])
    {
        [self navigateWithData:aReq];
		return;
    }
}

-(void) dataDownloadFail:(ASIHTTPRequest*)aReq
{
//    [ DismissFromView:[AppDelegate sharedDelegate].window];
	[UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
   // if([aCaller respondsToSelector:@selector(dataDownloadFail:withMethood:)])
    {
        //[aCaller dataDownloadFail:aReq withMethood:aReq.methodName];
        NSLog(@"DATA DOWNLOAD Error--%@",aReq.error);
        [Utils showAlertMessage:KMessageTitle Message:[NSString stringWithFormat:[aReq.error localizedDescription]]];
    }
}

#pragma mark - Navigation methods
- (void)navigateWithData:(ASIHTTPRequest*)aReq
{
    //[SVProgressHUD dismiss];
    
    //if([[AppDel.currentTabBarNavigation.viewControllers lastObject] isEqual:self.aCaller])
   // {
        [self.aCaller dataDidFinishDowloading:aReq withMethood:MethoodName withOBJ:self];
   // }
  //  else
  //  {
   //     [self removeWebComm:self];
   // }
    
    [aCaller release];
    aCaller = nil;
    
}
//- (UINavigationController *)getCurrentNavigation:(MainViewController *)mainController
//{
////    if ([mainController.aDetailView.NavReport.view isEqual:mainController.aDetailView]) {
////        
////    }
//}
-(void)removeWebComm:(WebCommunicationClass*)webComm
{
    @try {
        if (webComm) {
            
            [webComm setACaller:nil];
            [webComm release];
            webComm = nil;
            
        }
    }
    @catch (NSException *exception) {
        NSLog(@"Exception : %@ %s",exception.description,__PRETTY_FUNCTION__);
    }
    @finally {
        
    }
    
}
@end
