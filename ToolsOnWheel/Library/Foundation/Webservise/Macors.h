//
//  Macors.h
//  Knight Frank
//
//  Created by Ashok Jangid on 28/11/14.
//  Copyright (c) 2014 Dotsquares. All rights reserved.
//

#ifndef Knight_Frank_Macors_h
#define Knight_Frank_Macors_h
#define KDeleteSavedSearch @"DELETE_SAVED_SEARCH"


#define KMessageTitle @"Blue Heaven"
#define kSelectYear @"selectyear"


#define kSelectHistoryYear @"select_history_year"
#define kSavedSelectHistoryYear @"select_saved_history_year"

#define currentLatitude @"latitude"
#define currentLongitude @"longitude"

#define kStartMonth @"startmonth"
#define kStartYear  @"startyear"
#define kStartDate  @"startDate"

#define kEndDate  @"EndDate"
#define kselectedArea  @"selectedSearch"


#define kEndMonth @"endmonth"
#define kEndYear  @"endyear"

#define kUserId @"user_id"

#define kMinCore @"mincore"
#define kMaxCore @"maxcore"
#define kMinIRR @"minirr"
#define kMaxIRR @"maxirr"
#define kMinNLA @"minnla"
#define kMaxNLA @"maxnla"
#define kMinPrice @"minprice"
#define kMaxPrice @"maxprice"
#define kMinWale @"minwale"
#define kMaxWale @"maxwale"


#define kMinCoreValue @"mincorevalue"
#define kMaxCoreValue @"maxcorevalue"
#define kMinIRRValue @"minirrvalue"
#define kMaxIRRValue @"maxirrvalue"
#define kMinNLAValue @"minnlavalue"
#define kMaxNLAValue @"maxnlavalue"
#define kMinPriceValue @"minpricevalue"
#define kMaxPriceValue @"maxpricevalue"
#define kMinWaleValue @"minwalevalue"
#define kMaxWaleValue @"maxwalevalue"

#define kTermCondition @"termcondition"

#define kAreaValue @"areavalue"

#define IsRunningTallPhone() ([UIDevice currentDevice].userInterfaceIdiom == UIUserInterfaceIdiomPhone && [UIScreen mainScreen].bounds.size.height == 568)

#define SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(v)   ([[[UIDevice currentDevice] systemVersion] compare:v options:NSNumericSearch] !=   NSOrderedAscending)

#endif
