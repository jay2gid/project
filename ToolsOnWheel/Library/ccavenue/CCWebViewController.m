//
//  CCPOViewController.m
//  CCIntegrationKit
//
//  Created by test on 5/12/14.
//  Copyright (c) 2014 Avenues. All rights reserved.
//

#import "CCWebViewController.h"
#import "RSA.h"

@interface CCWebViewController ()
{
    IBOutlet UIView *viewHeader;
    
    IBOutlet UILabel *lblStatus;
    
    IBOutlet UILabel *lblOrerID;
    IBOutlet UILabel *lblTID;
    
    NSString *trackingID,*dateTime,*mode,*bank_ref_no,*status;
    
    
}
@end

@implementation CCWebViewController

@synthesize rsaKeyUrl;@synthesize accessCode;@synthesize merchantId;@synthesize orderId;
@synthesize amount;@synthesize currency;@synthesize redirectUrl;@synthesize cancelUrl,wallet,address;

- (void)viewDidLoad
{
    [super viewDidLoad];
    
   
    
    viewWeb.delegate = self;
    
    //Getting RSA Key
    NSString *rsaKeyDataStr = [NSString stringWithFormat:@"access_code=%@&order_id=%@",accessCode,orderId];
    
    NSData *requestData = [NSData dataWithBytes: [rsaKeyDataStr UTF8String] length: [rsaKeyDataStr length]];
    
    NSMutableURLRequest *rsaRequest = [[NSMutableURLRequest alloc] initWithURL: [NSURL URLWithString: rsaKeyUrl]];
    
    [rsaRequest setValue:@"application/x-www-form-urlencoded" forHTTPHeaderField:@"content-type"];
    
    [rsaRequest setHTTPMethod: @"POST"];
    
    [rsaRequest setHTTPBody: requestData];
    
    NSData *rsaKeyData = [NSURLConnection sendSynchronousRequest: rsaRequest returningResponse: nil error: nil];
    
    NSString *rsaKey = [[NSString alloc] initWithData:rsaKeyData encoding:NSASCIIStringEncoding];
    
    rsaKey = [rsaKey stringByTrimmingCharactersInSet:[NSCharacterSet newlineCharacterSet]];
    
    rsaKey = [NSString stringWithFormat:@"-----BEGIN PUBLIC KEY-----\n%@\n-----END PUBLIC KEY-----\n",rsaKey];
    NSLog(@"%@",rsaKey);
    
    //Encrypting Card Details
    
    NSString *myRequestString = [NSString stringWithFormat:@"amount=%@&currency=%@",amount,currency];

    
    NSString *encVal = [RSA encryptString:myRequestString publicKey:rsaKey];
    
    encVal = (NSString *)CFBridgingRelease(CFURLCreateStringByAddingPercentEscapes(NULL,
                                                                                   (CFStringRef)encVal,
                                                                                   NULL,
                                                                                   (CFStringRef)@"!*'();:@&=+$,/?%#[]",
                                                                                   kCFStringEncodingUTF8 ));
    
    //Preparing for a webview call
    
    NSString *urlAsString = [NSString stringWithFormat:@"https://secure.ccavenue.com/transaction/initTrans"];
    
    NSString *email = @"";
    if (USER_EMAIL) {
        email = USER_EMAIL;
    }
    
    NSString *encryptedStr = [NSString stringWithFormat:@"merchant_id=%@&order_id=%@&redirect_url=%@&cancel_url=%@&enc_val=%@&access_code=%@&billing_address=%@&walletamount=%@&billing_name=%@&billing_country=India&billing_tel=%@&billing_email=%@&billing_city=Jaipur&billing_state=Rajasthan",merchantId,orderId,redirectUrl,cancelUrl,encVal,accessCode,address,wallet,USER_NAME,USER_PHONE,email];
    
    NSData *myRequestData = [NSData dataWithBytes: [encryptedStr UTF8String] length: [encryptedStr length]];
    
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] initWithURL: [NSURL URLWithString: urlAsString]];
    
    [request setValue:@"application/x-www-form-urlencoded" forHTTPHeaderField:@"content-type"];
    
    [request setValue:urlAsString forHTTPHeaderField:@"Referer"];
    
    [request setHTTPMethod: @"POST"];
    
    [request setHTTPBody: myRequestData];
    
    
    
    [viewWeb loadRequest:request];
}


-(void)webViewDidStartLoad:(UIWebView *)webView
{
    indicator.hidden = false;
    [indicator startAnimating];
}

- (void)webViewDidFinishLoad:(UIWebView *)webView
{
    
    [indicator stopAnimating];
    indicator.hidden = true;
    
    NSString *string = webView.request.URL.absoluteString;

    if ([string rangeOfString:redirectUrl].location != NSNotFound)
    {
        NSString *html = [webView stringByEvaluatingJavaScriptFromString:@"document.documentElement.outerHTML"];

        NSString *transStatus = @"Not Known";
 
        if(([html rangeOfString:@"tracking_id"].location != NSNotFound) &&  ([html rangeOfString:@"bin_country"].location != NSNotFound))
        {
            
            trackingID = [NSString stringWithFormat:@"%ld",(long)[[html substringWithRange:NSMakeRange([html rangeOfString:@"tracking_id"].location + 20, 20)] integerValue]];
            
            if (([html rangeOfString:@"bank_ref_no"].location != NSNotFound)) {
               
                bank_ref_no =  [html substringWithRange:NSMakeRange([html rangeOfString:@"bank_ref_no"].location + 20, 50)];
                bank_ref_no  = [bank_ref_no substringWithRange:NSMakeRange(0, [bank_ref_no rangeOfString:@"</td>"].location)];
                
            }

            if (([html rangeOfString:@"payment_mode"].location != NSNotFound)) {
                
                mode =  [html substringWithRange:NSMakeRange([html rangeOfString:@"payment_mode"].location + 21, 50)];
                mode  = [mode substringWithRange:NSMakeRange(0, [mode rangeOfString:@"</td>"].location)];
                
            }

            
          

            
            
            if (([html rangeOfString:@"Aborted"].location != NSNotFound) ||
                ([html rangeOfString:@"Cancel"].location != NSNotFound)) {
                transStatus = @"Transaction Cancelled";
                [self dismissViewControllerAnimated:true completion:nil];

                status = @"Aborted";
            }
            
            else if (([html rangeOfString:@"Success"].location != NSNotFound)) {
                transStatus = @"Transaction Successful";
                status = @"Success";

               
                
                viewWeb.hidden = true;
                
                [self callAPI];

            }
            else if (([html rangeOfString:@"Fail"].location != NSNotFound)) {
                
                status = @"Fail";
                transStatus = @"Transaction Failed";
                [self callAPI];
            }
            
            viewWeb.hidden = true;


          //  lblStatus.text = transStatus;
          //  lblOrerID.text = [NSString stringWithFormat:@"ORDER ID : %@" , orderId];
            

//            CCResultViewController* controller = [self.storyboard instantiateViewControllerWithIdentifier:@"CCResultViewController"];
//
//            controller.transStatus = transStatus;
//
//            controller.modalTransitionStyle = UIModalTransitionStyleCrossDissolve;
//
//            [self presentViewController:controller animated:YES completion:nil];
        }
    }
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    
}


- (IBAction)tapCancel:(id)sender {
 
    [self dismissViewControllerAnimated:true completion:nil];
}

- (IBAction)tapDone:(id)sender {
    [_delegate onTransiction:successCC];
   [self dismissViewControllerAnimated:true completion:nil];

}


-(void)callAPI{
    
    //trackingID = @"demo";
    viewHeader.hidden = true;
    HUD_SHOW
    NSDictionary *dict= @{@"OrderId":orderId,
                          @"action":@"afterPaymentSucess",
                          @"tracking_id":trackingID,
                          @"bank_ref_no":bank_ref_no,
                          @"payment_amount":amount,
                          @"order_status":status,
                          @"payment_mode":mode,
                          @"dateTime":[WebServiceCalls getCurrentDate],
                          @"rewardPoint":wallet,
                          @"userId":USER_ID};
    
   // http://www.toolsonwheel.com/tools/apidata.php?action=afterPaymentSucess&OrderId=101459&bank_ref_no=20180406111212800110168055622448633&dateTime=2018-04-06%2010:16:09&order_status=Success&payment_amount=1&payment_mode=ONLINE&rewardPoint=0&tracking_id=107352659861&userId=2515
    
   //   NSString *urlString = [NSString stringWithFormat:@"afterPaymentSucess&OrderId=%@&bank_ref_no=%@&dateTime=%@&order_status=%@&payment_amount=%@&payment_mode=%@&rewardPoint=0&tracking_id=107352659861&userId=2515"];
    
    [WebServiceCalls POST:@"afterPaymentSucess" parameter:dict completionBlock:^(id JSON, WebServiceResult result)
     {
         HUD_HIDE
         @try{
             NSLog(@"%@", JSON);
             if (result == WebServiceResultSuccess) {

//                 [_delegate onTransiction:successCC];
//                 [self dismissViewControllerAnimated:true completion:nil];
                 
                 if ([JSON[@"status"] integerValue] == 1) {
                     
                     
                     if ([self->status isEqualToString:@"Success"]) {
                         [self->_delegate onTransiction:successCC tid:self->trackingID];
                     }else{
                         [self->_delegate onTransiction:cancelCC tid:self->trackingID];
                     }
                     
                     [self dismissViewControllerAnimated:true completion:nil];
                     
                 }
                 
                 else
                 {
                 
                     [WebServiceCalls alert:@"Payment Failed"];
                 }
             }
         }
         @catch (NSException *exception) { }
         @finally { }
     }];
}

@end
