//
//  CCPOViewController.h
//  CCIntegrationKit
//
//  Created by test on 5/12/14.
//  Copyright (c) 2014 Avenues. All rights reserved.
//

#import <UIKit/UIKit.h>


typedef enum : NSUInteger {
    successCC,
    failedCC,
    cancelCC,
} ccStatus;


@protocol transictionDelegate <NSObject>

-(void)onTransiction:(ccStatus )status;
-(void)onTransiction:(ccStatus )status tid:(NSString*)tid;

@end

@interface CCWebViewController : UIViewController <UIWebViewDelegate>
{
    IBOutlet UIWebView *viewWeb;
    IBOutlet UIActivityIndicatorView *indicator;
}

@property (strong, nonatomic) NSString *accessCode;
@property (strong, nonatomic) NSString *merchantId;
@property (strong, nonatomic) NSString *orderId;
@property (strong, nonatomic) NSString *amount;
@property (strong, nonatomic) NSString *currency;
@property (strong, nonatomic) NSString *redirectUrl;
@property (strong, nonatomic) NSString *cancelUrl;
@property (strong, nonatomic) NSString *rsaKeyUrl;
@property (strong, nonatomic) NSString *address;
@property (strong, nonatomic) NSString *wallet;

@property (strong, nonatomic) id<transictionDelegate> delegate;

@end
