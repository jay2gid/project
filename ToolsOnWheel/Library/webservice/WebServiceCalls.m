//
//  WebServiceCalls.m
//  Ponder_remake
//
//  Created by Yudiz Solutions on 04/07/13.
//  Copyright (c) 2013 Yudiz Solutions. All rights reserved.
//

#import "WebServiceCalls.h"
#import "AFNetworking.h"
#import "Reachabilit.h"


#define BASE_URL @"http://www.toolsonwheel.com/apidata.php?action="

static AFHTTPSessionManager *manager;


@interface WebServiceCalls(){
}
@end

static NSString *getuserphone;

@implementation WebServiceCalls

+ (void)initialize
{
    
    manager = [[AFHTTPSessionManager alloc]init];
    manager.responseSerializer = [AFJSONResponseSerializer serializerWithReadingOptions:NSJSONReadingAllowFragments | NSJSONReadingMutableContainers | NSJSONReadingMutableLeaves];
}

+ (void)POST:(NSString *)url parameter:(NSDictionary *)parameter completionBlock:(WebCallBlock)block
{
    if ([self isNetwork])
    {
        @try
        {
            NSString *URLSTRING = [NSString stringWithFormat:@"%@%@",BASE_URL,url];
            
            [manager POST:URLSTRING parameters:parameter constructingBodyWithBlock:^(id<AFMultipartFormData>  _Nonnull formData) {
                //
            } progress:^(NSProgress * _Nonnull uploadProgress) {
                //
            } success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject)
             {
                 //                 NSMutableArray * responseJson = [[NSMutableArray alloc]init];
                 //                 responseJson = [NSJSONSerialization JSONObjectWithData:responseObject options:0 error:nil];
                 block(responseObject,WebServiceResultSuccess);
             }
                  failure:^(NSURLSessionDataTask *dataTask, NSError *error)
             {
                 
                 block(@"1",WebServiceResultSuccess);
             }];
            
        }
        @catch (NSException *exception)
        {

            block(@"1",WebServiceResultSuccess);
        }
    }
}

+ (void)POST:(NSString *)url parameter:(NSDictionary *)parameter imageData:(NSData *)imageData completionBlock:(WebCallBlock)block
{
    
    @try
    {
        
        NSString *URLSTRING = [NSString stringWithFormat:@"%@%@",BASE_URL,url];
        
        [manager POST:URLSTRING parameters:parameter constructingBodyWithBlock:^(id<AFMultipartFormData>  _Nonnull formData)
        {
            NSString *imageName = [NSString stringWithFormat:@"%@.jpg",[self isDateTime]];
            
            [formData appendPartWithFileData:imageData name:@"image" fileName:imageName mimeType:@"image/jpeg"];
            
        } progress:^(NSProgress * _Nonnull uploadProgress){
            
        } success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject)
        {
            block(responseObject,WebServiceResultSuccess);
        }
    failure:^(NSURLSessionDataTask *dataTask, NSError *error)
        {
            block(@"1",WebServiceResultSuccess);
            
        }];
        

    }
    @catch (NSException *exception)
    {

        block(@"1",WebServiceResultSuccess);
    }
    
}




+ (void)POST_WITH_URL:(NSString *)url parameter:(NSDictionary *)parameter completionBlock:(WebCallBlock)block
{
    if ([self isNetwork])
    {
        @try
        {
            NSString *URLSTRING = url;
            
            [manager POST:URLSTRING parameters:parameter constructingBodyWithBlock:^(id<AFMultipartFormData>  _Nonnull formData) {
                //
            } progress:^(NSProgress * _Nonnull uploadProgress) {
                //
            } success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject)
             {
                 //                 NSMutableArray * responseJson = [[NSMutableArray alloc]init];
                 //                 responseJson = [NSJSONSerialization JSONObjectWithData:responseObject options:0 error:nil];
                 block(responseObject,WebServiceResultSuccess);
             }
                  failure:^(NSURLSessionDataTask *dataTask, NSError *error)
             {
                 
                 block(@"1",WebServiceResultSuccess);
             }];
            
        }
        @catch (NSException *exception)
        {
            
            block(@"1",WebServiceResultSuccess);
        }
    }
}



+ (void)POST_UploadFile:(NSString *)url parameter:(NSDictionary *)parameter imageData:(NSData *)imageData completionBlock:(WebCallBlock)block
{
    
    @try
    {
        
        /*NSData *userData = [[NSUserDefaults standardUserDefaults]objectForKey:@"userDetails"];
        NSDictionary *userDict = (NSDictionary*) [NSKeyedUnarchiver unarchiveObjectWithData:userData];
        NSString *txt = [NSString stringWithFormat:@"%@",userDict[@"authenticationtoken"]];
        
        [manager.requestSerializer setValue:User_id forHTTPHeaderField:@"userid"];
        [manager.requestSerializer setValue:txt forHTTPHeaderField:@"authenticationtoken"];*/
        
        [manager POST:url parameters:parameter constructingBodyWithBlock:^(id<AFMultipartFormData>  _Nonnull formData)
         {
             if (imageData)
             {
                 NSString *date = [self isDateTime];
                 
                 NSString *fName = [NSString stringWithFormat:@"%@_image_iPhone.jpg", date];
                 
                 [formData appendPartWithFileData:imageData name:@"image1" fileName:fName mimeType:@"image/jpeg"];
             }
             
         } progress:^(NSProgress * _Nonnull uploadProgress){
             
         } success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject)
         {
             block(responseObject,WebServiceResultSuccess);
         }
              failure:^(NSURLSessionDataTask *dataTask, NSError *error)
         {
             
             block(@"1",WebServiceResultSuccess);
             
         }];
        
        
        
        
    }
    @catch (NSException *exception)
    {
        

        block(@"1",WebServiceResultSuccess);
    }
    
}


+ (void)GET:(NSString *)url parameter:(NSDictionary *)parameter completionBlock:(WebCallBlock)block
{
    
    
    @try
    {
        
        
       /* NSString *URLSTRING = [NSString stringWithFormat:@"%@%@",BASE_URL,url];
        
        NSData *userData = [[NSUserDefaults standardUserDefaults]objectForKey:@"userDetails"];
        NSDictionary *userDict = (NSDictionary*) [NSKeyedUnarchiver unarchiveObjectWithData:userData];
        NSString *txt = [NSString stringWithFormat:@"%@",userDict[@"authenticationtoken"]];
        
        [manager.requestSerializer setValue:User_id forHTTPHeaderField:@"userid"];
        [manager.requestSerializer setValue:txt forHTTPHeaderField:@"authenticationtoken"];
        
        
        [manager GET:[URLSTRING stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding] parameters:nil progress:^(NSProgress * _Nonnull downloadProgress) {
            
        } success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
            block(responseObject,WebServiceResultSuccess);

        } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        
            block(@"1",WebServiceResultSuccess);

        }];
        
        [manager GET:URLSTRING parameters:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
            
            NSLog(@"%@",responseObject);
            NSLog(@"%@",responseObject);
            
        } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
            
        }];*/
        
        
                      //
                           
                           NSString *URLSTRING = [[NSString stringWithFormat:@"%@%@",BASE_URL,url] stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
                           
                           /*NSData *userData = [[NSUserDefaults standardUserDefaults]objectForKey:@"userDetails"];
                           NSDictionary *userDict = (NSDictionary*) [NSKeyedUnarchiver unarchiveObjectWithData:userData];
                           NSString *txt = [NSString stringWithFormat:@"%@",userDict[@"authenticationtoken"]];*/
                    
                           NSMutableURLRequest *request=[NSMutableURLRequest requestWithURL:[NSURL URLWithString:URLSTRING]];
                           
                           /*[request setValue:txt forHTTPHeaderField:@"authenticationtoken"];
                           [request setValue:User_id forHTTPHeaderField:@"userid"];*/
                           
        
                           NSError *error = NULL;
                           NSURLResponse *response = NULL;
                           NSData *data  = [NSURLConnection sendSynchronousRequest:request returningResponse:&response error:&error ];
                           id vehicel_dic = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingMutableContainers error:nil];
                           block(vehicel_dic,WebServiceResultSuccess);
                           
                     
        
        
    }
    @catch (NSException *exception)
    {
        block(@"1",WebServiceResultSuccess);
       
    }
    
}

+(void)warningAlert:(NSString *)alertString
{
    
    UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"Alert" message:alertString preferredStyle:UIAlertControllerStyleAlert];
    UIAlertAction *okAction = [UIAlertAction actionWithTitle:@"Ok" style:UIAlertActionStyleDefault handler:^(UIAlertAction * action){
        
    }];
    
    [alert addAction:okAction];
    [[UIApplication sharedApplication].keyWindow.rootViewController presentViewController:alert animated:YES completion:nil];
    
   
}
+(void)alert:(NSString *)alertString
{
    UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"" message:alertString preferredStyle:UIAlertControllerStyleAlert];
    UIAlertAction *okAction = [UIAlertAction actionWithTitle:@"Ok" style:UIAlertActionStyleDefault handler:^(UIAlertAction * action){
        
    }];
    
    [alert addAction:okAction];
    [[UIApplication sharedApplication].keyWindow.rootViewController presentViewController:alert animated:YES completion:nil];
    
    
}


+(void)alertTitle:(NSString *)title message:(NSString *)alertString
{
    
    UIAlertController *alert = [UIAlertController alertControllerWithTitle:title message:alertString preferredStyle:UIAlertControllerStyleAlert];
    UIAlertAction *okAction = [UIAlertAction actionWithTitle:@"Ok" style:UIAlertActionStyleDefault handler:^(UIAlertAction * action){
        
    }];
    
    [alert addAction:okAction];
    [[UIApplication sharedApplication].keyWindow.rootViewController presentViewController:alert animated:YES completion:nil];
    
    
}


+(NSString *)isDateTime
{
    NSDateFormatter* dateFormatter = [[NSDateFormatter alloc] init];
    dateFormatter.dateFormat = @"yyyyMMddhhmmss";
    return [dateFormatter stringFromDate:[NSDate date]];
}

+(NSString *)isDate_Time
{
    NSDateFormatter* dateFormatter = [[NSDateFormatter alloc] init];
    dateFormatter.dateFormat = @"yyyy-MM-dd hh:mm:ss a";
    return [dateFormatter stringFromDate:[NSDate date]];
}


+(BOOL)isNetwork
{
    Reachabilit *networkReachability = [Reachabilit reachabilityForInternetConnection];
    NetworkStatus networkStatus = [networkReachability currentReachabilityStatus];
   
    if (networkStatus == NotReachable) {
        [Helper stopLoading];
        [WebServiceCalls alertTitle:@"Alert" message:@"check internet connection"];
        return NO;
    }else{
        return YES;
    }
}


+(BOOL)isValidEmail:(NSString *)email
{
    NSString *emailRegEx =
    @"(?:[A-Za-z0-9!#$%\\&'*+/=?\\^_`{|}~-]+(?:\\.[A-Za-z0-9!#$%\\&'*+/=?\\^_`{|}"
    @"~-]+)*|\"(?:[\\x01-\\x08\\x0b\\x0c\\x0e-\\x1f\\x21\\x23-\\x5b\\x5d-\\"
    @"x7f]|\\\\[\\x01-\\x09\\x0b\\x0c\\x0e-\\x7f])*\")@(?:(?:[A-Za-z0-9](?:[a-"
    @"z0-9-]*[A-Za-z0-9])?\\.)+[A-Za-z0-9](?:[A-Za-z0-9-]*[A-Za-z0-9])?|\\[(?:(?:25[0-5"
    @"]|2[0-4][0-9]|[01]?[0-9][0-9]?)\\.){3}(?:25[0-5]|2[0-4][0-9]|[01]?[0-"
    @"9][0-9]?|[A-Za-z0-9-]*[A-Za-z0-9]:(?:[\\x01-\\x08\\x0b\\x0c\\x0e-\\x1f\\x21"
    @"-\\x5a\\x53-\\x7f]|\\\\[\\x01-\\x09\\x0b\\x0c\\x0e-\\x7f])+)\\])";
    
    NSPredicate *emailTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", emailRegEx];
    BOOL isvalid =[emailTest evaluateWithObject:email];
    return isvalid;
}



+(NSString *)getCurrentDate
{
    NSDateFormatter* dateFormatter = [[NSDateFormatter alloc] init];
    dateFormatter.dateFormat = @"yyyy-MM-dd HH:mm:ss";
    return [dateFormatter stringFromDate:[NSDate date]];
}




+(BOOL)isValidPhone:(NSString *)phone;
{
    NSString *phoneRegex = @"[0-9]{6,14}$";
    NSPredicate *phoneTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", phoneRegex];
    
    BOOL isvalid =[phoneTest evaluateWithObject:phone];
    return isvalid;
}

+(UIColor*)colorWithHexString:(NSString*)hex
{
    NSString *cString = [[hex stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]] uppercaseString];
    
    // String should be 6 or 8 characters
    if ([cString length] < 6) return [UIColor grayColor];
    
    // strip 0X if it appears
    if ([cString hasPrefix:@"0X"]) cString = [cString substringFromIndex:2];
    
    if ([cString length] != 6) return  [UIColor grayColor];
    
    // Separate into r, g, b substrings
    NSRange range;
    range.location = 0;
    range.length = 2;
    NSString *rString = [cString substringWithRange:range];
    
    range.location = 2;
    NSString *gString = [cString substringWithRange:range];
    
    range.location = 4;
    NSString *bString = [cString substringWithRange:range];
    
    // Scan values
    unsigned int r, g, b;
    [[NSScanner scannerWithString:rString] scanHexInt:&r];
    [[NSScanner scannerWithString:gString] scanHexInt:&g];
    [[NSScanner scannerWithString:bString] scanHexInt:&b];
    
    return [UIColor colorWithRed:((float) r / 255.0f)
                           green:((float) g / 255.0f)
                            blue:((float) b / 255.0f)
                           alpha:1.0f];
}


+ (NSString *)dateFromString:(NSString *)date {
    
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"yyyy-MM-dd"];
    
    NSDate * timeNotFormatted = [dateFormatter dateFromString:[date substringWithRange:NSMakeRange(0, 10)]];
    
    [dateFormatter setDateFormat:@"dd MMMM yyyy"];
    return  [dateFormatter stringFromDate:timeNotFormatted];
}


@end

