//
//  GlobalDataPersistance.h
//  Inarq
//
//  Created by Sandeep Jangir on 05/10/1937 SAKA.
//
//

#import <Foundation/Foundation.h>

@interface GlobalDataPersistance : NSObject
+ (GlobalDataPersistance *)sharedGlobalDataPersistence;

@property (nonatomic, assign)NSInteger selectedMenuIndex;
@property (nonatomic, assign)NSString  *device_id;
@property (nonatomic, retain)NSMutableArray *arrSliderImage;
@property (nonatomic, retain)NSMutableArray *arrSubcategory;
@end
