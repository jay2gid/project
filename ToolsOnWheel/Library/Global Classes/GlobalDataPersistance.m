//
//  GlobalDataPersistance.m
//  Inarq
//
//  Created by Sandeep Jangir on 05/10/1937 SAKA.
//
//

#import "GlobalDataPersistance.h"

@implementation GlobalDataPersistance : NSObject

static GlobalDataPersistance *sharedGlobalDataPersistence=nil;

+ (GlobalDataPersistance *)sharedGlobalDataPersistence
{
    if(sharedGlobalDataPersistence == nil)
    {
        sharedGlobalDataPersistence = [[super allocWithZone:NULL] init];
        
    }
    
    return sharedGlobalDataPersistence;
}
-(id)init
{
    if (self = [super init]) {
        
               
    }
    return self;
}
    @end
