//
//  WebServiceCalls.m
//  Ponder_remake
//
//  Created by Yudiz Solutions on 04/07/13.
//  Copyright (c) 2013 Yudiz Solutions. All rights reserved.
//

#import "WebServiceCalls.h"
#import "AFNetworking.h"
#import "Reachabilit.h"

//#define BASE_URL @"http://toolsonwheel.com/tools/apidata.php?action="  /// demo
#define BASE_URL @"http://www.toolsonwheel.com/apidata.php?action="

 // http://www.toolsonwheel.com/tools/apidata.php?action=lastCompleteorder&userId=2500
static AFHTTPRequestOperationManager *manager;

@interface WebServiceCalls(){
    
}
@end

static NSString *getuserphone;

@implementation WebServiceCalls

+ (void)initialize
{
    manager = [[AFHTTPRequestOperationManager alloc] init];
    manager.securityPolicy.allowInvalidCertificates = YES;//This is for https
    manager.responseSerializer = [AFHTTPResponseSerializer serializer];
}

+ (void)POSTJSON:(NSString *)url parameter:(NSString *)parameter completionBlock:(WebCallBlock)block
{
    @try
    {
        if([WebServiceCalls isNetwork]==YES)
        {
            NSData *postData = [parameter dataUsingEncoding:NSASCIIStringEncoding];
            NSURL *urlStr=  [NSURL URLWithString:[[NSString stringWithFormat:@"%@%@",BASE_URL,url] stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding]];
            NSMutableURLRequest *request=[NSMutableURLRequest requestWithURL:urlStr];
            
            [request setHTTPBody:postData];
            [request setHTTPMethod:@"POST"];
            [request setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
            
            NSError *error = NULL;
            NSURLResponse *response = NULL;
            NSData *data = [NSURLConnection sendSynchronousRequest:request returningResponse:&response error:&error ];
            id responce = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingMutableContainers error:nil];
            block(responce,WebServiceResultSuccess);
        }
    }
    @catch (NSException *exception)
    {
        block(@"1",WebServiceResultFail);
    }
}


+ (void)POST:(NSString *)url parameter:(NSDictionary *)parameter completionBlock:(WebCallBlock)block
{
    if ([self isNetwork])
    {
        @try
        {
            NSString *URLSTRING = [NSString stringWithFormat:@"%@%@",BASE_URL,url];
            [manager POST:[URLSTRING stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding] parameters:parameter constructingBodyWithBlock:^(id<AFMultipartFormData> formData)
             {}
             success:^(AFHTTPRequestOperation *operation, id responseObject)
             {
                 id responseJson = [NSJSONSerialization JSONObjectWithData:responseObject options:0 error:nil];
                
                 if (responseJson)
                     block(responseJson,WebServiceResultSuccess);
                 else
                     block(@{@"success":@"0",@"message":@"Error : Could not getting proper responce"},WebServiceResultFail);

             }
            failure:^(AFHTTPRequestOperation *operation, NSError* error)
             {
                
                 block(@{@"success":@"0",@"message":@"Network Error"},WebServiceResultFail);
             }];
        }
        @catch (NSException *exception)
        {
            block(@{@"success":@"0",@"message":@"Network Error"},WebServiceResultFail);
        }
    }
}

+ (void)POST:(NSString *)url parameter:(NSDictionary *)parameter imageData:(NSData *)imageData completionBlock:(WebCallBlock)block
{
    
    @try
    {
        NSString *URLSTRING = [NSString stringWithFormat:@"%@%@",BASE_URL,url];

        AFHTTPRequestOperationManager *manager = [[AFHTTPRequestOperationManager alloc] init];
        manager.securityPolicy.allowInvalidCertificates = YES;//This is for https
        manager.responseSerializer = [AFHTTPResponseSerializer serializer];
        
        [manager POST:URLSTRING parameters:parameter constructingBodyWithBlock:^(id<AFMultipartFormData> formData)
         {
            
                 NSDateFormatter* dateFormatter = [[NSDateFormatter alloc] init];
                 dateFormatter.dateFormat = @"yyyyMMddHHmmss";
                 NSString *name = [dateFormatter stringFromDate:[NSDate date]];
             
             
             name = [NSString stringWithFormat:@"ios%@.jpg",name];
             [formData appendPartWithFileData:imageData name:@"file" fileName:name mimeType:@"image/jpeg"];
         }
              success:^(AFHTTPRequestOperation *operation, id responseObject)
         {
             
             id responseJson = [NSJSONSerialization JSONObjectWithData:responseObject options:0 error:nil];
             if (responseJson)
                 block(responseJson,WebServiceResultSuccess);
             else
                 block(@{@"success":@"0",@"message":@"Error : Could not getting proper responce"},WebServiceResultFail);
         }
         
              failure:^(AFHTTPRequestOperation *operation, NSError* error)
         {
             block(@{@"success":@"0",@"message":@"Network Error"},WebServiceResultFail);

         }];
        
    }
    @catch (NSException *exception)
    {
        block(@{@"success":@"0",@"message":@"Network Error"},WebServiceResultFail);
    }
    
}

+ (void)GET:(NSString *)url parameter:(NSDictionary *)parameter completionBlock:(WebCallBlock)block
{
    
    
    @try
    {
        
        NSString *URLSTRING = [NSString stringWithFormat:@"%@%@",BASE_URL,url];
        [manager GET:[URLSTRING stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding] parameters:nil success:^(AFHTTPRequestOperation * _Nonnull operation, id  _Nonnull responseObject)
         {
             NSMutableArray * responseJson = [[NSMutableArray alloc]init];
             responseJson = [NSJSONSerialization JSONObjectWithData:responseObject options:0 error:nil];
             if (responseJson)
                 block(responseJson,WebServiceResultSuccess);
             else
                 block(@{@"success":@"0",@"message":@"Could not getting proper responce"},WebServiceResultFail);
         } failure:^(AFHTTPRequestOperation * _Nullable operation, NSError * _Nonnull error) {
             block(@{@"success":@"0",@"message":@"Network Error"},WebServiceResultFail);

         }];
        
        /*NSURL *urlStr=  [NSURL URLWithString:[[NSString stringWithFormat:@"%@%@",BASE_URL,url] stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding]];
         NSMutableURLRequest *request=[NSMutableURLRequest requestWithURL:urlStr];
         NSError *error = NULL;
         NSURLResponse *response = NULL;
         NSData *data  = [NSURLConnection sendSynchronousRequest:request returningResponse:&response error:&error ];
         NSDictionary *vehicel_dic = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingMutableContainers error:nil];
         block(vehicel_dic,WebServiceResultSuccess);*/
        
    }
    @catch (NSException *exception)
    {
        block(@{@"success":@"0",@"message":@"Network Error"},WebServiceResultFail);
    }
    
}


+(BOOL)isNetwork
{
    Reachabilit *networkReachability = [Reachabilit reachabilityForInternetConnection];
    NetworkStatus networkStatus = [networkReachability currentReachabilityStatus];
    if (networkStatus == NotReachable)
    {
        UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"Network Error" message:@"Check internet connection" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
        [alert show];
        return NO;
    }
    else
    {
        return YES;
    }
}

+(NSString *)getCurrentDate
{
    NSDateFormatter* dateFormatter = [[NSDateFormatter alloc] init];
    dateFormatter.dateFormat = @"yyyy-MM-dd HH:mm:ss";
    return [dateFormatter stringFromDate:[NSDate date]];
}



+(BOOL)isValidEmail:(NSString *)email
{
    NSString *emailRegEx =
    @"^[A-Z0-9a-z\\._%+-]+@([A-Za-z0-9-]+\\.)+[A-Za-z]{2,4}$";
    
    NSPredicate *emailTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", emailRegEx];
    BOOL isvalid =[emailTest evaluateWithObject:email];
    return isvalid;
}

+(BOOL)isValidPhone:(NSString *)phone;
{
    NSString *phoneRegex = @"[0-9]{6,14}$";
    NSPredicate *phoneTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", phoneRegex];
    
    BOOL isvalid =[phoneTest evaluateWithObject:phone];
    return isvalid;
}

+(UIColor*)colorWithHexString:(NSString*)hex
{
    NSString *cString = [[hex stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]] uppercaseString];
    
    // String should be 6 or 8 characters
    if ([cString length] < 6) return [UIColor grayColor];
    
    // strip 0X if it appears
    if ([cString hasPrefix:@"0X"]) cString = [cString substringFromIndex:2];
    
    if ([cString length] != 6) return  [UIColor grayColor];
    
    // Separate into r, g, b substrings
    NSRange range;
    range.location = 0;
    range.length = 2;
    NSString *rString = [cString substringWithRange:range];
    
    range.location = 2;
    NSString *gString = [cString substringWithRange:range];
    
    range.location = 4;
    NSString *bString = [cString substringWithRange:range];
    
    // Scan values
    unsigned int r, g, b;
    [[NSScanner scannerWithString:rString] scanHexInt:&r];
    [[NSScanner scannerWithString:gString] scanHexInt:&g];
    [[NSScanner scannerWithString:bString] scanHexInt:&b];
    
    return [UIColor colorWithRed:((float) r / 255.0f)
                           green:((float) g / 255.0f)
                            blue:((float) b / 255.0f)
                           alpha:1.0f];
}


+ (NSString *)dateFromString:(NSString *)date {
        
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"yyyy-MM-dd"];
    
    NSDate * timeNotFormatted = [dateFormatter dateFromString:[date substringWithRange:NSMakeRange(0, 10)]];
    
    [dateFormatter setDateFormat:@"dd MMMM yyyy"];
    return  [dateFormatter stringFromDate:timeNotFormatted];
}

@end
