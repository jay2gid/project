//
//  Helper.m
//  ToolsOnWheel
//
//  Created by Sanjay on 30/03/18.
//  Copyright © 2018 Kapil Goyal. All rights reserved.
//


#import "Helper.h"

@implementation Helper


+(NSString *)getString:(id)object
{
    if (object != nil)
    {
        NSString *value = [NSString stringWithFormat:@"%@",object];
        if (![value isEqualToString:@"<null>"])
        {
            return value;
        }
        return @"";
    }
    return @"";
}


+(NSString *)getStringORZero:(id)object
{
    if (object != nil)
    {
        NSString *value = [NSString stringWithFormat:@"%@",object];
        if (![value isEqualToString:@"<null>"])
        {
            return value;
        }
        return @"0";
    }
    return @"0";
}



+(void)saveUserImage:(NSString *)image{
    
        NSString *url = [NSString stringWithFormat:@"http://www.toolsonwheel.com/userfiles/%@",image];
    
    [[NSUserDefaults standardUserDefaults]setObject:url forKey:@"user_image"];
}

+(NSString *)getUserImage{
    
    if ([[NSUserDefaults standardUserDefaults] objectForKey:@"user_image"]) {
        return  [[NSUserDefaults standardUserDefaults] objectForKey:@"user_image"];
    }
    
    return nil;
}

+(NSString *)dateConstant {
    
    NSDateFormatter* dateFormatter = [[NSDateFormatter alloc] init];
    dateFormatter.dateFormat = @"yyyyMMddHHmmss";
    return [dateFormatter stringFromDate:[NSDate date]];
}

+(void)putServiceID:(NSString*)sid{
    
    if ([[NSUserDefaults standardUserDefaults] objectForKey:@"serRated"]){
        
        NSMutableDictionary *dic = [NSMutableDictionary dictionaryWithDictionary:[[NSUserDefaults standardUserDefaults] objectForKey:@"serRated"]];
        [dic setObject:sid forKey:sid];
        [[NSUserDefaults standardUserDefaults]setObject:dic forKey:@"serRated"];

        
    }else{
        NSDictionary *dic = @{sid:sid};
        [[NSUserDefaults standardUserDefaults]setObject:dic forKey:@"serRated"];
    }
    
}

+(BOOL)checkID:(NSString*)sid{
   
    if ([[NSUserDefaults standardUserDefaults] objectForKey:@"serRated"]) {
        
        NSDictionary *dic = [[NSUserDefaults standardUserDefaults] objectForKey:@"serRated"];
        
        if(dic[sid]){
            return true;
        }else{
            
        }
    }else{
       
    }
    
    return false;
}


+(void)SaveId:(NSString*)sid{
    
    if ([[NSUserDefaults standardUserDefaults] objectForKey:@"serRated"]) {
        
        NSDictionary *dic = [[NSUserDefaults standardUserDefaults] objectForKey:@"serRated"];
        
        if(dic[sid]){
            
        }else{
            NSMutableDictionary *dic = [NSMutableDictionary dictionaryWithDictionary:[[NSUserDefaults standardUserDefaults] objectForKey:@"serRated"]];
            [dic setObject:sid forKey:sid];
            [[NSUserDefaults standardUserDefaults]setObject:dic forKey:@"serRated"];
        }
    }else{
        NSDictionary *dic = @{sid:sid};
        [[NSUserDefaults standardUserDefaults]setObject:dic forKey:@"serRated"];
    }
}

+(void)setOerderID:(NSString *)oderID{
    [[NSUserDefaults standardUserDefaults]setObject:oderID forKey:@"order_id_helper"];
}

+(void )removeOrderID{
    [[NSUserDefaults standardUserDefaults]removeObjectForKey:@"order_id_helper"];
}


+(void )showLoading{
    [MBProgressHUD showHUDAddedTo:[AppDelegate sharedDelegate].window animated:YES];
}

+(void)stopLoading{
    [MBProgressHUD hideAllHUDsForView:[AppDelegate sharedDelegate].window animated:YES];
}


+(NSString*)signature{
   
    if ([[NSUserDefaults standardUserDefaults] objectForKey:@"secureSignature"]) {
        return [[NSUserDefaults standardUserDefaults] objectForKey:@"secureSignature"];
    }else{
        return @"";
    }
    
}

@end
