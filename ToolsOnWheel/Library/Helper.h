//
//  Helper.h
//  ToolsOnWheel
//
//  Created by Sanjay on 30/03/18.
//  Copyright © 2018 Kapil Goyal. All rights reserved.
//




#import <Foundation/Foundation.h>

#define USER_ID [[NSUserDefaults standardUserDefaults]objectForKey:@"userid"]
#define USER_PHONE [[NSUserDefaults standardUserDefaults]objectForKey:@"phone"]
#define USER_PASSWORD [[NSUserDefaults standardUserDefaults]objectForKey:@"password"]
#define ORDER_ID_OLD [[NSUserDefaults standardUserDefaults]objectForKey:@"order_id_helper"]
#define USER_NAME [[NSUserDefaults standardUserDefaults]objectForKey:@"name"]
#define USER_EMAIL [[NSUserDefaults standardUserDefaults]objectForKey:@"email"]
#define SECURE_SIGNATURE_API [Helper signature]
#define SECURE_SIGNATURE [Helper signature]

#define isCalender [AppDelegate sharedDelegate].isCal
#define isDropDown [AppDelegate sharedDelegate].isDrop

#define END_KEY_VIEW [self endEditing:true];
#define END_KEY_VC [self.view endEditing:true];


#define WIDTH ((([UIApplication sharedApplication].statusBarOrientation == UIInterfaceOrientationPortrait) || ([UIApplication sharedApplication].statusBarOrientation == UIInterfaceOrientationPortraitUpsideDown)) ? [[UIScreen mainScreen] bounds].size.width : [[UIScreen mainScreen] bounds].size.height)
#define HEIGHT ((([UIApplication sharedApplication].statusBarOrientation == UIInterfaceOrientationPortrait) || ([UIApplication sharedApplication].statusBarOrientation == UIInterfaceOrientationPortraitUpsideDown)) ? [[UIScreen mainScreen] bounds].size.height : [[UIScreen mainScreen] bounds].size.width)


#define isRatingPopop [[NSUserDefaults standardUserDefaults] boolForKey:@"ratingPopup"]

#define reamovedPopup [[NSUserDefaults standardUserDefaults] removeObjectForKey:@"ratingPopup"];


@interface Helper : NSObject
+(NSString *)getString:(id)object;
+(NSString *)getStringORZero:(id)object;


+(void)saveUserImage:(NSString *)image;
+(NSString *)getUserImage;
+(NSString *)dateConstant;
+(void)putServiceID:(NSString*)sid;
+(BOOL)checkID:(NSString*)sid;
+(void)setOerderID:(NSString *)oderID;
+(void)SaveId:(NSString*)sid;
+(void )removeOrderID;

+(NSString*)signature;

+(void)stopLoading;
+(void)showLoading;

//+ (void)alertTitle:(NSString *)title message:(NSString *)alertString;
//+ (void)alert:(NSString *)alertString;
//+ (void)warningAlert:(NSString *)alertString;
//+ (void)validataionAlert:(NSString *)alertString;

@end
