//
//  CalPopUp.m
//  CalGPDemo
//
//  Created by Shikhar Khanna on 24/04/16.
//  Copyright © 2016 Shikhar Khanna. All rights reserved.
//

#import "CalPopUp.h"
#import "Config.h"
#import "FSCalendar.h"
#import "AppDelegate.h"



@implementation CalPopUp
@synthesize calendar;
/*
 // Only override drawRect: if you perform custom drawing.
 // An empty implementation adversely affects performance during animation.
 - (void)drawRect:(CGRect)rect {
 // Drawing code
 }
 */

-(void)UpdateView {
    
    self.gregorian = [[NSCalendar alloc] initWithCalendarIdentifier:NSCalendarIdentifierGregorian];
    self.dateFormatter = [[NSDateFormatter alloc] init];
    self.dateFormatter.dateFormat = @"yyyy-MM-dd";
    UIView *view;
    UIButton *button;
    
    CGFloat height = [[UIDevice currentDevice].model hasPrefix:@"iPad"] ? 400 : 300;
    float yPos = (self.frame.size.height-height-50)/2;
    
    view = [[UIView alloc] initWithFrame:CGRectMake(0, yPos, CGRectGetWidth(self.frame),height+10)];
    view.backgroundColor = [UIColor clearColor];
    view.clipsToBounds = false;
    [self addSubview:view];
    
    calendar = [[FSCalendar alloc] initWithFrame:CGRectMake(0, 0, CGRectGetWidth(self.frame), height)];
    calendar.backgroundColor = [UIColor whiteColor];
    calendar.dataSource = self;
    calendar.delegate = self;
    
    calendar.placeholderType = FSCalendarPlaceholderTypeNone;
    NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
    [dateFormat setDateFormat:@"yyyy-MM-dd"];
    strCurrentDate = [dateFormat stringFromDate:[NSDate date]];
    
    NSDate *sixDaysAfter = [[NSDate date] dateByAddingTimeInterval:6*24*60*60];
    sevenDayAfter = [dateFormat stringFromDate:sixDaysAfter];
    
    calendar.currentPage = [self.dateFormatter dateFromString:[strCurrentDate substringWithRange:NSMakeRange(0, 10)]];
    calendar.firstWeekday = 2;
    calendar.scrollDirection = FSCalendarScrollDirectionHorizontal;
    calendar.scrollEnabled = true;
    
    [view addSubview:calendar];
    self.calendar = calendar;
    [calendar selectDate:[NSDate date]];
    
    //    buttonPrevious = [UIButton buttonWithType:UIButtonTypeSystem];
    //    buttonPrevious.frame = CGRectMake(5, 10, 60, 20);
    //    [buttonPrevious setTitle:@"Previous" forState:UIControlStateNormal];
    //    [buttonPrevious addTarget:self action:@selector(prevClicked:) forControlEvents:UIControlEventTouchUpInside];
    //    [buttonPrevious setFont:[UIFont systemFontOfSize:11.0]];
    //    [calendar addSubview:buttonPrevious];
    //    self.prevButton = buttonPrevious;
    
    //    buttonNext = [UIButton buttonWithType:UIButtonTypeSystem];
    //    buttonNext.frame = CGRectMake(calendar.frame.size.width-55, 10, 50, 20);
    //    [buttonNext setTitle:@"Next" forState:UIControlStateNormal];
    //    [buttonNext addTarget:self action:@selector(nextClicked:) forControlEvents:UIControlEventTouchUpInside];
    //    [buttonNext setFont:[UIFont systemFontOfSize:11.0]];
    
    
    [calendar addSubview:buttonNext];
    self.nextButton = buttonNext;
    
    [buttonPrevious setEnabled:FALSE];
    [buttonNext setEnabled:YES];
    
    view1 = [[UIView alloc]initWithFrame:CGRectMake(0, calendar.frame.origin.y+calendar.frame.size.height  , self.frame.size.width, 50)];
    [view1 setBackgroundColor:[UIColor whiteColor]];
    [view addSubview:view1];
    
    
    buttonCancel = [UIButton buttonWithType:UIButtonTypeSystem];
    buttonCancel.frame = CGRectMake(calendar.frame.size.width-130, 10, 60, 30);
    [buttonCancel setTitle:@"Cancel" forState:UIControlStateNormal];
    [buttonCancel addTarget:self action:@selector(cancelClicked:) forControlEvents:UIControlEventTouchUpInside];
    buttonCancel.layer.borderWidth = 1;
    buttonCancel.layer.borderColor = button.tintColor.CGColor;
    buttonCancel.layer.cornerRadius = 6;
    buttonCancel.titleLabel.font = [UIFont systemFontOfSize:13.0];
    [view1 addSubview:buttonCancel];
    
   
    buttonOk = [UIButton buttonWithType:UIButtonTypeSystem];
    buttonOk.frame = CGRectMake(calendar.frame.size.width-50, 10, 40, 30);
    [buttonOk setTitle:@"Ok" forState:UIControlStateNormal];
    [buttonOk addTarget:self action:@selector(okClicked:) forControlEvents:UIControlEventTouchUpInside];
    buttonOk.layer.borderWidth = 1;
    buttonOk.layer.borderColor = button.tintColor.CGColor;
    buttonOk.layer.cornerRadius = 6;
    buttonOk.titleLabel.font = [UIFont systemFontOfSize:13.0];
    [view1 addSubview:buttonOk];
    
    //  [calendar addSubview:self.bottomContainer];
}

-(void)delayRemove
{
    isCalender = false;
    // [self removeFromSuperview];
    self.hidden = true;
    [[NSUserDefaults standardUserDefaults] removeObjectForKey:@"date"];
    [[NSUserDefaults standardUserDefaults] synchronize];
}
- (void)calendar:(FSCalendar *)calendar boundingRectWillChange:(CGRect)bounds animated:(BOOL)animated{
    calendar.frame = (CGRect){calendar.frame.origin,bounds.size};
    self->view1.frame = CGRectMake(0, self->calendar.frame.origin.y + self->calendar.frame.size.height, self->view1.frame.size.width, self->view1.frame.size.height);
}

- (void)calendarCurrentPageDidChange:(FSCalendar *)calendar{
    [self performSelector:@selector(frameChange) withObject:self afterDelay:0.01];
}

-(void)frameChange{
    [UIView animateWithDuration:0.2 animations:^{
        self->view1.frame = CGRectMake(0, self->calendar.frame.origin.y + self->calendar.frame.size.height, self->view1.frame.size.width, self->view1.frame.size.height);
    }];
}

- (NSDate *)minimumDateForCalendar:(FSCalendar *)calendar
{
    return [self.dateFormatter dateFromString:strCurrentDate];
}

- (NSDate *)maximumDateForCalendar:(FSCalendar *)calendar
{
    return [self.dateFormatter dateFromString:sevenDayAfter];
}

- (void)dealloc
{
    NSLog(@"%s",__FUNCTION__);
}

#pragma mark - <FSCalendarDelegate>
//- (void)calendar:(FSCalendar *)calendar boundingRectWillChange:(CGRect)bounds animated:(BOOL)animated
//{
//    //calendar.frame = (CGRect){calendar.frame.origin,bounds.size};
//    //    self.bottomContainer.frame = kContainerFrame;
//}

#pragma mark - Target action
- (void)nextClicked:(id)sender
{
//    NSDate *nextMonth = [self.gregorian dateByAddingUnit:NSCalendarUnitMonth value:1 toDate:calendar.currentPage options:0];
//    [calendar setCurrentPage:nextMonth animated:YES];
//    [buttonNext setEnabled:FALSE];
//    [buttonPrevious setEnabled:YES];
}

- (void)prevClicked:(id)sender
{
//    NSDate *prevMonth = [self.gregorian dateByAddingUnit:NSCalendarUnitMonth value:-1 toDate:calendar.currentPage options:0];
//    [calendar setCurrentPage:prevMonth animated:YES];
//    [buttonPrevious setEnabled:FALSE];
//    [buttonNext setEnabled:YES];
}

- (void)cancelClicked:(id)sender
{
    [self delayRemove];
}



- (void)okClicked:(id)sender
{
    NSDateFormatter *df = [NSDateFormatter new];
    [df setDateFormat:@"yyyy-MM-dd"];
    NSString *strDate = [df stringFromDate:calendar.selectedDate ];
    
    [_caldelegate onDateSelected:strDate];
    
    [self performSelector:@selector(delayRemove) withObject:nil afterDelay:0.1];
}

- (IBAction)tapOnView:(id)sender {
    self.hidden = true;
}


@end
