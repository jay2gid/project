//
//  CalPopUp.h
//  CalGPDemo
//
//  Created by Shikhar Khanna on 24/04/16.
//  Copyright © 2016 Shikhar Khanna. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "VRGCalendarView.h"
#import "FSCalendar.h"

@protocol calenderDelegate <NSObject>

-(void)onDateSelected:(NSString *)date;

@end

@interface CalPopUp : UIView<FSCalendarDataSource,FSCalendarDelegate,FSCalendarDelegateAppearance,UIScrollViewDelegate>
{
    IBOutlet UIView *viewBlank;
    NSString *strDay;
    NSString *strCurrentDate;
    NSString *sevenDayAfter;
    UIButton *buttonNext;
    UIButton *buttonPrevious;
    UIButton *buttonCancel;
    UIButton *buttonOk;
    NSDate *date;

    UIView *view1;
}
@property (strong, nonatomic) FSCalendar *calendar;
@property (weak, nonatomic) UIView *bottomContainer;
@property (weak, nonatomic) UILabel *eventLabel;
@property (weak, nonatomic) UIButton *nextButton;
@property (weak, nonatomic) UIButton *prevButton;
@property (weak, nonatomic) UIButton *cancelButton;
@property (weak, nonatomic) UIButton *okButton;
@property (strong, nonatomic) NSString *strOpenCal;

@property (strong, nonatomic) NSCalendar *gregorian;
@property (strong, nonatomic) NSDateFormatter *dateFormatter;

@property (strong, nonatomic) id<calenderDelegate> caldelegate;



- (void)nextClicked:(id)sender;
- (void)prevClicked:(id)sender;
-(void)UpdateView;
@end


