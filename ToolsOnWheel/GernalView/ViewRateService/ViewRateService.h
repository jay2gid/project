//
//  ViewRateService.h
//  ToolsOnWheel
//
//  Created by Sanjay on 31/03/18.
//  Copyright © 2018 Kapil Goyal. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "StarRatingControl.h"
#import "RatingOrder.h"

@interface ViewRateService : UIView<StarRatingDelegate,ASIHTTPRequestDelegate,ratingDelegaeComplete>
@property (strong, nonatomic) IBOutlet UIButton *btnCancel;
@property (strong, nonatomic) IBOutlet UIButton *btnSubmit;

@property (strong, nonatomic) IBOutlet UILabel *lblOrdrId;

@property (strong, nonatomic) IBOutlet UILabel *lblSerQuantity;
@property (strong, nonatomic) IBOutlet UILabel *lblSerID;
@property (strong, nonatomic) IBOutlet UILabel *lblSerDate;
@property (strong, nonatomic) IBOutlet UILabel *lblSerTime;
@property (strong, nonatomic) IBOutlet UILabel *lblSerRate;

@property (strong, nonatomic) IBOutlet StarRatingControl *rating;

@property (strong, nonatomic) NSDictionary *info;

-(void)setData;

@end
