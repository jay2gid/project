//
//  ViewRateService.m
//  ToolsOnWheel
//
//  Created by Sanjay on 31/03/18.
//  Copyright © 2018 Kapil Goyal. All rights reserved.
//

#import "ViewRateService.h"

@implementation ViewRateService
{
    RatingOrder *ratingOrderView;
}
@synthesize btnCancel,btnSubmit,info;

- (void)drawRect:(CGRect)rect {
    
    btnSubmit.layer.cornerRadius = 25;
    btnCancel.layer.cornerRadius = 25;
    self.rating.delegate = self;
    
    //[Helper SaveId:[Helper getString:info[@"serviceId"]]];
    
  //  NSLog(@"acess  ---   %@",info[@"serviceId"] );
}

-(void)setData{
    _lblSerID.text = [Helper getString:info[@"serviceId"]];
    _lblOrdrId.text = [Helper getString:info[@"orderId"]];
    _lblSerDate.text = [Helper getString:info[@"serviceDate"]];
    _lblSerTime.text = [Helper getString:info[@"serviceTime"]];
    _lblSerRate.text = [Helper getString:info[@"rates"]];
    _lblSerQuantity.text = [Helper getString:info[@"quantity"]];
}


-(void)starRatingControl:(StarRatingControl *)control didUpdateRating:(NSUInteger)rating {
    if (rating < 4) {        
        [self ratingOrderView];
    }
}




-(void )ratingOrderView {
    
    NSArray *arrNibs = [NSArray arrayWithArray:[[NSBundle mainBundle] loadNibNamed:NSStringFromClass([RatingOrder class]) owner:self options:nil]];
    
    if (ratingOrderView!=nil) {
        [ratingOrderView removeFromSuperview];
        ratingOrderView = nil;
    }
    
    ratingOrderView = [arrNibs firstObject];

    ratingOrderView.strServiceId = [Helper getString:info[@"serviceId"]];
    ratingOrderView.strRating = [NSString stringWithFormat:@"%ld",self.rating.rating];
    [ratingOrderView updateView];
    ratingOrderView.frame = self.bounds;
    [self addSubview:ratingOrderView];
    ratingOrderView.lblTitle.text = @"What could be improved ?";
    ratingOrderView.delegate = self;
}


-(void)ratingCompleted {
    [self disableRatingStaus];
    [self removeFromSuperview];
}

- (void)exitRatingPopup {

}

-(void)disableRatingStaus{
    
    reamovedPopup
    NSString *string = [NSString stringWithFormat:@"updateRatingStatus&ratingStatus=0&userId=%@&secureSignature=%@",USER_ID,SECURE_SIGNATURE_API];
   
    [WebServiceCalls POST:string parameter:nil completionBlock:^(id JSON, WebServiceResult result)
     {
         @try{
             NSLog(@"%@", JSON);
             if (result == WebServiceResultSuccess) {
                 
                 if ([JSON[@"status"] integerValue] == 1) {
                     
                 }else {
                     
                 }
             }
         }
         @catch (NSException *exception) { }
         @finally { }
     }];
}

#pragma mark - UIButton Actions

- (IBAction)btnSubmitPressed:(id)sender
{
    if (_rating.rating>3) {
        
        NSDictionary *param = @{@"ratingComment":@"",
                                @"rating":[NSString stringWithFormat:@"%ld",(unsigned long)self.rating.rating],
                                @"orderMatrixId":[Helper getString:info[@"serviceId"]]
                                };
        
        [WebServiceCalls POST:kOrderRating parameter:param completionBlock:^(id JSON, WebServiceResult result)
         {
             @try{
                 NSLog(@"%@", JSON);
                 if (result == WebServiceResultSuccess) {
                     
                     if ([JSON[@"status"] integerValue] == 1) {
                         [Utils showAlertMessage:KMessageTitle Message:[Helper getString:JSON[@"result"]]];
                         [self removeFromSuperview];
                     }else {
                         
                     }
                 }
             }
             @catch (NSException *exception) { }
             @finally { }
         }];
    }else{
        
        [self ratingOrderView];
        
    }
    
    [self disableRatingStaus];

}

-(void)crossThisView{
    

    [UIView animateWithDuration:0.2 animations:^{
        
    }];
}


- (IBAction)tapCancel:(id)sender {
    
    [self disableRatingStaus];
    self.hidden = true;
}


@end



