//
//  ViewAddMore.h
//  ToolsOnWheel
//
//  Created by Sanjay on 30/03/18.
//  Copyright © 2018 Kapil Goyal. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ViewAddMore : UIView
@property (strong, nonatomic) IBOutlet UIButton *btnAddMore;
@property (strong, nonatomic) IBOutlet UIButton *btnProceed;

@property (strong, nonatomic) UIViewController* selfCon;

@end
