//
//  ViewAddMore.m
//  ToolsOnWheel
//
//  Created by Sanjay on 30/03/18.
//  Copyright © 2018 Kapil Goyal. All rights reserved.
//

#import "ViewAddMore.h"
#import "MyCartViewController.h"
#import "HomeVC.h"

@implementation ViewAddMore
{
    
    IBOutlet UIView *viewMid;
}
- (void)drawRect:(CGRect)rect {
    
    viewMid.layer.cornerRadius = 15;
    
    _btnAddMore.layer.cornerRadius = 20;
    _btnProceed.layer.cornerRadius = 20;

}
- (IBAction)tapAddMore:(id)sender {
    
    NSArray *viewControllers = [[self.selfCon navigationController] viewControllers];
    for( int i=0;i<[viewControllers count];i++){
        id obj=[viewControllers objectAtIndex:i];
        if([obj isKindOfClass:[HomeVC class]]){
            [[self.selfCon navigationController] popToViewController:obj animated:YES];
        }
    }
}

- (IBAction)tapProceedToCheckout:(id)sender {
    
    MyCartViewController *vc = [[MyCartViewController alloc] initWithNibName:@"MyCartViewController" bundle:nil];
    [self.selfCon.navigationController pushViewController:vc animated:YES];
}



@end
