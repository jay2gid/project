//
//  CatTVCell.h
//  TreeApp
//
//  Created by Shikhar Khanna on 12/09/15.
//  Copyright (c) 2015 Shikhar Khanna. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CatTVCell : UITableViewCell

@property (strong, nonatomic) IBOutlet UILabel *lblCat;
@property (strong, nonatomic) IBOutlet UIButton *btnCat;
@property (strong, nonatomic) IBOutlet UIImageView *imgCat;

@end
