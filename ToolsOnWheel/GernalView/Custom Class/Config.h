

#define KALERT(TITLE,MSG,DELEGATE) [[UIAlertView alloc]initWithTitle:TITLE message:MSG delegate:DELEGATE cancelButtonTitle:@"OK" otherButtonTitles:nil, nil]

#define KALERT_YN(TITLE,MSG,DELEGATE) [[UIAlertView alloc]initWithTitle:TITLE message:MSG delegate:DELEGATE cancelButtonTitle:@"NO" otherButtonTitles:@"YES", nil]

#define App_Delegate (AppDelegate*)[UIApplication sharedApplication].delegate

#define IS_OS_8_OR_LATER ([[[UIDevice currentDevice] systemVersion] floatValue] >= 8.0)


//#define APP_ManageObject ((AppDelegate*)iTicket_AppDelegate).managedObjectContext

#define whiteCharacterSet [NSCharacterSet whitespaceAndNewlineCharacterSet]
#define AppName  @"MySafe"

#define kParseAppKey                    @"LtAYczzZ7RDLxHlbkiVTWWrQMSm7bVIIJKsjPCPL"
#define kParseClientKey                 @"HPy5yNLk5618sCVSh6oxlGJXZcjL3ZtNjVO1XAxV"

#define AppDeviceToken @"Device_token"
#define KCurrentLoactionAddress @"current_location_address"

#define iOSVersion [[[UIDevice currentDevice] systemVersion] floatValue]
#define IS_IPHONE (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone)

//#define ServerAdd @"http://ds211.projectstatus.co.uk/knight_frank_app/webservices/"
//#define ServerAdd @"http://192.168.2.11/asbestos/webservices/" //local url
//#define ServerAdd @"http://demo.blueheavencosmetics.in/json_webservice_api/" //demo url
//#define ServerAdd @"http://carpark-management.co.uk/mobile-app/users/"  //live url
//#define kBadgeIconColor [UIColor colorWithRed:(219.0/255.0) green:(21.0/255.0) blue:(3.0/255.0) alpha:1.0]

//#define ServerAdd @"http://cctv.keshamrit.com/json/"
#define ServerAdd2 @"http://demo2.bidsnship.com/json/"
#define ServerAdd1 @"http://cctv.keshamrit.com/json/"
#define ServerAdd  @"http://toolsonwheel.com/tools/"

#define IS_IPAD (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)

//#define kBadgeIconColor [UIColor colorWithRed:0/255.0 green:122/255.0 blue:222/255.0 alpha:1.0]
#define kBadgeIconColor [UIColor blackColor]

#define kUpdateTabbar @"kUpdateTabBar"
#define kChangeAddress @"kChangeAddress"
#define kUpdateTabBarSaveCartNotification @"updatetabbarsavecartnotification"
#define kUpdateTabBarSaveCartProductDetailNotification @"product_Detail_notification"
#define kUpdateTabBarSaveCartProductListingNotification @"product_listing_notification"
#define kUpdateTabBarSaveCartAddToCartNotification @"AddToCart_notification"
#define kUpdateTabBarSaveCartWishlistNotification @"wishlist_notification"

#define kInventoryProduct @"inventory-product.php"
#define kInsertInventory @"insert-inventory.php"
#define kProductQuotationBuy @"product_quotation_buy.php"
#define kProductQuotationBuy @"product_quotation_buy.php"
#define kPaymentSuccess @"payment-sucess.php"
#define kMainSlider @"main_slider.php"

//#define kIndex @"index"
#define showBlankImage @"Access-control-systems.png"
#define kLoginStatus @"loginstatus"
#define kAgentLoginStatus @"agentloginstatus"
#define kAgentType @"agenttype"

#define kPaymentId @"paymentid"
#define kEmail @"email"
#define kGender @"gender"
#define kPhone @"phone"
#define kOtp @"otp"

#define kCouponCode @"couponcode"
#define kSortType @"sorttype"
#define KFirstName @"Kfname"
#define KLastName @"Klname"

#define KApplyName @"KApplyName"

#define kBrand @"brand"
#define kColor @"color"
#define kShade @"shade"
#define kSize @"size"
#define kMessageAlert @"Kindly login to continue"
#define kPincode @"pincode"
#define kCartItems @"cartItems"
#define kCartItems @"cartItems"
#define kOodoUserId @"odoo_userId"

#define kAgentId @"agentId"
#define kAgentPassword @"agentPassword"
#define kAgentEmail @"agentEmail"
#define kInventoryAgentId @"InventoryAgentId"
#define kInventoryLeadId @"InventoryLeadtId"
#define kInventoryAmount @"InventoryAmount"
#define kResendInventoryOTP @"resend-inventory-otp.php"

#define kSubTotal @"Subtotal"
#define kShippingCharges @"ShippingCharges"
#define kDiscount @"Discount"
#define kExcludeTax @"ExcludeTax"
#define kTaxCollected @"TaxCollected"
#define kIncludeTax @"IncludeTax"
#define kWishlistNotification @"wishlistnotification"
#define kLogoutWishlistNotification @"logoutwishlistnotification"
#define kLogin @"login"
#define kLoginNotificaiton @"loginnotification"
#define kCurrentLeadCompleteNotificaiton @"current_lead_complete_notification"
#define kLeadAcceptDeclineNotificaiton @"accept_decline_lead_notification"
#define kInventoryButtonNotificaiton @"inventory_notification_button"
#define kProductComplete @"productlead-complete.php"

#define InventoryPayment @"inventory-payment.php"
#define ServiceDetails @"service_details.php"
#define Cart @"cart.php"
#define MyCart @"my-cart.php"
#define ResendOTP @"resend-otp.php"
#define SocialPhone @"socialphone.php"
#define MyCases @"mycase.php"
#define DeleteCartProduct @"delete-cart-product.php"
#define DeleteCart @"delete-cart.php"

#define MyCasesDetail @"mycaseDetail.php"
#define ProductCategory @"product_category.php"
#define ServiceType @"servicetype.php"
#define categoryDrawer @"categoryDrawer.php"
#define GetSearchResult @"getsearchresults"
#define getReports @"getreports"
#define Login @"login.php"
#define Registration @"signup.php"
#define SocialLogin @"socialLogin.php"
#define HomePage @"category.php"
#define VerifyOTP @"verifyotp.php"
#define ProductSubCategory @"product_sub_cat.php"
#define SocialWithOutUserId @"socialPhoneWithoutUserid.php"

#pragma mark AgentLogin

#define kCartAddress @"checkout.php"
#define kCartProductOut @"checkoutProduct.php"

#define kCheckOutDetail @"checkout-detail.php"

#define kVoteCity @"vote_city.php"
#define AgentLogin @"agentLogin.php"
#define AgentCurrentLead @"agentCurrent-lead.php"
#define AgentPastLead @"agentPast-lead.php"
#define AgentAcceptLead @"agentlead-accept.php"
#define AgentDeclinetLead @"agentlead-decline.php"
#define AgentCompleteLead @"agentlead-complete.php"
#define AgentAllLead @"agentAll-lead.php"
#define ProductAllLead @"productAll-lead.php"
#define ProductDeclinetLead @"productlead-decline.php"
#define ProductAcceptLead @"productlead-accept.php"
#define ProductCompleteLead @"productlead-complete.php"

#define AgentCurrentPastLeads @"agentCurrentPast-lead.php"
#define ProductCurrentPastLeads @"productCurrentPast-lead.php"

#define Location @"product-location.php"
#define Product_brand @"product-brand.php"

#define Brand @"brand.php"
#define Quantity @"quantity.php"
#define Quote_result @"quote_result.php"
#define kGetState @"get_state.php"
#define kGetCity @"get_city.php"
#define KAboutUs @"about-us.php"

#define CategoryProduct @"category.php"
#define ProductDetail @"product.php"
#define SearchText @"search.php"
#define ViewAddress @"my-address.php"
#define Getforgot_password @"forgot-password.php"
#define DeleteAddress @"delete-address.php"
#define AddAddress @"add-address.php"
#define EditAddress @"update-address.php"
#define ShowProfile @"profile.php"
#define AddtoCartProduct @"cart-product.php"
#define checkQuatation @"check-quatation.php"
#define kcart_count @"cart_count.php"

#define kAboutUs @"about-us.php"
#define kPrivacyPolicy @"privacy-policy.php"
#define kTermsCondition @"terms-conditions.php"

#define EditProfile @"update-profile.php"
#define ChangePassword @"changePassword.php"
#define ContactUs @"contact.php"
#define CartDetail @"cart.php"
#define DisplayWishList @"wishlist.php"
#define DeleteWishList @"wishlistDelete.php"
#define AddWishList @"wishlistAdd.php"
#define DisplayOrder @"order.php"
#define Discount @"discount.php"
#define OrderDetail @"orderDetail.php"
#define OrderCreate @"orderCreate.php"
#define Payment @"payment.php"
#define Shipping @"shipping.php"
#define Filter @"filter.php"
#define changeAddress @"changeAddress.php"
#define specialOffer @"specialOffers.php"
#define productSwatch @"productSwatch.php"
#define pinCodeChecker @"pincode_checker.php"
#define payMoneyOrderUpdate @"orderUpdate.php"
//#define HelpAndSupport @"helpSupport.php"
#define BeautyTips @"beautyTips.php"
#define BeautyTipsDetails @"beautyTipsDetail.php"


#define ServiceTechnology @"servicetechnology.php"
#define ServiceFault @"servicefault.php"
#define ServicePrice @"serviceprice.php"

#define InstalltionTechnology @"insttechnology.php"
#define InstalltionLocation @"instlocation.php"
#define InstalltionPrice @"instprice.php"

#define kUserId @"UserID"
#define SavedProperty @"saved_property"
#define mySavedProperty @"mysaved_property"
#define deleteSavedProperty @"delete_saved_property"
#define Getcontact @"getpages"
#define Getadvertisement @"getadvertisement"
#define Sendmail @"send_email"
#define GetSaleshistory @"sale_history"
#define Editprofile @"myprofile"
#define Subscription @"update_user_reciept"
#define SubscriptionProductID @"com.KnightFrank.com.inapprage.12monthlyrageface"

#define IS_IPHONE (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone)


///////My India CCTV
#define KALERT(TITLE,MSG,DELEGATE) [[UIAlertView alloc]initWithTitle:TITLE message:MSG delegate:DELEGATE cancelButtonTitle:@"OK" otherButtonTitles:nil, nil]

#define KALERT_YN(TITLE,MSG,DELEGATE) [[UIAlertView alloc]initWithTitle:TITLE message:MSG delegate:DELEGATE cancelButtonTitle:@"NO" otherButtonTitles:@"YES", nil]

#define IsRunningTallPhone() ([UIDevice currentDevice].userInterfaceIdiom == UIUserInterfaceIdiomPhone && [UIScreen mainScreen].bounds.size.height == 568)

#define SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(v)   ([[[UIDevice currentDevice] systemVersion] compare:v options:NSNumericSearch] !=   NSOrderedAscending)

#define KMessageTitle @"My India CCTV"





