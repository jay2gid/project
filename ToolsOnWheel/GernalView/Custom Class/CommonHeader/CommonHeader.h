//
//  CommonHeader.h
//  Fnight Frank
//
//  Created by Sheetal on 11/25/14.
//  Copyright (c) 2014 Dotsquares. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CommonHeader : UIView

@property (strong, nonatomic) IBOutlet UIButton *btnSearch;
@property (strong, nonatomic) IBOutlet UILabel *lblTitle;

@property (strong, nonatomic) IBOutlet UIButton *btnMenu;

@end
