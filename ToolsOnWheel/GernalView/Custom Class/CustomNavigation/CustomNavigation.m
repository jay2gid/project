//
//  CustomNavigation.m
//  Fnight Frank
//
//  Created by Sheetal on 11/25/14.
//  Copyright (c) 2014 Dotsquares. All rights reserved.
//

#import "CustomNavigation.h"
#import "AppDelegate.h"
#import "CommonHeader.h"
#import "MFSideMenu.h"


@implementation CustomNavigation

+ (void)addTarget:(UIViewController *)hostview backRequired:(BOOL)backRequired title:(NSString *)title
{
    CommonHeader  *header;
    NSString *nibname = @"CommonHeader";
    NSArray *arrItems = [[NSBundle mainBundle]loadNibNamed:nibname owner:nil options:nil];
    for (id object in arrItems)
    {
        if([object isKindOfClass:[CommonHeader class]])
        {
            header = (CommonHeader *)object;
            break;
        }
    }
   
    header.lblTitle.text = title;
    [hostview.view addSubview:header];
    [header setFrame:CGRectMake(0, 0, [UIScreen mainScreen].bounds.size.width, 64)];
}

@end
