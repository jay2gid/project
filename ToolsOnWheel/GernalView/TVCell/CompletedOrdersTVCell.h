//
//  CompletedOrdersTVCell.h
//  ToolsOnWheel
//
//  Created by Shikhar Khanna on 06/01/18.
//  Copyright © 2018 Kapil Goyal. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "HCSStarRatingView.h"

@interface CompletedOrdersTVCell : UITableViewCell

@property (strong, nonatomic) IBOutlet UIImageView *imgViewLogo;
@property (strong, nonatomic) IBOutlet UIView *viewImage;
@property (strong, nonatomic) IBOutlet UIView *viewContainer;
@property (strong, nonatomic) IBOutlet UILabel *lblOrderId;
@property (strong, nonatomic) IBOutlet UILabel *lblTitle;
@property (strong, nonatomic) IBOutlet UILabel *lblPrice;
@property (strong, nonatomic) IBOutlet StarRatingControl  *starView;
@property (strong, nonatomic) IBOutlet UILabel *lblQty;
@property (strong, nonatomic) IBOutlet UILabel *lblServiceId;
@property (strong, nonatomic) IBOutlet UILabel *lblServiceDate;
@property (strong, nonatomic) IBOutlet UILabel *lblServiceTime;
@property (strong, nonatomic) IBOutlet UIButton *btnComplaint;

@property (strong, nonatomic) IBOutlet NSLayoutConstraint *bottomHeight;
@property (strong, nonatomic) IBOutlet UIView *viewBottom;
@property (strong, nonatomic) IBOutlet UIButton *btnUpdate;
@property (strong, nonatomic) IBOutlet UIButton *btnCancel;
@property (strong, nonatomic) IBOutlet UIImageView *imgBackground;

@end
