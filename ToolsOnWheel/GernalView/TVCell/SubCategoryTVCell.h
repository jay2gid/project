//
//  CatView.h
//  TreeApp
//
//  Created by Shikhar Khanna on 12/09/15.
//  Copyright (c) 2015 Shikhar Khanna. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SubCategoryTVCell : UITableViewCell

@property (strong, nonatomic) IBOutlet UILabel *lblSubCategory;
@property (strong, nonatomic) IBOutlet UIView *viewContainer;
@property (strong, nonatomic) IBOutlet UIView *viewColor;


@end
