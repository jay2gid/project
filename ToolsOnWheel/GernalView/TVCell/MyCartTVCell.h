//
//  MyCartTVCell.h
//  ToolsOnWheel
//
//  Created by Shikhar Khanna on 06/01/18.
//  Copyright © 2018 Kapil Goyal. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MyCartTVCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UIView *imageContainer;
@property (strong, nonatomic) IBOutlet UIView *viewSeprator;

@property (strong, nonatomic) IBOutlet UIView *viewContainer;
@property (strong, nonatomic) IBOutlet UILabel *lblTitle;
@property (strong, nonatomic) IBOutlet UILabel *lblPrice;
@property (strong, nonatomic) IBOutlet UILabel *lblQty;
@property (strong, nonatomic) IBOutlet UILabel *lblServiceDate;
@property (strong, nonatomic) IBOutlet UILabel *lblTime;
@property (strong, nonatomic) IBOutlet UIButton *btnEdit;
@property (strong, nonatomic) IBOutlet UIButton *btnDelete;
@property (strong, nonatomic) IBOutlet UIImageView *imgViewSmall;
@property (strong, nonatomic) IBOutlet UIImageView *imgBackground;


@end
