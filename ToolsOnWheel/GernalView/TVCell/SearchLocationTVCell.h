//
//  SearchLocationTVCell.h
//  Qboy
//
//  Created by Shikhar Khanna on 16/03/17.
//  Copyright © 2017 Mac. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SearchLocationTVCell : UITableViewCell

@property (strong, nonatomic) IBOutlet UILabel *lblLocation;
@end
