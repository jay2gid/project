//
//  AppDelegate.h
//  ToolsOnWheel
//
//  Created by Kapil Goyal on 17/09/17.
//  Copyright © 2017 Kapil Goyal. All rights reserved.
//


#import <UIKit/UIKit.h>
#import "HomeVC.h"
#import "SidebarVC.h"
#import "MFSideMenu.h"

@protocol mainDelegate <NSObject>

-(void)didBecomeActive;

@end

@interface AppDelegate : UIResponder <UIApplicationDelegate>
{
    
}
@property (strong, nonatomic) UIWindow *window;
@property (nonatomic,strong) UINavigationController *navigationController;
@property (nonatomic,strong) HomeVC *homeVC;
@property (nonatomic,strong) SidebarVC *sideBar;
@property (nonatomic,retain) MFSideMenuContainerViewController *container;
@property (nonatomic,assign)int cartCount;
@property (nonatomic,strong) NSMutableArray *arrCartItems;
@property (nonatomic,assign)BOOL  isDateSelected;

@property (nonatomic,assign)BOOL  isHome;

@property(nonatomic, strong) NSTimer *scrollTimer;
+(AppDelegate*)sharedDelegate;
+(BOOL)checkNetwork;

-(void)loginAPI;


-(void)setRootViewController;
-(NSAttributedString*)getColoredString:(UIColor*)color string:(NSString*)strRupees;
-(NSAttributedString*)getStrikeOutColoredString:(UIColor*)color string:(NSString*)strRupees;




@property (strong, nonatomic) id<mainDelegate> mainDelegate;

@property (nonatomic,readwrite)BOOL isCal;
@property (nonatomic,readwrite)BOOL isDrop;

@end

