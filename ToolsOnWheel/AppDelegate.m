
//
//  AppDelegate.m
//  ToolsOnWheel
//
//  Created by Kapil Goyal on 17/09/17.
//  Copyright © 2017 Kapil Goyal. All rights reserved.
//

#import "AppDelegate.h"
#import "Reachabilit.h"
#import "Utils.h"
#import "MBProgressHUD.h"
#import "Config.h"
#import "HomeVC.h"
#import "MFSideMenu.h"
#import "GlobalDataPersistance.h"
#import "InitVC.h"
#import "SAMKeychain.h"
#import "ViewRateService.h"

@interface AppDelegate ()

@end

@implementation AppDelegate
@synthesize sideBar,homeVC,container,navigationController,arrCartItems;

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
    // Override point for customization after application launch.
    
    if (isRatingPopop) {
        reamovedPopup
    }
    
    self.window = [[UIWindow alloc] initWithFrame:[[UIScreen mainScreen] bounds]];
    
    InitVC *vc = [[InitVC alloc] initWithNibName:@"InitVC" bundle:nil];
    
    self.navigationController = [[UINavigationController alloc] initWithRootViewController:vc];
    
    [self.navigationController setNavigationBarHidden:YES];

    self.window.rootViewController = self.navigationController;
    
    [self.window makeKeyAndVisible];
    
    arrCartItems = [[NSMutableArray alloc] init];
    
    UIDevice *device = [UIDevice currentDevice];
    NSString  *currentDeviceId = [[device identifierForVendor]UUIDString];
    if ([SAMKeychain passwordForService:@"ToolsOnWheel" account:@"ToolsOnWheel"] == nil)
    {
        [SAMKeychain setPassword:currentDeviceId forService:@"ToolsOnWheel" account:@"ToolsOnWheel" error:nil];
        [[NSUserDefaults standardUserDefaults] setObject:currentDeviceId forKey:@"deviceid"];
        [[NSUserDefaults standardUserDefaults] synchronize];
    }
    else
    {
        NSString *deviceId = [SAMKeychain passwordForService:@"ToolsOnWheel" account:@"ToolsOnWheel"];
        NSLog(@"Device Id: %@", deviceId);
        [[NSUserDefaults standardUserDefaults] setObject:deviceId forKey:@"deviceid"];
        [[NSUserDefaults standardUserDefaults] synchronize];
        
    }
    return YES;
}

-(void)setRootViewController
{
    GlobalDataPersistance *GDP = [GlobalDataPersistance sharedGlobalDataPersistence];
    GDP.selectedMenuIndex = 0;
    self.sideBar  = [[SidebarVC alloc] init];
    self.homeVC = [[HomeVC alloc] init];
    UINavigationController *nav = [[UINavigationController alloc ] initWithRootViewController:self.homeVC];
    nav.navigationBarHidden  = YES;
    
    
    
    container = [MFSideMenuContainerViewController containerWithCenterViewController:nav leftMenuViewController:self.sideBar rightMenuViewController:nil];
    
    self.navigationController = [[UINavigationController alloc]initWithRootViewController:container];
    [self.navigationController setNavigationBarHidden:YES];
    
    [self.window setRootViewController:navigationController];
}

- (void)applicationWillResignActive:(UIApplication *)application {
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
}

- (void)applicationDidEnterBackground:(UIApplication *)application {
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
}

- (void)applicationWillEnterForeground:(UIApplication *)application {
    // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
}

- (void)applicationDidBecomeActive:(UIApplication *)application {
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    [_mainDelegate didBecomeActive];
    
    // [self rateServiceView:@{@"":@""}];
    [self loginAPI];
}

- (void)applicationWillTerminate:(UIApplication *)application {
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
}

-(NSAttributedString*)getColoredString:(UIColor*)color string:(NSString*)strRupees
{
    NSString *strEffect = [NSString stringWithFormat:@"%@%@",@"\u20B9",strRupees];
    NSMutableAttributedString *attributeString = [[NSMutableAttributedString alloc] initWithString:strEffect];
    [attributeString addAttribute:NSForegroundColorAttributeName value:color range:NSMakeRange(0, 1)];
    return attributeString;
}

-(NSAttributedString*)getStrikeOutColoredString:(UIColor*)color string:(NSString*)strRupees
{
    NSString *strEffect = [NSString stringWithFormat:@"%@%@",@"\u20B9",strRupees];
    NSMutableAttributedString *attributeString = [[NSMutableAttributedString alloc] initWithString:strEffect];
    [attributeString addAttribute:NSForegroundColorAttributeName value:color range:NSMakeRange(0, 1)];
    
    [attributeString addAttribute:NSStrikethroughStyleAttributeName
                            value:@1
                            range:NSMakeRange(0, [strEffect length])];
    return attributeString;
}

+(AppDelegate*)sharedDelegate
{
    return (AppDelegate*)[UIApplication sharedApplication].delegate;
}

#pragma mark - Network Methods
+ (BOOL)checkNetwork
{
    Reachabilit *reachability = [Reachabilit reachabilityForInternetConnection];
    NetworkStatus networkStatus = [reachability currentReachabilityStatus];
    if (networkStatus == NotReachable)
    {
        [MBProgressHUD hideAllHUDsForView:[AppDelegate sharedDelegate].window animated:YES];
        [Utils showAlertMessage:@"ToolsOnWheel" Message:@"Please check network connection"];

        return NO;
    }
    return YES;
}

- (BOOL) shouldAutorotate
{
    return NO;
}




/////  -- order rating



-(void)loginAPI{
    
    if (!_isHome) {
        return;
    }
    
    if (USER_ID) {
        
        NSDictionary *param =@{@"userMob_login":USER_PHONE,
                               @"userPassword_login":USER_PASSWORD,
                               @"secureSignature": SECURE_SIGNATURE_API
                               };

        [WebServiceCalls POST:kLogin_user parameter:param completionBlock:^(id JSON, WebServiceResult result)
         {
             @try{
                 
                 if (result == WebServiceResultSuccess) {
                     
                     if ([JSON[@"status"] integerValue] == 1) {
                         
                         [self.navigationController popViewControllerAnimated:YES];
                         [[NSUserDefaults standardUserDefaults] setObject:[JSON objectForKey:@"userId"] forKey:@"userid"];
                         [[NSUserDefaults standardUserDefaults] setObject:[JSON objectForKey:@"email"] forKey:@"email"];
                         [[NSUserDefaults standardUserDefaults] setObject:[JSON objectForKey:@"phone"] forKey:@"phone"];
                         [[NSUserDefaults standardUserDefaults] setObject:[JSON objectForKey:@"name"] forKey:@"name"];
                         [[NSUserDefaults standardUserDefaults] setObject:[JSON objectForKey:@"refCode"] forKey:@"refcode"];
                         
                         [Helper saveUserImage:[Helper getString:JSON[@"img"]]];
                         
                         if ([JSON[@"ratingStatus"] integerValue] == 1) {
                             [self callService];
                         }
                         
                         NSArray *arr = [[JSON objectForKey:@"address"] componentsSeparatedByString:@"/n/nLandmark:"];
                         if ([arr count]>0)
                         {
                             for (int i=0; i<[arr count]; i++)
                             {
                                 if(i==0)
                                     [[NSUserDefaults standardUserDefaults] setObject:[arr objectAtIndex:i] forKey:@"address"];
                                 else
                                     [[NSUserDefaults standardUserDefaults] setObject:[arr objectAtIndex:i] forKey:@"areaName"];
                             }
                         }
                         [[NSUserDefaults standardUserDefaults] synchronize];
                         
                     }else {
                     }
                 }
             }
             @catch (NSException *exception) { }
             @finally { }
         }];
    }
}


// http://www.toolsonwheel.com/tools/apidata.php?action=updateRatingStatus&ratingStatus=0&userId=2015&secureSignature

-(void)callService{
    
    NSString *urlString = [NSString stringWithFormat:@"lastCompleteorder&userId=%@",USER_ID];
    
    [WebServiceCalls GET:urlString parameter:nil completionBlock:^(id JSON, WebServiceResult result)
     {
         @try{
             NSLog(@"%@", JSON);
             if (result == WebServiceResultSuccess) {
                 
                 if ([JSON[@"status"] integerValue] == 1) {
                     
                     if ([Helper checkID:[Helper getString:JSON[@"result"][0][@"serviceId"]]] == false ){
                         [self rateServiceView:JSON[@"result"][0]];
                     }
                     
                     
                 }else {
                     
                 }
             }
         }
         @catch (NSException *exception) { }
         @finally { }
     }];
}


-(void)rateServiceView:(NSDictionary *)info{
    
    ViewRateService *view = [[[NSBundle mainBundle]loadNibNamed:@"ViewRateService" owner:self options:nil]objectAtIndex:0];
    view.frame = [UIApplication sharedApplication].keyWindow.frame;
    [[UIApplication sharedApplication].keyWindow addSubview:view];
    view.center = CGPointMake(view.center.x,view.center.y+view.frame.size.height);
    view.info = info;
    [view setData];
    
    [UIView animateWithDuration:0.3 animations:^{
        view.center = CGPointMake(view.center.x,view.center.y-view.frame.size.height);
    }];
}

@end
